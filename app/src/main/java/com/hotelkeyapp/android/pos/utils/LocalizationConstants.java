package com.hotelkeyapp.android.pos.utils;

import com.hotelkey.hkandroidlib.HKAndroidApplication;
import com.hotelkey.hkandroidlib.utils.LocalizationStringHelper;

/**
 * Created by ashutoshghui on 21/12/16.
 */

public class LocalizationConstants {

    private LocalizationStringHelper localizationStringHelper;
    private HKAndroidApplication application;

    private LocalizationConstants() {
        application = HKAndroidApplication.getApplication();
        localizationStringHelper = LocalizationStringHelper.getInstance();
    }


    public static LocalizationConstants getInstance() {
        return new LocalizationConstants();

    }

    public String getKeyValue(String key) {
        if (com.hotelkeyapp.android.pos.BuildConfig.LOCALISATION && !localizationStringHelper.fetchLocalizedKeyString(key).equals(key)) {
            return localizationStringHelper.fetchLocalizedKeyString(key);

        } else {
            int valueId = application.getResources().getIdentifier(key, "string", application.getApplicationContext().getPackageName());
            if (valueId != 0) {
                return application.getApplicationContext().getString(valueId);
            }
        }
        return key;
    }
}
