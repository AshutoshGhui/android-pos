package com.hotelkeyapp.android.pos.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.epson.epos2.discovery.DeviceInfo;
import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.model.PrinterProfile;

import java.util.ArrayList;

/**
 * Created by vrushalimankar on 17/10/18.
 */

public class NearbyDevicesAdapter extends RecyclerView.Adapter<NearbyDevicesAdapter.ItemViewHolder> {

    private ArrayList<PrinterProfile> mItems;
    private Integer selectedPos;

    public NearbyDevicesAdapter(ArrayList<PrinterProfile> deviceInfos) {
        this.mItems = deviceInfos;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_nearby_device, viewGroup, false);
        return new NearbyDevicesAdapter.ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ItemViewHolder holder, int position) {
        PrinterProfile printerProfile = mItems.get(holder.getAdapterPosition());
        DeviceInfo deviceInfo = printerProfile.getDeviceInfo();

        holder.txvPrinterName.setText(deviceInfo.getDeviceName());
        holder.txvIPAddress.setText(deviceInfo.getIpAddress());
        holder.radioButton.setChecked(printerProfile.isSelected());
        holder.radioButton.setChecked(selectedPos != null && position == selectedPos);
    }

    public Integer getSelectedPos() {
        return selectedPos;
    }

    public void setSelectedPos(int pos) {
        this.selectedPos = pos;
    }

    @Override
    public int getItemCount() {
        if (mItems != null) {
            return mItems.size();
        }
        return 0;
    }

    public ArrayList<PrinterProfile> getItems() {
        return mItems;
    }

    public void setItems(ArrayList<PrinterProfile> mItems) {
        this.mItems = mItems;
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        RadioButton radioButton;
        TextView txvPrinterName, txvIPAddress;
        LinearLayout llPrinter;
        View parentView;

        ItemViewHolder(View convertView) {
            super(convertView);
            parentView = convertView;
            llPrinter = convertView.findViewById(R.id.ll_printer);
            txvPrinterName = convertView.findViewById(R.id.txv_printer_name);
            txvIPAddress = convertView.findViewById(R.id.txv_ip_address);
            radioButton = convertView.findViewById(R.id.radio_btn);
        }
    }

}
