package com.hotelkeyapp.android.pos.utils;

public interface RefreshService {
    void refresh();

    void handleApiFailure(String message);
}
