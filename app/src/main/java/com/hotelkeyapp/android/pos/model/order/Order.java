package com.hotelkeyapp.android.pos.model.order;

import com.google.gson.annotations.SerializedName;
import com.hotelkeyapp.android.pos.db.orm.OrderDB;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by vrushalimankar on 10/05/18.
 */

public class Order implements Serializable {

    @SerializedName("id")
    private String id;

    @SerializedName("property_id")
    private String property_id;

    @SerializedName("order_number")
    private String order_number;

    @SerializedName("order_batch_id")
    private String order_batch_id;

    @SerializedName("status")
    private String status;

    @SerializedName("display_label")
    private String display_label;

    @SerializedName("guest_name")
    private String guest_name;

    @SerializedName("total_charges")
    private double total_charges;

    @SerializedName("total_taxes")
    private double total_taxes;

    @SerializedName("net_payments")
    private double net_payments;

    @SerializedName("order_items")
    private ArrayList<OrderItems> orderItems;

    @SerializedName("table_id")
    private String table_id;

    @SerializedName("user_id")
    private String user_id;

    @SerializedName("persons")
    private int persons;

    @SerializedName("waiter_id")
    private String waiter_id;

    @SerializedName("device_created_at")
    private Double device_created_at;

    @SerializedName("sequence_number")
    private int sequence_number;

    @SerializedName("business_date")
    private String business_date;

    @SerializedName("modifiedTimestamp")
    private long modifiedTimestamp;

    public String getProperty_id() {
        return property_id;
    }

    public void setProperty_id(String property_id) {
        this.property_id = property_id;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public String getOrder_batch_id() {
        return order_batch_id;
    }

    public void setOrder_batch_id(String order_batch_id) {
        this.order_batch_id = order_batch_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDisplay_label() {
        return display_label;
    }

    public void setDisplay_label(String display_label) {
        this.display_label = display_label;
    }

    public String getGuest_name() {
        return guest_name;
    }

    public void setGuest_name(String guest_name) {
        this.guest_name = guest_name;
    }

    public double getTotal_charges() {
        return total_charges;
    }

    public void setTotal_charges(double total_charges) {
        this.total_charges = total_charges;
    }

    public double getTotal_taxes() {
        return total_taxes;
    }

    public void setTotal_taxes(double total_taxes) {
        this.total_taxes = total_taxes;
    }

    public double getNet_payments() {
        return net_payments;
    }

    public void setNet_payments(double net_payments) {
        this.net_payments = net_payments;
    }

    public ArrayList<OrderItems> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(ArrayList<OrderItems> orderItems) {
        this.orderItems = orderItems;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTable_id() {
        return table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public int getPersons() {
        return persons;
    }

    public void setPersons(int persons) {
        this.persons = persons;
    }

    public String getWaiter_id() {
        return waiter_id;
    }

    public void setWaiter_id(String waiter_id) {
        this.waiter_id = waiter_id;
    }

    public Double getDevice_created_at() {
        return device_created_at;
    }

    public void setDevice_created_at(Double device_created_at) {
        this.device_created_at = device_created_at;
    }

    public int getSequence_number() {
        return sequence_number;
    }

    public void setSequence_number(int sequence_number) {
        this.sequence_number = sequence_number;
    }

    public OrderDB toOrdersDB() {
        OrderDB orderDB = new OrderDB();
        orderDB.setProperty_id(property_id);
        orderDB.setOrder_number(order_number);
        orderDB.setOrder_batch_id(order_batch_id);
        orderDB.setStatus(status);
        orderDB.setDisplay_label(display_label);
        orderDB.setGuest_name(guest_name);
        orderDB.setTotal_charges(total_charges);
        orderDB.setTotal_taxes(total_taxes);
        orderDB.setNet_payments(net_payments);
        orderDB.setUser_id(user_id);
        orderDB.setWaiter_id(waiter_id);
        orderDB.setPersons(persons);
        orderDB.setTableId(table_id);
        orderDB.setSequence_number(sequence_number);
        orderDB.setDevice_created_at(device_created_at);
        orderDB.setBusiness_date(business_date);
        orderDB.setId(id);
        orderDB.setModifiedTimestamp(modifiedTimestamp);
        return orderDB;
    }
}
