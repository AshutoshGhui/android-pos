package com.hotelkeyapp.android.pos.controllers;

import com.hotelkey.hkandroidlib.exceptions.ServerError;
import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.db.orm.DaoSession;
import com.hotelkeyapp.android.pos.db.orm.WaiterDB;
import com.hotelkeyapp.android.pos.model.MenuGroup;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vrushalimankar on 15/05/18.
 */

public class MenuItemController {

    public interface MenuItemsCallbacks {
        void onFailure(String message);
        void onSuccess(List<MenuGroup> waiters);
    }

    private MenuItemsCallbacks callbacks;

    public MenuItemController(MenuItemsCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    public void downloadMenuGroups(String propertyId) {

        final POSApplication posApplication = POSApplication.getInstance();

        String userID = PreferenceUtils.getCurrentUser(posApplication).getId();
        Call<ArrayList<MenuGroup>> property = POSApplication.getInstance().getNetworkService()
                .getMenus(PreferenceUtils.getEntIdentifier(posApplication), propertyId);

        property.enqueue(new Callback<ArrayList<MenuGroup>>() {
            @Override
            public void onResponse(Call<ArrayList<MenuGroup>> call, Response<ArrayList<MenuGroup>> response) {
                if (response.isSuccessful()) {
                    DaoSession daoSession = posApplication.getDaoSession();
                    daoSession.deleteAll(WaiterDB.class);

                    for (MenuGroup waiter : response.body()) {
//                        WaiterDB waiterDB = waiter.toWaiterDB();
//                        daoSession.insertOrReplace(waiterDB);
                    }
                    callbacks.onSuccess(response.body());
                } else {
                    onFailure(call, new ServerError(response.code(), response.errorBody().toString()));
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MenuGroup>> call, Throwable t) {
                callbacks.onFailure(t.getMessage());
            }
        });

    }
}
