package com.hotelkeyapp.android.pos;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.util.Base64;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.hotelkey.hkandroidlib.HKAndroidApplication;
import com.hotelkey.hkandroidlib.models.AppInfo;
import com.hotelkey.hkandroidlib.utils.Constants;
import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkeyapp.android.pos.activity.common.HKHomeActivity;
import com.hotelkeyapp.android.pos.apiService.SyncService;
import com.hotelkeyapp.android.pos.db.orm.ChargeReqDB;
import com.hotelkeyapp.android.pos.db.orm.ChargeTypesDB;
import com.hotelkeyapp.android.pos.db.orm.DaoMaster;
import com.hotelkeyapp.android.pos.db.orm.DaoSession;
import com.hotelkeyapp.android.pos.db.orm.MenuGroupDB;
import com.hotelkeyapp.android.pos.db.orm.MenuItemDB;
import com.hotelkeyapp.android.pos.db.orm.NotificationDB;
import com.hotelkeyapp.android.pos.db.orm.OrderDB;
import com.hotelkeyapp.android.pos.db.orm.OrderDBDao;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDB;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDBDao;
import com.hotelkeyapp.android.pos.db.orm.OrderTypeDB;
import com.hotelkeyapp.android.pos.db.orm.PropertyTableDB;
import com.hotelkeyapp.android.pos.db.orm.TableGroupDB;
import com.hotelkeyapp.android.pos.db.orm.TaxReqDB;
import com.hotelkeyapp.android.pos.db.orm.TaxTypePeriodsDB;
import com.hotelkeyapp.android.pos.db.orm.TaxTypesDB;
import com.hotelkeyapp.android.pos.db.orm.WaiterDB;
import com.hotelkeyapp.android.pos.fragment.DashboardFragment;
import com.hotelkeyapp.android.pos.login.HKLoginActivity;
import com.hotelkeyapp.android.pos.model.OrderPushPayload;
import com.hotelkeyapp.android.pos.model.PushMessage;
import com.hotelkeyapp.android.pos.network.POSNetworkService;
import com.hotelkeyapp.android.pos.utils.AccountPreferences;

import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import de.greenrobot.dao.query.Query;
import io.fabric.sdk.android.Fabric;
import retrofit2.Retrofit;

import static com.hotelkeyapp.android.pos.utils.Constants.EVENT_NIGHT_AUDIT_COMPLETED;
import static com.hotelkeyapp.android.pos.utils.Constants.EVENT_ORDER_ITEM_MINOR_UPDATE;
import static com.hotelkeyapp.android.pos.utils.Constants.EVENT_ORDER_UPDATED;
import static com.hotelkeyapp.android.pos.utils.Constants.EVENT_SHIFT_UPDATED;
import static com.hotelkeyapp.android.pos.utils.Constants.EVENT_TABLE_UPDATED;
import static com.hotelkeyapp.android.pos.utils.Constants.UPDATE_ORDERS_TABLES;

/**
 * Created by vrushalimankar on 09/05/18.
 */

public class POSApplication extends HKAndroidApplication {

    private static POSApplication application;

    private POSNetworkService posNetworkService;

    private DaoSession daoSession;

    private static final IntentFilter intentFilter;

    static {
        intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_TIME_TICK);
        intentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        intentFilter.addAction(Intent.ACTION_TIME_CHANGED);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        POSApplication.application = this;
        registerReceiver(deviceTimeZoneReceiver, intentFilter);
        if (BuildConfig.BUILD_TYPE.equals("release")) {
            final Fabric fabric = new Fabric.Builder(this)
                    .kits(new Crashlytics())
                    .debuggable(true)
                    .build();
            Fabric.with(fabric);
            logUser();
        }
        setTimeZoneChanged();
        resetNetwork();
    }

    private void logUser() {
        if (PreferenceUtils.getCurrentUser(getApplicationContext()) != null) {
            Crashlytics.setUserEmail(PreferenceUtils.getCurrentUser(getApplicationContext()).getEmail());

            Crashlytics.setUserName(PreferenceUtils.getCurrentUser(getApplicationContext()).getFirstName()
                    + " " + PreferenceUtils.getCurrentUser(getApplicationContext()).getLastName()
                    + " (" + PreferenceUtils.getCurrentUser(getApplicationContext()).getUsername() + ")");
        }

        if (PreferenceUtils.getCurrentProperty(getApplicationContext()) != null) {
            Crashlytics.setUserIdentifier("PROPERTY CODE -"
                    + PreferenceUtils.getCurrentProperty(getApplicationContext()).getCode());
        }
    }

    private final BroadcastReceiver deviceTimeZoneReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action != null && (action.equals(Intent.ACTION_TIME_CHANGED) || action.equals(Intent.ACTION_TIMEZONE_CHANGED))) {
                setTimeZoneChanged();
            }
        }
    };

    private void setTimeZoneChanged() {
        PreferenceUtils.setTimeOffset(HKAndroidApplication.getApplication().getApplicationContext(), TimeZone.getDefault().getID());
        TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
    }


    public static POSApplication getInstance() {
        return application;
    }

    @Override
    public Intent getHomeIntent() {
        return HKHomeActivity.getLaunchIntent(application);
    }

    @Override
    public void setApplication() {
        PreferenceUtils.setAppDataAtHome(this, true);
    }

    @Override
    public String[] getEnvironments() {
        return BuildConfig.ENV_NAMES;
    }

    @Override
    public String[] getEnvUrls() {
        return BuildConfig.URL_ARRAY;
    }

    @Override
    public void resetNetwork() {
        super.resetNetwork();
    }

    @Override
    public AppInfo getAppInfo() {
        AppInfo appInfo = new AppInfo();

        appInfo.setAppName(BuildConfig.APP_NAME);

        appInfo.setAppId(BuildConfig.APPLICATION_ID);

        appInfo.setDeviceType("ANDROID");

        appInfo.setOsVersion(Integer.toString(Build.VERSION.SDK_INT));

        appInfo.setBaseUrl(PreferenceUtils.getEnvUrl(POSApplication.getInstance()));

        appInfo.setAppNameLabel(BuildConfig.APP_NAME_LABEL);

        appInfo.setAppVersion(BuildConfig.VERSION_NAME);

        appInfo.setLocalizeConfig(BuildConfig.LOCALISATION);

        appInfo.setNeedHotelProperties(BuildConfig.NEED_HOTEL_PROPS);

        appInfo.setNeedPOSProperties(BuildConfig.NEED_POS_PROPS);

        return appInfo;
    }

    @Override
    public AppInfo setAppinfo() {
        return getAppInfo();
    }

    public POSNetworkService getNetworkService() {
        if (getAppInfo().getBaseUrl().isEmpty()) {
            Toast.makeText(POSApplication.getInstance().getApplicationContext(), "Session Expired", Toast.LENGTH_SHORT).show();
            HKAndroidApplication.getApplication().logout();
        } else {
            String region = Constants.TYPE_ENTERPRISE;

            if (PreferenceUtils.getCurrentProperty(this) != null) {
                if (PreferenceUtils.getPropertyRegion(this) != null) {
                    region = PreferenceUtils.getPropertyRegion(this);
                    if (region == null) {
                        region = "";
                    }
                }
            }
            if (posNetworkService == null) {
                Retrofit retrofit = getNetworkServiceProvider().getRetrofit(region);
                posNetworkService = retrofit.create(POSNetworkService.class);
            } else {
                String selectedEnv = PreferenceUtils.getEnv(POSApplication.getInstance());
                if (selectedEnv.equals("Dev 0") || BuildConfig.FLAVOR.equalsIgnoreCase("independent")) {
                    getNetworkServiceProvider().resetNetwork();
                    Retrofit retrofit = getNetworkServiceProvider().getRetrofit(region);
                    posNetworkService = retrofit.create(POSNetworkService.class);

                } else {
                    getNetworkServiceProvider().resetNetwork();
                    Retrofit retrofit = getNetworkServiceProvider().getRetrofit("");
                    posNetworkService = retrofit.create(POSNetworkService.class);
                }
            }
        }
        return posNetworkService;
    }

    private void prepareDBSession() {
        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(this, "pos-app.db", null);
        DaoMaster daoMaster = new DaoMaster(devOpenHelper.getWritableDatabase());
        this.daoSession = daoMaster.newSession();
    }

    public DaoSession getDaoSession() {
        if (daoSession == null) {
            prepareDBSession();
        }
        return daoSession;
    }

    @Override
    public void onPropertySwitch() {
        super.onPropertySwitch();
        PreferenceUtils.savePropertyCBD(this, "");
    }

    public void logout() {
        super.logout();
        Locale locale = new Locale(Resources.getSystem().getConfiguration().locale.getLanguage());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        getDaoSession().deleteAll(PropertyTableDB.class);
        getDaoSession().deleteAll(OrderDB.class);
        getDaoSession().deleteAll(TableGroupDB.class);
        getDaoSession().deleteAll(WaiterDB.class);
        getDaoSession().deleteAll(OrderItemsDB.class);
        getDaoSession().deleteAll(ChargeReqDB.class);
        getDaoSession().deleteAll(ChargeTypesDB.class);
        getDaoSession().deleteAll(MenuItemDB.class);
        getDaoSession().deleteAll(MenuGroupDB.class);
        getDaoSession().deleteAll(NotificationDB.class);
        getDaoSession().deleteAll(OrderTypeDB.class);
        getDaoSession().deleteAll(TaxReqDB.class);
        getDaoSession().deleteAll(TaxTypePeriodsDB.class);
        getDaoSession().deleteAll(TaxTypesDB.class);

        PreferenceUtils.savePropertyRegion(POSApplication.getInstance(), null);
        PreferenceUtils.clear(POSApplication.getInstance());
        AccountPreferences.clear(POSApplication.getInstance());

        Intent intent = new Intent(POSApplication.getInstance(), HKLoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void pushNotificationReceived(PushMessage pushMessage) {
        if (pushMessage != null && pushMessage.getEvent() != null) {
            if (pushMessage.getSenderTokenId() != null) {
                if (!pushMessage.getSenderTokenId().equals(PreferenceUtils.getToken(POSApplication.getInstance()))) {

                    switch (pushMessage.getEvent()) {
                        case EVENT_ORDER_ITEM_MINOR_UPDATE: {
                            updateOrderItemStatus(pushMessage);
                            break;
                        }

                        case EVENT_TABLE_UPDATED:
                        case EVENT_ORDER_UPDATED: {
                            Intent msgIntent = new Intent(this, SyncService.class);
                            msgIntent.putExtra(UPDATE_ORDERS_TABLES, true);
                            startService(msgIntent);
                            break;
                        }

                        case EVENT_SHIFT_UPDATED: {
                            AccountPreferences.saveOrderBatchId(POSApplication.application, null);
                            AccountPreferences.saveShiftID(POSApplication.application, null);

                            Intent msgIntent = new Intent(this, SyncService.class);
                            msgIntent.putExtra(UPDATE_ORDERS_TABLES, false);
                            startService(msgIntent);
                            break;
                        }
                    }
                }


            } else if (pushMessage.getEvent().equals(EVENT_NIGHT_AUDIT_COMPLETED)) {
                refreshBadge("");
                Intent msgIntent = new Intent(this, SyncService.class);
                msgIntent.putExtra(UPDATE_ORDERS_TABLES, false);
                startService(msgIntent);
            }
        }
    }


    private void updateOrderItemStatus(PushMessage pushMessage) {

        if (pushMessage.getMessageBase64Content() != null) {
            try {
                String payloadBase64 = pushMessage.getMessageBase64Content();
                String payload = new String(Base64.decode(payloadBase64, Base64.NO_WRAP));
                Gson gson = new Gson();
                OrderPushPayload orderPushPayload = gson.fromJson(payload, OrderPushPayload.class);

                if (orderPushPayload != null && orderPushPayload.getOrder_item_ids() != null && !orderPushPayload.getOrder_item_ids().isEmpty()) {
                    if (orderPushPayload.getStatus() != null && !orderPushPayload.getStatus().isEmpty()) {

                        for (String orderItemId : orderPushPayload.getOrder_item_ids()) {
                            DaoSession daoSession = application.getDaoSession();
                            Query<OrderItemsDB> queryBuilder = daoSession.getOrderItemsDBDao().queryBuilder()
                                    .where(OrderItemsDBDao.Properties.Id.eq(orderItemId)).build();
                            List<OrderItemsDB> orderItemsDBList = queryBuilder.list();

                            if (orderItemsDBList != null && !orderItemsDBList.isEmpty()) {
                                OrderItemsDB orderItemsDB = orderItemsDBList.get(0);
                                if (orderItemsDB != null) {
                                    orderItemsDB.setStatus(orderPushPayload.getStatus());
                                    daoSession.update(orderItemsDB);
                                }
                            }
                        }
                    }
                    if (orderPushPayload.getOrder_id() != null) {
                        refreshBadge(orderPushPayload.getOrder_id());
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void refreshBadge(String orderID) {
        if (!orderID.isEmpty()) {
            DaoSession daoSession = application.getDaoSession();
            Query<OrderDB> queryBuilder = daoSession.getOrderDBDao().queryBuilder()
                    .where(OrderDBDao.Properties.Id.eq(orderID)).build();
            if (queryBuilder != null && queryBuilder.list() != null && !queryBuilder.list().isEmpty()) {

                OrderDB orderDb = queryBuilder.list().get(0);
                if (PreferenceUtils.getCurrentUser(application) != null && orderDb.getUser_id() != null) {
                    if (PreferenceUtils.getCurrentUser(application).getId().equals(orderDb.getUser_id())) {
                        int n = AccountPreferences.getNotifications(application) + 1;
                        AccountPreferences.saveNotifications(application, n);

                        Intent broadcastIntent = new Intent();
                        broadcastIntent.setAction(DashboardFragment.NotificationReceived.REFRESH_BADGE);
                        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
                        sendBroadcast(broadcastIntent);
                    }
                }
            }

        } else {
            AccountPreferences.saveNotifications(application, 0);

            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction(DashboardFragment.NotificationReceived.REFRESH_BADGE);
            broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
            sendBroadcast(broadcastIntent);
        }
    }
}
