package com.hotelkeyapp.android.pos.model;

import java.util.ArrayList;

/**
 * Created by vrushalimankar on 01/10/18.
 */

public class DeviceInfoTransient {

    private static DeviceInfoTransient deviceInfoTransient;

    private ArrayList<PrinterProfile> deviceInfoArrayList;

    private PrinterProfile deviceInfo;

    private DeviceInfoTransient() {
    }

    public static DeviceInfoTransient getInstance() {
        if (deviceInfoTransient == null) {
            deviceInfoTransient = new DeviceInfoTransient();
        }
        return deviceInfoTransient;
    }

    public ArrayList<PrinterProfile> getDeviceInfoArrayList() {
        return deviceInfoArrayList;
    }

    public void setDeviceInfoArrayList(ArrayList<PrinterProfile> deviceInfoArrayList) {
        this.deviceInfoArrayList = deviceInfoArrayList;
    }

    public PrinterProfile getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(PrinterProfile deviceInfo) {
        this.deviceInfo = deviceInfo;
    }
}
