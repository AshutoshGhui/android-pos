package com.hotelkeyapp.android.pos.network;


import com.hotelkeyapp.android.pos.db.orm.DepartmentDB;
import com.hotelkeyapp.android.pos.db.orm.OrderDB;
import com.hotelkeyapp.android.pos.model.MenuGroup;
import com.hotelkeyapp.android.pos.model.Notifications;
import com.hotelkeyapp.android.pos.model.OrderItemUpdate;
import com.hotelkeyapp.android.pos.model.OrderType;
import com.hotelkeyapp.android.pos.model.Property;
import com.hotelkeyapp.android.pos.model.Shift;
import com.hotelkeyapp.android.pos.model.TableGroup;
import com.hotelkeyapp.android.pos.model.Waiter;
import com.hotelkeyapp.android.pos.model.table.Table;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by vrushalimankar on 10/05/18.
 */

public interface POSNetworkService {

//    @GET("/v3/{enterprise}/properties/{propertyID}/basic-data")
//    Call<Property> getPropertyDetails(@Path("enterprise") String var1, @Path("propertyID") String var2);

    @GET("{enterprise}/properties/{propertyId}")
    Call<Property> getPropertyDetails(@Path("enterprise") String var1, @Path("propertyId") String var2);

    @GET("/hk-pos/{enterprise}/properties/{propertyID}/tables")
    Call<ArrayList<Table>> getTables(@Path("enterprise") String enterprise, @Path("propertyID") String propertyId);

    @GET("/hk-pos/{enterprise}/properties/{propertyID}/orders")
    Call<ArrayList<OrderDB>> getOrders(@Path("enterprise") String enterprise,
                                       @Path("propertyID") String propertyId,
                                       @Query("lastSync") Long lastSync);

    @GET("/hk-pos/{enterprise}/properties/{propertyID}/order-types")
    Call<ArrayList<OrderType>> getOrderTypes(@Path("enterprise") String enterprise, @Path("propertyID") String propertyId);

    @GET("/hk-pos/{enterprise}/properties/{propertyID}/menus")
    Call<ArrayList<MenuGroup>> getMenus(@Path("enterprise") String enterprise, @Path("propertyID") String propertyId);

    @GET("/v3/{enterprise}/properties/{propertyID}/users/{userId}/children")
    Call<ArrayList<Waiter>> getWaiterList(@Path("enterprise") String enterprise,
                                          @Path("propertyID") String propertyId,
                                          @Path("userId") String userId,
                                          @Query("role") String role);

    @GET("/hk-pos/{enterprise}/properties/{propertyID}/table-groups")
    Call<ArrayList<TableGroup>> getTableGroups(@Path("enterprise") String enterprise,
                                               @Path("propertyID") String propertyId);

    @PUT("/hk-pos/{enterprise}/properties/{propertyID}/orders")
    Call<OrderDB> createOrder(@Path("enterprise") String enterprise,
                              @Path("propertyID") String propertyId,
                              @Body OrderDB orderRequest);

    @GET("/hk-pos/{enterprise}/properties/{propertyID}/pos/shifts")
    Call<Shift> getShifts(@Path("enterprise") String enterprise,
                          @Path("propertyID") String propertyId);

    @GET("/hk-pos/{enterprise}/properties/{propertyID}/orders/order-batches/shifts/{shiftId}")
    Call<ArrayList<Shift>> getOrderBatches(@Path("enterprise") String enterprise,
                                           @Path("propertyID") String propertyId,
                                           @Path("shiftId") String shiftId);

    @GET("/hk-pos/{enterprise}/properties/{propertyID}/orders/order-items-activity")
    Call<ArrayList<Notifications>> getNotifications(@Path("enterprise") String enterprise,
                                                    @Path("propertyID") String propertyId,
                                                    @Query("lastSync") long lastSync);

    @GET("/hk-inventory/{enterprise}/properties/{propertyID}/inventory-systems")
    Call<ArrayList<DepartmentDB>> getDepartments(@Path("enterprise") String enterprise,
                                                 @Path("propertyID") String propertyId);


    @PUT("/hk-pos/{enterprise}/properties/{propertyID}/orders/{orderID}/update-order-items")
    Call<ResponseBody> updateOrderItem(@Path("enterprise") String enterprise,
                                       @Path("propertyID") String propertyId,
                                       @Path("orderID") String orderId,
                                       @Body OrderItemUpdate orderItemUpdate);
}
