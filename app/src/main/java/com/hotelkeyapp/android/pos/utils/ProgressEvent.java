package com.hotelkeyapp.android.pos.utils;


import com.hotelkeyapp.android.pos.model.PrinterProfile;

import java.util.ArrayList;

/**
 * Created by vrushalimankar on 14/05/18.
 */

public class ProgressEvent extends com.hotelkey.hkandroidlib.service.ProgressEvent {

    public static final int CBD_FETCHED = 3;
    public static final int ORDER_CANCELLED_OR_COMPLETED = 4;
    public static final int ORDER_UPDATED = 5;
    public static final int PRINTER_DISCOVERED = 6;

    private String status, orderId;

    private ArrayList<PrinterProfile> deviceInfoArrayList;

    public ProgressEvent(int type) {
        super(type);
    }

    public static ProgressEvent getDiscoveredPrinters(ArrayList<PrinterProfile> deviceInfoArrayList) {
        ProgressEvent progressEvent = new ProgressEvent(6);
        progressEvent.setDeviceInfoArrayList(deviceInfoArrayList);
        return progressEvent;
    }

    public static ProgressEvent orderStatus(String status, String orderID) {
        ProgressEvent progressEvent = new ProgressEvent(4);
        progressEvent.status = status;
        progressEvent.orderId = orderID;
        return progressEvent;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public ArrayList<PrinterProfile> getDeviceInfoArrayList() {
        return deviceInfoArrayList;
    }

    public void setDeviceInfoArrayList(ArrayList<PrinterProfile> deviceInfoArrayList) {
        this.deviceInfoArrayList = deviceInfoArrayList;
    }
}
