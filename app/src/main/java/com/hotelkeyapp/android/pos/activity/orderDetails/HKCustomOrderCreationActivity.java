package com.hotelkeyapp.android.pos.activity.orderDetails;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.hotelkey.hkandroidlib.activity.common.AuthenticatedActivity;
import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.db.orm.ChargeReqDB;
import com.hotelkeyapp.android.pos.db.orm.ChargeTypesDB;
import com.hotelkeyapp.android.pos.db.orm.ChargeTypesDBDao;
import com.hotelkeyapp.android.pos.db.orm.DaoSession;
import com.hotelkeyapp.android.pos.db.orm.FolioReqDB;
import com.hotelkeyapp.android.pos.db.orm.MenuItemDB;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDB;
import com.hotelkeyapp.android.pos.db.orm.OrderTypeDB;
import com.hotelkeyapp.android.pos.db.orm.OrderTypeDBDao;
import com.hotelkeyapp.android.pos.db.orm.PropertyTableDB;
import com.hotelkeyapp.android.pos.db.orm.PropertyTableDBDao;
import com.hotelkeyapp.android.pos.db.orm.TaxReqDB;
import com.hotelkeyapp.android.pos.db.orm.TaxTypePeriodsDB;
import com.hotelkeyapp.android.pos.db.orm.TaxTypesDB;
import com.hotelkeyapp.android.pos.utils.AccountPreferences;
import com.hotelkeyapp.android.pos.utils.FolioHelper;
import com.hotelkeyapp.android.pos.utils.FormatUtils;
import com.hotelkeyapp.android.pos.utils.LocalizationConstants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import de.greenrobot.dao.query.QueryBuilder;

import static com.hotelkeyapp.android.pos.utils.Constants.ARG_CUSTOM_ORDER_ITEM;
import static com.hotelkeyapp.android.pos.utils.Constants.CHARGE_STATUS_CONFIRMED;

public class HKCustomOrderCreationActivity extends AuthenticatedActivity implements View.OnClickListener, TextWatcher {

    private static final String ARG_TABLE_ID = "Table ID";

    private EditText edtItemName, edtPrice, edtRemarks;

    private TextView txvChargeType;

    private HashMap<String, ChargeTypesDB> chargeTypesDBHashMap;

    private ChargeTypesDB selectChargeType;

    private ImageView imgCancelDialog;

    private Button btnAdd;

    private ScrollView scrollView;

    public static Intent getLaunchIntent(Context context, String tableID) {
        Intent intent = new Intent(context, HKCustomOrderCreationActivity.class);
        intent.putExtra(ARG_TABLE_ID, tableID);
        return intent;
    }

    private TextView txvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hkcustom_order_creation);

        Toolbar toolbar = findViewById(R.id.toolbar_top);
        txvTitle = toolbar.findViewById(R.id.toolbar_title);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        initViews();
    }

    private void initViews() {
        try {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String tableId = getIntent().getStringExtra(ARG_TABLE_ID);
        DaoSession daoSession = POSApplication.getInstance().getDaoSession();
        if (tableId != null) {
            QueryBuilder<PropertyTableDB> queryBuilder = daoSession.getPropertyTableDBDao().queryBuilder().where(PropertyTableDBDao.Properties.Id.eq(tableId));
            if (queryBuilder != null && queryBuilder.list() != null && !queryBuilder.list().isEmpty()) {

                PropertyTableDB propertyTableDB = queryBuilder.list().get(0);
                String tableNo = String.format(LocalizationConstants.getInstance().getKeyValue("lbl_table_no_with_value"), propertyTableDB.getNumber());
                txvTitle.setText(tableNo);
            }
        }

        chargeTypesDBHashMap = new HashMap<>();
        QueryBuilder<ChargeTypesDB> queryBuilder = daoSession.getChargeTypesDBDao().queryBuilder().where(ChargeTypesDBDao.Properties.Is_active.eq("true"));
        if (queryBuilder != null && queryBuilder.list() != null) {
            List<ChargeTypesDB> list = queryBuilder.list();
            for (ChargeTypesDB chargeTypesDB : list) {
                chargeTypesDBHashMap.put(chargeTypesDB.getId(), chargeTypesDB);
            }
        }

        edtItemName = findViewById(R.id.edt_name);
        edtItemName.setHint(LocalizationConstants.getInstance().getKeyValue("msg_enter_item_name"));
        edtItemName.addTextChangedListener(this);
//        edtItemName.setFilters(new InputFilter[]{TextHelper.getEditTextFilterEmoji()});
        imgCancelDialog = findViewById(R.id.img_cancel_dialog);
        imgCancelDialog.setOnClickListener(this);

        if (edtItemName.getText().toString().isEmpty()) {
            imgCancelDialog.setVisibility(View.GONE);
        } else {
            imgCancelDialog.setVisibility(View.VISIBLE);
        }


        TextView txvPriceLbl = findViewById(R.id.txv_price);
        txvPriceLbl.setText(LocalizationConstants.getInstance().getKeyValue("lbl_price"));

        edtPrice = findViewById(R.id.edt_price);
        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                int maxDigitsBeforeDecimalPoint = 6;
                int maxDigitsAfterDecimalPoint = 2;

                StringBuilder builder = new StringBuilder(dest);
                builder.replace(dstart, dend, source
                        .subSequence(start, end).toString());
                if (!builder.toString().matches(
                        "(([1-9]{1})([0-9]{0," + (maxDigitsBeforeDecimalPoint - 1) + "})?)?(\\.[0-9]{0," + maxDigitsAfterDecimalPoint + "})?")) {
                    if (source.length() == 0)
                        return dest.subSequence(dstart, dend);
                    return "";
                }
                return null;
            }
        };

        edtPrice.setFilters(new InputFilter[]{filter});
        edtRemarks = findViewById(R.id.edt_remarks);
        scrollView = findViewById(R.id.scrollView);

        TextView txvChargeTypeLbl = findViewById(R.id.txv_charge_type_lbl);
        txvChargeTypeLbl.setText(LocalizationConstants.getInstance().getKeyValue("lbl_charge_type"));
        txvChargeTypeLbl.setOnClickListener(this);

        TextView txvCurrencySymbol = findViewById(R.id.txv_currency_symbol);
        txvCurrencySymbol.setText(FormatUtils.getCurrencySymbol());

        txvChargeType = findViewById(R.id.txv_charge_type_name);
        txvChargeType.setOnClickListener(this);
        edtRemarks.setOnClickListener(this);
        edtRemarks.setHint(LocalizationConstants.getInstance().getKeyValue("lbl_remarks"));

        btnAdd = findViewById(R.id.btn_add);
        btnAdd.setText(LocalizationConstants.getInstance().getKeyValue("btn_add"));
        btnAdd.setOnClickListener(this);
        findViewById(R.id.img_expandable_view).setOnClickListener(this);


        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = scrollView.getScrollY(); // For ScrollView
                int scrollX = scrollView.getScrollX(); // For HorizontalScrollView
                // hideKeyboard(scrollView);
                // DO SOMETHING WITH THE SCROLL COORDINATES
            }
        });
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.edt_remarks) {
            scrollView.scrollTo(0, (int) edtRemarks.getY());
        }


        if (view.getId() == R.id.img_expandable_view || view == txvChargeType || view.getId() == R.id.txv_charge_type_lbl) {

            if (chargeTypesDBHashMap != null && !chargeTypesDBHashMap.isEmpty()) {
                final List<ChargeTypesDB> list = new ArrayList<>(chargeTypesDBHashMap.values());
                if (!list.isEmpty()) {
                    PopupMenu popup = new PopupMenu(HKCustomOrderCreationActivity.this, view);
                    for (int i = 0; i < list.size(); i++) {
                        String fullName = list.get(i).getName();
                        popup.getMenu().add(Menu.NONE, i, Menu.NONE, fullName);
                    }

                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            selectChargeType = list.get(item.getItemId());
                            ((TextView) findViewById(R.id.txv_charge_type_name)).setText(selectChargeType.getName());
                            return true;
                        }
                    });
                    popup.show();
                } else {
                    Toast.makeText(this, LocalizationConstants.getInstance().getKeyValue("toast_no_charge_types_configured"), Toast.LENGTH_SHORT).show();
                }
            }

        } else if (view == imgCancelDialog) {
            edtItemName.getText().clear();

        } else if (view == btnAdd) {
//            if (edtItemName.getText().toString().isEmpty()) {
//                Toast.makeText(this, "Please enter item name", Toast.LENGTH_SHORT).show();
//
//            } else
            if (edtPrice.getText().toString().isEmpty()) {
                Toast.makeText(this, LocalizationConstants.getInstance().getKeyValue("toast_enter_amount"), Toast.LENGTH_SHORT).show();

            } else if (selectChargeType == null) {
                Toast.makeText(this, LocalizationConstants.getInstance().getKeyValue("toast_enter_charge_type"), Toast.LENGTH_SHORT).show();
            } else {
                createOrder();
            }
        }
    }

    private void hideKeyboard(View view) {
        InputMethodManager im = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (im != null) {
            im.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void createOrder() {
        DaoSession daoSession = POSApplication.getInstance().getDaoSession();

        String status = "";
        QueryBuilder<OrderTypeDB> qb = daoSession.getOrderTypeDBDao().queryBuilder().where(OrderTypeDBDao.Properties.Default_type.eq("true"));
        if (qb != null && qb.list() != null && !qb.list().isEmpty()) {
            OrderTypeDB orderTypeDB = qb.list().get(0);

            if (orderTypeDB.getItem_statuses() != null && !orderTypeDB.getItem_statuses().isEmpty()) {
                ArrayList statusList = new ArrayList<>(Arrays.asList(orderTypeDB.getItem_statuses().split(",")));
                status = (String) statusList.get(0);
                if (status.contains("[")) {
                    status = status.replace("[", "");
                }
                if (status.contains("]")) {
                    status = status.replace("]", "");
                }
            }
        }

        OrderItemsDB orderItemsDb = new OrderItemsDB();

        orderItemsDb.setStatus(status);
        orderItemsDb.setMenu_item_id(String.valueOf(UUID.randomUUID()));

        String itemName = "CUSTOM ORDER";
        if (!edtItemName.getText().toString().isEmpty()) {
            itemName = edtItemName.getText().toString();
        }
        orderItemsDb.setDisplay_label(itemName);
        orderItemsDb.setId(String.valueOf(UUID.randomUUID()));
        orderItemsDb.setRemarks(edtRemarks.getText().toString());

        Double amount = 0.0;
        try {
            amount = Double.valueOf(edtPrice.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), LocalizationConstants.getInstance().getKeyValue("toast_enter_valid_amount"), Toast.LENGTH_SHORT).show();
            return;
        }

        MenuItemDB menuItems = new MenuItemDB();
        menuItems.setId(orderItemsDb.getMenu_item_id());
        menuItems.setName(itemName);
        menuItems.setPrice(amount);
        menuItems.setIsCustom(true);
        menuItems.setCharge_type_id(selectChargeType.getId());

        orderItemsDb.setMenuItemDB(menuItems);

        String chargeId = String.valueOf(UUID.randomUUID());
        orderItemsDb.setCharge_id(chargeId);

        ChargeReqDB charge = new ChargeReqDB();
        charge.setId(chargeId);
        charge.setAdults(0);
        charge.setStatus(CHARGE_STATUS_CONFIRMED);
        charge.setAmount(amount);
        charge.setCharge_type_id(selectChargeType.getId());
//        charge.setProduct_id(menuItemDB.getProduct_id());

        charge.setDate(FormatUtils.formatRequestDate(PreferenceUtils.getCBD(POSApplication.getInstance())));
        charge.setUser_id(PreferenceUtils.getCurrentUser(POSApplication.getInstance()).getId());

        ArrayList<TaxReqDB> taxes = new ArrayList<>();
        double totalTaxes = 0;
        if (selectChargeType.getId() != null) {
            try {
                QueryBuilder<ChargeTypesDB> queryBuilder = daoSession.getChargeTypesDBDao().queryBuilder()
                        .where(ChargeTypesDBDao.Properties.Id.eq(selectChargeType.getId()));

                if (queryBuilder != null && queryBuilder.list() != null) {
                    List<ChargeTypesDB> chargeTypesDBS = queryBuilder.list();
                    if (chargeTypesDBS != null && !chargeTypesDBS.isEmpty()) {
                        ChargeTypesDB chargeTypesDB = chargeTypesDBS.get(0);
                        charge.setCustom_field_10(chargeTypesDB.getCustom_field_10_label());

                        List<TaxTypesDB> taxTypesList = chargeTypesDB.getTaxTypes();

                        if (taxTypesList != null && !taxTypesList.isEmpty()) {
                            for (TaxTypesDB aTaxTypesDB : taxTypesList) {
                                if (aTaxTypesDB.getIs_active() && aTaxTypesDB.getTaxTypePeriods() != null) {

                                    List<TaxTypePeriodsDB> taxTypePeriods = aTaxTypesDB.getTaxTypePeriods();
                                    if (taxTypePeriods != null && !taxTypePeriods.isEmpty()) {

                                        for (TaxTypePeriodsDB aTaxTypePeriods : taxTypePeriods) {

                                            if (aTaxTypePeriods != null) {
                                                String startDate = aTaxTypePeriods.getStart_date();
                                                String endDate = aTaxTypePeriods.getEnd_date();

                                                Date start = FormatUtils.toFormatedDate(startDate);
                                                Date chargeDate = PreferenceUtils.getCBD(POSApplication.getInstance());

                                                if (chargeDate.equals(start) || chargeDate.after(start)) {
                                                    if (endDate != null && !endDate.isEmpty()) {
                                                        Date end = FormatUtils.toFormatedDate(endDate);
                                                        if (chargeDate.equals(end) || chargeDate.before(end)) {
                                                            TaxReqDB tax = new TaxReqDB();
                                                            tax.setId(UUID.randomUUID().toString());
                                                            double taxAmount = 0;
                                                            if (aTaxTypePeriods.getPercentage() != null && aTaxTypePeriods.getPercentage() != 0) {
                                                                taxAmount = (charge.getAmount() * aTaxTypePeriods.getPercentage()) / 100;

                                                            } else if (aTaxTypePeriods.getFlat_amount() != null && aTaxTypePeriods.getFlat_amount() != 0) {
                                                                taxAmount = aTaxTypePeriods.getFlat_amount();
                                                            }
                                                            tax.setAmount(taxAmount);
                                                            tax.setDate(charge.getDate());
                                                            tax.setName(aTaxTypesDB.getName());
                                                            tax.setTax_type_id(aTaxTypesDB.getTaxTypeId());

                                                            totalTaxes = totalTaxes + taxAmount;
                                                            taxes.add(tax);
                                                            break;
                                                        }
                                                    } else {
                                                        TaxReqDB tax = new TaxReqDB();
                                                        tax.setId(UUID.randomUUID().toString());
                                                        double taxAmount = 0;
                                                        if (aTaxTypePeriods.getPercentage() != null && aTaxTypePeriods.getPercentage() != 0) {
                                                            taxAmount = (charge.getAmount() * aTaxTypePeriods.getPercentage()) / 100;

                                                        } else if (aTaxTypePeriods.getFlat_amount() != null && aTaxTypePeriods.getFlat_amount() != 0) {
                                                            taxAmount = aTaxTypePeriods.getFlat_amount();
                                                        }
                                                        tax.setAmount(taxAmount);
                                                        tax.setDate(charge.getDate());
                                                        tax.setName(aTaxTypesDB.getName());
                                                        tax.setTax_type_id(aTaxTypesDB.getTaxTypeId());

                                                        totalTaxes = totalTaxes + taxAmount;
                                                        taxes.add(tax);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        charge.setBusiness_day_id(AccountPreferences.getBusinessDayID(POSApplication.getInstance()));
        charge.setLocalTaxes(taxes);
        charge.setShift_id(AccountPreferences.getShiftID(POSApplication.getInstance()));

        FolioReqDB folioReqDB = FolioHelper.getFolios(orderItemsDb.getMenu_item_id(), charge.getAmount(), totalTaxes);
        orderItemsDb.setLocalFolioReqDB(folioReqDB);
        orderItemsDb.setFolio_id(folioReqDB.getId());
        charge.setFolio_id(folioReqDB.getId());
        orderItemsDb.setQuantity(1);
        orderItemsDb.setChargeReqDB(charge);

        Intent intent = new Intent();
        intent.putExtra(ARG_CUSTOM_ORDER_ITEM, orderItemsDb);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (charSequence.toString().isEmpty()) {
            imgCancelDialog.setVisibility(View.GONE);
        } else {
            imgCancelDialog.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
