package com.hotelkeyapp.android.pos.utils;

import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.db.orm.ChargeReqDB;
import com.hotelkeyapp.android.pos.db.orm.ChargeTypesDB;
import com.hotelkeyapp.android.pos.db.orm.ChargeTypesDBDao;
import com.hotelkeyapp.android.pos.db.orm.DaoSession;
import com.hotelkeyapp.android.pos.db.orm.FolioReqDB;
import com.hotelkeyapp.android.pos.db.orm.MenuItemDB;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDB;
import com.hotelkeyapp.android.pos.db.orm.OrderTypeDB;
import com.hotelkeyapp.android.pos.db.orm.OrderTypeDBDao;
import com.hotelkeyapp.android.pos.db.orm.TaxReqDB;
import com.hotelkeyapp.android.pos.db.orm.TaxTypePeriodsDB;
import com.hotelkeyapp.android.pos.db.orm.TaxTypesDB;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import de.greenrobot.dao.query.QueryBuilder;

import static com.hotelkeyapp.android.pos.utils.Constants.CHARGE_STATUS_CONFIRMED;

/**
 * Created by vrushalimankar on 31/05/18.
 */

public class OrderRequestCreationHelper {

    public static ArrayList<OrderItemsDB> createRequest(MenuItemDB menuItemDB, int quantity, String remarks) {

        ArrayList<OrderItemsDB> orderItemsDBArrayList = new ArrayList<>();

        DaoSession daoSession = POSApplication.getInstance().getDaoSession();
        String status = "";
        QueryBuilder<OrderTypeDB> qb = daoSession.getOrderTypeDBDao().queryBuilder().where(OrderTypeDBDao.Properties.Default_type.eq("true"));
        if (qb != null && qb.list() != null && !qb.list().isEmpty()) {
            OrderTypeDB orderTypeDB = qb.list().get(0);

            if (orderTypeDB.getItem_statuses() != null && !orderTypeDB.getItem_statuses().isEmpty()) {
                ArrayList statusList = new ArrayList<>(Arrays.asList(orderTypeDB.getItem_statuses().split(",")));
                status = (String) statusList.get(0);
                if (status.contains("[")) {
                    status = status.replace("[", "");
                }
                if (status.contains("]")) {
                    status = status.replace("]", "");
                }
            }
        }

        if (menuItemDB != null) {
            while (quantity != 0) {
                OrderItemsDB orderItemsRequest = new OrderItemsDB();
                orderItemsRequest.setStatus(status);
                orderItemsRequest.setMenu_item_id(menuItemDB.getId());
                orderItemsRequest.setDisplay_label(menuItemDB.getName());
                orderItemsRequest.setId(String.valueOf(UUID.randomUUID()));
                orderItemsRequest.setRemarks(remarks);
                orderItemsRequest.setUser_id(PreferenceUtils.getCurrentUser(POSApplication.getInstance()).getId());

                String chargeId = String.valueOf(UUID.randomUUID());
                orderItemsRequest.setCharge_id(chargeId);

                orderItemsRequest.setMenuItemDB(null);

                ChargeReqDB charge = new ChargeReqDB();
                charge.setStatus(CHARGE_STATUS_CONFIRMED);
                charge.setId(chargeId);
                charge.setAdults(0);
                charge.setAmount(menuItemDB.getPrice());
                charge.setCharge_type_id(menuItemDB.getCharge_type_id());
                charge.setProduct_id(menuItemDB.getProduct_id());

                try {
                    charge.setDate(FormatUtils.formatRequestDate(PreferenceUtils.getCBD(POSApplication.getInstance())));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                charge.setUser_id(PreferenceUtils.getCurrentUser(POSApplication.getInstance()).getId());

                ArrayList<TaxReqDB> taxes = new ArrayList<>();
                double totalTaxes = 0;
                if (menuItemDB.getCharge_type_id() != null) {
                    try {
                        QueryBuilder<ChargeTypesDB> queryBuilder = daoSession.getChargeTypesDBDao().queryBuilder()
                                .where(ChargeTypesDBDao.Properties.Id.eq(menuItemDB.getCharge_type_id()));
                        if (queryBuilder == null || queryBuilder.list() == null) {
                            throw new Exception();
                        }

                        List<ChargeTypesDB> chargeTypesDBS = queryBuilder.list();
                        if (chargeTypesDBS == null || chargeTypesDBS.isEmpty()) {
                            throw new Exception();
                        }

                        ChargeTypesDB chargeTypesDB = chargeTypesDBS.get(0);
                        charge.setCustom_field_10(chargeTypesDB.getCustom_field_10_label());

                        List<TaxTypesDB> taxTypesList = chargeTypesDB.getTaxTypes();
                        if (taxTypesList == null || taxTypesList.isEmpty()) {
                            throw new Exception();
                        }

                        for (TaxTypesDB aTaxTypesDB : taxTypesList) {
                            if (aTaxTypesDB.getIs_active() && aTaxTypesDB.getTaxTypePeriods() != null) {
                                List<TaxTypePeriodsDB> taxTypePeriods = aTaxTypesDB.getTaxTypePeriods();
                                if (taxTypePeriods == null || taxTypePeriods.isEmpty()) {
                                    continue;
                                }

                                for (TaxTypePeriodsDB aTaxTypePeriods : taxTypePeriods) {
                                    if (aTaxTypePeriods == null || aTaxTypePeriods.getStart_date()==null) {
                                        continue;
                                    }
                                    String startDate = aTaxTypePeriods.getStart_date();
                                    String endDate = aTaxTypePeriods.getEnd_date();

                                    Date start = FormatUtils.toFormatedDate(startDate);
                                    Date chargeDate = PreferenceUtils.getCBD(POSApplication.getInstance());

                                    if (chargeDate.equals(start) || chargeDate.after(start)) {
                                        boolean isValid = false;

                                        if (endDate != null && !endDate.isEmpty()) {
                                            Date end = FormatUtils.toFormatedDate(endDate);
                                            if (chargeDate.equals(end) || chargeDate.before(end)) {
                                                isValid = true;
                                            }
                                        } else {
                                            isValid = true;
                                        }

                                        if (isValid) {
                                            TaxReqDB tax = new TaxReqDB();
                                            tax.setId(UUID.randomUUID().toString());
                                            double taxAmount = 0;
                                            if (aTaxTypePeriods.getPercentage() != null && aTaxTypePeriods.getPercentage() != 0) {
                                                taxAmount = (charge.getAmount() * aTaxTypePeriods.getPercentage()) / 100;

                                            } else if (aTaxTypePeriods.getFlat_amount() != null && aTaxTypePeriods.getFlat_amount() != 0) {
                                                taxAmount = aTaxTypePeriods.getFlat_amount();
                                            }
                                            tax.setAmount(taxAmount);
                                            tax.setDate(charge.getDate());
                                            tax.setName(aTaxTypesDB.getName());
                                            tax.setTax_type_id(aTaxTypesDB.getTaxTypeId());

                                            totalTaxes = totalTaxes + taxAmount;
                                            taxes.add(tax);
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                charge.setBusiness_day_id(AccountPreferences.getBusinessDayID(POSApplication.getInstance()));
                charge.setLocalTaxes(taxes);
                charge.setShift_id(AccountPreferences.getShiftID(POSApplication.getInstance()));

                FolioReqDB folioReqDB = FolioHelper.getFolios(orderItemsRequest.getMenu_item_id(), charge.getAmount(), totalTaxes);
                orderItemsRequest.setLocalFolioReqDB(folioReqDB);
                orderItemsRequest.setFolio_id(folioReqDB.getId());
                charge.setFolio_id(folioReqDB.getId());
                orderItemsRequest.setChargeReqDB(charge);

                orderItemsDBArrayList.add(orderItemsRequest);
                quantity--;
            }

        }

        return orderItemsDBArrayList;
    }
}
