package com.hotelkeyapp.android.pos.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.db.orm.DaoSession;
import com.hotelkeyapp.android.pos.db.orm.OrderDB;
import com.hotelkeyapp.android.pos.db.orm.OrderDBDao;
import com.hotelkeyapp.android.pos.db.orm.PropertyTableDB;
import com.hotelkeyapp.android.pos.utils.FormatUtils;
import com.hotelkeyapp.android.pos.utils.LocalizationConstants;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.dao.query.Query;

import static android.view.View.GONE;
import static com.hotelkeyapp.android.pos.utils.Constants.ORDER_STATUS_BILL_ISSUED;
import static com.hotelkeyapp.android.pos.utils.Constants.ORDER_STATUS_OPEN;
import static com.hotelkeyapp.android.pos.utils.Constants.TABLE_STATUS_BILL_ISSUED;
import static com.hotelkeyapp.android.pos.utils.Constants.TABLE_STATUS_RESERVED;
import static com.hotelkeyapp.android.pos.utils.Constants.TYPE_ITEM;
import static com.hotelkeyapp.android.pos.utils.Constants.TYPE_SECTION;

/**
 * Created by vrushalimankar on 10/05/18.
 */

public class TableAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private Context mContext;

    private List<Object> mItems;

    private List<Object> originalDataArrayList;

    public TableAdapter(Context mContext, List<Object> Items) {
        this.mContext = mContext;
        this.mItems = Items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == TYPE_SECTION) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_grid_section, viewGroup, false);
            return new SectionViewHolder(view);

        } else {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_dashboard_table, viewGroup, false);
            return new ItemViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {

        if (holder instanceof ItemViewHolder) {

            ItemViewHolder viewHolder = (ItemViewHolder) holder;

            final PropertyTableDB propertyTableDB = (PropertyTableDB) mItems.get(holder.getAdapterPosition());

            viewHolder.txvTableName.setText(propertyTableDB.getNumber());
            viewHolder.txvHeadCount.setText(String.valueOf(propertyTableDB.getHeadcount()));

            OrderDB orderDB = null;

            DaoSession daoSession = POSApplication.getInstance().getDaoSession();
            Query<OrderDB> qb = daoSession.getOrderDBDao().queryBuilder()
                    .where(OrderDBDao.Properties.TableId.eq(propertyTableDB.getId())).build();

            if (qb != null && qb.list() != null && !qb.list().isEmpty()) {
                for (OrderDB order : qb.list()) {
                    if (order.getStatus().equals(ORDER_STATUS_OPEN)) {
                        propertyTableDB.setTable_status(TABLE_STATUS_RESERVED);
                        orderDB = order;
                        break;
                    } else if (order.getStatus().equals(ORDER_STATUS_BILL_ISSUED)) {
                        propertyTableDB.setTable_status(TABLE_STATUS_BILL_ISSUED);
                        orderDB = order;
                        break;
                    }
                }
            }

            if (orderDB != null && propertyTableDB.getTable_status().equals(TABLE_STATUS_BILL_ISSUED)) {
                viewHolder.llTableName.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_table_bill_issued));
                viewHolder.txvTableName.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                viewHolder.txvHeadCount.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                viewHolder.imgHeadcount.setColorFilter(ContextCompat.getColor(mContext, R.color.colorWhite), android.graphics.PorterDuff.Mode.SRC_ATOP);
                viewHolder.txvTableStatus.setText(LocalizationConstants.getInstance().getKeyValue("lbl_order_stat_bill_issued"));
                viewHolder.view_table_seperator.setVisibility(GONE);
                viewHolder.txvElapsedTime.setVisibility(View.GONE);
                viewHolder.imgTableStatus.setVisibility(GONE);
                viewHolder.txvHeadCount.setText(String.valueOf(orderDB.getPersons()));
                viewHolder.txvElapsedTime.setText(FormatUtils.convertTimeStamp(orderDB.getDevice_created_at()));
                viewHolder.txvElapsedTime.setVisibility(View.VISIBLE);

            } else if (orderDB != null && propertyTableDB.getTable_status().equals(TABLE_STATUS_RESERVED)) {

                viewHolder.llTableName.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_table_reserved));
                viewHolder.txvTableName.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                viewHolder.txvHeadCount.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                viewHolder.imgHeadcount.setColorFilter(ContextCompat.getColor(mContext, R.color.colorWhite), android.graphics.PorterDuff.Mode.SRC_ATOP);
                viewHolder.txvTableStatus.setText(LocalizationConstants.getInstance().getKeyValue("lbl_reserved"));
                viewHolder.view_table_seperator.setVisibility(GONE);
                viewHolder.txvElapsedTime.setVisibility(View.GONE);
                viewHolder.imgTableStatus.setVisibility(GONE);
                viewHolder.txvHeadCount.setText(String.valueOf(orderDB.getPersons()));
                viewHolder.txvElapsedTime.setText(FormatUtils.convertTimeStamp(orderDB.getDevice_created_at()));
                viewHolder.txvElapsedTime.setVisibility(View.VISIBLE);

            } else {
                if (propertyTableDB.getBlocked()) {

                    viewHolder.llTableName.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_table_blocked));
                    viewHolder.txvTableName.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                    viewHolder.txvHeadCount.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                    viewHolder.imgTableStatus.setImageResource(R.drawable.ic_block);
                    viewHolder.imgHeadcount.setColorFilter(ContextCompat.getColor(mContext, R.color.colorWhite), android.graphics.PorterDuff.Mode.SRC_ATOP);
                    viewHolder.txvTableStatus.setText(LocalizationConstants.getInstance().getKeyValue("lbl_blocked"));
                    viewHolder.view_table_seperator.setVisibility(GONE);
                    viewHolder.txvElapsedTime.setVisibility(GONE);
                    viewHolder.imgTableStatus.setVisibility(View.VISIBLE);

                } else if (propertyTableDB.getDirty()) {
                    viewHolder.llTableName.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_table_dirty));
                    viewHolder.txvTableName.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                    viewHolder.txvHeadCount.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                    viewHolder.imgTableStatus.setImageResource(R.drawable.ic_dirty);
                    viewHolder.imgHeadcount.setColorFilter(ContextCompat.getColor(mContext, R.color.colorWhite), android.graphics.PorterDuff.Mode.SRC_ATOP);
                    viewHolder.txvTableStatus.setText(LocalizationConstants.getInstance().getKeyValue("lbl_dirty"));
                    viewHolder.view_table_seperator.setVisibility(GONE);
                    viewHolder.txvElapsedTime.setVisibility(GONE);
                    viewHolder.imgTableStatus.setVisibility(View.VISIBLE);

                } else {
                    viewHolder.llTableName.setBackground(ContextCompat.getDrawable(mContext, R.color.colorTransparent));
                    viewHolder.txvTableName.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlackVariant));
                    viewHolder.txvHeadCount.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlackVariant));
                    viewHolder.imgTableStatus.setImageResource(R.drawable.ic_done);
                    viewHolder.txvTableStatus.setText(LocalizationConstants.getInstance().getKeyValue("lbl_available"));
                    viewHolder.view_table_seperator.setVisibility(View.VISIBLE);
                    viewHolder.imgHeadcount.setColorFilter(ContextCompat.getColor(mContext, R.color.colorBlackVariant), android.graphics.PorterDuff.Mode.SRC_ATOP);
                    viewHolder.txvElapsedTime.setVisibility(GONE);
                    viewHolder.imgTableStatus.setVisibility(View.VISIBLE);
                }

            }
        } else if (holder instanceof SectionViewHolder) {
            SectionViewHolder viewHolder = (SectionViewHolder) holder;

            if (mItems.get(i) instanceof String) {
                String title = (String) mItems.get(i);
                viewHolder.txvTableGroupName.setText(title);
            }
        }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemCount() {
        if (mItems != null) {
            return mItems.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (mItems.get(position) instanceof String) {
            return TYPE_SECTION;
        } else {
            return TYPE_ITEM;
        }
    }

    public List<Object> getData() {
        return mItems;
    }

    public void setData(List<Object> tables) {
        mItems = tables;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults filterResults = new FilterResults();
                final ArrayList<Object> resultData = new ArrayList<>();

//                if (originalDataArrayList == null)
                originalDataArrayList = mItems;

                if (constraint != null) {
                    if (originalDataArrayList != null && !originalDataArrayList.isEmpty()) {
                        for (Object data : originalDataArrayList) {
                            if (data != null && data instanceof PropertyTableDB) {

                                PropertyTableDB propertyTableDB = (PropertyTableDB) data;
                                if (propertyTableDB.getName() != null) {
                                    if ((propertyTableDB.getNumber().toLowerCase().contains(constraint.toString().toLowerCase()))) {
                                        resultData.add(data);
                                    }
                                }
                            }
                        }
                    }
                    filterResults.values = resultData;
                }
                return filterResults;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mItems = (ArrayList<Object>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView txvTableName, txvHeadCount, txvTableStatus, txvElapsedTime;
        ImageView imgTableStatus, imgHeadcount;
        RelativeLayout llTableName;
        View view_table_seperator;
        LinearLayout ll_parent_layout;

        ItemViewHolder(View view) {
            super(view);
            llTableName = view.findViewById(R.id.ll_table_name);
            txvTableName = view.findViewById(R.id.txv_table_name);
            txvHeadCount = view.findViewById(R.id.txv_headcount);
            txvTableStatus = view.findViewById(R.id.txv_table_status);
            imgTableStatus = view.findViewById(R.id.img_table_status);
            imgHeadcount = view.findViewById(R.id.img_headcount);
            view_table_seperator = view.findViewById(R.id.view_table_seperator);
            txvElapsedTime = view.findViewById(R.id.txv_elapsed_time);
            ll_parent_layout = view.findViewById(R.id.ll_parent_layout);
        }
    }

    public static class SectionViewHolder extends RecyclerView.ViewHolder {
        TextView txvTableGroupName;

        SectionViewHolder(View view) {
            super(view);
            txvTableGroupName = view.findViewById(R.id.txv_title);
        }
    }
}
