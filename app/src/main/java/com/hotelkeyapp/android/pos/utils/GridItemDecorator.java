package com.hotelkeyapp.android.pos.utils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by vrushalimankar on 14/05/18.
 */

public class GridItemDecorator extends RecyclerView.ItemDecoration{

    private int space;

    public GridItemDecorator(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left = space;
        outRect.right = space;
        outRect.bottom = space;
        outRect.top = space;
    }
}
