package com.hotelkeyapp.android.pos.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by ashutoshghui on 01/06/18.
 */

public class AdjustmentTransient {

    @SerializedName("id")
    private String id;

    @SerializedName("business_day_id")
    private String buisnessDayId;

    @SerializedName("amount")
    private Double amount;

    @SerializedName("discount_id")
    private String discountId;

    @SerializedName("posted_business_day_id")
    private String postedBusinessId;

    @SerializedName("shift_id")
    private String shiftId;

    @SerializedName("date")
    private Date date;

    @SerializedName("taxes")
    private ArrayList<Tax> taxArrayList;

    @SerializedName("quantity")
    private int quantity;

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    public int getQuantity() {
        return quantity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuisnessDayId() {
        return buisnessDayId;
    }

    public void setBuisnessDayId(String buisnessDayId) {
        this.buisnessDayId = buisnessDayId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getDiscountId() {
        return discountId;
    }

    public void setDiscountId(String discountId) {
        this.discountId = discountId;
    }

    public String getPostedBusinessId() {
        return postedBusinessId;
    }

    public void setPostedBusinessId(String postedBusinessId) {
        this.postedBusinessId = postedBusinessId;
    }

    public String getShiftId() {
        return shiftId;
    }

    public void setShiftId(String shiftId) {
        this.shiftId = shiftId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ArrayList<Tax> getTaxArrayList() {
        return taxArrayList;
    }

    public void setTaxArrayList(ArrayList<Tax> taxArrayList) {
        this.taxArrayList = taxArrayList;
    }
}
