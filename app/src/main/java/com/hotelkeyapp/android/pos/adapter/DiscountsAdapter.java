package com.hotelkeyapp.android.pos.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.model.DiscountsModel;
import com.hotelkeyapp.android.pos.utils.FormatUtils;
import com.hotelkeyapp.android.pos.utils.LocalizationConstants;

import java.util.List;
import java.util.Set;

/**
 * Created by ashutoshghui on 02/06/18.
 */

public class DiscountsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<DiscountsModel> mItems;
    private CheckBoxListener checkBoxListener;
    private Set<String> appliedDiscounts;

    public DiscountsAdapter(Context context, List<DiscountsModel> mItems, Set<String> discountIds, CheckBoxListener checkBoxListener) {
        this.mItems = mItems;
        this.mContext = context;
        this.checkBoxListener = checkBoxListener;
        this.appliedDiscounts = discountIds;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_discounts, parent, false);
        return new DiscountsAdapter.DiscountViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof DiscountsAdapter.DiscountViewHolder) {

            final DiscountsAdapter.DiscountViewHolder itemViewHolder = (DiscountsAdapter.DiscountViewHolder) holder;
            final DiscountsModel discountTypesDB = mItems.get(position);

            if (discountTypesDB.isCustom()) {
                itemViewHolder.txvTitle.setText(LocalizationConstants.getInstance().getKeyValue("lbl_add_custom_uppercase"));
                itemViewHolder.txvDetails.setVisibility(View.GONE);
                itemViewHolder.imgArrow.setVisibility(View.VISIBLE);
                itemViewHolder.checkBox.setVisibility(View.GONE);

                itemViewHolder.llParentLayout.setOnClickListener
                        (new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                checkBoxListener.addCustomDiscount(discountTypesDB);
                            }
                        });

            } else {
                itemViewHolder.txvTitle.setText(discountTypesDB.getName());
                String amount = "";
                if (discountTypesDB.isPercentage()) {
                    amount = String.valueOf(discountTypesDB.getAmount()) + "%";
                } else {
                    amount = FormatUtils.getCurrencySymbol() + String.valueOf(discountTypesDB.getAmount());
                }
                itemViewHolder.txvDetails.setText(amount);
                itemViewHolder.imgArrow.setVisibility(View.GONE);
                itemViewHolder.checkBox.setVisibility(View.VISIBLE);

                if (appliedDiscounts.contains(discountTypesDB.getId())) {
                    itemViewHolder.checkBox.setChecked(true);
//                    itemViewHolder.checkBox.setEnabled(false);
                } else {
                    itemViewHolder.checkBox.setChecked(false);
//                    itemViewHolder.checkBox.setEnabled(true);
                }
                itemViewHolder.llParentLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        itemViewHolder.checkBox.setChecked(!itemViewHolder.checkBox.isChecked());
                    }
                });

                itemViewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                        if (isChecked) {
                            checkBoxListener.discountApplied(discountTypesDB);
                        } else {
                            checkBoxListener.discountRemoved(discountTypesDB);
                        }
                    }
                });
            }
        }
    }

    public void setItems(List<DiscountsModel> discountItems) {
        this.mItems = discountItems;
        this.notifyDataSetChanged();
    }

    public List<DiscountsModel> getItems() {
        return mItems;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public static class DiscountViewHolder extends RecyclerView.ViewHolder {
        TextView txvTitle, txvDetails;
        ImageView imgArrow;
        CheckBox checkBox;
        LinearLayout llParentLayout;

        DiscountViewHolder(View view) {
            super(view);
            txvTitle = view.findViewById(R.id.txv_title);
            txvDetails = view.findViewById(R.id.txv_details);
            imgArrow = view.findViewById(R.id.txv_quantity);
            checkBox = view.findViewById(R.id.checkbox);
            llParentLayout = view.findViewById(R.id.parent_layout);
        }
    }

    public interface CheckBoxListener {
        public void discountApplied(DiscountsModel discountsModel);

        public void discountRemoved(DiscountsModel discountTypesDB);

        public void addCustomDiscount(DiscountsModel discountTypesDB);
    }
}


