package com.hotelkeyapp.android.pos.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDB;
import com.hotelkeyapp.android.pos.utils.FormatUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Locale;

/**
 * Created by vrushalimankar on 17/05/18.
 */

public class CartOrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;

    private LinkedHashMap<String, ArrayList<OrderItemsDB>> orderRequests;

    private ArrayList<String> menuItemIds;

    private static int ITEM_VIEW = 0;
    private static int CURRENT_ORDER_SECTION = 1;

    public CartOrderAdapter(Context context, LinkedHashMap<String, ArrayList<OrderItemsDB>> orderRequests) {
        this.mContext = context;
        this.orderRequests = orderRequests;
        if (!orderRequests.isEmpty()) {
            this.menuItemIds = new ArrayList<>(orderRequests.keySet());
        } else {
            this.menuItemIds = new ArrayList<>();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType ==ITEM_VIEW) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_placed_order, parent, false);
            return new ItemViewHolder(itemView);
        }else {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_new_order_section_header, parent, false);
            return new CurrentOrderSectionHeader(itemView);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (menuItemIds != null && 0 <= position && position < menuItemIds.size()) {

            if (viewHolder instanceof ItemViewHolder) {

                ItemViewHolder holder = (ItemViewHolder) viewHolder;

                String menuId = menuItemIds.get(position);
                if (orderRequests != null && !orderRequests.isEmpty() && orderRequests.containsKey(menuId)) {

                    ArrayList<OrderItemsDB> orderItemsDBArrayList = orderRequests.get(menuId);

                    if (orderItemsDBArrayList != null && !orderItemsDBArrayList.isEmpty()) {

                        double charge = 0.0;
                        String remarks = "";
                        for (OrderItemsDB item : orderItemsDBArrayList) {
                            if (item.getChargeReqDB() != null) {
                                charge = charge + item.getChargeReqDB().getAmount();
                            }
                            if (item.getRemarks() != null && !item.getRemarks().isEmpty()) {
                                remarks = item.getRemarks();
                            }
                        }

                        final OrderItemsDB item = orderItemsDBArrayList.get(0);
                        holder.txvItemName.setText(item.getDisplay_label());

                        if (!remarks.isEmpty()) {
                            holder.txvRemarks.setText(remarks);
                            holder.rlRemarks.setVisibility(View.VISIBLE);
                            holder.txvRemarks.setTypeface(holder.txvRemarks.getTypeface());
                        } else {
                            holder.txvRemarks.setText("No remarks");
                            holder.rlRemarks.setVisibility(View.VISIBLE);
                            holder.txvRemarks.setTypeface(holder.txvRemarks.getTypeface(), Typeface.ITALIC);
                        }
                        holder.txvItemQuantity.setText(String.format(Locale.US, "X %d", orderItemsDBArrayList.size()));

                        String currency = FormatUtils.getCurrencySymbol();
                        holder.txvNewPrice.setText(String.valueOf(currency + " " + charge));

                        holder.txvOldPrice.setPaintFlags(holder.txvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        holder.txvOldPrice.setVisibility(View.VISIBLE);
                        holder.txvDiscountPercent.setVisibility(View.VISIBLE);
                        holder.llDiscount.setVisibility(View.GONE);

                    }
                }
            }else if (viewHolder instanceof CurrentOrderSectionHeader){
                CurrentOrderSectionHeader header = (CurrentOrderSectionHeader) viewHolder;
                header.txvHeaderName.setText("Existing Order");

            }
        }
    }

    @Override
    public int getItemCount() {
        if (orderRequests != null) {
            return orderRequests.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (menuItemIds.get(position).equals("EXISTING ORDER")){
            return CURRENT_ORDER_SECTION;
        }else {
            return ITEM_VIEW;
        }
    }

    public LinkedHashMap<String, ArrayList<OrderItemsDB>> getOrderRequests() {
        return orderRequests;
    }

    public ArrayList<String> getMenuItemIds() {
        return menuItemIds;
    }

    public void removeItem(String menuId, int position) {
        orderRequests.remove(menuId);
        menuItemIds.remove(menuId);
        notifyItemRemoved(position);
    }

    public void restoreItem(OrderItemsDB item, int position) {
//        orderRequests.add(position, item);
        notifyItemInserted(position);
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView txvItemName, txvRemarks, txvDiscountPercent, txvItemQuantity, txvOldPrice, txvNewPrice;
        LinearLayout viewForeground, rlRemarks, llDiscount;

        ItemViewHolder(View view) {
            super(view);
            txvItemName = view.findViewById(R.id.txv_item_name);
            txvItemQuantity = view.findViewById(R.id.txv_quantity);
            txvRemarks = view.findViewById(R.id.txv_remarks);
            txvDiscountPercent = view.findViewById(R.id.txv_discount);
            txvOldPrice = view.findViewById(R.id.txv_old_price);
            txvNewPrice = view.findViewById(R.id.txv_new_price);
            viewForeground = view.findViewById(R.id.view_foreground);

            rlRemarks = view.findViewById(R.id.rl_remarks);
            llDiscount = view.findViewById(R.id.ll_discount);
        }
    }

    public class CurrentOrderSectionHeader extends RecyclerView.ViewHolder {
        TextView txvHeaderName;
        LinearLayout layout;

        public CurrentOrderSectionHeader(View view) {
            super(view);
            txvHeaderName = view.findViewById(R.id.txv_current_order);
            layout =view.findViewById(R.id.ll_header);
            layout.setEnabled(false);
            txvHeaderName.setEnabled(false);
            layout.setClickable(false);
            txvHeaderName.setClickable(false);
        }
    }


    public interface ItemClickListeners {
        public void removedItemOrder(ArrayList<OrderItemsDB> requests);
        public void appliedDiscount(ArrayList<OrderItemsDB> requests);
    }
}
