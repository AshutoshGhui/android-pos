package com.hotelkeyapp.android.pos.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vrushalimankar on 16/05/18.
 */

public class Tax implements Parcelable{

    @SerializedName("id")
    private String id;

    @SerializedName("amount")
    private double amount;

    @SerializedName("name")
    private String name;

    @SerializedName("tax_type_id")
    private String tax_type_id;

    @SerializedName("date")
    private String date;

    public Tax() {
    }

    protected Tax(Parcel in) {
        id = in.readString();
        amount = in.readDouble();
        name = in.readString();
        tax_type_id = in.readString();
        date = in.readString();
    }

    public static final Creator<Tax> CREATOR = new Creator<Tax>() {
        @Override
        public Tax createFromParcel(Parcel in) {
            return new Tax(in);
        }

        @Override
        public Tax[] newArray(int size) {
            return new Tax[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTax_type_id() {
        return tax_type_id;
    }

    public void setTax_type_id(String tax_type_id) {
        this.tax_type_id = tax_type_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeDouble(amount);
        parcel.writeString(name);
        parcel.writeString(tax_type_id);
        parcel.writeString(date);
    }
}
