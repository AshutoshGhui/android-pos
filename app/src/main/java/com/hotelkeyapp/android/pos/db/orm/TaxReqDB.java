package com.hotelkeyapp.android.pos.db.orm;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
import android.os.Parcel;
import android.os.Parcelable;

import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.utils.FormatUtils;

import java.util.Date;
import java.util.List;

import de.greenrobot.dao.query.QueryBuilder;

// KEEP INCLUDES END


/**
 * Entity mapped to table "TAX_REQ_DB".
 */
public class TaxReqDB implements Parcelable{

    private String id;
    private Double amount;
    private String name;
    private String tax_type_id;
    private String date;
    private String chargeId;

    // KEEP FIELDS - put your custom fields here

    private double adjustedTax;
    // KEEP FIELDS END

    public TaxReqDB() {
    }

    public TaxReqDB(String id) {
        this.id = id;
    }

    public TaxReqDB(String id, Double amount, String name, String tax_type_id, String date, String chargeId) {
        this.id = id;
        this.amount = amount;
        this.name = name;
        this.tax_type_id = tax_type_id;
        this.date = date;
        this.chargeId = chargeId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTax_type_id() {
        return tax_type_id;
    }

    public void setTax_type_id(String tax_type_id) {
        this.tax_type_id = tax_type_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getChargeId() {
        return chargeId;
    }

    public void setChargeId(String chargeId) {
        this.chargeId = chargeId;
    }



    // KEEP METHODS - put your custom methods here


    public double getAdjustedTax() {
        return adjustedTax;
    }

    public void setAdjustedTax(double adjustedTax) {
        this.adjustedTax = adjustedTax;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        if (amount == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeDouble(amount);
        }
        parcel.writeString(name);
        parcel.writeString(tax_type_id);
        parcel.writeString(date);
        parcel.writeString(chargeId);
    }
    protected TaxReqDB(Parcel in) {
        id = in.readString();
        if (in.readByte() == 0) {
            amount = null;
        } else {
            amount = in.readDouble();
        }
        name = in.readString();
        tax_type_id = in.readString();
        date = in.readString();
        chargeId = in.readString();
    }

    public static final Creator<TaxReqDB> CREATOR = new Creator<TaxReqDB>() {
        @Override
        public TaxReqDB createFromParcel(Parcel in) {
            return new TaxReqDB(in);
        }

        @Override
        public TaxReqDB[] newArray(int size) {
            return new TaxReqDB[size];
        }
    };

    public TaxTypePeriodsDB getTax(){

        DaoSession daoSession = POSApplication.getInstance().getDaoSession();
        QueryBuilder<TaxTypesDB> queryBuilder = daoSession.getTaxTypesDBDao().queryBuilder()
                .where(TaxTypesDBDao.Properties.TaxTypeId.eq(this.tax_type_id));
        TaxTypesDB aTaxTypesDB = queryBuilder.list().get(0);

        List<TaxTypePeriodsDB> taxTypePeriods = aTaxTypesDB.getTaxTypePeriods();
        for (TaxTypePeriodsDB aTaxTypePeriods : taxTypePeriods) {

            if (aTaxTypePeriods != null) {
                String startDate = aTaxTypePeriods.getStart_date();
                String endDate = aTaxTypePeriods.getEnd_date();

                Date start = FormatUtils.toFormatedDate(startDate);
                Date chargeDate = PreferenceUtils.getCBD(POSApplication.getInstance());

                if (chargeDate.equals(start) || chargeDate.after(start)) {
                    boolean isValid =false;
                    if (endDate != null && !endDate.isEmpty()) {
                        Date end = FormatUtils.toFormatedDate(endDate);
                        if (chargeDate.equals(end) || chargeDate.before(end)) {
                            isValid =true;
                        }
                    } else {
                        isValid =true;
                    }
                    if (isValid) {
                        return aTaxTypePeriods;
                    }
                }
            }
        }
        return null;
    }
    // KEEP METHODS END

}
