package com.hotelkeyapp.android.pos.utils;

import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.db.orm.DaoSession;
import com.hotelkeyapp.android.pos.db.orm.DepartmentDB;
import com.hotelkeyapp.android.pos.db.orm.DepartmentDBDao;
import com.hotelkeyapp.android.pos.db.orm.FolioReqDB;
import com.hotelkeyapp.android.pos.db.orm.FolioTypeDB;
import com.hotelkeyapp.android.pos.db.orm.FolioTypeDBDao;
import com.hotelkeyapp.android.pos.db.orm.MenuItemDB;
import com.hotelkeyapp.android.pos.db.orm.MenuItemDBDao;

import de.greenrobot.dao.query.Query;

/**
 * Created by vrushalimankar on 01/06/18.
 */

public class FolioHelper {

    public static FolioReqDB getFolios(String menuId, Double totalCharge, double totalTaxes) {
        DaoSession daoSession = POSApplication.getInstance().getDaoSession();

        String departmentId = null, folioTypeId = null;
        Query<MenuItemDB> qb = daoSession.getMenuItemDBDao().queryBuilder()
                .where(MenuItemDBDao.Properties.Id.eq(menuId)).build();
        if (qb != null && qb.list() != null && !qb.list().isEmpty()) {
            MenuItemDB menuItemDB = qb.list().get(0);
            if (menuItemDB != null) {
                departmentId = menuItemDB.getInventory_system_id();
            }
        }

        if (departmentId != null && !departmentId.isEmpty()) {
            Query<DepartmentDB> queryBuilder = daoSession.getDepartmentDBDao().queryBuilder()
                    .where(DepartmentDBDao.Properties.Id.eq(departmentId)).build();
            if (queryBuilder != null && queryBuilder.list() != null && !queryBuilder.list().isEmpty()) {
                DepartmentDB departmentDB = queryBuilder.list().get(0);
                if (departmentDB.getActive()) {
                    folioTypeId = departmentDB.getFolio_type_id();
                }
            }
        }

        FolioTypeDB folioTypeDB = null;
        if (folioTypeId != null && !folioTypeId.isEmpty()) {
            Query<FolioTypeDB> query = daoSession.getFolioTypeDBDao().queryBuilder()
                    .where(FolioTypeDBDao.Properties.Id.eq(folioTypeId)).build();
            if (query != null && query.list() != null && !query.list().isEmpty()) {
                folioTypeDB = query.list().get(0);
            }

        } else {
            Query<FolioTypeDB> query = daoSession.getFolioTypeDBDao().queryBuilder()
                    .where(FolioTypeDBDao.Properties.Code.eq("GENERAL")).build();
            if (query != null && query.list() != null && !query.list().isEmpty()) {
                folioTypeDB = query.list().get(0);
                folioTypeId = folioTypeDB.getId();
            }
        }

        FolioReqDB folio = new FolioReqDB();
        if (folioTypeDB != null) {
            folio.setFolio_type_id(folioTypeId);
            folio.setName(folioTypeDB.getName());
            folio.setCode(folioTypeDB.getCode());
            folio.setTotal_charges(totalCharge);
            folio.setTotal_taxes(totalTaxes);
        }
        return folio;
    }
}
