package com.hotelkeyapp.android.pos.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.hotelkeyapp.android.pos.db.orm.ChargeTypesDB;

import java.util.ArrayList;

/**
 * Created by vrushalimankar on 1/13/18.
 */

public class ChargeType implements Parcelable {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("code")
    private String code;

    @SerializedName("active")
    private boolean isActive;

    @SerializedName("tax_types")
    private ArrayList<TaxTypes> taxTypes;

    @SerializedName("custom_field_10_label")
    private String custom_field_10_label;

    protected ChargeType(Parcel in) {
        id = in.readString();
        name = in.readString();
        code = in.readString();
        isActive = in.readByte() != 0;
        taxTypes = in.createTypedArrayList(TaxTypes.CREATOR);
        custom_field_10_label =in.readString();
    }

    public static final Creator<ChargeType> CREATOR = new Creator<ChargeType>() {
        @Override
        public ChargeType createFromParcel(Parcel in) {
            return new ChargeType(in);
        }

        @Override
        public ChargeType[] newArray(int size) {
            return new ChargeType[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(code);
        dest.writeByte((byte) (isActive ? 1 : 0));
        dest.writeTypedList(taxTypes);
        dest.writeString(custom_field_10_label);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public ArrayList<TaxTypes> getTaxTypes() {
        return taxTypes;
    }

    public void setTaxTypes(ArrayList<TaxTypes> taxTypes) {
        this.taxTypes = taxTypes;
    }


    public String getCustom_field_10_label() {
        return custom_field_10_label;
    }

    public void setCustom_field_10_label(String custom_field_10_label) {
        this.custom_field_10_label = custom_field_10_label;
    }

    public ChargeTypesDB toChargeTypeDB() {
        ChargeTypesDB chargeTypesDB =new ChargeTypesDB();
        chargeTypesDB.setId(getId());
        chargeTypesDB.setName(getName());
        chargeTypesDB.setCode(getCode());
        chargeTypesDB.setIs_active(isActive());
        chargeTypesDB.setCustom_field_10_label(getCustom_field_10_label());
        return chargeTypesDB;
    }
}
