package com.hotelkeyapp.android.pos.activity.common;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hotelkey.hkandroidlib.HKAndroidApplication;
import com.hotelkey.hkandroidlib.activity.common.AuthenticatedActivity;
import com.hotelkey.hkandroidlib.models.User;
import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.activity.about.HKAboutActivity;
import com.hotelkeyapp.android.pos.apiService.SyncService;
import com.hotelkeyapp.android.pos.utils.LocalizationConstants;


public class HKNavigationDrawerActivity extends AuthenticatedActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final int RQ_SWITCH_PROPERTY = 10001;

    private LocalizationConstants localizationConstants;

    private NavigationView navigationView;

    protected RelativeLayout contentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_hknavigation_drawer_activity);
        localizationConstants = LocalizationConstants.getInstance();

        setup();
        setNavTitle();
    }

    private void setup() {
        Toolbar toolbar = findViewById(R.id.navigation_drawer_toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setCheckedItem(R.id.nav_dashboard);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);

        TextView navTitle = headerView.findViewById(R.id.nav_txt_title);
        TextView navDetails = headerView.findViewById(R.id.nav_txt_details);

        HKAndroidApplication application = HKAndroidApplication.getApplication();
        User currentUser = application.getCurrentUser();

        if (currentUser != null && currentUser.getFirstName() != null) {
            navTitle.setText(String.format("%s %s", currentUser.getFirstName(), currentUser.getLastName()));

            if (currentUser.getEmail() != null && !currentUser.getEmail().isEmpty()) {
                navDetails.setText(currentUser.getEmail());
                navDetails.setVisibility(View.VISIBLE);
            } else {
                navDetails.setVisibility(View.GONE);
            }
            contentLayout = findViewById(R.id.nav_content);
        } else {
            finish();
        }
    }

    private void setNavTitle() {
        Menu menu = navigationView.getMenu();

        MenuItem navDashboard = menu.findItem(R.id.nav_dashboard);
        navDashboard.setTitle(localizationConstants.getKeyValue("btn_dashboard"));

        MenuItem navAbout = menu.findItem(R.id.nav_about);
        navAbout.setTitle(localizationConstants.getKeyValue("btn_about"));

        MenuItem navSwitchProperty = menu.findItem(R.id.nav_switch_property);
        navSwitchProperty.setTitle(localizationConstants.getKeyValue("btn_switch_property"));

        MenuItem navLogout = menu.findItem(R.id.nav_logout);
        navLogout.setTitle(localizationConstants.getKeyValue("btn_logout"));
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        boolean result = handleNavigation(item.getItemId());
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return result;
    }

    protected boolean handleNavigation(int id) {
        if (id == R.id.nav_about) {
            Intent intent = new Intent(this, HKAboutActivity.class);
            startActivity(intent);
            return true;

        } else if (id == R.id.nav_switch_property) {
            Intent intent = HKSwitchPropertyActivity.getLaunchIntentForSwitch(this);
            startActivityForResult(intent, RQ_SWITCH_PROPERTY);
            return true;

        } else if (id == R.id.nav_logout) {
            showYesNoDialog(LocalizationConstants.getInstance().getKeyValue("msg_logout"),
                    LocalizationConstants.getInstance().getKeyValue("btn_yes"),
                    LocalizationConstants.getInstance().getKeyValue("btn_no"), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (i == Dialog.BUTTON_POSITIVE) {
                                logout();
                            } else {
                                dialogInterface.cancel();
                            }
                        }
                    });

            return true;
        }
        return false;
    }

    public NavigationView getNavigationView() {
        return navigationView;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RQ_SWITCH_PROPERTY && resultCode == RESULT_OK) {
            PreferenceUtils.saveAppReady(this, false);
            onPropertySwitched();
        }
    }

    protected void onPropertySwitched() {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(SyncService.CANCEL_ALL_REQUESTS);
        sendBroadcast(broadcastIntent);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
