package com.hotelkeyapp.android.pos.controllers;

import android.util.Pair;

import com.hotelkey.hkandroidlib.common.BaseController;
import com.hotelkey.hkandroidlib.utils.MessageHelper;
import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.db.orm.DaoSession;
import com.hotelkeyapp.android.pos.db.orm.NotificationDB;
import com.hotelkeyapp.android.pos.db.orm.NotificationDBDao;
import com.hotelkeyapp.android.pos.db.orm.OrderDB;
import com.hotelkeyapp.android.pos.db.orm.OrderDBDao;
import com.hotelkeyapp.android.pos.model.Notifications;
import com.hotelkeyapp.android.pos.utils.AccountPreferences;
import com.hotelkeyapp.android.pos.utils.RetrofitCustomCallback;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import de.greenrobot.dao.query.QueryBuilder;
import retrofit2.Call;

/**
 * Created by vrushalimankar on 23/05/18.
 */

public class NotificationsController extends BaseController {

    public interface NotificationsCallbacks {
        void onFailure(Pair<String, Boolean> message);

        void onSuccess(List<NotificationDB> notifications);
    }

    private NotificationsCallbacks callbacks;

    public NotificationsController(NotificationsCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    public void downloadNotifications() {

        final POSApplication posApplication = POSApplication.getInstance();

        String propertyId = PreferenceUtils.getCurrentProperty(posApplication).getId();
        Long lastSync = AccountPreferences.getNotificationsLastSync(posApplication);

        Call<ArrayList<Notifications>> property = POSApplication.getInstance().getNetworkService()
                .getNotifications(PreferenceUtils.getEntIdentifier(posApplication), propertyId, lastSync);

        property.enqueue(new RetrofitCustomCallback<ArrayList<Notifications>>() {
            @Override
            public void onResponse(ArrayList<Notifications> notifications) {
                List<NotificationDB> notificationDBS = new ArrayList<>();

                DaoSession daoSession = posApplication.getDaoSession();
                for (Notifications aNotifications : notifications) {
                    NotificationDB notificationDB = aNotifications.toNotificationDB();
                    QueryBuilder<OrderDB> orderDBDaoQueryBuilder = daoSession.getOrderDBDao().queryBuilder().where(OrderDBDao.Properties.Id.eq(notificationDB.getOrder_id()));
                    if ((orderDBDaoQueryBuilder != null) &&(orderDBDaoQueryBuilder.list().size() > 0))
                    {
                        QueryBuilder<NotificationDB> queryBuilder = daoSession.getNotificationDBDao().queryBuilder().where(NotificationDBDao.Properties.Id.eq(notificationDB.getId()));
                        try {
                            if (queryBuilder.list() != null && (queryBuilder.list().size() > 0)) {
                                notificationDB.setItem_recently_updated(false);
                            } else {
                                notificationDB.setItem_recently_updated(true);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            notificationDB.setItem_recently_updated(false);
                        }

                        daoSession.insertOrReplace(notificationDB);
                        notificationDBS.add(notificationDB);
                    }
                }
                Calendar calendar =Calendar.getInstance();
                TimeZone tz = TimeZone.getTimeZone(PreferenceUtils.getTimeZoneOffset(POSApplication.getInstance()));
                calendar.setTimeZone(tz);

                AccountPreferences.saveNotificationsLastSync(posApplication, calendar.getTimeInMillis());
                callbacks.onSuccess(notificationDBS);
            }

            @Override
            public void onFailure(Call<ArrayList<Notifications>> call, Throwable t) {
                callbacks.onFailure(MessageHelper.getMessageForError(posApplication, t));
            }
        });
    }
}
