package com.hotelkeyapp.android.pos.activity.orderDetails;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.hotelkey.hkandroidlib.activity.common.AuthenticatedActivity;
import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.controllers.OrderCreationController;
import com.hotelkeyapp.android.pos.controllers.WaitersController;
import com.hotelkeyapp.android.pos.db.orm.DaoSession;
import com.hotelkeyapp.android.pos.db.orm.OrderDB;
import com.hotelkeyapp.android.pos.db.orm.OrderDBDao;
import com.hotelkeyapp.android.pos.db.orm.PropertyTableDB;
import com.hotelkeyapp.android.pos.db.orm.PropertyTableDBDao;
import com.hotelkeyapp.android.pos.db.orm.WaiterDB;
import com.hotelkeyapp.android.pos.db.orm.WaiterDBDao;
import com.hotelkeyapp.android.pos.model.Waiter;
import com.hotelkeyapp.android.pos.model.order.AuditLogs;
import com.hotelkeyapp.android.pos.utils.Constants;
import com.hotelkeyapp.android.pos.utils.LocalizationConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.greenrobot.dao.DaoException;
import de.greenrobot.dao.query.Query;
import de.greenrobot.dao.query.QueryBuilder;

import static com.hotelkeyapp.android.pos.utils.Constants.ARG_MESSAGE;

public class HKGuestDetailsActivity extends AuthenticatedActivity implements WaitersController.WaitersControllerData,
        View.OnClickListener, OrderCreationController.OrderCreationCallbacks {

    private static final String ARG_TABLE_ID = "Table id";

    private static final int RQ_ORDER_CREATION = 4567;

    private TextView txvQuantity, txvTitle, txvWaiterName;

    private EditText edtGuestName;

    private Button btnSave;

    private ImageButton btnMinus, btnPlus;

    private PropertyTableDB propertyTableDB;

    private HashMap<String, WaiterDB> waiterDBList;

    private WaiterDB waiterDB;

    private int headcount;

    private OrderDB orderDB;

    private String assignedWaiterId;

    public static Intent getLaunchIntent(Context context, OrderDB orderDB) {
        Intent intent = new Intent(context, HKGuestDetailsActivity.class);
        intent.putExtra(ARG_TABLE_ID, orderDB.getTableId());

        OrderHelper orderHelper = OrderHelper.getInstance();
        orderHelper.setOrderDB(orderDB);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hkguest_details);

        Toolbar toolbar = findViewById(R.id.toolbar_top);
        txvTitle = toolbar.findViewById(R.id.toolbar_title);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        HashMap<String, WaiterDB> waiterDBList = getWaitersList();
        if (waiterDBList == null || waiterDBList.isEmpty()) {
            makeWaitersReq();
        }
        initViews();

    }


    private void initViews() {
        String tableId = getIntent().getStringExtra(ARG_TABLE_ID);
        if (tableId != null) {
            DaoSession daoSession = POSApplication.getInstance().getDaoSession();
            QueryBuilder<PropertyTableDB> queryBuilder = daoSession.getPropertyTableDBDao().queryBuilder().where(PropertyTableDBDao.Properties.Id.eq(tableId));

            if (queryBuilder != null) {
                try {
                    propertyTableDB = queryBuilder.unique();
                    if (propertyTableDB != null && propertyTableDB.getNumber() != null) {
                        String tableNo = String.format(LocalizationConstants.getInstance().getKeyValue("lbl_table_no_with_value"), propertyTableDB.getNumber());
                        txvTitle.setText(tableNo);
                    }
                }catch (DaoException e){
                    e.printStackTrace();
                }
            }
        }

        if (propertyTableDB == null) {
            finish();
            return;
        }

        findViewById(R.id.img_expandable_view).setOnClickListener(this);
        txvWaiterName = findViewById(R.id.txv_waiter_name);
        txvWaiterName.setOnClickListener(this);

        edtGuestName = findViewById(R.id.edt_guest_name);
        edtGuestName.setHint(LocalizationConstants.getInstance().getKeyValue("msg_enter_guest_name"));

        try {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        TextView txvHeadCount = findViewById(R.id.txv_head_count);
        txvHeadCount.setText(LocalizationConstants.getInstance().getKeyValue("lbl_headcount"));

        TextView txvWaiter = findViewById(R.id.txv_waiter_lbl);
        txvWaiter.setText(LocalizationConstants.getInstance().getKeyValue("lbl_waiter"));

        btnPlus = findViewById(R.id.btn_plus);
        btnMinus = findViewById(R.id.btn_minus);
        txvQuantity = findViewById(R.id.txv_quantity);

        btnSave = findViewById(R.id.btn_save);
        btnSave.setText(LocalizationConstants.getInstance().getKeyValue("btn_save"));
        btnSave.setOnClickListener(this);

        orderDB = OrderHelper.getInstance().getOrderDB();

        if (orderDB != null) {

            if (orderDB.getGuest_name() != null) {
                edtGuestName.setText(orderDB.getGuest_name());
                edtGuestName.setSelection(orderDB.getGuest_name().length());
            }

            if (orderDB.getWaiter_id() != null) {
                if (waiterDBList != null && waiterDBList.containsKey(orderDB.getWaiter_id())) {
                    waiterDB = waiterDBList.get(orderDB.getWaiter_id());
                    String waiterName = waiterDB.getFirst_name() + " " + waiterDB.getLast_name();
                    txvWaiterName.setText(waiterName);
                    assignedWaiterId = waiterDB.getId();
                }
            }
            headcount = orderDB.getPersons();

            txvQuantity.setText(String.valueOf(orderDB.getPersons()));
            btnSave.setText(LocalizationConstants.getInstance().getKeyValue("btn_save"));

        } else {
            headcount = 1;
        }

        if (1 == propertyTableDB.getHeadcount()) {
            btnMinus.setOnClickListener(null);
            btnPlus.setOnClickListener(null);
            btnMinus.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_disable));
            btnPlus.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_disable));

        } else {
            btnMinus.setOnClickListener(this);
            btnPlus.setOnClickListener(this);
            if (headcount == 1) {
                btnMinus.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_disable));
            } else {
                btnMinus.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_enable));
            }
            if (headcount != propertyTableDB.getHeadcount()) {
                btnPlus.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_enable));
            } else {
                btnPlus.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_disable));
            }
        }
    }

    private HashMap<String, WaiterDB> getWaitersList() {
        waiterDBList = new HashMap<>();

        DaoSession daoSession = POSApplication.getInstance().getDaoSession();
        QueryBuilder<WaiterDB> waiterDBS = daoSession.getWaiterDBDao().queryBuilder().where(WaiterDBDao.Properties.Active.eq("true"));
        if (waiterDBS != null && waiterDBS.list() != null) {
            List<WaiterDB> waiters = waiterDBS.list();

            if (waiters != null && !waiters.isEmpty()) {
                for (WaiterDB waiterDB : waiters) {
                    waiterDBList.put(waiterDB.getId(), waiterDB);
                }
            }
        }
        return waiterDBList;
    }

    private void makeWaitersReq() {
        showLoadingDialog(LocalizationConstants.getInstance().getKeyValue("ldr_loading"));
        WaitersController waitersController = new WaitersController(this);
        waitersController.downloadWaitersList(PreferenceUtils.getCurrentProperty(getApplicationContext()).getId());
    }


    @Override
    public void onFailure(Pair<String, Boolean> message) {
        handleApiFailure(message);
    }

    private void handleApiFailure(Pair<String, Boolean> message) {
        cancelLoadingDialog();
        if (message.second) {
            Intent intent = new Intent();
            intent.putExtra(ARG_MESSAGE, message.first);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            showMessageDialog(message.first, LocalizationConstants.getInstance().getKeyValue("btn_ok"), null);
        }
    }

    @Override
    public void onSuccess(List<Waiter> waiters) {
        waiterDBList = getWaitersList();
        if (waiterDBList != null) {
            if (orderDB != null && waiterDBList.containsKey(orderDB.getWaiter_id())) {
                waiterDB = waiterDBList.get(orderDB.getWaiter_id());
                String waiterName = waiterDB.getFirst_name() + " " + waiterDB.getLast_name();
                txvWaiterName.setText(waiterName);
            }
        }
        cancelLoadingDialog();
    }

    @Override
    public void onSuccess(boolean id, OrderDB orderDB, ArrayList<String> newOrderItemIds) {
        cancelLoadingDialog();
        String waiterName = "";
        if (waiterDB != null) {
            this.orderDB.setWaiter_id(waiterDB.getId());
            waiterName = waiterDB.getFirst_name() + " " + waiterDB.getLast_name();
        }

        Toast.makeText(this, LocalizationConstants.getInstance().getKeyValue("toast_change_save_successfully"), Toast.LENGTH_SHORT).show();

        Intent intent = new Intent();
        OrderHelper.getInstance().setOrderDB(this.orderDB);
        intent.putExtra(Constants.ARG_WAITER_NAME, waiterName);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_expandable_view
                || view.getId() == R.id.txv_waiter_name
                || view.getId() == R.id.txv_waiter_lbl) {

            if (waiterDBList != null && !waiterDBList.values().isEmpty()) {
                final List<WaiterDB> list = new ArrayList<>(waiterDBList.values());
                PopupMenu popup = new PopupMenu(HKGuestDetailsActivity.this, view);
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getActive()) {
                        String fullName = list.get(i).getFirst_name() + " " + list.get(i).getLast_name();
                        popup.getMenu().add(Menu.NONE, i, Menu.NONE, fullName);
                    }
                }

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        waiterDB = list.get(item.getItemId());
                        String fullName = waiterDB.getFirst_name() + " " + waiterDB.getLast_name();
                        txvWaiterName.setText(fullName);
                        return true;
                    }
                });
                popup.show();
            } else {
                Toast.makeText(this, LocalizationConstants.getInstance().getKeyValue("toast_no_waiters"), Toast.LENGTH_SHORT).show();
            }

        } else if (view == btnPlus) {
            if (propertyTableDB != null) {
                int maxSits = propertyTableDB.getHeadcount();

                if (headcount < maxSits) {
                    headcount++;
                    txvQuantity.setText(String.valueOf(headcount));

                    YoYo.with(Techniques.SlideInUp)
                            .duration(250)
                            .repeat(0)
                            .playOn(txvQuantity);

                    if (headcount == maxSits) {
                        btnPlus.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_disable));
                    } else {
                        btnPlus.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_enable));
                    }

                } else if (headcount == maxSits) {
                    btnPlus.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_disable));
                }
                btnMinus.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_enable));

                if (headcount == maxSits) {
                    btnPlus.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_disable));
                } else {
                    btnPlus.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_enable));
                }
            }

        } else if (view == btnMinus) {
            if (propertyTableDB != null) {
                int minSits = 1;

                if (headcount > minSits) {
                    headcount--;
                    txvQuantity.setText(String.valueOf(headcount));
                    YoYo.with(Techniques.SlideInDown)
                            .duration(250)
                            .repeat(0)
                            .playOn(txvQuantity);

                    if (headcount == minSits) {
                        btnMinus.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_disable));
                    } else {
                        btnMinus.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_enable));
                    }
                } else if (headcount == minSits) {
                    btnMinus.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_disable));
                }

                if (headcount == propertyTableDB.getHeadcount()) {
                    btnPlus.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_disable));
                } else {
                    btnPlus.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_enable));
                }
            }

        } else if (view == btnSave) {
            updateGuestDetails();
        }
    }

    private void updateGuestDetails() {
        String waiterName = "";
        if (orderDB != null) {
            orderDB.setGuest_name(edtGuestName.getText().toString().trim());
            orderDB.setPersons(headcount);

            if (waiterDB != null) {
                orderDB.setWaiter_id(waiterDB.getId());
                waiterName = waiterDB.getFirst_name() + " " + waiterDB.getLast_name();

                if (assignedWaiterId == null || !assignedWaiterId.equals(waiterDB.getId())) {
                    ArrayList<AuditLogs> auditLogs = orderDB.getAuditLogs();
                    if (auditLogs == null) {
                        auditLogs = new ArrayList<>();
                    }
                    auditLogs.add(orderDB.getWaiterChangeAuditLog(waiterName, assignedWaiterId));

                    assignedWaiterId = waiterDB.getId();
                    orderDB.setAuditLogs(auditLogs);
                }
            }

            DaoSession daoSession = POSApplication.getInstance().getDaoSession();
            Query<OrderDB> queryBuilder = daoSession.getOrderDBDao().queryBuilder()
                    .where(OrderDBDao.Properties.Id.eq(orderDB.getId())).build();

            if (queryBuilder != null && queryBuilder.list() != null && !queryBuilder.list().isEmpty()) {
                showLoadingDialog(LocalizationConstants.getInstance().getKeyValue("ldr_loading"));
                OrderCreationController orderCreationController = new OrderCreationController(this, new ArrayList<String>());
                orderCreationController.execute(orderDB);

            } else {
                Intent intent = new Intent();
                OrderHelper.getInstance().setOrderDB(orderDB);
                intent.putExtra(Constants.ARG_WAITER_NAME, waiterName);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RQ_ORDER_CREATION && resultCode == RESULT_OK) {
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        OrderDB orderDB = OrderHelper.getInstance().getOrderDB();
        if (orderDB != null) {
            String guestName = edtGuestName.getText().toString().trim();
            int persons = Integer.parseInt(txvQuantity.getText().toString());
            boolean changedWaiterId = false;
            if (waiterDB != null && waiterDB.getId() != null) {
                if (orderDB.getWaiter_id() != null) {
                    if (!orderDB.getWaiter_id().equals(waiterDB.getId())) {
                        changedWaiterId = true;
                    }
                } else {
                    changedWaiterId = true;
                }
            }


            if (!guestName.equals(orderDB.getGuest_name()) || persons != orderDB.getPersons() || changedWaiterId) {
                String message = LocalizationConstants.getInstance().getKeyValue("message_save_changes");
                String yesLbl = LocalizationConstants.getInstance().getKeyValue("btn_yes");
                String noLbl = LocalizationConstants.getInstance().getKeyValue("btn_no");

                showYesNoDialog(message, yesLbl, noLbl, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == Dialog.BUTTON_POSITIVE) {
                            dialogInterface.cancel();
                            updateGuestDetails();
                        } else {
                            dialogInterface.cancel();
                            finish();
                        }
                    }
                });
            } else {
                finish();
            }

        } else {
            finish();
        }
    }

}
