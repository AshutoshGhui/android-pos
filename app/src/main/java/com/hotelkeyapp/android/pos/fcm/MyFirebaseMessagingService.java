package com.hotelkeyapp.android.pos.fcm;

import android.util.Base64;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.model.PushMessage;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static String TAG = MyFirebaseMessagingService.class.toString();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        Gson gson = new Gson();

        PushMessage pushMessage;

        String payloadBase64 = remoteMessage.getData().get("message");

        if (payloadBase64 != null) {
            try {
                String payload = new String(Base64.decode(payloadBase64, Base64.NO_WRAP));
                System.out.println(TAG + payload);
                Log.d(TAG, "Decode message payload: " + payload);
                pushMessage = gson.fromJson(payload, PushMessage.class);

                (POSApplication.getInstance()).pushNotificationReceived(pushMessage);
            } catch (Exception ex) {
                Log.e(TAG, ex.getMessage(), ex);
            }
        }
    }
}
