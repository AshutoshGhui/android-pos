package com.hotelkeyapp.android.pos.model.table;

import com.google.gson.annotations.SerializedName;
import com.hotelkeyapp.android.pos.db.orm.PropertyTableDB;

/**
 * Created by vrushalimankar on 10/05/18.
 */

public class Table {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("code")
    private String code;

    @SerializedName("number")
    private String number;

    @SerializedName("property_id")
    private String property_id;

    @SerializedName("sits")
    private int headcount;

    @SerializedName("active")
    private boolean active;

    @SerializedName("blocked")
    private boolean blocked;

    @SerializedName("dirty")
    private boolean dirty;

    @SerializedName("table_group_id")
    private String table_group_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getProperty_id() {
        return property_id;
    }

    public void setProperty_id(String property_id) {
        this.property_id = property_id;
    }

    public int getHeadcount() {
        return headcount;
    }

    public void setHeadcount(int headcount) {
        this.headcount = headcount;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    public String getTable_group_id() {
        return table_group_id;
    }

    public void setTable_group_id(String table_group_id) {
        this.table_group_id = table_group_id;
    }

    public PropertyTableDB toTableDB() {
        PropertyTableDB tableDB = new PropertyTableDB();
        tableDB.setId(id);
        tableDB.setName(name);
        tableDB.setCode(code);
        tableDB.setNumber(number);
        tableDB.setProperty_id(property_id);
        tableDB.setHeadcount(headcount);
        tableDB.setActive(active);
        tableDB.setBlocked(blocked);
        tableDB.setDirty(dirty);
        tableDB.setTable_group_id(table_group_id);
        return tableDB;
    }

}
