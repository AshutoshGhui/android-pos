package com.hotelkeyapp.android.pos.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vrushalimankar on 11/08/18.
 */

public class OrderItemUpdate {

    @SerializedName("status")
    private String status;

    @SerializedName("table_id")
    private String table_id;

    @SerializedName("order_item_ids")
    private ArrayList<String> order_item_ids;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTable_id() {
        return table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    public ArrayList<String> getOrder_item_ids() {
        return order_item_ids;
    }

    public void setOrder_item_ids(ArrayList<String> order_item_ids) {
        this.order_item_ids = order_item_ids;
    }
}
