package com.hotelkeyapp.android.pos.utils;

import android.os.AsyncTask;

import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.db.orm.DaoSession;
import com.hotelkeyapp.android.pos.db.orm.PropertyTableDB;
import com.hotelkeyapp.android.pos.db.orm.PropertyTableDBDao;
import com.hotelkeyapp.android.pos.db.orm.TableGroupDB;
import com.hotelkeyapp.android.pos.db.orm.TableGroupDBDao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.greenrobot.dao.query.QueryBuilder;

/**
 * Created by vrushalimankar on 28/05/18.
 */

public class TablesAsyncTask extends AsyncTask<Void, Void, List<Object>> {

    private ArrayList<String> selectedTableGroups, selectedStatusList;

    private AsyncTaskListeners callbacks;

    public TablesAsyncTask(AsyncTaskListeners callbacks, ArrayList<String> selectedTableGroups, ArrayList<String> selectedStatusList) {
        this.callbacks = callbacks;
        this.selectedTableGroups = selectedTableGroups;
        this.selectedStatusList = selectedStatusList;
    }

    @Override
    protected List<Object> doInBackground(Void... voids) {
        HashMap<String, TableGroupDB> tableGroupsMap = new HashMap<>();
        HashMap<String, String> tableGroupNameMap = new HashMap<>();

        DaoSession daoSession = POSApplication.getInstance().getDaoSession();
        QueryBuilder<TableGroupDB> tableGroupQB = daoSession.getTableGroupDBDao().queryBuilder().where(TableGroupDBDao.Properties.Active.eq("true"));

        if (tableGroupQB != null && tableGroupQB.list() != null && !tableGroupQB.list().isEmpty()) {
            for (TableGroupDB tableGroupDB : tableGroupQB.list()) {
                if (!selectedTableGroups.isEmpty()) {
                    if (selectedTableGroups.contains(tableGroupDB.getId())) {
                        tableGroupsMap.put(tableGroupDB.getId(), tableGroupDB);
                        tableGroupNameMap.put(tableGroupDB.getId(), tableGroupDB.getName());
                    }
                } else {
                    tableGroupsMap.put(tableGroupDB.getId(), tableGroupDB);
                    tableGroupNameMap.put(tableGroupDB.getId(), tableGroupDB.getName());
                }
            }
        }

        QueryBuilder<PropertyTableDB> queryBuilder = daoSession.getPropertyTableDBDao().queryBuilder()
                .where(PropertyTableDBDao.Properties.Active.eq("true"));

        if (queryBuilder != null && queryBuilder.list() != null) {
            List<PropertyTableDB> filteredPropertyTableDBList = new ArrayList<>();
            List<PropertyTableDB> tableDBS = queryBuilder.list();

            try {
                Collections.sort(tableDBS, new Comparator<PropertyTableDB>() {
                    @Override
                    public int compare(PropertyTableDB f1, PropertyTableDB f2) {
                        int label1 = Integer.parseInt(f1.getNumber());
                        int label2 = Integer.parseInt(f2.getNumber());
                        return label1 - label2;
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }


            if (!selectedStatusList.isEmpty()) {
                for (PropertyTableDB propertyTableDB : tableDBS) {
                    String tableStatus = propertyTableDB.getTable_status();
                    if (tableStatus == null) {
                        continue;
                    }

                    if (tableStatus.equals(Constants.TABLE_STATUS_DIRTY) && selectedStatusList.contains(LocalizationConstants.getInstance().getKeyValue("lbl_dirty"))) {
                        filteredPropertyTableDBList.add(propertyTableDB);
                    } else if (tableStatus.equals(Constants.TABLE_STATUS_AVAILABLE) && selectedStatusList.contains(LocalizationConstants.getInstance().getKeyValue("lbl_available"))) {
                        filteredPropertyTableDBList.add(propertyTableDB);
                    } else if (tableStatus.equals(Constants.TABLE_STATUS_BLOCKED) && selectedStatusList.contains(LocalizationConstants.getInstance().getKeyValue("lbl_blocked"))) {
                        filteredPropertyTableDBList.add(propertyTableDB);
                    } else if (tableStatus.equals(Constants.TABLE_STATUS_RESERVED) && selectedStatusList.contains(LocalizationConstants.getInstance().getKeyValue("lbl_reserved"))) {
                        filteredPropertyTableDBList.add(propertyTableDB);
                    }else if (tableStatus.equals(Constants.TABLE_STATUS_BILL_ISSUED) && selectedStatusList.contains(LocalizationConstants.getInstance().getKeyValue("lbl_order_stat_bill_issued"))){
                        filteredPropertyTableDBList.add(propertyTableDB);
                    }
                }
            } else {
                filteredPropertyTableDBList = tableDBS;
            }

            List<Object> finalFilteredList = new ArrayList<>();

            if (tableGroupsMap.isEmpty()) {
                finalFilteredList.addAll(filteredPropertyTableDBList);

            } else {

                HashMap<String, List<PropertyTableDB>> map = new HashMap<>();
                for (PropertyTableDB propertyTableDB : filteredPropertyTableDBList) {
                    if (propertyTableDB.getTable_group_id() != null) {

                        if (tableGroupsMap.containsKey(propertyTableDB.getTable_group_id())) {
                            if (map.containsKey(propertyTableDB.getTable_group_id())) {
                                List<PropertyTableDB> propertyTableDBS = map.get(propertyTableDB.getTable_group_id());
                                propertyTableDBS.add(propertyTableDB);
                                map.put(propertyTableDB.getTable_group_id(), propertyTableDBS);
                            } else {
                                List<PropertyTableDB> propertyTableDBS = new ArrayList<>();
                                propertyTableDBS.add(propertyTableDB);
                                map.put(propertyTableDB.getTable_group_id(), propertyTableDBS);
                            }
                        }
                    } else {
                        List<PropertyTableDB> propertyTableDBS;
                        if (map.containsKey(LocalizationConstants.getInstance().getKeyValue("lbl_other"))) {
                            propertyTableDBS = map.get(LocalizationConstants.getInstance().getKeyValue("lbl_other"));
                            propertyTableDBS.add(propertyTableDB);
                        } else {
                            propertyTableDBS = new ArrayList<>();
                            propertyTableDBS.add(propertyTableDB);
                        }
                        map.put(LocalizationConstants.getInstance().getKeyValue("lbl_other"), propertyTableDBS);
                    }
                }

                List<Map.Entry<String, String>> sortedEntries = new ArrayList<>(tableGroupNameMap.entrySet());

                Collections.sort(sortedEntries, new Comparator<Map.Entry<String, String>>() {
                            @Override
                            public int compare(Map.Entry<String, String> e1, Map.Entry<String, String> e2) {
                                return e1.getValue().compareTo(e2.getValue());
                            }
                        }
                );

                for (Map.Entry<String, String> tableGroupId : sortedEntries) {
                    if (tableGroupsMap.containsKey(tableGroupId.getKey()) && map.get(tableGroupId.getKey()) != null) {
                        finalFilteredList.add(tableGroupId.getValue());
                        finalFilteredList.addAll(map.get(tableGroupId.getKey()));
                    }
                }

                if (map.containsKey(LocalizationConstants.getInstance().getKeyValue("lbl_other"))) {
                    finalFilteredList.add(LocalizationConstants.getInstance().getKeyValue("lbl_other"));
                    finalFilteredList.addAll(map.get(LocalizationConstants.getInstance().getKeyValue("lbl_other")));
                }

            }
            return finalFilteredList;

        } else {
            return new ArrayList<>();
        }
    }

    @Override
    protected void onPostExecute(List<Object> objects) {
        super.onPostExecute(objects);
        callbacks.getTables(objects);
    }

    public interface AsyncTaskListeners {
        public void getTables(List<Object> objects);
    }
}
