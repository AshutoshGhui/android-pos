package com.hotelkeyapp.android.pos.db.orm;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.model.OrderTip;
import com.hotelkeyapp.android.pos.model.order.AuditLogs;
import com.hotelkeyapp.android.pos.utils.FormatUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import de.greenrobot.dao.DaoException;
import de.greenrobot.dao.query.Query;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS
// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table "ORDER_DB".
 */
public class OrderDB implements Parcelable {

    private String id;
    private String guest_name;
    private String status;
    private Integer persons;
    private String server_guest_name;
    private Integer server_persons;
    private String server_waiter_id;
    private String user_id;
    private Double device_created_at;
    private String business_date;
    private String order_type_id;
    private String order_batch_id;
    private Double total_charges;
    private Double total_taxes;
    private Double net_payments;
    private String waiter_id;
    private Long modifiedTimestamp;
    private String property_id;
    private String order_number;
    private String display_label;
    private Integer sequence_number;

    /**
     * Used to resolve relations
     */
    private transient DaoSession daoSession;

    /**
     * Used for active entity operations.
     */
    private transient OrderDBDao myDao;

    private transient List<OrderItemsDB> order_items;

    // KEEP FIELDS - put your custom fields here

    @SerializedName("folios")
    private ArrayList<FolioReqDB> folioReqDBS;

    @SerializedName("table_id")
    private String tableId;

    @SerializedName("payment_sources")
    private ArrayList payment_source = new ArrayList();

    @SerializedName("order_tips")
    private ArrayList<OrderTip> order_tips = new ArrayList();

    @SerializedName("order_items")
    private ArrayList<OrderItemsDB> localOrderItems;

    @SerializedName("audit_logs")
    private ArrayList<AuditLogs> auditLogs;
    // KEEP FIELDS END

    public OrderDB() {
    }

    public OrderDB(String id) {
        this.id = id;
    }

    public OrderDB(String id, String guest_name, String status, Integer persons, String server_guest_name, Integer server_persons, String server_waiter_id, String user_id, Double device_created_at, String business_date, String order_type_id, String order_batch_id, Double total_charges, Double total_taxes, Double net_payments, String waiter_id, Long modifiedTimestamp, String property_id, String order_number, String display_label, Integer sequence_number, String tableId) {
        this.id = id;
        this.guest_name = guest_name;
        this.status = status;
        this.persons = persons;
        this.server_guest_name = server_guest_name;
        this.server_persons = server_persons;
        this.server_waiter_id = server_waiter_id;
        this.user_id = user_id;
        this.device_created_at = device_created_at;
        this.business_date = business_date;
        this.order_type_id = order_type_id;
        this.order_batch_id = order_batch_id;
        this.total_charges = total_charges;
        this.total_taxes = total_taxes;
        this.net_payments = net_payments;
        this.waiter_id = waiter_id;
        this.modifiedTimestamp = modifiedTimestamp;
        this.property_id = property_id;
        this.order_number = order_number;
        this.display_label = display_label;
        this.sequence_number = sequence_number;
        this.tableId = tableId;
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getOrderDBDao() : null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGuest_name() {
        return guest_name;
    }

    public void setGuest_name(String guest_name) {
        this.guest_name = guest_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getPersons() {
        return persons;
    }

    public void setPersons(Integer persons) {
        this.persons = persons;
    }

    public String getServer_guest_name() {
        return server_guest_name;
    }

    public void setServer_guest_name(String server_guest_name) {
        this.server_guest_name = server_guest_name;
    }

    public Integer getServer_persons() {
        return server_persons;
    }

    public void setServer_persons(Integer server_persons) {
        this.server_persons = server_persons;
    }

    public String getServer_waiter_id() {
        return server_waiter_id;
    }

    public void setServer_waiter_id(String server_waiter_id) {
        this.server_waiter_id = server_waiter_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public Double getDevice_created_at() {
        return device_created_at;
    }

    public void setDevice_created_at(Double device_created_at) {
        this.device_created_at = device_created_at;
    }

    public String getBusiness_date() {
        return business_date;
    }

    public void setBusiness_date(String business_date) {
        this.business_date = business_date;
    }

    public String getOrder_type_id() {
        return order_type_id;
    }

    public void setOrder_type_id(String order_type_id) {
        this.order_type_id = order_type_id;
    }

    public String getOrder_batch_id() {
        return order_batch_id;
    }

    public void setOrder_batch_id(String order_batch_id) {
        this.order_batch_id = order_batch_id;
    }

    public Double getTotal_charges() {
        return total_charges;
    }

    public void setTotal_charges(Double total_charges) {
        this.total_charges = total_charges;
    }

    public Double getTotal_taxes() {
        return total_taxes;
    }

    public void setTotal_taxes(Double total_taxes) {
        this.total_taxes = total_taxes;
    }

    public Double getNet_payments() {
        return net_payments;
    }

    public void setNet_payments(Double net_payments) {
        this.net_payments = net_payments;
    }

    public String getWaiter_id() {
        return waiter_id;
    }

    public void setWaiter_id(String waiter_id) {
        this.waiter_id = waiter_id;
    }

    public Long getModifiedTimestamp() {
        return modifiedTimestamp;
    }

    public void setModifiedTimestamp(Long modifiedTimestamp) {
        this.modifiedTimestamp = modifiedTimestamp;
    }

    public String getProperty_id() {
        return property_id;
    }

    public void setProperty_id(String property_id) {
        this.property_id = property_id;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public String getDisplay_label() {
        return display_label;
    }

    public void setDisplay_label(String display_label) {
        this.display_label = display_label;
    }

    public Integer getSequence_number() {
        return sequence_number;
    }

    public void setSequence_number(Integer sequence_number) {
        this.sequence_number = sequence_number;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    /**
     * To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity.
     */
    public List<OrderItemsDB> getOrder_items() {
        if (order_items == null) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            OrderItemsDBDao targetDao = daoSession.getOrderItemsDBDao();
            List<OrderItemsDB> order_itemsNew = targetDao._queryOrderDB_Order_items(id);
            synchronized (this) {
                if (order_items == null) {
                    order_items = order_itemsNew;
                }
            }
        }
        return order_items;
    }

    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    public synchronized void resetOrder_items() {
        order_items = null;
    }

    /**
     * Convenient call for {@link AbstractDao#delete(Object)}. Entity must attached to an entity context.
     */
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link AbstractDao#update(Object)}. Entity must attached to an entity context.
     */
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /**
     * Convenient call for {@link AbstractDao#refresh(Object)}. Entity must attached to an entity context.
     */
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    // KEEP METHODS - put your custom methods here


    public ArrayList<FolioReqDB> getFolioReqDBS() {
        return folioReqDBS;
    }

    public void setFolioReqDBS(ArrayList<FolioReqDB> folioReqDBS) {
        this.folioReqDBS = folioReqDBS;
    }

    public ArrayList getPayment_source() {
        return payment_source;
    }

    public void setPayment_source(ArrayList payment_source) {
        this.payment_source = payment_source;
    }

    public ArrayList<OrderTip> getOrder_tips() {
        return order_tips;
    }

    public void setOrder_tips(ArrayList<OrderTip> order_tips) {
        this.order_tips = order_tips;
    }

    public ArrayList<OrderItemsDB> getOrderItemsDBS() {
        return localOrderItems;
    }

    public void setOrderItemsDBS(ArrayList<OrderItemsDB> localOrderItems) {
        this.localOrderItems = localOrderItems;
    }

    public ArrayList<AuditLogs> getAuditLogs() {
        return auditLogs;
    }

    public void setAuditLogs(ArrayList<AuditLogs> auditLogs) {
        this.auditLogs = auditLogs;
    }

    protected OrderDB(Parcel in) {
        id = in.readString();
        guest_name = in.readString();
        status = in.readString();
        if (in.readByte() == 0) {
            persons = null;
        } else {
            persons = in.readInt();
        }
        user_id = in.readString();
        if (in.readByte() == 0) {
            device_created_at = null;
        } else {
            device_created_at = in.readDouble();
        }
        business_date = in.readString();
        order_type_id = in.readString();
        order_batch_id = in.readString();
        if (in.readByte() == 0) {
            total_charges = null;
        } else {
            total_charges = in.readDouble();
        }
        if (in.readByte() == 0) {
            total_taxes = null;
        } else {
            total_taxes = in.readDouble();
        }
        if (in.readByte() == 0) {
            net_payments = null;
        } else {
            net_payments = in.readDouble();
        }
        waiter_id = in.readString();
        if (in.readByte() == 0) {
            modifiedTimestamp = null;
        } else {
            modifiedTimestamp = in.readLong();
        }
        property_id = in.readString();
        order_number = in.readString();
        display_label = in.readString();
        if (in.readByte() == 0) {
            sequence_number = null;
        } else {
            sequence_number = in.readInt();
        }
        tableId = in.readString();
        localOrderItems = in.createTypedArrayList(OrderItemsDB.CREATOR);
        folioReqDBS = in.createTypedArrayList(FolioReqDB.CREATOR);
        auditLogs = in.createTypedArrayList(AuditLogs.CREATOR);
        server_guest_name = in.readString();
        server_persons = in.readInt();
        server_waiter_id = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(guest_name);
        dest.writeString(status);
        if (persons == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(persons);
        }
        dest.writeString(user_id);
        if (device_created_at == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(device_created_at);
        }
        dest.writeString(business_date);
        dest.writeString(order_type_id);
        dest.writeString(order_batch_id);
        if (total_charges == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(total_charges);
        }
        if (total_taxes == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(total_taxes);
        }
        if (net_payments == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(net_payments);
        }
        dest.writeString(waiter_id);
        if (modifiedTimestamp == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(modifiedTimestamp);
        }
        dest.writeString(property_id);
        dest.writeString(order_number);
        dest.writeString(display_label);
        if (sequence_number == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(sequence_number);
        }
        dest.writeString(tableId);
        dest.writeTypedList(localOrderItems);
        dest.writeTypedList(folioReqDBS);
        dest.writeTypedList(auditLogs);
        dest.writeString(server_guest_name);
        dest.writeInt(server_persons);
        dest.writeString(server_waiter_id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OrderDB> CREATOR = new Creator<OrderDB>() {
        @Override
        public OrderDB createFromParcel(Parcel in) {
            return new OrderDB(in);
        }

        @Override
        public OrderDB[] newArray(int size) {
            return new OrderDB[size];
        }
    };

    public OrderDB getOrderDBObject(String orderID) {
        DaoSession daoSession = POSApplication.getInstance().getDaoSession();
        Query<OrderDB> query = daoSession.getOrderDBDao().queryBuilder().where(OrderDBDao.Properties.Id.eq(orderID)).build();
        return query.unique();
    }

    public String getWaiterFullName(String waiterId) {
        DaoSession daoSession = POSApplication.getInstance().getDaoSession();

        Query<WaiterDB> queryBuilder = daoSession.getWaiterDBDao().queryBuilder().limit(1)
                .where(WaiterDBDao.Properties.Id.eq(waiterId)).build();

        if (queryBuilder != null) {
            List<WaiterDB> waiterDBS = queryBuilder.list();
            if (waiterDBS != null && !waiterDBS.isEmpty()) {
                WaiterDB waiterDB = waiterDBS.get(0);
                return waiterDB.getFirst_name() + " " + waiterDB.getLast_name();
            }
        }
        return "";
    }

    public void newOrder(OrderDB modifiedOrderDB) {
        setProperty_id(modifiedOrderDB.property_id);
        setOrder_number(modifiedOrderDB.order_number);
        setOrder_batch_id(modifiedOrderDB.order_batch_id);
        setStatus(modifiedOrderDB.status);
        setDisplay_label(modifiedOrderDB.display_label);
        setTotal_charges(modifiedOrderDB.total_charges);
        setTotal_taxes(modifiedOrderDB.total_taxes);
        setNet_payments(modifiedOrderDB.net_payments);
        setUser_id(modifiedOrderDB.user_id);
        setTableId(modifiedOrderDB.getTableId());
        setSequence_number(modifiedOrderDB.sequence_number);
        setDevice_created_at(modifiedOrderDB.device_created_at);
        setBusiness_date(modifiedOrderDB.business_date);
        setId(modifiedOrderDB.id);
        setModifiedTimestamp(modifiedOrderDB.modifiedTimestamp);
    }

    public AuditLogs getWaiterChangeAuditLog(String waiterName, String previousWaiterId) {
        AuditLogs auditLogs = new AuditLogs();
        auditLogs.setId(UUID.randomUUID().toString());
        auditLogs.setAction("ASSIGN_WAITER");
        auditLogs.setCurrent_value(waiterName);
        if (previousWaiterId != null && !previousWaiterId.isEmpty()) {
            auditLogs.setPrevious_value(getWaiterFullName(previousWaiterId));
        }
        auditLogs.setBusiness_date(PreferenceUtils.getCBD(POSApplication.getInstance()));
        auditLogs.setDate(FormatUtils.formatDateWithDeviceTimezone());

        POSApplication context = POSApplication.getInstance();
        String fullName = PreferenceUtils.getCurrentUser(context).getFirstName() + " " + PreferenceUtils.getCurrentUser(context).getLastName();
        String deviceNo = PreferenceUtils.getDeviceNumber(context);
        auditLogs.setUser(fullName + " - " + deviceNo);

        return auditLogs;
    }
    // KEEP METHODS END

}
