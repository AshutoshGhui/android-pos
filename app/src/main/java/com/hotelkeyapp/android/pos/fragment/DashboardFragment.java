package com.hotelkeyapp.android.pos.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.hotelkey.hkandroidlib.common.BaseFragment;
import com.hotelkey.hkandroidlib.service.RefreshEvent;
import com.hotelkey.hkandroidlib.utils.ItemClickSupport;
import com.hotelkey.hkandroidlib.utils.LocalizationStringHelper;
import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.activity.filters.HKFilterActivity;
import com.hotelkeyapp.android.pos.activity.notifications.HKNotificationsActivity;
import com.hotelkeyapp.android.pos.activity.orderDetails.HKOrderDetailsActivity;
import com.hotelkeyapp.android.pos.adapter.TableAdapter;
import com.hotelkeyapp.android.pos.db.orm.PropertyTableDB;
import com.hotelkeyapp.android.pos.utils.AccountPreferences;
import com.hotelkeyapp.android.pos.utils.Constants;
import com.hotelkeyapp.android.pos.utils.FormatUtils;
import com.hotelkeyapp.android.pos.utils.GridItemDecorator;
import com.hotelkeyapp.android.pos.utils.LocalizationConstants;
import com.hotelkeyapp.android.pos.utils.PermissionHelper;
import com.hotelkeyapp.android.pos.utils.RefreshService;
import com.hotelkeyapp.android.pos.utils.TablesAsyncTask;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.support.v7.widget.RecyclerView.SCROLL_STATE_IDLE;
import static com.hotelkeyapp.android.pos.utils.Constants.ARG_MESSAGE;
import static com.hotelkeyapp.android.pos.utils.Constants.TYPE_ITEM;
import static com.hotelkeyapp.android.pos.utils.Constants.TYPE_SECTION;

/**
 * Created by vrushalimankar on 10/05/18.
 */

public class DashboardFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener,
        ItemClickSupport.OnItemClickListener, TablesAsyncTask.AsyncTaskListeners, SearchView.OnQueryTextListener {

    private static final int RQ_FILTERS = 3003;
    private static final int RQ_ORDER_DETAILS = 3004;
    private static final int RQ_NOTIFICATIONS = 3005;

    private LocalizationConstants localizationConstants;

    private RecyclerView recyclerView;

    private SwipeRefreshLayout swipeRefreshLayout;

    RefreshService refreshService;

    private TableAdapter tableAdapter;

    private Context mContext;

    private ArrayList<String> selectedTableGroups, selectedStatusList;

    private TextView txvNoTablesFound;

    private NotificationReceived receiver;

    private SearchView searchView;

    private LinearLayout searchLayout;

    public DashboardFragment() {
        // Required empty public constructor
    }

    public static DashboardFragment newInstance() {
        return new DashboardFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        refreshService = (RefreshService) context;
        mContext = context;

        IntentFilter filter = new IntentFilter(NotificationReceived.REFRESH_BADGE);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        receiver = new NotificationReceived();
        mContext.registerReceiver(receiver, filter);
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        initViews(view);
        setup();
    }

    private void initViews(View view) {
        searchView = view.findViewById(R.id.simpleSearchView);
        searchLayout = view.findViewById(R.id.search_bar);
        setupSearchView(view);
        recyclerView = view.findViewById(R.id.gv_dashboard_statistics);
        recyclerView.requestFocus();
        txvNoTablesFound = view.findViewById(R.id.txv_no_table_found);
        txvNoTablesFound.setText(LocalizationConstants.getInstance().getKeyValue("lbl_no_tables_found"));
        swipeRefreshLayout = view.findViewById(R.id.swipeContainer);
        selectedTableGroups = new ArrayList<>();
        selectedStatusList = new ArrayList<>();

    }

    private void setupSearchView(View view) {
        if (searchView != null) {
            SearchManager searchManager = (SearchManager) mContext.getSystemService(Context.SEARCH_SERVICE);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
            searchView.setIconifiedByDefault(false);
            searchView.setQueryHint(LocalizationStringHelper.getInstance().fetchLocalizedKeyString("lbl_search_table"));
            searchView.setOnQueryTextListener(this);
            searchView.setIconified(false);

            try {
                View v = searchView.findViewById(android.support.v7.appcompat.R.id.search_plate);
                v.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorTransparent));
                TextView searchText = view.findViewById(android.support.v7.appcompat.R.id.search_src_text);
                searchText.setTextSize(14);
                Typeface typeface = ResourcesCompat.getFont(mContext, R.font.montserrat);
                searchText.setTypeface(typeface);

                InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void setup() {

        localizationConstants = LocalizationConstants.getInstance();

        List<Object> statistics = new ArrayList<>();
        tableAdapter = new TableAdapter(mContext, statistics);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(this);
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (tableAdapter.getItemViewType(position)) {
                    case TYPE_SECTION:
                        return 3;
                    case TYPE_ITEM:
                        return 1;
                    default:
                        return 1;
                }
            }
        });
        recyclerView.setLayoutManager(gridLayoutManager);

        recyclerView.addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == SCROLL_STATE_IDLE) {
                    int firstVisibleItemPos = gridLayoutManager.findFirstVisibleItemPosition();
                    if (firstVisibleItemPos <= 0) {
                        searchLayout.animate()
                                .alpha(1.0f)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {
                                        super.onAnimationStart(animation);
                                        getToggleAnimation(searchLayout, searchLayout.getHeight(),
                                                FormatUtils.convertDpToPixels(40, mContext)).start();
                                    }
                                });

                    } else {
                        searchLayout.animate()
                                .alpha(0.0f)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {
                                        super.onAnimationStart(animation);
                                        getToggleAnimation(searchLayout, searchLayout.getHeight() + 6, 0).start();
                                    }
                                });
                    }

                }
            }
        });


        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.grid_spacing);
        recyclerView.addItemDecoration(new GridItemDecorator(spacingInPixels));
        recyclerView.setAdapter(tableAdapter);

        txvNoTablesFound.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        swipeRefreshLayout.setOnRefreshListener(this);

        refreshAdapter();
    }

    private ValueAnimator getToggleAnimation(final android.view.View view, int startHeight, int endHeight) {
        ValueAnimator animator = ValueAnimator.ofInt(startHeight, endHeight);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int val = (Integer) animation.getAnimatedValue();
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
                params.height = val;
                view.setLayoutParams(params);
            }
        });
        animator.setDuration(200);
        return animator;
    }

    @Override
    public void onItemClicked(RecyclerView recyclerView, int i, View view) {
        HashMap<String, String> permissions = PermissionHelper.getUserPermission();
        boolean hasOrderModificationPermission = permissions != null && permissions.containsKey(Constants.PERMISSION_ORDER_MODULE);

        List<Object> objects = tableAdapter.getData();
        if (objects != null && objects.size() > i && i >= 0) {

            if (objects.get(i) instanceof PropertyTableDB) {
                PropertyTableDB propertyTableDB = (PropertyTableDB) objects.get(i);

                if (hasOrderModificationPermission) {
                    if (permissions.get(Constants.PERMISSION_ORDER_MODULE).equals(Constants.PERMISSION_READ)
                            && propertyTableDB.getTable_status().equals(Constants.TABLE_STATUS_AVAILABLE)) {

                        YoYo.with(Techniques.Shake)
                                .duration(100)
                                .repeat(0)
                                .playOn(view);

                    } else if (!propertyTableDB.getTable_status().equals(Constants.TABLE_STATUS_BLOCKED)
                            && !propertyTableDB.getTable_status().equals(Constants.TABLE_STATUS_DIRTY)) {
                        Intent intent = HKOrderDetailsActivity.getLaunchIntent(mContext, propertyTableDB.getId());
                        startActivityForResult(intent, RQ_ORDER_DETAILS);
                    }

                }else if (propertyTableDB.getTable_status().equals(Constants.TABLE_STATUS_AVAILABLE)){

                    YoYo.with(Techniques.Shake)
                            .duration(100)
                            .repeat(0)
                            .playOn(view);
                }
            }
        }
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
        refreshService.refresh();
    }

    @Subscribe
    public void refresh(RefreshEvent event) {
        refreshAdapter();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_filter, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (menu != null) {

            final MenuItem alertMenuItem = menu.findItem(R.id.notifications);
            alertMenuItem.setTitle(localizationConstants.getKeyValue("lbl_notifications"));

            LinearLayout rootView = (LinearLayout) alertMenuItem.getActionView();
            RelativeLayout notificationLayout = rootView.findViewById(R.id.rl_notifications);
            RelativeLayout rlNotificationBadge = rootView.findViewById(R.id.view_alert_red_circle);
            TextView txvNoOfNotifications = rootView.findViewById(R.id.view_alert_count_textview);

            if (AccountPreferences.getNotifications(mContext) != 0) {
                if (!isFilterApplied) {
                    YoYo.with(Techniques.Tada)
                            .duration(900)
                            .repeat(0)
                            .playOn(notificationLayout);
                }

                rlNotificationBadge.setVisibility(View.VISIBLE);
                txvNoOfNotifications.setVisibility(View.VISIBLE);
                txvNoOfNotifications.setText(String.valueOf(AccountPreferences.getNotifications(mContext)));
            } else {
                rlNotificationBadge.setVisibility(View.GONE);
                txvNoOfNotifications.setVisibility(View.GONE);
            }

            RelativeLayout rlFilterBadge = rootView.findViewById(R.id.view_alert_red_circle_filter);
            if (selectedStatusList.isEmpty() && selectedTableGroups.isEmpty()) {
                rlFilterBadge.setVisibility(View.GONE);
            } else {
                rlFilterBadge.setVisibility(View.VISIBLE);
            }

            RelativeLayout filterLayout = rootView.findViewById(R.id.rl_filter);

            notificationLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = HKNotificationsActivity.getLaunchIntent(mContext);
                    startActivityForResult(intent, RQ_NOTIFICATIONS);
                }
            });

            filterLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = HKFilterActivity.getLaunchIntent(mContext, selectedTableGroups, selectedStatusList);
                    startActivityForResult(intent, RQ_FILTERS);
                }
            });
        }
    }

    public class NotificationReceived extends BroadcastReceiver {
        public static final String REFRESH_BADGE = "REFRESH_BADGE";

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (getActivity() != null) {
                    isFilterApplied = false;
                    getActivity().invalidateOptionsMenu();
                }
                Log.d(DashboardFragment.class.getCanonicalName(), "BROADCAST RECEIVED");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    boolean isFilterApplied = false;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RQ_FILTERS && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                selectedTableGroups = data.getStringArrayListExtra("ARG_SELECTED_ZONES");
                selectedStatusList = data.getStringArrayListExtra("ARG_SELECTED_STATUS");
                if (getActivity() != null) {
                    isFilterApplied = true;
                    getActivity().invalidateOptionsMenu();
                }
                searchView.setQuery("", false);
                refreshAdapter();
            }

        } else if (requestCode == RQ_ORDER_DETAILS && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                String message = data.getStringExtra(ARG_MESSAGE);
                if (message != null && !message.isEmpty()) {
                    refreshService.handleApiFailure(message);
                }
            } else {
                refreshAdapter();
            }

        } else if (requestCode == RQ_NOTIFICATIONS && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                String message = data.getStringExtra(ARG_MESSAGE);
                if (message != null && !message.isEmpty()) {
                    refreshService.handleApiFailure(message);
                }
            } else if (getActivity() != null) {
                isFilterApplied = false;
                getActivity().invalidateOptionsMenu();
            }
        }

        try {
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(searchLayout.getWindowToken(), 0);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void refreshAdapter() {
        TablesAsyncTask setTablesAsyncTask = new TablesAsyncTask(this, selectedTableGroups, selectedStatusList);
        setTablesAsyncTask.execute();
    }

    private List<Object> tables;

    @Override
    public void getTables(List<Object> objects) {
        this.tables = objects;
        if (objects.isEmpty()) {
            txvNoTablesFound.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            txvNoTablesFound.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            onQueryTextChange(searchView.getQuery().toString());
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        tableAdapter.setData(tables);
        if (!newText.isEmpty()) {
            tableAdapter.getFilter().filter(newText);
        } else {
            if (tables != null) {
                tableAdapter.notifyDataSetChanged();
            }
        }
        return false;
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        refreshService = null;
    }

    @Override
    public void onDestroy() {
        mContext.unregisterReceiver(receiver);
        mContext = null;
        super.onDestroy();
    }
}
