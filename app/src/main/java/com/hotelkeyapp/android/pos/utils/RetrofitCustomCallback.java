package com.hotelkeyapp.android.pos.utils;

import com.hotelkey.hkandroidlib.exceptions.ServerError;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vrushalimankar on 23/05/18.
 */

public abstract class RetrofitCustomCallback<T> implements Callback<T> {

    public RetrofitCustomCallback() {
    }

    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful()) {
            this.onResponse(response.body());
            response.body();
        } else {
            try {
                if (response.code() == 400) {
                    JSONObject jsonObject = new JSONObject(response.errorBody().string());
                    this.onFailure(call, new ServerError(response.code(), jsonObject.getString("message")));
                } else {
                    this.onFailure(call, new ServerError(response.code(), response.errorBody().string()));
                }
            } catch (IOException | JSONException ioEx) {
                ioEx.printStackTrace();
                this.onFailure(call, new ServerError(response.code(), "Failed to process the request"));
            }
        }

    }

    public abstract void onResponse(T var1);
}
