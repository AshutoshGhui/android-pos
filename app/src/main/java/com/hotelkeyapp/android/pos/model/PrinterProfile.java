package com.hotelkeyapp.android.pos.model;

import com.epson.epos2.discovery.DeviceInfo;

import java.util.UUID;

/**
 * Created by vrushalimankar on 05/10/18.
 */

public class PrinterProfile {

    private String id;

    private String profileName;

    private String printerType;

    private String printerMode;

    private int numberOfCopies;

    private boolean isActive;

    private String ipAddress;

    private DeviceInfo deviceInfo;

    private boolean isSelected;

    public PrinterProfile(DeviceInfo deviceInfo, boolean isSelected) {
        this.id = UUID.randomUUID().toString();
        this.deviceInfo = deviceInfo;
        this.isSelected = isSelected;
    }

    public PrinterProfile(String profileName, boolean isActive, String ipAddress, DeviceInfo selectedDeviceInfo) {
        this.id = UUID.randomUUID().toString();
        this.profileName = profileName;
        this.printerType = "EPSON";
        this.printerMode = "KOT";
        this.isActive = isActive;
        this.ipAddress = ipAddress;
        this.deviceInfo = selectedDeviceInfo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getPrinterType() {
        return printerType;
    }

    public void setPrinterType(String printerType) {
        this.printerType = printerType;
    }

    public String getPrinterMode() {
        return printerMode;
    }

    public void setPrinterMode(String printerMode) {
        this.printerMode = printerMode;
    }

    public int getNumberOfCopies() {
        return numberOfCopies;
    }

    public void setNumberOfCopies(int numberOfCopies) {
        this.numberOfCopies = numberOfCopies;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
}
