package com.hotelkeyapp.android.pos.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.hotelkeyapp.android.pos.model.order.Charge;

import java.util.ArrayList;

/**
 * Created by vrushalimankar on 16/05/18.
 */

public class OrderItemsRequest implements Parcelable {

    @SerializedName("status")
    private String status;

    @SerializedName("display_label")
    private String display_label;

    @SerializedName("menu_item_id")
    private String menu_item_id;

    @SerializedName("id")
    private String id;

    @SerializedName("charge_id")
    private String charge_id;

    @SerializedName("remarks")
    private String remarks;

    @SerializedName("charge")
    private Charge charge;

    @SerializedName("user_id")
    private String user_id;

    @SerializedName("menu_item")
    private MenuItems menuItem;

    @SerializedName("order_options")
    private ArrayList order_options = new ArrayList();

    private transient int quantity;

    public OrderItemsRequest(String status) {
        this.status = status;
    }

    protected OrderItemsRequest(Parcel in) {
        status = in.readString();
        display_label = in.readString();
        menu_item_id = in.readString();
        id = in.readString();
        charge_id = in.readString();
        remarks = in.readString();
        charge = in.readParcelable(Charge.class.getClassLoader());
        quantity =in.readInt();
        user_id =in.readString();
        menuItem =in.readParcelable(MenuItems.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(display_label);
        dest.writeString(menu_item_id);
        dest.writeString(id);
        dest.writeString(charge_id);
        dest.writeString(remarks);
        dest.writeParcelable(charge, flags);
        dest.writeInt(quantity);
        dest.writeString(user_id);
        dest.writeParcelable(menuItem, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OrderItemsRequest> CREATOR = new Creator<OrderItemsRequest>() {
        @Override
        public OrderItemsRequest createFromParcel(Parcel in) {
            return new OrderItemsRequest(in);
        }

        @Override
        public OrderItemsRequest[] newArray(int size) {
            return new OrderItemsRequest[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDisplay_label() {
        return display_label;
    }

    public void setDisplay_label(String display_label) {
        this.display_label = display_label;
    }

    public String getMenu_item_id() {
        return menu_item_id;
    }

    public void setMenu_item_id(String menu_item_id) {
        this.menu_item_id = menu_item_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCharge_id() {
        return charge_id;
    }

    public void setCharge_id(String charge_id) {
        this.charge_id = charge_id;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Charge getCharge() {
        return charge;
    }

    public void setCharge(Charge charge) {
        this.charge = charge;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public MenuItems getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(MenuItems menuItem) {
        this.menuItem = menuItem;
    }
}
