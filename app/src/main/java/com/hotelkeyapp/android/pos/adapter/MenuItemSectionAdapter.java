package com.hotelkeyapp.android.pos.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.db.orm.MenuItemDB;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDB;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import static com.hotelkeyapp.android.pos.utils.Constants.TYPE_FOOTER;
import static com.hotelkeyapp.android.pos.utils.Constants.TYPE_ITEM;

/**
 * Created by vrushalimankar on 15/05/18.
 */

public class MenuItemSectionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private List<MenuItemDB> mItems;

    private List<MenuItemDB> originalDataArrayList;

    private LinkedHashMap<String, ArrayList<OrderItemsDB>> orderItemsMap;

    private CharSequence searchQuery;

    private Context mContext;

    public MenuItemSectionAdapter(Context context, List<MenuItemDB> mItems) {
        this.mItems = mItems;
        this.mContext = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == TYPE_FOOTER) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_footer, viewGroup, false);
            return new FooterViewHolder(view);

        } else {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_menu_items, viewGroup, false);
            return new ItemViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {
            MenuItemDB menuItemDB = mItems.get(holder.getAdapterPosition());
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

            if (searchQuery != null && menuItemDB.getName().toLowerCase().contains(searchQuery)) {

                SpannableString spannable = new SpannableString(menuItemDB.getName());
                int color = ContextCompat.getColor(mContext, R.color.colorPrimary);

                StyleSpan styleSpan = new StyleSpan(Typeface.BOLD);
                ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(color);

                int startIndex = menuItemDB.getName().toLowerCase().indexOf(searchQuery.toString());
                int endIndex = startIndex + searchQuery.length();

                spannable.setSpan(styleSpan, startIndex, endIndex, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                spannable.setSpan(foregroundColorSpan, startIndex, endIndex, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                itemViewHolder.txvTitle.setText(spannable);

            } else {
                itemViewHolder.txvTitle.setText(menuItemDB.getName());
            }
            itemViewHolder.txvDetails.setText(menuItemDB.getMenu_group_name());

            if (orderItemsMap != null && orderItemsMap.containsKey(menuItemDB.getId())) {
                int quantity = orderItemsMap.get(menuItemDB.getId()).size();
                itemViewHolder.txvQuantity.setText(String.format(Locale.US, "X %d", quantity));
                itemViewHolder.txvQuantity.setVisibility(View.VISIBLE);

            } else {
                itemViewHolder.txvQuantity.setVisibility(View.GONE);
            }
        }
    }


    @Override
    public int getItemCount() {
        if (mItems != null) {
            return mItems.size() + 1;
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == mItems.size()) {
            return TYPE_FOOTER;
        } else {
            return TYPE_ITEM;
        }
    }

    public void setItems(List<MenuItemDB> mItems, LinkedHashMap<String, ArrayList<OrderItemsDB>> orderItemsMap) {
        this.mItems = mItems;
        this.orderItemsMap = orderItemsMap;
    }

    public void setOrderItemsMap(LinkedHashMap<String, ArrayList<OrderItemsDB>> orderItemsMap) {
        this.orderItemsMap = orderItemsMap;
    }

    public List<MenuItemDB> getItems() {
        return this.mItems;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults filterResults = new FilterResults();
                final ArrayList<MenuItemDB> resultData = new ArrayList<>();

                if (originalDataArrayList == null)
                    originalDataArrayList = mItems;

                if (constraint != null) {
                    if (originalDataArrayList != null && !originalDataArrayList.isEmpty()) {
                        for (MenuItemDB data : originalDataArrayList) {
                            if (data != null && data.getName() != null) {
                                if ((data.getName().toLowerCase().contains(constraint.toString().toLowerCase()))) {
//                                        || data.getMenu_group_name().toLowerCase().contains(constraint.toString().toLowerCase())) {
                                    resultData.add(data);
                                }
                            }
                        }
                    }
                    filterResults.values = resultData;
                }
                return filterResults;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mItems = (ArrayList<MenuItemDB>) results.values;
                searchQuery = constraint;
                notifyDataSetChanged();
            }
        };
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView txvTitle, txvDetails, txvQuantity;

        ItemViewHolder(View view) {
            super(view);
            txvTitle = view.findViewById(R.id.txv_title);
            txvDetails = view.findViewById(R.id.txv_details);
            txvQuantity = view.findViewById(R.id.txv_quantity);
        }
    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {
        LinearLayout llFooterLayout;

        FooterViewHolder(View view) {
            super(view);
            llFooterLayout = view.findViewById(R.id.ll_footer);
            llFooterLayout.setOnClickListener(null);
        }
    }

}
