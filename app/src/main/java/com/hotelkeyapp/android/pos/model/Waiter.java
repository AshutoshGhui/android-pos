package com.hotelkeyapp.android.pos.model;

import com.google.gson.annotations.SerializedName;
import com.hotelkeyapp.android.pos.db.orm.WaiterDB;

/**
 * Created by vrushalimankar on 10/05/18.
 */

public class Waiter {

    @SerializedName("id")
    private String id;

    @SerializedName("email")
    private String email;

    @SerializedName("username")
    private String username;

    @SerializedName("first_name")
    private String first_name;

    @SerializedName("last_name")
    private String last_name;

    @SerializedName("language_code")
    private String language_code;

    @SerializedName("counter")
    private int counter;

    @SerializedName("active")
    private boolean active;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getLanguage_code() {
        return language_code;
    }

    public void setLanguage_code(String language_code) {
        this.language_code = language_code;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public WaiterDB toWaiterDB(){
        WaiterDB waiterDB =new WaiterDB();
        waiterDB.setId(id);
        waiterDB.setFirst_name(first_name);
        waiterDB.setLast_name(last_name);
        waiterDB.setActive(active);
        waiterDB.setLanguage_code(language_code);
        waiterDB.setCounter(counter);
        return waiterDB;
    }
}
