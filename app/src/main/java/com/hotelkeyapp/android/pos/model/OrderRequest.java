package com.hotelkeyapp.android.pos.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vrushalimankar on 15/05/18.
 */

public class OrderRequest implements Parcelable {

    @SerializedName("id")
    private String id;

    @SerializedName("guest_name")
    private String guest_name;

    @SerializedName("status")
    private String status;

    @SerializedName("persons")
    private int persons;

    @SerializedName("table_id")
    private String table_id;

    @SerializedName("user_id")
    private String user_id;

    @SerializedName("device_created_at")
    private double device_created_at;

    @SerializedName("business_date")
    private String business_date;

    @SerializedName("order_type_id")
    private String order_type_id;

    @SerializedName("order_items")
    private ArrayList<OrderItemsRequest> orderItemsRequests;

    @SerializedName("order_batch_id")
    private String order_batch_id;

    @SerializedName("total_charges")
    private double total_charges;

    @SerializedName("total_taxes")
    private double total_taxes;

    @SerializedName("net_payments")
    private double net_payments = 0;

    @SerializedName("waiter_id")
    private String waiter_id;

    @SerializedName("modifiedTimestamp")
    private long modifiedTimestamp;

    @SerializedName("payment_sources")
    private ArrayList payment_source = new ArrayList();

    @SerializedName("order_tips")
    private ArrayList<OrderTip> order_tips = new ArrayList();

    public OrderRequest() {
    }

    protected OrderRequest(Parcel in) {
        id = in.readString();
        guest_name = in.readString();
        status = in.readString();
        persons = in.readInt();
        table_id = in.readString();
        user_id = in.readString();
        device_created_at = in.readDouble();
        business_date = in.readString();
        order_type_id = in.readString();
        orderItemsRequests = in.createTypedArrayList(OrderItemsRequest.CREATOR);
        order_batch_id = in.readString();
        total_charges = in.readDouble();
        total_taxes = in.readDouble();
        net_payments = in.readDouble();
        waiter_id = in.readString();
        modifiedTimestamp = in.readLong();
    }

    public static final Creator<OrderRequest> CREATOR = new Creator<OrderRequest>() {
        @Override
        public OrderRequest createFromParcel(Parcel in) {
            return new OrderRequest(in);
        }

        @Override
        public OrderRequest[] newArray(int size) {
            return new OrderRequest[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGuest_name() {
        return guest_name;
    }

    public void setGuest_name(String guest_name) {
        this.guest_name = guest_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getPersons() {
        return persons;
    }

    public void setPersons(int persons) {
        this.persons = persons;
    }

    public String getTable_id() {
        return table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public double getDevice_created_at() {
        return device_created_at;
    }

    public void setDevice_created_at(double device_created_at) {
        this.device_created_at = device_created_at;
    }

    public String getBusiness_date() {
        return business_date;
    }

    public void setBusiness_date(String business_date) {
        this.business_date = business_date;
    }

    public String getOrder_type_id() {
        return order_type_id;
    }

    public void setOrder_type_id(String order_type_id) {
        this.order_type_id = order_type_id;
    }

    public ArrayList<OrderItemsRequest> getOrderItemsRequests() {
        return orderItemsRequests;
    }

    public void setOrderItemsRequests(ArrayList<OrderItemsRequest> orderItemsRequests) {
        this.orderItemsRequests = orderItemsRequests;
    }

    public String getOrder_batch_id() {
        return order_batch_id;
    }

    public void setOrder_batch_id(String order_batch_id) {
        this.order_batch_id = order_batch_id;
    }

    public double getTotal_charges() {
        return total_charges;
    }

    public void setTotal_charges(double total_charges) {
        this.total_charges = total_charges;
    }

    public double getTotal_taxes() {
        return total_taxes;
    }

    public void setTotal_taxes(double total_taxes) {
        this.total_taxes = total_taxes;
    }

    public double getNet_payments() {
        return net_payments;
    }

    public void setNet_payments(double net_payments) {
        this.net_payments = net_payments;
    }

    public String getWaiter_id() {
        return waiter_id;
    }

    public void setWaiter_id(String waiter_id) {
        this.waiter_id = waiter_id;
    }

    public long getModifiedTimestamp() {
        return modifiedTimestamp;
    }

    public void setModifiedTimestamp(long modifiedTimestamp) {
        this.modifiedTimestamp = modifiedTimestamp;
    }

    public ArrayList getPayment_source() {
        return payment_source;
    }

    public void setPayment_source(ArrayList payment_source) {
        this.payment_source = payment_source;
    }

    public ArrayList<OrderTip> getOrder_tips() {
        return order_tips;
    }

    public void setOrder_tips(ArrayList<OrderTip> order_tips) {
        this.order_tips = order_tips;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(guest_name);
        parcel.writeString(status);
        parcel.writeInt(persons);
        parcel.writeString(table_id);
        parcel.writeString(user_id);
        parcel.writeDouble(device_created_at);
        parcel.writeString(business_date);
        parcel.writeString(order_type_id);
        parcel.writeTypedList(orderItemsRequests);
        parcel.writeString(order_batch_id);
        parcel.writeDouble(total_charges);
        parcel.writeDouble(total_taxes);
        parcel.writeDouble(net_payments);
        parcel.writeString(waiter_id);
        parcel.writeLong(modifiedTimestamp);
    }
}
