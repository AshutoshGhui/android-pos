package com.hotelkeyapp.android.pos.model;

import com.google.gson.annotations.SerializedName;
import com.hotelkeyapp.android.pos.db.orm.DiscountPeriodsDB;

/**
 * Created by ashutoshghui on 01/06/18.
 */

public class DiscountPeriods {

    @SerializedName("percentage")
    private Double percentageValue;


    @SerializedName("flat_amount")
    private Double flatAmount;

    @SerializedName("start_date")
    private String startDate;

    @SerializedName("end_date")
    private String endDate;

    @SerializedName("id")
    private String id;

    @SerializedName("discount_id")
    private String discountId;

    public Double getPercentageValue() {
        return percentageValue;
    }

    public void setPercentageValue(Double percentageValue) {
        this.percentageValue = percentageValue;
    }

    public Double getFlatAmount() {
        return flatAmount;
    }

    public void setFlatAmount(Double flatAmount) {
        this.flatAmount = flatAmount;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDiscountId() {
        return discountId;
    }

    public void setDiscountId(String discountId) {
        this.discountId = discountId;
    }


    public DiscountPeriodsDB toDiscountsPeriod(){
        DiscountPeriodsDB discountPeriodsDB =new DiscountPeriodsDB();
        discountPeriodsDB.setStart_date(this.getStartDate());
        discountPeriodsDB.setId(this.getId());
        discountPeriodsDB.setFlatAmount(this.getFlatAmount());
        discountPeriodsDB.setPercentage(this.getPercentageValue());
        return discountPeriodsDB;
    }
}
