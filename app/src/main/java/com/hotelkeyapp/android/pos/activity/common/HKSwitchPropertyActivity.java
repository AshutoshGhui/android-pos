package com.hotelkeyapp.android.pos.activity.common;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.hotelkey.hkandroidlib.HKAndroidApplication;
import com.hotelkey.hkandroidlib.activity.common.AuthenticatedActivity;
import com.hotelkey.hkandroidlib.activity.switchproperty.PropertyListAdapter;
import com.hotelkey.hkandroidlib.activity.switchproperty.SwitchPropertyController;
import com.hotelkey.hkandroidlib.db.orm.PropertyDB;
import com.hotelkey.hkandroidlib.db.orm.PropertyDBDao;
import com.hotelkey.hkandroidlib.utils.DividerItemDecoration;
import com.hotelkey.hkandroidlib.utils.ItemClickSupport;
import com.hotelkey.hkandroidlib.utils.LocalizationStringHelper;
import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.db.orm.ChargeReqDB;
import com.hotelkeyapp.android.pos.db.orm.ChargeTypesDB;
import com.hotelkeyapp.android.pos.db.orm.MenuGroupDB;
import com.hotelkeyapp.android.pos.db.orm.MenuItemDB;
import com.hotelkeyapp.android.pos.db.orm.NotificationDB;
import com.hotelkeyapp.android.pos.db.orm.OrderDB;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDB;
import com.hotelkeyapp.android.pos.db.orm.OrderTypeDB;
import com.hotelkeyapp.android.pos.db.orm.PropertyTableDB;
import com.hotelkeyapp.android.pos.db.orm.TableGroupDB;
import com.hotelkeyapp.android.pos.db.orm.TaxReqDB;
import com.hotelkeyapp.android.pos.db.orm.TaxTypePeriodsDB;
import com.hotelkeyapp.android.pos.db.orm.TaxTypesDB;
import com.hotelkeyapp.android.pos.db.orm.WaiterDB;
import com.hotelkeyapp.android.pos.utils.AccountPreferences;
import com.hotelkeyapp.android.pos.utils.LocalizationConstants;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class HKSwitchPropertyActivity extends AuthenticatedActivity implements ItemClickSupport.OnItemClickListener,
        SwitchPropertyController.SwitchPropertyControllerCallbacks, SearchView.OnQueryTextListener {

    public static final String ARG_START_HOME = "START_HOME";
    public static final String CONSOLIDATED_SALES_TITLE = "ALL PROPERTIES";
    public static final String PROPERTYS = "Property ids";

    private String okLbl;

    private List<PropertyDB> miniPropertyList;

    private RecyclerView recyclerView;

    private PropertyListAdapter propertyListAdapter;

    private SwitchPropertyController controller;

    private TextView txvTitle;

    public static Intent getLaunchIntent(Context context) {
        Intent intent = new Intent(context, HKSwitchPropertyActivity.class);
        intent.putExtra("START_HOME", true);
        return intent;
    }

    public static Intent getLaunchIntentForSwitch(Context context) {
        Intent intent = new Intent(context, HKSwitchPropertyActivity.class);
        intent.putExtra("START_HOME", false);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hkswitch_property);

        Toolbar toolbar = findViewById(R.id.toolbar_top);
        setSupportActionBar(toolbar);

        txvTitle = toolbar.findViewById(R.id.toolbar_title);
        txvTitle.setText(LocalizationConstants.getInstance().getKeyValue("title_select_property"));

        boolean isPropertySelection = getIntent().getBooleanExtra("START_HOME", false);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(!isPropertySelection);
            getSupportActionBar().setHomeButtonEnabled(!isPropertySelection);
        }

        this.controller = new SwitchPropertyController(this);
        this.init();
        this.setup();
    }

    private void init() {
        this.okLbl = LocalizationStringHelper.getInstance().fetchLocalizedKeyString("btn_ok");
        this.recyclerView = this.findViewById(com.hotelkey.hkandroidlib.R.id.switch_property_rcview);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, 1, com.hotelkey.hkandroidlib.R.drawable.divider);
        this.recyclerView.addItemDecoration(itemDecoration);
    }

    protected void setup() {
        this.miniPropertyList = this.readAllProperties();

        if (this.miniPropertyList != null && !this.miniPropertyList.isEmpty()) {
            PropertyDB miniProperty;

            if (this.miniPropertyList.size() == 1) {
                miniProperty = this.miniPropertyList.get(0);
                this.controller.switchProperty(miniProperty);
            }

            this.propertyListAdapter = new PropertyListAdapter(this.miniPropertyList);
            this.recyclerView.setAdapter(this.propertyListAdapter);
            ItemClickSupport.addTo(this.recyclerView).setOnItemClickListener(this);

        } else {
            LocalBroadcastManager.getInstance(this.getApplicationContext()).sendBroadcast(new Intent("logout-event"));
            this.logout();
            this.finish();
        }
    }

    protected List<PropertyDB> readAllProperties() {
        PropertyDBDao propertyDBDao = HKAndroidApplication.getApplication().getMasterDaoSession().getPropertyDBDao();
        List<PropertyDB> propertyDBList = propertyDBDao.loadAll();
        if (propertyDBList != null && propertyDBList.size() > 1) {

            Collections.sort(propertyDBList, new Comparator<PropertyDB>() {
                @Override
                public int compare(PropertyDB propertyDB1, PropertyDB propertyDB2) {
                    String group1 = propertyDB1.getCode();
                    String group2 = propertyDB2.getCode();
                    if ((group1 == null || group1.isEmpty())) {
                        if (group1 != null && !group1.isEmpty()) {
                            return -1;
                        } else {
                            return 0;
                        }
                    }
                    if (group2 == null || group2.isEmpty()) {
                        return 1;
                    }
                    return group1.compareToIgnoreCase(group2);
                }
            });
        }
        return propertyDBList;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = this.getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.search_view);
        SearchView mSearchView = (SearchView) searchItem.getActionView();
        SearchManager searchManager = (SearchManager) this.getSystemService(Context.SEARCH_SERVICE);
        if (mSearchView != null) {
            mSearchView.setSearchableInfo(searchManager.getSearchableInfo(this.getComponentName()));
            mSearchView.setIconifiedByDefault(true);
            mSearchView.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            mSearchView.setQueryHint(LocalizationStringHelper.getInstance().fetchLocalizedKeyString("btn_search"));
            mSearchView.setOnQueryTextListener(this);

            mSearchView.setOnSearchClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    txvTitle.setVisibility(View.GONE);
                }
            });
            mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
                @Override
                public boolean onClose() {
                    txvTitle.setVisibility(View.VISIBLE);
                    return false;
                }
            });
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void switchPropertyError(Pair<String, Boolean> message) {
        if (message.second) {
            this.showYesNoDialog(message.first, LocalizationStringHelper.getInstance().fetchLocalizedKeyString("btn_logout"),
                    LocalizationStringHelper.getInstance().fetchLocalizedKeyString("btn_dismiss_uppercase"), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (i == -1) {
                                HKSwitchPropertyActivity.this.logout();
                                HKSwitchPropertyActivity.this.finish();
                            } else {
                                dialogInterface.cancel();
                            }
                        }
                    });
        } else {
            this.showMessageDialog(message.first, this.okLbl, null);
        }
    }

    public void showLoading() {
        this.showLoadingDialog(LocalizationStringHelper.getInstance().fetchLocalizedKeyString("ldr_login_progress_title"));
    }

    public void hideLoading() {
        this.cancelLoadingDialog();
    }

    @Override
    public void switchPropertySuccess() {
        HKAndroidApplication.getApplication().setApplication();
        if (PreferenceUtils.getAppDataAtHome(this)) {
            if (getIntent().getBooleanExtra(ARG_START_HOME, false)) {
                Intent homeIntent = HKAndroidApplication.getApplication().getHomeIntent();
                startActivity(homeIntent);
            }

            POSApplication posApplication = POSApplication.getInstance();
            posApplication.getDaoSession().deleteAll(PropertyTableDB.class);
            posApplication.getDaoSession().deleteAll(OrderDB.class);
            posApplication.getDaoSession().deleteAll(TableGroupDB.class);
            posApplication.getDaoSession().deleteAll(WaiterDB.class);
            posApplication.getDaoSession().deleteAll(OrderItemsDB.class);
            posApplication.getDaoSession().deleteAll(ChargeReqDB.class);
            posApplication.getDaoSession().deleteAll(ChargeTypesDB.class);
            posApplication.getDaoSession().deleteAll(MenuItemDB.class);
            posApplication.getDaoSession().deleteAll(MenuGroupDB.class);
            posApplication.getDaoSession().deleteAll(NotificationDB.class);
            posApplication.getDaoSession().deleteAll(OrderTypeDB.class);
            posApplication.getDaoSession().deleteAll(TaxReqDB.class);
            posApplication.getDaoSession().deleteAll(TaxTypePeriodsDB.class);
            posApplication.getDaoSession().deleteAll(TaxTypesDB.class);

            AccountPreferences.clear(POSApplication.getInstance());
            PreferenceUtils.savePropLanguage(this, "");
            PreferenceUtils.savePropertyCBD(getApplicationContext(), "");
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        this.propertyListAdapter.getFilter().filter(newText);
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public void onItemClicked(RecyclerView recyclerView, int i, View view) {
        this.miniPropertyList = this.propertyListAdapter.getSelectedRoom();
        PropertyDB miniProperty = this.miniPropertyList.get(i);
        this.controller.switchProperty(miniProperty);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!getIntent().getBooleanExtra("START_HOME", false)) {
            super.onBackPressed();
            finish();
        }
    }
}