package com.hotelkeyapp.android.pos.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vrushalimankar on 30/05/18.
 */

public class Folio {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("type")
    private String type;

    @SerializedName("net_payments")
    private double net_payments;

    @SerializedName("total_taxes")
    private double total_taxes;

    @SerializedName("folio_type_id")
    private String folio_type_id;

    @SerializedName("device_created_at")
    private double device_created_at;

    @SerializedName("total_charges")
    private double total_charges;

    @SerializedName("payments")
    private ArrayList payments = new ArrayList();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getNet_payments() {
        return net_payments;
    }

    public void setNet_payments(double net_payments) {
        this.net_payments = net_payments;
    }

    public double getTotal_taxes() {
        return total_taxes;
    }

    public void setTotal_taxes(double total_taxes) {
        this.total_taxes = total_taxes;
    }

    public String getFolio_type_id() {
        return folio_type_id;
    }

    public void setFolio_type_id(String folio_type_id) {
        this.folio_type_id = folio_type_id;
    }

    public double getDevice_created_at() {
        return device_created_at;
    }

    public void setDevice_created_at(double device_created_at) {
        this.device_created_at = device_created_at;
    }

    public double getTotal_charges() {
        return total_charges;
    }

    public void setTotal_charges(double total_charges) {
        this.total_charges = total_charges;
    }

    public ArrayList getPayments() {
        return payments;
    }

    public void setPayments(ArrayList payments) {
        this.payments = payments;
    }
}
