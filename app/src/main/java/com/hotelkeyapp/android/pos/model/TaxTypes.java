package com.hotelkeyapp.android.pos.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.hotelkeyapp.android.pos.db.orm.TaxTypesDB;

import java.util.ArrayList;

/**
 * Created by vrushalimankar on 1/13/18.
 */

public class TaxTypes implements Parcelable {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("code")
    private String code;

    @SerializedName("active")
    private boolean isActive;

    @SerializedName("floating")
    private boolean floating;

    @SerializedName("parent_tax_type_id")
    private String parentTaxTypeId;

    @SerializedName("propertyId")
    private String propertyId;

    @SerializedName("tax_type_periods")
    private ArrayList<TaxTypePeriod> taxTypePeriod;

    protected TaxTypes(Parcel in) {
        id = in.readString();
        name = in.readString();
        code = in.readString();
        isActive = in.readByte() != 0;
        floating = in.readByte() != 0;
        parentTaxTypeId = in.readString();
        propertyId = in.readString();
        taxTypePeriod = in.createTypedArrayList(TaxTypePeriod.CREATOR);
    }

    public static final Creator<TaxTypes> CREATOR = new Creator<TaxTypes>() {
        @Override
        public TaxTypes createFromParcel(Parcel in) {
            return new TaxTypes(in);
        }

        @Override
        public TaxTypes[] newArray(int size) {
            return new TaxTypes[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(code);
        dest.writeByte((byte) (isActive ? 1 : 0));
        dest.writeByte((byte) (floating ? 1 : 0));
        dest.writeString(parentTaxTypeId);
        dest.writeString(propertyId);
        dest.writeTypedList(taxTypePeriod);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isFloating() {
        return floating;
    }

    public void setFloating(boolean floating) {
        this.floating = floating;
    }

    public String getParentTaxTypeId() {
        return parentTaxTypeId;
    }

    public void setParentTaxTypeId(String parentTaxTypeId) {
        this.parentTaxTypeId = parentTaxTypeId;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public ArrayList<TaxTypePeriod> getTaxTypePeriod() {
        return taxTypePeriod;
    }

    public void setTaxTypePeriod(ArrayList<TaxTypePeriod> taxTypePeriod) {
        this.taxTypePeriod = taxTypePeriod;
    }

    public TaxTypesDB toTaxTypeDB() {
        TaxTypesDB taxTypesDB =new TaxTypesDB();
        taxTypesDB.setTaxTypeId(getId());
        taxTypesDB.setCode(getCode());
        taxTypesDB.setName(getName());
        taxTypesDB.setFloating(isFloating());
        taxTypesDB.setParent_tax_type_id(getParentTaxTypeId());
        taxTypesDB.setIs_active(isActive());
        return taxTypesDB;
    }
}
