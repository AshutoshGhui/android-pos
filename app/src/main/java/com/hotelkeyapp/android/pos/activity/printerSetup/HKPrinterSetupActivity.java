package com.hotelkeyapp.android.pos.activity.printerSetup;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.epson.epos2.Epos2Exception;
import com.epson.epos2.discovery.DeviceInfo;
import com.epson.epos2.discovery.Discovery;
import com.epson.epos2.discovery.DiscoveryListener;
import com.epson.epos2.discovery.FilterOption;
import com.hotelkey.hkandroidlib.activity.common.AuthenticatedActivity;
import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.model.DeviceInfoTransient;
import com.hotelkeyapp.android.pos.model.PrinterProfile;
import com.hotelkeyapp.android.pos.printerUtils.TestConnectionAsyncTask;
import com.hotelkeyapp.android.pos.utils.AccountPreferences;
import com.hotelkeyapp.android.pos.utils.CustomBottomSheet;
import com.hotelkeyapp.android.pos.utils.LocalizationConstants;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;

public class HKPrinterSetupActivity extends AuthenticatedActivity implements
        View.OnClickListener, TextWatcher, TestConnectionAsyncTask.TestConnectionCallbacks {

    private static final String ARG_IS_NEW_PROFILE = "IS NEW PROFILE";
    private static final String ARG_PROFILE_ID = "PROFILE ID";

    private static final int RQ_SEARCH_NEARBY_PRINTERS = 5005;

    private FilterOption mFilterOption = null;

    private HashMap<String, PrinterProfile> nearbyDevices;

    private EditText edtHostIPAddress, edtProfileName;

    private PrinterProfile mPrinterProfile;

    private Button btnActions;

    private TextView txvQuantityValue;

    boolean enableSaveBtn = false;

    private ImageButton incQuantity, decQuantity;

    private int noOfPrints = 1;

    private DeviceInfo selectedDeviceInfo;

    public static Intent getInstance(Context context, boolean isNewProfile, String profileId) {
        Intent intent = new Intent(context, HKPrinterSetupActivity.class);
        intent.putExtra(ARG_IS_NEW_PROFILE, isNewProfile);
        intent.putExtra(ARG_PROFILE_ID, profileId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hkprinter_setup);

        Toolbar toolbar = findViewById(R.id.toolbar_top);
        TextView txvTitle = toolbar.findViewById(R.id.toolbar_title);
        txvTitle.setText(LocalizationConstants.getInstance().getKeyValue("title_printer_setup"));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        initViews();
    }

    private void initViews() {
        String profileId = getIntent().getStringExtra(ARG_PROFILE_ID);
        if (profileId != null && !profileId.isEmpty()) {
            ArrayList<PrinterProfile> printerProfiles = AccountPreferences.getPrinterProfiles(getApplicationContext());

            if (printerProfiles != null && !printerProfiles.isEmpty()) {
                for (PrinterProfile printerProfile : printerProfiles) {
                    if (printerProfile.getId().equals(profileId)) {
                        mPrinterProfile = printerProfile;
                        break;
                    }
                }
            }
        }

        TextView txvPrinterType = findViewById(R.id.txv_printer_type);
        txvPrinterType.setText(LocalizationConstants.getInstance().getKeyValue("lbl_printer_type"));
        TextView txvPrinterTypeName = findViewById(R.id.txv_printer_type_name);
        txvPrinterTypeName.setText("EPSON");

        TextView txvPrinterMode = findViewById(R.id.txv_print_mode);
        txvPrinterMode.setText(LocalizationConstants.getInstance().getKeyValue("lbl_printer_print_mode"));
        TextView txvPrintModeName = findViewById(R.id.txv_print_mode_name);
        txvPrintModeName.setText("KOT");

        TextView txvProfileName = findViewById(R.id.txv_profile_name);
        txvProfileName.setText(LocalizationConstants.getInstance().getKeyValue("lbl_profile_name"));

        TextView txvHostIP = findViewById(R.id.txv_host_ip);
        txvHostIP.setText(LocalizationConstants.getInstance().getKeyValue("lbl_host_ip"));

        TextView txvNoOfCopies = findViewById(R.id.txv_no_of_copies);
        txvNoOfCopies.setText(LocalizationConstants.getInstance().getKeyValue("lbl_number_of_copies"));

        txvQuantityValue = findViewById(R.id.txv_quantity_value);
        txvQuantityValue.setText("1");

        incQuantity = findViewById(R.id.btn_plus);
        incQuantity.setOnClickListener(this);

        decQuantity = findViewById(R.id.btn_minus);
        decQuantity.setOnClickListener(this);

        nearbyDevices = new HashMap<>();
        edtHostIPAddress = findViewById(R.id.edt_ip_address);
        edtHostIPAddress.setHint(LocalizationConstants.getInstance().getKeyValue("lbl_plhldr_enter"));
        edtHostIPAddress.addTextChangedListener(this);

        edtProfileName = findViewById(R.id.edt_profile_name);
        edtProfileName.setHint(LocalizationConstants.getInstance().getKeyValue("lbl_plhldr_enter"));
        edtProfileName.addTextChangedListener(this);

        btnActions = findViewById(R.id.btn_actions);
        btnActions.setText(LocalizationConstants.getInstance().getKeyValue("lbl_actions_uppercase"));
        btnActions.setOnClickListener(this);

        if (mPrinterProfile != null) {
            if (mPrinterProfile.getIpAddress() != null
                    && !mPrinterProfile.getIpAddress().isEmpty()) {
                edtHostIPAddress.setText(mPrinterProfile.getIpAddress());
            }
            edtProfileName.setText(mPrinterProfile.getProfileName());
            edtProfileName.setSelection(edtProfileName.getText().length());
            noOfPrints = mPrinterProfile.getNumberOfCopies();
            txvQuantityValue.setText(String.valueOf(mPrinterProfile.getNumberOfCopies()));
            btnActions.setEnabled(true);
            selectedDeviceInfo = mPrinterProfile.getDeviceInfo();

        } else {
            btnActions.setEnabled(false);
        }

        if (noOfPrints == 1) {
            decQuantity.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_disable));
        }
        enableSaveBtn = false;
    }

    private void discoverPrinters() {
        mFilterOption = new FilterOption();
        mFilterOption.setDeviceType(Discovery.TYPE_PRINTER);
        mFilterOption.setEpsonFilter(Discovery.FILTER_NAME);

        while (true) {
            try {
                Discovery.stop();
                break;
            } catch (Epos2Exception e) {
                if (e.getErrorStatus() != Epos2Exception.ERR_PROCESSING) {
                    e.printStackTrace();
                    break;
                }
            }
        }

        try {
            showLoadingDialog();
            Discovery.start(getApplicationContext(), mFilterOption, mDiscoveryListener);
        } catch (Epos2Exception e) {
            cancelLoadingDialog();
            e.printStackTrace();
        }
        cancelDiscovery();
    }

    private void cancelDiscovery() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                cancelLoadingDialog();
                if (nearbyDevices == null || nearbyDevices.isEmpty()) {
                    showMessageAlert(LocalizationConstants.getInstance().getKeyValue("msg_unable_to_connect_to_printer"));
                }
            }
        }, 5000);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        btnActions.setEnabled(!edtHostIPAddress.getText().toString().trim().isEmpty()
                && !edtProfileName.getText().toString().trim().isEmpty());
    }

    @Override
    public void afterTextChanged(Editable editable) {
        enableSaveBtn = true;
    }

    private boolean showVisiblePrintersScreen = false;

    private DiscoveryListener mDiscoveryListener = new DiscoveryListener() {
        @Override
        public void onDiscovery(final DeviceInfo deviceInfo) {
            runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    PrinterProfile printerProfile = new PrinterProfile(deviceInfo, false);
                    nearbyDevices.put(deviceInfo.getTarget(), printerProfile);
                    cancelLoadingDialog();

                    try {
                        if (getFragmentManager() != null) {
                            if (!showVisiblePrintersScreen) {
                                showVisiblePrintersScreen = true;
                                Intent nearbyPrintersFragment = HKNearbyPrintersActivity.newInstance(getApplicationContext(), new ArrayList<>(nearbyDevices.values()));
                                startActivityForResult(nearbyPrintersFragment, RQ_SEARCH_NEARBY_PRINTERS);

                            } else {
                                EventBus.getDefault().post(com.hotelkeyapp.android.pos.utils.ProgressEvent.getDiscoveredPrinters(new ArrayList<>(nearbyDevices.values())));
                            }
                        }
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RQ_SEARCH_NEARBY_PRINTERS) {

            if (resultCode == RESULT_OK) {
                PrinterProfile printerProfile = DeviceInfoTransient.getInstance().getDeviceInfo();

                showVisiblePrintersScreen = false;
                selectedDeviceInfo = printerProfile.getDeviceInfo();
                edtHostIPAddress.setText(printerProfile.getDeviceInfo().getIpAddress());
            }
            showVisiblePrintersScreen = false;
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_actions) {
            showActionButtons();

        } else if (view == incQuantity) {
            try {
                noOfPrints = txvQuantityValue.getText().toString().isEmpty() ? 0 : Integer.parseInt(txvQuantityValue.getText().toString());
            } catch (Exception e) {
                noOfPrints = 1;
                e.printStackTrace();
            }

            if (noOfPrints < 200) {
                noOfPrints++;
                txvQuantityValue.setCursorVisible(false);
                txvQuantityValue.setText(String.valueOf(noOfPrints));

                YoYo.with(Techniques.SlideInUp)
                        .duration(250)
                        .repeat(0)
                        .playOn(txvQuantityValue);
            }

            if (noOfPrints >= 200) {
                incQuantity.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_disable));
            } else {
                incQuantity.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_enable));
            }

            if (noOfPrints <= 1) {
                decQuantity.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_disable));
            } else {
                decQuantity.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_enable));
            }

            enableSaveBtn = true;

        } else if (view == decQuantity) {
            try {
                noOfPrints = txvQuantityValue.getText().toString().isEmpty() ? 0 : Integer.parseInt(txvQuantityValue.getText().toString());
            } catch (Exception e) {
                noOfPrints = 1;
                e.printStackTrace();
            }

            if (noOfPrints > 1) {
                noOfPrints--;
                txvQuantityValue.setCursorVisible(false);
                txvQuantityValue.setText(String.valueOf(noOfPrints));

                YoYo.with(Techniques.SlideInDown)
                        .duration(250)
                        .repeat(0)
                        .playOn(txvQuantityValue);

            }
            if (noOfPrints <= 1) {
                decQuantity.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_disable));
            } else {
                decQuantity.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_enable));
            }

            if (noOfPrints >= 200) {
                incQuantity.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_disable));
            } else {
                incQuantity.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_enable));
            }
            enableSaveBtn = true;
        }
    }

    private void showActionButtons() {

        String[] titles;

        if (mPrinterProfile == null) {
            titles = new String[]{LocalizationConstants.getInstance().getKeyValue("btn_profile_test_connection_uppercase")
                    , LocalizationConstants.getInstance().getKeyValue("btn_save_uppercase")};

        } else if (!enableSaveBtn) {
            if (mPrinterProfile.isActive()) {
                titles = new String[]{LocalizationConstants.getInstance().getKeyValue("btn_profile_test_connection_uppercase")
                        , LocalizationConstants.getInstance().getKeyValue("btn_profile_make_inactive_uppercase")
                        , LocalizationConstants.getInstance().getKeyValue("btn_profile_delete")};
            } else {
                titles = new String[]{LocalizationConstants.getInstance().getKeyValue("btn_profile_test_connection_uppercase")
                        , LocalizationConstants.getInstance().getKeyValue("btn_profile_make_active_uppercase")
                        , LocalizationConstants.getInstance().getKeyValue("btn_profile_delete")};
            }
        } else {
            titles = new String[]{LocalizationConstants.getInstance().getKeyValue("btn_profile_test_connection_uppercase")
                    , LocalizationConstants.getInstance().getKeyValue("btn_save_uppercase")
                    , LocalizationConstants.getInstance().getKeyValue("btn_profile_delete")};
        }


        final String[] finalTitles = titles;
        CustomBottomSheet.createBuilder(this, getSupportFragmentManager())
                .setCancelButtonTitle(LocalizationConstants.getInstance().getKeyValue("btn_cancel"))
                .setOtherButtonTitles(titles)
                .setCancelableOnTouchOutside(true)
                .setListener(new CustomBottomSheet.CustomBottomSheetListener() {
                    @Override
                    public void onDismiss(CustomBottomSheet actionSheet, boolean isCancel) {
                        actionSheet.dismiss();
                    }

                    @Override
                    public void onOtherButtonClick(CustomBottomSheet actionSheet, int index) {
                        if (finalTitles[index].equals(LocalizationConstants.getInstance().getKeyValue("btn_profile_test_connection_uppercase"))) {
                            testConnections();

                        } else if (finalTitles[index].equals(LocalizationConstants.getInstance().getKeyValue("btn_save_uppercase"))) {
                            if (!edtProfileName.getText().toString().trim().isEmpty()) {
                                ArrayList<PrinterProfile> deviceInfoArrayList = AccountPreferences.getPrinterProfiles(getApplicationContext());

                                String ipAddress = edtHostIPAddress.getText().toString().trim();

                                if (mPrinterProfile != null) {
                                    for (PrinterProfile printerProfile : deviceInfoArrayList) {
                                        if (printerProfile.getId().equals(mPrinterProfile.getId())) {
                                            printerProfile.setIpAddress(ipAddress);
                                            printerProfile.setNumberOfCopies(noOfPrints);
                                            printerProfile.setProfileName(edtProfileName.getText().toString().trim());
                                            break;
                                        }
                                    }

                                } else {
                                    PrinterProfile printerProfile = new PrinterProfile(edtProfileName.getText().toString().trim(), true, ipAddress, selectedDeviceInfo);
                                    printerProfile.setNumberOfCopies(noOfPrints);
                                    deviceInfoArrayList.add(printerProfile);
                                }

                                AccountPreferences.setPrinterProfiles(getApplicationContext(), deviceInfoArrayList);

                                setResult(RESULT_OK);
                                finish();
                            }

                        } else if (finalTitles[index].equals(LocalizationConstants.getInstance().getKeyValue("btn_profile_delete"))) {
                            boolean isNewProfile = getIntent().getBooleanExtra(ARG_IS_NEW_PROFILE, false);
                            final String profileId = getIntent().getStringExtra(ARG_PROFILE_ID);
                            if (!isNewProfile && profileId != null) {

                                final String message = LocalizationConstants.getInstance().getKeyValue("msg_delete_profile");
                                String yesLbl = LocalizationConstants.getInstance().getKeyValue("btn_yes");
                                String noLbl = LocalizationConstants.getInstance().getKeyValue("btn_no");

                                showYesNoDialog(message, yesLbl, noLbl, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        if (i == Dialog.BUTTON_POSITIVE) {
                                            AccountPreferences.deletePrinterProfile(getApplicationContext(), profileId);
                                            setResult(RESULT_OK);
                                            finish();
                                            Toast.makeText(getApplicationContext(), LocalizationConstants.getInstance().getKeyValue("msg_profile_deleted"), Toast.LENGTH_SHORT).show();
                                        }
                                        dialogInterface.dismiss();
                                    }
                                });


                            }

                        } else if (finalTitles[index].equals(LocalizationConstants.getInstance().getKeyValue("btn_profile_make_inactive_uppercase"))) {
                            mPrinterProfile.setActive(false);
                            ArrayList<PrinterProfile> deviceInfoArrayList = AccountPreferences.getPrinterProfiles(getApplicationContext());
                            for (int i = 0; i < deviceInfoArrayList.size(); i++) {
                                PrinterProfile printerProfile = deviceInfoArrayList.get(i);
                                if (printerProfile.getId().equals(mPrinterProfile.getId())) {
                                    deviceInfoArrayList.set(i, mPrinterProfile);
                                    break;
                                }
                            }
                            AccountPreferences.setPrinterProfiles(getApplicationContext(), deviceInfoArrayList);

                            setResult(RESULT_OK);
                            finish();

                        } else if (finalTitles[index].equals(LocalizationConstants.getInstance().getKeyValue("btn_profile_make_active_uppercase"))) {
                            mPrinterProfile.setActive(true);
                            ArrayList<PrinterProfile> deviceInfoArrayList = AccountPreferences.getPrinterProfiles(getApplicationContext());
                            for (int i = 0; i < deviceInfoArrayList.size(); i++) {
                                PrinterProfile printerProfile = deviceInfoArrayList.get(i);
                                if (printerProfile.getId().equals(mPrinterProfile.getId())) {
                                    deviceInfoArrayList.set(i, mPrinterProfile);
                                    break;
                                }
                            }
                            AccountPreferences.setPrinterProfiles(getApplicationContext(), deviceInfoArrayList);

                            setResult(RESULT_OK);
                            finish();
                        }
                    }
                }).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_printer, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (menu != null) {
            MenuItem menuItem = menu.findItem(R.id.btn_search);
            menuItem.setTitle(LocalizationConstants.getInstance().getKeyValue("btn_search"));
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.btn_search) {
            discoverPrinters();

        } else if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void testConnections() {
        showLoadingDialog();
        TestConnectionAsyncTask testConnectionAsyncTask = new TestConnectionAsyncTask(getApplicationContext(), edtHostIPAddress.getText().toString().trim(), this);
        testConnectionAsyncTask.execute();
    }

    @Override
    public void connectionCallbacks(String message) {
        cancelLoadingDialog();
        if (message == null) {
            Toast.makeText(getApplicationContext(), LocalizationConstants.getInstance().getKeyValue("toast_profile_setup"), Toast.LENGTH_SHORT).show();
        } else {
            showMessageAlert(message);
        }
    }

    private void showMessageAlert(String message) {
        showMessageDialog(message, LocalizationConstants.getInstance().getKeyValue("btn_ok"), null);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}
