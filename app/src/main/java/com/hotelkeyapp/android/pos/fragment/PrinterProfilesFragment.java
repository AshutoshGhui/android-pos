package com.hotelkeyapp.android.pos.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hotelkey.hkandroidlib.common.BaseFragment;
import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.activity.printerSetup.HKPrinterSetupActivity;
import com.hotelkeyapp.android.pos.model.PrinterProfile;
import com.hotelkeyapp.android.pos.utils.AccountPreferences;
import com.hotelkeyapp.android.pos.utils.LocalizationConstants;

import java.util.ArrayList;

/**
 * Created by vrushalimankar on 01/10/18.
 */

public class PrinterProfilesFragment extends BaseFragment implements View.OnClickListener {

    private static final int RQ_ADD_PROFILE = 4005;

    private Context mContext;

    private LinearLayout llProfileParentLayout;

    public static PrinterProfilesFragment newInstance() {
        return new PrinterProfilesFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_printer_profiles, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        initViews(view);
    }

    private void initViews(View view) {
        TextView txvAddProfile = view.findViewById(R.id.txv_add_profile);
        txvAddProfile.setText(LocalizationConstants.getInstance().getKeyValue("btn_profile_add"));
        txvAddProfile.setOnClickListener(this);

        llProfileParentLayout = view.findViewById(R.id.ll_set_profiles);

        setProfileLayouts();
    }

    private void setProfileLayouts() {
        llProfileParentLayout.removeAllViews();
        final ArrayList<PrinterProfile> deviceInfoArrayList = AccountPreferences.getPrinterProfiles(mContext);
        if (deviceInfoArrayList.isEmpty()) {
            llProfileParentLayout.setVisibility(View.GONE);

        } else {
            llProfileParentLayout.setVisibility(View.VISIBLE);

            for (int i = 0; i < deviceInfoArrayList.size(); i++) {
                final PrinterProfile printerProfile = deviceInfoArrayList.get(i);

                if (printerProfile == null) {
                    continue;
                }

                View convertView = getLayoutInflater().inflate(R.layout.layout_printer_profiles, null);

                TextView txvPrinterProfileName = convertView.findViewById(R.id.txv_printer_name);
                txvPrinterProfileName.setText(printerProfile.getProfileName());

                TextView txvPrinterProfileStatus = convertView.findViewById(R.id.txv_status);
                if (printerProfile.isActive()) {
                    txvPrinterProfileStatus.setText(LocalizationConstants.getInstance().getKeyValue("lbl_active"));
                } else {
                    txvPrinterProfileStatus.setText(LocalizationConstants.getInstance().getKeyValue("lbl_inactive"));
                }

                View separator = convertView.findViewById(R.id.separator);

                if (i != deviceInfoArrayList.size() - 1) {
                    separator.setVisibility(View.VISIBLE);
                } else {
                    separator.setVisibility(View.INVISIBLE);
                }

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ArrayList<PrinterProfile> deviceInfoArrayList = AccountPreferences.getPrinterProfiles(mContext);
                        for (int i = 0; i < deviceInfoArrayList.size(); i++) {
                            PrinterProfile printerProfile = deviceInfoArrayList.get(i);

                            if (view.getTag().equals(deviceInfoArrayList.get(i).getId())) {
                                Intent intent = HKPrinterSetupActivity.getInstance(getActivity(), false, printerProfile.getId());
                                startActivityForResult(intent, RQ_ADD_PROFILE);
                                break;
                            }
                        }
                    }
                });

                convertView.setTag(printerProfile.getId());
                llProfileParentLayout.addView(convertView);

            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.txv_add_profile) {
            Intent intent = HKPrinterSetupActivity.getInstance(getActivity(), true, null);
            startActivityForResult(intent, RQ_ADD_PROFILE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RQ_ADD_PROFILE && resultCode == Activity.RESULT_OK) {
            setProfileLayouts();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }
}
