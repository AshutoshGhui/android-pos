package com.hotelkeyapp.android.pos.model.order;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vrushalimankar on 10/05/18.
 */

public class MeneItem{

    @SerializedName("menu_id")
    private String menu_id;

    @SerializedName("name")
    private String name;

    @SerializedName("price")
    private double price;

    @SerializedName("charge_type_id")
    private String charge_type_id;
}
