package com.hotelkeyapp.android.pos.utils;

import com.hotelkey.hkandroidlib.db.orm.DaoSession;
import com.hotelkey.hkandroidlib.db.orm.PermissionDB;
import com.hotelkey.hkandroidlib.db.orm.PermissionDBDao;
import com.hotelkeyapp.android.pos.POSApplication;

import java.util.HashMap;
import java.util.List;

/**
 * Created by vrushalimankar on 06/06/18.
 */

public class PermissionHelper {

    public static HashMap<String, String> getUserPermission() {
        HashMap<String, String> permissions = new HashMap<>();
        DaoSession materDaoSession = POSApplication.getInstance().getMasterDaoSession();
        PermissionDBDao permissionDBDao = materDaoSession.getPermissionDBDao();
        List<PermissionDB> list = permissionDBDao.loadAll();

        for (PermissionDB permissionDB : list) {
            if (permissionDB.getModule().equals(Constants.PERMISSION_MODULE_POS) && permissionDB.getType().equals(Constants.PERMISSION_DELETE_ORDER)) {
                permissions.put(Constants.PERMISSION_DELETE_ORDER, permissionDB.getAccess());
                continue;
            }

            if (permissionDB.getModule().equals(Constants.PERMISSION_MODULE_POS) && permissionDB.getType().equals(Constants.PERMISSION_APPLY_DISCOUNT)) {
                permissions.put(Constants.PERMISSION_APPLY_DISCOUNT, permissionDB.getAccess());
                continue;
            }

            if (permissionDB.getModule().equals(Constants.PERMISSION_MODULE_POS) && permissionDB.getType().equals(Constants.PERMISSION_CUSTOM_ORDER)){
                permissions.put(Constants.PERMISSION_CUSTOM_ORDER, permissionDB.getAccess());
                continue;
            }

            if (permissionDB.getModule().equals(Constants.PERMISSION_MODULE_POS) && permissionDB.getType().equals(Constants.PERMISSION_ORDER_MODULE)){
                permissions.put(Constants.PERMISSION_ORDER_MODULE, permissionDB.getAccess());
                continue;
            }

            if (permissions.size() == 4) {
                break;
            }
        }
        return permissions;
    }

}
