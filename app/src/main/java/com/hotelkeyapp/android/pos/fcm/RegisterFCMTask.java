package com.hotelkeyapp.android.pos.fcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.hotelkey.hkandroidlib.utils.PreferenceUtils;

public class RegisterFCMTask extends AsyncTask<Void, Void, Boolean> {
    private static final String TAG = RegisterFCMTask.class.toString();

    private Context context;
    private RegisterFCMTask.RegisterFCMTaskListener listener;

    public RegisterFCMTask(Context context, RegisterFCMTask.RegisterFCMTaskListener listener) {
        this.context = context;
        this.listener = listener;
    }

    protected Boolean doInBackground(Void... params) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.context);

        try {

            String token;

            int count = 15;
            do {
                //There will be some delay hence we have to add loop`
                token = FirebaseInstanceId.getInstance().getToken();
                threadSleepMode();
                count++;
            } while (token == null && count != 0);

            if (count == 0) {
                return false;
            }
            PreferenceUtils.saveToken(context, token);

//            BackupManager backUpAgent = new BackupManager(this.context);
//            backUpAgent.dataChanged();

            Log.d("Push", "Token:" + token);
            sharedPreferences.edit().putBoolean("sentTokenToServer", true).apply();
            return true;
        } catch (Exception var6) {
            Log.d(TAG, "Failed to complete token refresh", var6);
            sharedPreferences.edit().putBoolean("sentTokenToServer", false).apply();
            return false;
        }
    }

    private void threadSleepMode() {
        try {
            Thread.sleep(2000);
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage(), ex);
        }
    }

    protected void onPostExecute(Boolean result) {
        if (result) {
            this.listener.onPushNotificationRegisterSuccessful();
        } else {
            this.listener.onPushNotificationRegisterFailed();
        }
    }

    public interface RegisterFCMTaskListener {
        void onPushNotificationRegisterSuccessful();

        void onPushNotificationRegisterFailed();
    }
}
