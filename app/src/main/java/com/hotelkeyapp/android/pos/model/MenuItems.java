package com.hotelkeyapp.android.pos.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.hotelkeyapp.android.pos.db.orm.MenuItemDB;

/**
 * Created by vrushalimankar on 10/05/18.
 */

public class MenuItems implements Parcelable{

    @SerializedName("id")
    private String id;

    @SerializedName("menu_id")
    private String menu_id;

    @SerializedName("name")
    private String name;

    @SerializedName("code")
    private String code;

    @SerializedName("description")
    private String description;

    @SerializedName("image")
    private String image;

    @SerializedName("price")
    private double price;

    @SerializedName("active")
    private boolean active;

    @SerializedName("charge_type_id")
    private String charge_type_id;

    @SerializedName("product_id")
    private String product_id;

    @SerializedName("inventory_system_id")
    private String inventory_system_id;

    public MenuItems() {
    }

    protected MenuItems(Parcel in) {
        id = in.readString();
        menu_id = in.readString();
        name = in.readString();
        code = in.readString();
        description = in.readString();
        image = in.readString();
        price = in.readDouble();
        active = in.readByte() != 0;
        charge_type_id = in.readString();
        product_id = in.readString();
        inventory_system_id =in.readString();
    }

    public static final Creator<MenuItems> CREATOR = new Creator<MenuItems>() {
        @Override
        public MenuItems createFromParcel(Parcel in) {
            return new MenuItems(in);
        }

        @Override
        public MenuItems[] newArray(int size) {
            return new MenuItems[size];
        }
    };

    public MenuItemDB toMenuItemDB() {
        MenuItemDB menuItemDB = new MenuItemDB();
        menuItemDB.setId(id);
        menuItemDB.setName(name);
        menuItemDB.setCode(code);
        menuItemDB.setDescription(description);
        menuItemDB.setImage(image);
        menuItemDB.setPrice(price);
        menuItemDB.setActive(active);
        menuItemDB.setCharge_type_id(charge_type_id);
        menuItemDB.setProduct_id(product_id);
        menuItemDB.setInventory_system_id(inventory_system_id);
        return menuItemDB;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCharge_type_id() {
        return charge_type_id;
    }

    public void setCharge_type_id(String charge_type_id) {
        this.charge_type_id = charge_type_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getInventory_system_id() {
        return inventory_system_id;
    }

    public void setInventory_system_id(String inventory_system_id) {
        this.inventory_system_id = inventory_system_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(menu_id);
        parcel.writeString(name);
        parcel.writeString(code);
        parcel.writeString(description);
        parcel.writeString(image);
        parcel.writeDouble(price);
        parcel.writeByte((byte) (active ? 1 : 0));
        parcel.writeString(charge_type_id);
        parcel.writeString(product_id);
        parcel.writeString(inventory_system_id);
    }
}
