package com.hotelkeyapp.android.pos.model;

import com.google.gson.annotations.SerializedName;
import com.hotelkeyapp.android.pos.db.orm.NotificationDB;

/**
 * Created by vrushalimankar on 23/05/18.
 */

public class Notifications {

    @SerializedName("order_id")
    private String order_id;

    @SerializedName("order_item_id")
    private String order_item_id;

    @SerializedName("user_id")
    private String user_id;

    @SerializedName("status")
    private String status;

    @SerializedName("table_id")
    private String table_id;

    @SerializedName("id")
    private String id;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("updated_at")
    private String updated_at;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_item_id() {
        return order_item_id;
    }

    public void setOrder_item_id(String order_item_id) {
        this.order_item_id = order_item_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTable_id() {
        return table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public NotificationDB toNotificationDB() {
        NotificationDB notificationDB = new NotificationDB();
        notificationDB.setId(id);
        notificationDB.setOrder_id(order_id);
        notificationDB.setOrder_item_id(order_item_id);
        notificationDB.setUser_id(user_id);
        notificationDB.setStatus(status);
        notificationDB.setCreated_at(created_at);
        notificationDB.setTable_id(table_id);
        notificationDB.setUpdated_at(updated_at);
        return notificationDB;
    }
}
