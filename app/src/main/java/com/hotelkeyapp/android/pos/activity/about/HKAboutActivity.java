package com.hotelkeyapp.android.pos.activity.about;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.hotelkey.hkandroidlib.utils.LocalizationStringHelper;
import com.hotelkeyapp.android.pos.BuildConfig;
import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.utils.LocalizationConstants;


public class HKAboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        Toolbar toolbar = findViewById(R.id.toolbar_top);
        TextView txvTitle = toolbar.findViewById(R.id.toolbar_title);
        txvTitle.setText(LocalizationConstants.getInstance().getKeyValue("btn_about"));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        initViews();
    }

    private void initViews() {
        TextView txtVersionName = findViewById(R.id.txt_about_versionName);
        TextView txtAboutMessage = findViewById(R.id.txv_about_message);
        txtAboutMessage.setText("HotelKey Order Taking App");

        String version = LocalizationStringHelper.getInstance().fetchLocalizedKeyString("lbl_version")
                + " " + BuildConfig.VERSION_NAME + " (" + BuildConfig.VERSION_CODE + ")";

        txtVersionName.setText(version);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
