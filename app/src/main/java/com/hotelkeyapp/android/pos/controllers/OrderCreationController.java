package com.hotelkeyapp.android.pos.controllers;

import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;

import com.hotelkey.hkandroidlib.exceptions.AuthenticationFailedException;
import com.hotelkey.hkandroidlib.exceptions.ServerError;
import com.hotelkey.hkandroidlib.utils.ExceptionHelper;
import com.hotelkey.hkandroidlib.utils.MessageHelper;
import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.db.orm.AdjustmentTaxDB;
import com.hotelkeyapp.android.pos.db.orm.AdjustmentsDB;
import com.hotelkeyapp.android.pos.db.orm.AdjustmentsDBDao;
import com.hotelkeyapp.android.pos.db.orm.ChargeReqDB;
import com.hotelkeyapp.android.pos.db.orm.DaoSession;
import com.hotelkeyapp.android.pos.db.orm.FolioReqDB;
import com.hotelkeyapp.android.pos.db.orm.MenuItemDB;
import com.hotelkeyapp.android.pos.db.orm.MenuItemDBDao;
import com.hotelkeyapp.android.pos.db.orm.OrderDB;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDB;
import com.hotelkeyapp.android.pos.db.orm.OrderTypeDB;
import com.hotelkeyapp.android.pos.db.orm.OrderTypeDBDao;
import com.hotelkeyapp.android.pos.db.orm.PropertyTableDB;
import com.hotelkeyapp.android.pos.db.orm.PropertyTableDBDao;
import com.hotelkeyapp.android.pos.db.orm.TaxReqDB;
import com.hotelkeyapp.android.pos.model.OrderTip;
import com.hotelkeyapp.android.pos.model.order.AuditLogs;
import com.hotelkeyapp.android.pos.utils.AccountPreferences;
import com.hotelkeyapp.android.pos.utils.Constants;
import com.hotelkeyapp.android.pos.utils.FormatUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.UUID;

import de.greenrobot.dao.query.Query;
import de.greenrobot.dao.query.QueryBuilder;
import retrofit2.Call;
import retrofit2.Response;

import static com.hotelkeyapp.android.pos.utils.Constants.CHARGE_STATUS_CONFIRMED;
import static com.hotelkeyapp.android.pos.utils.Constants.ORDER_STATUS_BILL_ISSUED;
import static com.hotelkeyapp.android.pos.utils.Constants.ORDER_STATUS_OPEN;

/**
 * Created by vrushalimankar on 18/05/18.
 */

public class OrderCreationController extends AsyncTask<OrderDB, Void, Pair<String, Boolean>> {


    private OrderDB orderDB;

    public interface OrderCreationCallbacks {
        void onFailure(Pair<String, Boolean> message);

        void onSuccess(boolean id, OrderDB orderDB, ArrayList<String> newOrderItemIds);
    }

    private boolean isNewOrder = false;

    private OrderCreationCallbacks creationCallbacks;

    private ArrayList<String> newOrderItemIds;

    public OrderCreationController(OrderCreationCallbacks creationCallbacks, ArrayList<String> newOrderItemIds) {
        this.newOrderItemIds = newOrderItemIds;
        this.creationCallbacks = creationCallbacks;
    }

    @Override
    protected Pair<String, Boolean> doInBackground(OrderDB... orderDBS) {

        ArrayList<AuditLogs> newAuditLogs = new ArrayList<>();

        OrderDB orderDB = orderDBS[0];

        if (orderDB != null && orderDB.getOrderItemsDBS() != null) {
            if (newOrderItemIds != null && !newOrderItemIds.isEmpty()) {
                ArrayList<OrderItemsDB> orderItemsDBS = orderDB.getOrderItemsDBS();
                for (OrderItemsDB orderItemsDB : orderItemsDBS) {
                    if (newOrderItemIds.contains(orderItemsDB.getId())) {
                        AuditLogs newLog = orderItemsDB.createLog();
                        newAuditLogs.add(newLog);
                    }
                }
                ArrayList<AuditLogs> auditLogs = orderDB.getAuditLogs();
                if (auditLogs == null) {
                    auditLogs = new ArrayList<>();
                }
                auditLogs.addAll(newAuditLogs);
                orderDB.setAuditLogs(auditLogs);
            }
        }

        return createOrder(orderDB);
    }

    @Override
    protected void onPostExecute(Pair<String, Boolean> message) {
        super.onPostExecute(message);
        if (message == null) {
            creationCallbacks.onSuccess(isNewOrder, orderDB, newOrderItemIds);
        } else {
            creationCallbacks.onFailure(message);
        }
    }

    private Pair<String, Boolean> createOrder(OrderDB newOrder) {
        POSApplication posApplication = POSApplication.getInstance();
        DaoSession daoSession = posApplication.getDaoSession();

        if (newOrder.getOrderItemsDBS() == null) {
            Log.d(OrderCreationController.class.getCanonicalName(), "Order items are null");
            return new Pair<>("", false);
        }
        ArrayList<OrderItemsDB> orderItemsRequests = newOrder.getOrderItemsDBS();

        if (newOrder.getDevice_created_at() == null || newOrder.getDevice_created_at() == 0) {
            TimeZone timeZone = TimeZone.getTimeZone(PreferenceUtils.getTimeZoneOffset(POSApplication.getInstance()));
            Calendar calendar = Calendar.getInstance(timeZone);
            long timeStamp = calendar.getTimeInMillis() / 1000L;

            Double deviceCreatedAt = new BigDecimal(timeStamp).doubleValue();

            newOrder.setDevice_created_at(deviceCreatedAt);
        }

        double totalCharge = 0, totalTax = 0;

        HashMap<String, FolioReqDB> mainFolioMap = new HashMap<>();
        if (newOrder.getFolioReqDBS() != null && !newOrder.getFolioReqDBS().isEmpty()) {
            for (FolioReqDB reqDB : newOrder.getFolioReqDBS()) {
                mainFolioMap.put(reqDB.getFolio_type_id(), reqDB);
            }
        }

        HashMap<String, FolioReqDB> folioMap = new HashMap<>();

        for (OrderItemsDB orderItem : orderItemsRequests) {

            Query<MenuItemDB> qb = daoSession.getMenuItemDBDao().queryBuilder()
                    .where(MenuItemDBDao.Properties.Id.eq(orderItem.getMenu_item_id())).build();
            if (qb != null && qb.list() != null && !qb.list().isEmpty()) {
                MenuItemDB menuItemDB = qb.list().get(0);
                if (menuItemDB != null) {
                    if (!menuItemDB.getIsCustom() || menuItemDB.getMenuGroupID() != null) {
                        orderItem.setMenuItemDB(null);
                    }
                }
            }

            FolioReqDB folioReqDB = orderItem.getLocalFolioReqDB(orderItem.getFolio_id());
            if (folioReqDB == null) {
                Log.d(OrderCreationController.class.getCanonicalName(), "Folio not present in DB");
                return new Pair<>("", false);
            }

            folioReqDB.setOrderId(newOrder.getId());

            if (orderItem.getChargeReqDB() != null) {
                double aTotalFolioCharge = 0, aTotalFolioTax = 0;

                ChargeReqDB charge = orderItem.getChargeReqDB();
                charge.setFolio_id(orderItem.getFolio_id());

                if (charge.getAdjustments() == null || charge.getAdjustments().isEmpty()) {
                    Query<AdjustmentsDB> adjustmentsDBQuery = daoSession.getAdjustmentsDBDao().queryBuilder()
                            .where(AdjustmentsDBDao.Properties.Charge_id.eq(charge.getId())).build();

                    if (adjustmentsDBQuery != null && adjustmentsDBQuery.list() != null && !adjustmentsDBQuery.list().isEmpty()) {
                        ArrayList<AdjustmentsDB> adjustmentsDBS = (ArrayList<AdjustmentsDB>) adjustmentsDBQuery.list();
                        for (AdjustmentsDB adjustmentsDB : adjustmentsDBS) {
                            adjustmentsDB.setLocalTaxes((ArrayList<AdjustmentTaxDB>) adjustmentsDB.getAdjustmentTaxes());
                        }
                        charge.setAdjustments(adjustmentsDBS);
                    } else {
                        charge.setAdjustments(new ArrayList<AdjustmentsDB>());
                    }
                }

                double totalAdjustedCharge = 0, totalAdjustedTax = 0;
                if (charge.getAdjustments() != null && !charge.getAdjustments().isEmpty()) {
                    for (AdjustmentsDB adjustmentsDB : charge.getAdjustments()) {
                        if (adjustmentsDB.getStatus().equals(Constants.CHARGE_STATUS_CONFIRMED)) {
                            totalAdjustedCharge = totalAdjustedCharge + adjustmentsDB.getAmount();
                            if (adjustmentsDB.getLocalTaxes() != null && !adjustmentsDB.getLocalTaxes().isEmpty()) {
                                for (AdjustmentTaxDB aTax : adjustmentsDB.getLocalTaxes()) {
                                    if (aTax.getAmount() == null) {
                                        continue;
                                    }
                                    totalAdjustedTax = totalAdjustedTax + aTax.getAmount();
                                }
                            }
                        }
                    }
                }

                charge.setQuantity(1);
                if (charge.getStatus() == null || charge.getStatus().isEmpty()) {
                    charge.setStatus(CHARGE_STATUS_CONFIRMED);
                }
                charge.setOriginal_amount(0.0);
                charge.setChildren(0);
                charge.setPosted_date(FormatUtils.formatRequestDate(PreferenceUtils.getCBD(POSApplication.getInstance())));
                charge.setPosted_business_day_id(AccountPreferences.getBusinessDayID(POSApplication.getInstance()));

                if (!charge.getStatus().equals(Constants.CHARGE_STATUS_CANCELLED)) {
                    totalCharge = totalCharge + charge.getAmount() + totalAdjustedCharge;
                    aTotalFolioCharge = charge.getAmount() + totalAdjustedCharge;
                }

                if (charge.getLocalTaxes() != null && !charge.getLocalTaxes().isEmpty()) {
                    double aTotalTax = 0;
                    for (TaxReqDB aTax : charge.getLocalTaxes()) {
                        aTotalTax = aTotalTax + aTax.getAmount();
                    }
                    charge.setLocalTaxes(charge.getLocalTaxes());
                    if (!charge.getStatus().equals(Constants.CHARGE_STATUS_CANCELLED)) {
                        totalTax = totalTax + aTotalTax + totalAdjustedTax;
                        aTotalFolioTax = aTotalTax + totalAdjustedTax;
                    }
                }

                if (folioMap.containsKey(folioReqDB.getFolio_type_id())) {
                    FolioReqDB newlyAddedFolio = folioMap.get(folioReqDB.getFolio_type_id());
                    if (mainFolioMap.containsKey(folioReqDB.getFolio_type_id())) {
                        FolioReqDB existingFolio = mainFolioMap.get(folioReqDB.getFolio_type_id());
                        folioReqDB.setId(existingFolio.getId());
                    } else {
                        folioReqDB.setId(newlyAddedFolio.getId());
                    }

                    double newFolioCharge = newlyAddedFolio.getTotal_charges() + aTotalFolioCharge;
                    double newFolioTax = newlyAddedFolio.getTotal_taxes() + aTotalFolioTax;

                    folioReqDB.setTotal_charges(newFolioCharge);
                    folioReqDB.setTotal_taxes(newFolioTax);
                    folioMap.put(folioReqDB.getFolio_type_id(), folioReqDB);

                } else {
                    if (mainFolioMap.containsKey(folioReqDB.getFolio_type_id())) {
                        FolioReqDB existingFolio = mainFolioMap.get(folioReqDB.getFolio_type_id());
                        folioReqDB.setId(existingFolio.getId());
                    } else {
                        folioReqDB.setId(UUID.randomUUID().toString());
                    }
                    folioReqDB.setTotal_charges(aTotalFolioCharge);
                    folioReqDB.setTotal_taxes(aTotalFolioTax);
                    folioMap.put(folioReqDB.getFolio_type_id(), folioReqDB);
                }

                if (folioReqDB.getDevice_created_at() == null) {
                    folioReqDB.setDevice_created_at(newOrder.getDevice_created_at());
                }

                orderItem.setFolio_id(folioReqDB.getId());
                charge.setFolio_id(orderItem.getFolio_id());
                orderItem.setChargeReqDB(charge);

            }
        }

        newOrder.setFolioReqDBS(new ArrayList<>(folioMap.values()));
        newOrder.setOrderItemsDBS(orderItemsRequests);

        newOrder.setOrder_batch_id(AccountPreferences.getOrderBatchId(posApplication));
        newOrder.setTotal_charges(totalCharge);
        newOrder.setTotal_taxes(totalTax);
        newOrder.setNet_payments(0.0);
        newOrder.setOrder_tips(new ArrayList<OrderTip>());
        newOrder.setBusiness_date(FormatUtils.formatRequestDate(PreferenceUtils.getCBD(posApplication)));

        Date date = PreferenceUtils.getCBD(posApplication);
        newOrder.setModifiedTimestamp(date.getTime());

        if (newOrder.getGuest_name().isEmpty()) {
            newOrder.setGuest_name("Customer");
        }

        QueryBuilder<OrderTypeDB> queryBuilder = daoSession.getOrderTypeDBDao().queryBuilder()
                .where(OrderTypeDBDao.Properties.Default_type.eq("true"));

        if (queryBuilder != null && queryBuilder.list() != null && !queryBuilder.list().isEmpty()) {
            OrderTypeDB orderTypeDB = queryBuilder.list().get(0);
            newOrder.setOrder_type_id(orderTypeDB.getId());
        }

        String propertyId = PreferenceUtils.getCurrentProperty(posApplication).getId();
        Call<OrderDB> property = POSApplication.getInstance().getNetworkService()
                .createOrder(PreferenceUtils.getEntIdentifier(posApplication), propertyId, newOrder);
        try {
            Response<OrderDB> orderDBResponse = property.execute();

            if (orderDBResponse.isSuccessful()) {
                OrderDB orderDB = orderDBResponse.body();
                OrderDB dbObj = orderDB.getOrderDBObject(orderDB.getId());
                if (dbObj == null) {
                    isNewOrder = true;
                }
                orderDB.setServer_guest_name(orderDB.getGuest_name());
                orderDB.setServer_persons(orderDB.getPersons());
                orderDB.setServer_waiter_id(orderDB.getWaiter_id());

                orderDB.setPersons(orderDB.getPersons());
                orderDB.setTableId(orderDB.getTableId());
                orderDB.setOrderItemsDBS(orderDB.getOrderItemsDBS());

                if (orderDB.getFolioReqDBS() != null && !orderDB.getFolioReqDBS().isEmpty()) {
                    for (FolioReqDB folioReqDB : orderDB.getFolioReqDBS()) {
                        folioReqDB.setOrderId(orderDB.getId());
                        daoSession.insertOrReplace(folioReqDB);
                    }
                }

                if (orderDB.getOrderItemsDBS() != null && !orderDB.getOrderItemsDBS().isEmpty()) {

                    for (OrderItemsDB itemsDB : orderDB.getOrderItemsDBS()) {
                        ChargeReqDB chargeReqDB = itemsDB.getChargeReqDB();

                        if (chargeReqDB != null && chargeReqDB.getLocalTaxes() != null) {
                            for (TaxReqDB aTaxReqDB : chargeReqDB.getLocalTaxes()) {
                                aTaxReqDB.setChargeId(chargeReqDB.getId());
                                daoSession.insertOrReplace(aTaxReqDB);
                            }

                            if (chargeReqDB.getAdjustments() != null && !chargeReqDB.getAdjustments().isEmpty()) {
                                for (AdjustmentsDB adjustmentsDB : chargeReqDB.getAdjustments()) {
                                    adjustmentsDB.setCharge_id(chargeReqDB.getId());
                                    if (adjustmentsDB.getLocalTaxes() != null && !adjustmentsDB.getLocalTaxes().isEmpty()) {
                                        for (AdjustmentTaxDB adjustmentTaxDB : adjustmentsDB.getLocalTaxes()) {
                                            adjustmentTaxDB.setAdjustment_id(adjustmentsDB.getId());
                                            daoSession.insertOrReplace(adjustmentTaxDB);
                                        }
                                    }
                                    daoSession.insertOrReplace(adjustmentsDB);
                                }
                            }

                            chargeReqDB.setFolio_id(itemsDB.getFolio_id());
                            daoSession.insertOrReplace(chargeReqDB);
                        }

                        if (itemsDB.getMenu_item_id() != null && itemsDB.getMenuItemDB() != null) {
                            QueryBuilder<MenuItemDB> queryBuilder1 = daoSession.getMenuItemDBDao().queryBuilder()
                                    .where(MenuItemDBDao.Properties.Id.eq(itemsDB.getMenu_item_id()));
                            if (queryBuilder1 == null || queryBuilder1.list() == null || queryBuilder1.list().isEmpty()) {
                                MenuItemDB menuItemDB = itemsDB.getMenuItemDB();
                                menuItemDB.setIsCustom(true);
                                daoSession.insertOrReplace(menuItemDB);
                            }
                        }

                        itemsDB.setFolio_id(itemsDB.getFolio_id());
                        itemsDB.setOrderId(orderDB.getId());
                        daoSession.insertOrReplace(itemsDB);
                    }

                }
                daoSession.insertOrReplace(orderDB);


                //Save Table with updated status
                Query<PropertyTableDB> qb1 = daoSession.getPropertyTableDBDao().queryBuilder()
                        .where(PropertyTableDBDao.Properties.Id.eq(orderDB.getTableId())).build();

                if (qb1 != null && qb1.list() != null && !qb1.list().isEmpty()) {
                    PropertyTableDB propertyTableDB = qb1.list().get(0);
                    if (orderDB.getStatus().equals(ORDER_STATUS_OPEN)) {
                        propertyTableDB.setTable_status(Constants.TABLE_STATUS_RESERVED);

                    } else if (orderDB.getStatus().equals(ORDER_STATUS_BILL_ISSUED)) {
                        propertyTableDB.setTable_status(Constants.TABLE_STATUS_BILL_ISSUED);
                    }
                    daoSession.update(propertyTableDB);
                }
                this.orderDB = orderDB;

                return null;

            } else if (orderDBResponse.code() == 400) {
                JSONObject jsonObject = new JSONObject(orderDBResponse.errorBody().string());
                return new Pair<>(jsonObject.getString("message"), false);

            } else {
                ServerError exception = ExceptionHelper.parse(orderDBResponse);
                return MessageHelper.getMessageForError(posApplication, exception);
            }

        } catch (IOException | AuthenticationFailedException e) {
            e.printStackTrace();
            return MessageHelper.getMessageForError(posApplication, e);

        } catch (JSONException e) {
            e.printStackTrace();
            return new Pair<>("Bad Request", false);
        }
    }
}
