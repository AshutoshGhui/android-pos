package com.hotelkeyapp.android.pos.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDB;
import com.hotelkeyapp.android.pos.model.DiscountsModel;
import com.hotelkeyapp.android.pos.utils.FormatUtils;
import com.hotelkeyapp.android.pos.utils.LocalizationConstants;

import java.util.ArrayList;

/**
 * Created by vrushalimankar on 26/06/18.
 */

public class CustomDiscountFragment extends DialogFragment implements View.OnClickListener,
        Switch.OnCheckedChangeListener, TextWatcher {

    private static final String ARG_ORDER_ITEMS = "Order items";
    private static final String ARG_DISCOUNT = "DISCOUNT";

    private Context mContext;

    private ArrayList<OrderItemsDB> orderItemsRequests;

    private String mCustomDiscountId;

    private EditText edtName, edtAmount;

    private ImageView imgRemoveText;

    private Switch switchToggle;

    private TextView txvCurrencySymbol, txvPercentSymbol;

    private Button btnCancel, btnAdd;

    private CustomDiscountFragment.DialogListenerCallbacks callbacks;

    public CustomDiscountFragment() {
        super();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    public interface DialogListenerCallbacks {
        void appliedCustomDiscount(DiscountsModel discountsModel);
    }


    public static CustomDiscountFragment newInstance(ArrayList<OrderItemsDB> orderItemsDBS, String discountId) {
        CustomDiscountFragment createOrderDialogFragment = new CustomDiscountFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(ARG_ORDER_ITEMS, orderItemsDBS);
        bundle.putString(ARG_DISCOUNT, discountId);
        createOrderDialogFragment.setArguments(bundle);
        return createOrderDialogFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            orderItemsRequests = getArguments().getParcelableArrayList(ARG_ORDER_ITEMS);
            mCustomDiscountId = getArguments().getString(ARG_DISCOUNT);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_apply_custom_discount, container);

        callbacks = (CustomDiscountFragment.DialogListenerCallbacks) getTargetFragment();
        if (callbacks == null) {
            callbacks = (CustomDiscountFragment.DialogListenerCallbacks) getActivity();
        }

        edtName = view.findViewById(R.id.edt_name);
        edtName.setHint(LocalizationConstants.getInstance().getKeyValue("msg_enter_name"));
        edtName.addTextChangedListener(this);

        imgRemoveText = view.findViewById(R.id.img_cancel_dialog);
        imgRemoveText.setOnClickListener(this);

        if (edtName.getText().toString().isEmpty()) {
            imgRemoveText.setVisibility(View.GONE);
        } else {
            imgRemoveText.setVisibility(View.VISIBLE);
        }

        TextView txvPercentage = view.findViewById(R.id.txv_percentage);
        txvPercentage.setText(LocalizationConstants.getInstance().getKeyValue("lbl_percentage"));

        switchToggle = view.findViewById(R.id.switch_is_percent);
        switchToggle.setChecked(true);
        switchToggle.setOnCheckedChangeListener(this);

        txvCurrencySymbol = view.findViewById(R.id.txv_currency);
        txvCurrencySymbol.setText(FormatUtils.getCurrencySymbol());
        txvCurrencySymbol.setVisibility(View.GONE);

        edtAmount = view.findViewById(R.id.edt_value);
        edtAmount.addTextChangedListener(this);

        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                int maxDigitsBeforeDecimalPoint;
                if (!switchToggle.isChecked()) {
                    maxDigitsBeforeDecimalPoint = 6;
                } else {
                    maxDigitsBeforeDecimalPoint = 3;
                }
                int maxDigitsAfterDecimalPoint = 2;

                StringBuilder builder = new StringBuilder(dest);
                builder.replace(dstart, dend, source
                        .subSequence(start, end).toString());
                if (!builder.toString().matches(
                        "(([1-9]{1})([0-9]{0," + (maxDigitsBeforeDecimalPoint - 1) + "})?)?(\\.[0-9]{0," + maxDigitsAfterDecimalPoint + "})?")) {
                    if (source.length() == 0)
                        return dest.subSequence(dstart, dend);
                    return "";
                }
                return null;
            }
        };
        edtAmount.setFilters(new InputFilter[]{filter});
        edtAmount.setGravity(Gravity.CENTER_VERTICAL | Gravity.END);

        txvPercentSymbol = view.findViewById(R.id.txv_percentage_symbol);
        txvPercentSymbol.setText("%");
        txvPercentSymbol.setVisibility(View.VISIBLE);

        btnAdd = view.findViewById(R.id.btn_add);
        btnAdd.setOnClickListener(this);
        btnCancel = view.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);

        try {
            getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onClick(View view) {
        if (view == imgRemoveText) {
            edtName.getText().clear();

        } else if (view == btnAdd) {
            if (createDiscountObj()) {
                dismiss();
            } else {
                Toast.makeText(mContext, LocalizationConstants.getInstance().getKeyValue("toast_enter_valid_amt"), Toast.LENGTH_SHORT).show();
            }

        } else if (view == btnCancel) {
            dismiss();
        }
    }

    private boolean createDiscountObj() {
        try {
            String discountName = edtName.getText().toString();
            double discountAmount = Double.parseDouble(edtAmount.getText().toString());
            if (discountName.isEmpty()) {
                discountName = "CUSTOM";
            }

            DiscountsModel discountsModel = new DiscountsModel();
            discountsModel.setId(mCustomDiscountId);
            discountsModel.setName(discountName);
            discountsModel.setCustom(false);
            discountsModel.setPercentage(switchToggle.isChecked());
            discountsModel.setAmount(discountAmount);
            discountsModel.setActive(true);
            callbacks.appliedCustomDiscount(discountsModel);
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(mContext, LocalizationConstants.getInstance().getKeyValue("toast_enter_valid_amt"), Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isToggleOn) {
        if (isToggleOn) {
            edtAmount.setGravity(Gravity.CENTER_VERTICAL | Gravity.END);
            edtAmount.getText().clear();
            edtAmount.requestFocus();
            txvCurrencySymbol.setVisibility(View.GONE);
            txvPercentSymbol.setVisibility(View.VISIBLE);
        } else {
            edtAmount.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
            edtAmount.getText().clear();
            edtAmount.requestFocus();
            txvCurrencySymbol.setVisibility(View.VISIBLE);
            txvPercentSymbol.setVisibility(View.GONE);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (edtName.getText().hashCode() == charSequence.hashCode()) {
            if (charSequence.length() == 0) {
                imgRemoveText.setVisibility(View.GONE);
            } else {
                imgRemoveText.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (edtAmount.getText().hashCode() == editable.hashCode()) {
            try {
                Double amt = Double.valueOf(editable.toString());
                if (switchToggle.isChecked()) {
                    //Percent
                    if (amt > 100) {
                        edtAmount.setText(String.valueOf(100));
                        edtAmount.setSelection(edtAmount.getText().length());
                    }

                } else {
                    //Amount
                    if (amt > 999999) {
                        edtAmount.setText(String.valueOf(999999));
                        edtAmount.setSelection(edtAmount.getText().length());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }
}
