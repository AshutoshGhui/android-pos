package com.hotelkeyapp.android.pos.utils;

import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.db.orm.AdjustmentTaxDB;
import com.hotelkeyapp.android.pos.db.orm.AdjustmentsDB;
import com.hotelkeyapp.android.pos.db.orm.ChargeReqDB;
import com.hotelkeyapp.android.pos.db.orm.DaoSession;
import com.hotelkeyapp.android.pos.db.orm.DiscountTypesDB;
import com.hotelkeyapp.android.pos.db.orm.DiscountTypesDBDao;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDB;
import com.hotelkeyapp.android.pos.db.orm.TaxReqDB;
import com.hotelkeyapp.android.pos.db.orm.TaxTypePeriodsDB;
import com.hotelkeyapp.android.pos.model.DiscountsModel;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import de.greenrobot.dao.query.Query;

/**
 * Created by vrushalimankar on 26/06/18.
 */

public class DiscountsHelper {

    public interface DiscountController {
        void removeCustomDiscount();
    }

    private DiscountController discountController;

    public DiscountsHelper(DiscountController discountController) {
        this.discountController = discountController;
    }

    public ArrayList<OrderItemsDB> applyDiscounts(ArrayList<OrderItemsDB> newItems, ArrayList<OrderItemsDB> oldItems, String menuItemId) {
        if (oldItems == null || oldItems.isEmpty()) {
            return newItems;
        }

        OrderItemsDB orderItemsDB = null;
        for (OrderItemsDB oldItem : oldItems) {
            if (oldItem.getMenu_item_id().equals(menuItemId)) {
                orderItemsDB = oldItem;
                break;
            }
        }

        if (orderItemsDB == null) {
            return newItems;
        }

        ArrayList<String> appliedDiscounts = new ArrayList<>();

        if (orderItemsDB.getChargeReqDB() != null) {
            ArrayList<AdjustmentsDB> adjustmentsDBS = orderItemsDB.getChargeReqDB().getAdjustments();
            if (adjustmentsDBS != null && !adjustmentsDBS.isEmpty()) {
                for (AdjustmentsDB adjustmentsDB : adjustmentsDBS) {

                    if (adjustmentsDB.getStatus().equals(Constants.CHARGE_STATUS_CONFIRMED)) {
                        String discountCode = adjustmentsDB.getDiscountCodeFromDB(adjustmentsDB.getDiscount_id());
                        if (discountCode != null && discountCode.equals("CUSTOM")) {
                            discountController.removeCustomDiscount();
                            return new ArrayList<>();
                        }

                        appliedDiscounts.add(adjustmentsDB.getDiscount_id());
                    }
                }
            }
        }

        if (!appliedDiscounts.isEmpty() && !newItems.isEmpty()) {
            for (OrderItemsDB itemsDB : newItems) {
                if (itemsDB.getChargeReqDB() != null) {

                    ChargeReqDB chargeReqDB = itemsDB.getChargeReqDB();
                    double chargeAmount = chargeReqDB.getAmount();

                    ArrayList<AdjustmentsDB> adjustmentsDBS = new ArrayList<>();

                    for (String discountId : appliedDiscounts) {

                        DiscountsModel discountsModel = getDiscountFromDB(discountId);
                        if (discountsModel == null) {
                            continue;
                        }

                        double newCharge;
                        if (discountsModel.isPercentage()) {
                            newCharge = (chargeAmount * discountsModel.getAmount()) / 100;
                        } else {
                            newCharge = discountsModel.getAmount();
                        }

                        chargeAmount = chargeAmount - newCharge;

                        AdjustmentsDB adjustmentsDB = discountsModel.toAdjustments(newCharge, chargeReqDB.getCharge_type_id(), chargeReqDB.getId());

                        ArrayList<AdjustmentTaxDB> adjustmentTaxDBS = new ArrayList<>();

                        if (chargeReqDB.getLocalTaxes() != null && !chargeReqDB.getLocalTaxes().isEmpty()) {
                            for (TaxReqDB reqDB : chargeReqDB.getLocalTaxes()) {

                                double newTax = 0;

                                TaxTypePeriodsDB taxTypePeriodsDB = reqDB.getTax();
                                if (taxTypePeriodsDB == null) {
                                    continue;
                                }

                                if (taxTypePeriodsDB.getPercentage() != 0) {
                                    newTax = (newCharge * taxTypePeriodsDB.getPercentage()) / 100;
                                }else {
                                    if (chargeAmount== 0){
                                        newTax = taxTypePeriodsDB.getFlat_amount();
                                    }
                                }

                                AdjustmentTaxDB adjustmentTaxDB = new AdjustmentTaxDB();
                                adjustmentTaxDB.setId(UUID.randomUUID().toString());
                                adjustmentTaxDB.setAmount(-newTax);
                                adjustmentTaxDB.setName(reqDB.getName());
                                adjustmentTaxDB.setAdjustment_id(adjustmentsDB.getId());
                                adjustmentTaxDB.setTax_type_id(reqDB.getTax_type_id());
                                adjustmentTaxDB.setDate(adjustmentsDB.getDate());

                                adjustmentTaxDBS.add(adjustmentTaxDB);
                            }
                        }

                        adjustmentsDB.setLocalTaxes(adjustmentTaxDBS);
                        adjustmentsDBS.add(adjustmentsDB);
                    }
                    itemsDB.getChargeReqDB().setAdjustments(adjustmentsDBS);
                }
            }
        }

        return newItems;
    }

    private static DiscountsModel getDiscountFromDB(String discount_id) {
        DaoSession daoSession = POSApplication.getInstance().getDaoSession();
        Query<DiscountTypesDB> queryBuilder = daoSession.getDiscountTypesDBDao().queryBuilder()
                .where(DiscountTypesDBDao.Properties.Id.eq(discount_id)).limit(1).build();

        if (queryBuilder != null) {
            List<DiscountTypesDB> discountsDbList = queryBuilder.list();
            if (discountsDbList != null && !discountsDbList.isEmpty()) {
                DiscountTypesDB discountTypesDB = discountsDbList.get(0);
                return discountTypesDB.toDiscountModel();
            }
        }
        return null;
    }
}
