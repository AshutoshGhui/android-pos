package com.hotelkeyapp.android.pos.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.hotelkeyapp.android.pos.db.orm.TaxTypePeriodsDB;

/**
 * Created by vrushalimankar on 1/13/18.
 */

public class TaxTypePeriod implements Parcelable {

    @SerializedName("id")
    private String id;

    @SerializedName("percentage")
    private double percentage;

    @SerializedName("flat_amount")
    private double flatAmount;

    @SerializedName("start_date")
    private String start_date;

    @SerializedName("end_date")
    private String end_date;

    protected TaxTypePeriod(Parcel in) {
        id = in.readString();
        percentage = in.readDouble();
        flatAmount = in.readDouble();
        start_date = in.readString();
        end_date = in.readString();
    }

    public static final Creator<TaxTypePeriod> CREATOR = new Creator<TaxTypePeriod>() {
        @Override
        public TaxTypePeriod createFromParcel(Parcel in) {
            return new TaxTypePeriod(in);
        }

        @Override
        public TaxTypePeriod[] newArray(int size) {
            return new TaxTypePeriod[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeDouble(percentage);
        dest.writeDouble(flatAmount);
        dest.writeString(start_date);
        dest.writeString(end_date);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public double getFlatAmount() {
        return flatAmount;
    }

    public void setFlatAmount(double flatAmount) {
        this.flatAmount = flatAmount;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public TaxTypePeriodsDB toTaxTypePeriodDB() {
        TaxTypePeriodsDB taxTypePeriodsDB = new TaxTypePeriodsDB();
        taxTypePeriodsDB.setId(getId());
        taxTypePeriodsDB.setPercentage(getPercentage());
        taxTypePeriodsDB.setFlat_amount(getFlatAmount());
        if (getStart_date() != null) {
            taxTypePeriodsDB.setStart_date(getStart_date());
        } else {
            taxTypePeriodsDB.setStart_date("");
        }
        if (getEnd_date() != null) {
            taxTypePeriodsDB.setEnd_date(getEnd_date());
        } else {
            taxTypePeriodsDB.setEnd_date("");
        }
        return taxTypePeriodsDB;
    }
}
