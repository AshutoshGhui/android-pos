package com.hotelkeyapp.android.pos.activity.discounts;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.hotelkey.hkandroidlib.utils.DividerItemDecoration;
import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.activity.orderDetails.OrderHelper;
import com.hotelkeyapp.android.pos.adapter.DiscountsAdapter;
import com.hotelkeyapp.android.pos.db.orm.AdjustmentTaxDB;
import com.hotelkeyapp.android.pos.db.orm.AdjustmentsDB;
import com.hotelkeyapp.android.pos.db.orm.ChargeReqDB;
import com.hotelkeyapp.android.pos.db.orm.DaoSession;
import com.hotelkeyapp.android.pos.db.orm.DiscountTypesDB;
import com.hotelkeyapp.android.pos.db.orm.DiscountTypesDBDao;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDB;
import com.hotelkeyapp.android.pos.db.orm.TaxReqDB;
import com.hotelkeyapp.android.pos.db.orm.TaxTypePeriodsDB;
import com.hotelkeyapp.android.pos.fragment.CustomDiscountFragment;
import com.hotelkeyapp.android.pos.model.DiscountsModel;
import com.hotelkeyapp.android.pos.model.order.AuditLogs;
import com.hotelkeyapp.android.pos.utils.LocalizationConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import de.greenrobot.dao.query.Query;

import static com.hotelkeyapp.android.pos.utils.Constants.CHARGE_STATUS_CANCELLED;
import static com.hotelkeyapp.android.pos.utils.Constants.CHARGE_STATUS_CONFIRMED;


public class HKDiscountsActivity extends AppCompatActivity implements DiscountsAdapter.CheckBoxListener, CustomDiscountFragment.DialogListenerCallbacks {

    public static final String ARG_AUDIT_LOGS = "AUDIT LOGS";

    private RecyclerView recyclerView;

    private List<DiscountsModel> discountsModelList;

    private ArrayList<OrderItemsDB> orderItems;

    private HashMap<String, DiscountsModel> finalDiscounts;

    private Set<String> appliedDiscountIds;

    private ArrayList<AuditLogs> actions;


    public static Intent getLaunchIntent(Context context, ArrayList<OrderItemsDB> orderItemsDBS) {
        Intent intent = new Intent(context, HKDiscountsActivity.class);
        OrderHelper orderHelper = OrderHelper.getInstance();
        orderHelper.setOrderItems(orderItemsDBS);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discounts);

        Toolbar toolbar = findViewById(R.id.toolbar_top);
        TextView txvTitle = toolbar.findViewById(R.id.toolbar_title);
        txvTitle.setText(LocalizationConstants.getInstance().getKeyValue("title_select_discount"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        initViews();
        setUp();
    }

    private void initViews() {
        discountsModelList = new ArrayList<>();
        finalDiscounts = new HashMap<>();
        actions = new ArrayList<>();

        recyclerView = findViewById(R.id.rv_discounts);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void setUp() {
        String customDiscountId = "";
        DaoSession daoSession = POSApplication.getInstance().getDaoSession();
        Query<DiscountTypesDB> queryBuilder = daoSession.getDiscountTypesDBDao().queryBuilder()
                .where(DiscountTypesDBDao.Properties.Active.eq(true)).build();
        List<DiscountTypesDB> discountsDbList = queryBuilder.list();

        if (discountsDbList != null && !discountsDbList.isEmpty()) {
            for (DiscountTypesDB aDiscountTypesDB : discountsDbList) {

                DiscountsModel discountsModel = aDiscountTypesDB.toDiscountModel();
                if (discountsModel == null) {
                    continue;
                }

                if (aDiscountTypesDB.getCode().equals("CUSTOM")) {
                    discountsModelList.add(0, discountsModel);
                    discountsModel.setCustom(true);
                    customDiscountId = aDiscountTypesDB.getId();
                    aDiscountTypesDB.setCustom(true);
                } else {
                    discountsModelList.add(discountsModel);
                    discountsModel.setCustom(false);
                    aDiscountTypesDB.setCustom(false);
                }
            }
        }

        orderItems = OrderHelper.getInstance().getOrderItems();
        if (orderItems == null || orderItems.isEmpty()) {
            return;
        }

        appliedDiscountIds = new HashSet<>();

        for (OrderItemsDB orderItemsDB : orderItems) {
            if (orderItemsDB.getChargeReqDB() != null && !orderItemsDB.getChargeReqDB().getStatus().equals(CHARGE_STATUS_CANCELLED)) {

                if (orderItemsDB.getChargeReqDB().getLocalTaxes() != null && !orderItemsDB.getChargeReqDB().getLocalTaxes().isEmpty()) {
                    for (TaxReqDB taxReqDB : orderItemsDB.getChargeReqDB().getLocalTaxes()) {
                        taxReqDB.setAdjustedTax(taxReqDB.getAmount());
                    }
                }

                if (orderItemsDB.getChargeReqDB().getAdjustments() != null) {

                    ArrayList<AdjustmentsDB> adjustmentsDBS = orderItemsDB.getChargeReqDB().getAdjustments();
                    for (AdjustmentsDB adjustmentsDB : adjustmentsDBS) {

                        if (adjustmentsDB.getDiscount_id() == null || adjustmentsDB.getDiscount_id().isEmpty()) {
                            continue;
                        }

                        if (adjustmentsDB.getStatus().equals(CHARGE_STATUS_CONFIRMED)) {
                            if (!appliedDiscountIds.contains(adjustmentsDB.getDiscount_id()) && adjustmentsDB.getDiscount_id().equals(customDiscountId)) {
                                discountsModelList.add(adjustmentsDB.toDiscount());
                            }

                            finalDiscounts.put(adjustmentsDB.getDiscount_id(), adjustmentsDB.toDiscount());
                            appliedDiscountIds.add(adjustmentsDB.getDiscount_id());
                        }
                    }
                }
            }
        }


        DiscountsAdapter discountsAdapter = new DiscountsAdapter(getApplicationContext(), discountsModelList, appliedDiscountIds, this);
        recyclerView.setAdapter(discountsAdapter);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, 1, com.hotelkey.hkandroidlib.R.drawable.divider);
        recyclerView.addItemDecoration(itemDecoration);


    }

    @Override
    public void appliedCustomDiscount(DiscountsModel discountsModel) {
        discountApplied(discountsModel);
        findViewById(R.id.btn_done).performClick();
    }

    @Override
    public void discountApplied(DiscountsModel discountsModel) {
        discountsModel.setConfirmAdjustment(true);
        finalDiscounts.put(discountsModel.getId(), discountsModel);
    }

    @Override
    public void discountRemoved(DiscountsModel discountsModel) {
        if (finalDiscounts.containsKey(discountsModel.getId())) {
            if (appliedDiscountIds.contains(discountsModel.getId())) {
                discountsModel.setConfirmAdjustment(false);
                finalDiscounts.put(discountsModel.getId(), discountsModel);
            } else {
                finalDiscounts.remove(discountsModel.getId());
            }
        }
    }

    @Override
    public void addCustomDiscount(DiscountsModel discountTypesDB) {
        CustomDiscountFragment customDiscountFragment = CustomDiscountFragment.newInstance(orderItems, discountTypesDB.getId());
        customDiscountFragment.show(getSupportFragmentManager(), "CustomDiscount");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();

        } else if (item.getItemId() == R.id.btn_done) {
            applyDiscounts();

            Intent intent = new Intent();
            intent.putParcelableArrayListExtra(ARG_AUDIT_LOGS, actions);
            OrderHelper.getInstance().setOrderItems(orderItems);
            setResult(RESULT_OK, intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    private void applyDiscounts() {
        if (finalDiscounts.isEmpty()) {
            setResult(RESULT_CANCELED);
            finish();
            return;
        }

        orderItems = OrderHelper.getInstance().getOrderItems();
        if (orderItems != null && !orderItems.isEmpty()) {
            for (OrderItemsDB orderItemsDB : orderItems) {

                ChargeReqDB chargeReqDB = orderItemsDB.getChargeReqDB();
                double totalCharge = chargeReqDB.getAmount();

                ArrayList<AdjustmentsDB> adjustmentsDBS = chargeReqDB.getAdjustments();
                if (adjustmentsDBS == null) {
                    adjustmentsDBS = new ArrayList<>();

                } else {
                    for (AdjustmentsDB adjustmentsDB : adjustmentsDBS) {

                        if (finalDiscounts != null && finalDiscounts.containsKey(adjustmentsDB.getDiscount_id())) {

                            DiscountsModel finalDiscountModel = finalDiscounts.get(adjustmentsDB.getDiscount_id());

                            DiscountsModel discountsModel = null;
                            for (DiscountsModel aDiscountsModel : discountsModelList) {
                                if (aDiscountsModel.getId().equals(adjustmentsDB.getDiscount_id())) {
                                    discountsModel = aDiscountsModel;
                                    break;
                                }
                            }

                            if (discountsModel == null) {
                                continue;
                            }

                            if (finalDiscountModel.isConfirmAdjustment()) {
                                double newCharge;
                                if (discountsModel.isCustom()) {
                                    discountsModel.setAmount(finalDiscountModel.getAmount());
                                    discountsModel.setPercentage(finalDiscountModel.isPercentage());
                                }
                                if (discountsModel.isPercentage()) {
                                    newCharge = (totalCharge * discountsModel.getAmount()) / 100;
                                } else {
                                    newCharge = discountsModel.getAmount();
                                    if (newCharge > totalCharge) {
                                        newCharge = totalCharge;
                                    }
                                }

                                totalCharge = totalCharge - newCharge;
                                adjustmentsDB.setAmount(-newCharge);

                                ArrayList<AdjustmentTaxDB> adjustmentTaxDBS = adjustmentsDB.getLocalTaxes();
                                if (adjustmentTaxDBS != null && !adjustmentsDBS.isEmpty()) {
                                    for (AdjustmentTaxDB taxDB : adjustmentTaxDBS) {
                                        TaxTypePeriodsDB taxTypePeriodsDB = taxDB.getTax();
                                        if (taxTypePeriodsDB == null) {
                                            continue;
                                        }

                                        double newTax = 0;
                                        if (taxTypePeriodsDB.getPercentage() != 0) {
                                            newTax = ((newCharge * taxTypePeriodsDB.getPercentage()) / 100);
                                        } else {
                                            if (totalCharge == 0) {
                                                newTax = taxTypePeriodsDB.getFlat_amount();
                                            }
                                        }
                                        taxDB.setDate(adjustmentsDB.getDate());
                                        taxDB.setAmount(-newTax);
                                    }
                                }

                            } else {
                                adjustmentsDB.setStatus(CHARGE_STATUS_CANCELLED);
                                adjustmentsDB.setDeleted_at(PreferenceUtils.getCBD(getApplicationContext()));

                                AuditLogs auditLogs = adjustmentsDB.getAuditLog(orderItemsDB.getDisplay_label());
                                auditLogs.setPrevious_value(auditLogs.getCurrent_value());
                                auditLogs.setCurrent_value(null);
                                actions.add(auditLogs);
                            }
                        }

                    }
                }

                for (String discountId : finalDiscounts.keySet()) {

                    DiscountsModel discountsModel = finalDiscounts.get(discountId);


                    if (discountsModel.isConfirmAdjustment() && !appliedDiscountIds.contains(discountId)) {
                        double newCharge;
                        if (discountsModel.isPercentage()) {
                            newCharge = (totalCharge * discountsModel.getAmount()) / 100;
                        } else {
                            newCharge = discountsModel.getAmount();
                            if (newCharge > totalCharge) {
                                newCharge = totalCharge;
                            }
                        }

                        totalCharge = totalCharge - newCharge;

                        AdjustmentsDB adjustmentsDB = discountsModel.toAdjustments(newCharge, chargeReqDB.getCharge_type_id(), chargeReqDB.getId());

                        ArrayList<AdjustmentTaxDB> adjustmentTaxDBS = new ArrayList<>();
                        if (chargeReqDB.getLocalTaxes() != null && !chargeReqDB.getLocalTaxes().isEmpty()) {

                            ArrayList<TaxReqDB> taxReqDBS = chargeReqDB.getLocalTaxes();

                            for (TaxReqDB reqDB : taxReqDBS) {

                                double newTax = 0;

                                TaxTypePeriodsDB taxTypePeriodsDB = reqDB.getTax();
                                if (taxTypePeriodsDB == null) {
                                    continue;
                                }

                                if (taxTypePeriodsDB.getPercentage() != 0) {
                                    newTax = (newCharge * taxTypePeriodsDB.getPercentage()) / 100;
                                } else {
                                    if (totalCharge == 0) {
                                        newTax = taxTypePeriodsDB.getFlat_amount();
                                    }
                                }

                                AdjustmentTaxDB adjustmentTaxDB = new AdjustmentTaxDB();
                                adjustmentTaxDB.setId(UUID.randomUUID().toString());
                                adjustmentTaxDB.setAmount(-newTax);
                                adjustmentTaxDB.setName(reqDB.getName());
                                adjustmentTaxDB.setAdjustment_id(adjustmentsDB.getId());
                                adjustmentTaxDB.setTax_type_id(reqDB.getTax_type_id());
                                adjustmentTaxDB.setDate(adjustmentsDB.getDate());

                                adjustmentTaxDBS.add(adjustmentTaxDB);
                            }
                        }
                        adjustmentsDB.setLocalTaxes(adjustmentTaxDBS);
                        adjustmentsDBS.add(adjustmentsDB);
                        actions.add(adjustmentsDB.getAuditLog(orderItemsDB.getDisplay_label()));
                    }
                }
                chargeReqDB.setAdjustments(adjustmentsDBS);

            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
