package com.hotelkeyapp.android.pos.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by vrushalimankar on 05/06/18.
 */

public class OrderPushPayload {

    @SerializedName("order_item_ids")
    private ArrayList<String> order_item_ids;

    @SerializedName("revision_number")
    private int revision_number;

    @SerializedName("order_id")
    private String order_id;

    @SerializedName("status")
    private String status;


    public ArrayList<String> getOrder_item_ids() {
        return order_item_ids;
    }

    public void setOrder_item_ids(ArrayList<String> order_item_ids) {
        this.order_item_ids = order_item_ids;
    }

    public int getRevision_number() {
        return revision_number;
    }

    public void setRevision_number(int revision_number) {
        this.revision_number = revision_number;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
