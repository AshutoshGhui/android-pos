package com.hotelkeyapp.android.pos.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.hotelkey.hkandroidlib.models.property.ApplicationConfiguration;
import com.hotelkey.hkandroidlib.models.property.BusinessDay;
import com.hotelkey.hkandroidlib.models.property.Permission;
import com.hotelkey.hkandroidlib.models.property.RateCode;
import com.hotelkey.hkandroidlib.models.property.Room;
import com.hotelkey.hkandroidlib.models.property.RoomType;
import com.hotelkeyapp.android.pos.db.orm.FolioTypeDB;

import java.util.ArrayList;

/**
 * Created by vrushalimankar on 12/21/17.
 */

public class Property implements Parcelable {

    @SerializedName("charge_types")
    private ArrayList<ChargeType> chargeType;

    @SerializedName("folio_types")
    private ArrayList<FolioTypeDB> folioTypeDBS;

    @SerializedName("discounts")
    private ArrayList<DiscountsTransient> discountsArrayList;

    @SerializedName("language")
    private String language;

    @SerializedName("timezone_offset")
    private String timezoneOffset;

    @SerializedName("timezone_id")
    private String timezoneId;

    @SerializedName("business_day")
    private BusinessDay businessDay;

    @SerializedName("application_config")
    private ApplicationConfiguration applicationConfiguration;

    @SerializedName("rooms")
    private ArrayList<Room> rooms;

    @SerializedName("room_types")
    private ArrayList<RoomType> roomTypes;

    @SerializedName("rate_codes")
    private ArrayList<RateCode> rateCodes;

    @SerializedName("permissions")
    private ArrayList<Permission> permissions;

    @SerializedName("address")
    private String address;

    @SerializedName("city")
    private String city;

    @SerializedName("state")
    private String state;

    @SerializedName("zip_code")
    private String zip_code;

    public static final Creator<Property> CREATOR = new Creator<Property>() {
        @Override
        public Property createFromParcel(Parcel in) {
            return new Property(in);
        }

        @Override
        public Property[] newArray(int size) {
            return new Property[size];
        }
    };

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTimezoneOffset() {
        return timezoneOffset;
    }

    public void setTimezoneOffset(String timezoneOffset) {
        this.timezoneOffset = timezoneOffset;
    }

    public String getTimezoneId() {
        return timezoneId;
    }

    public void setTimezoneId(String timezoneId) {
        this.timezoneId = timezoneId;
    }

    public BusinessDay getBusinessDay() {
        return businessDay;
    }

    public void setBusinessDay(BusinessDay businessDay) {
        this.businessDay = businessDay;
    }

    public ApplicationConfiguration getApplicationConfiguration() {
        return applicationConfiguration;
    }

    public void setApplicationConfiguration(ApplicationConfiguration applicationConfiguration) {
        this.applicationConfiguration = applicationConfiguration;
    }

    public ArrayList<Room> getRooms() {
        return rooms;
    }

    public void setRooms(ArrayList<Room> rooms) {
        this.rooms = rooms;
    }

    public ArrayList<RoomType> getRoomTypes() {
        return roomTypes;
    }

    public void setRoomTypes(ArrayList<RoomType> roomTypes) {
        this.roomTypes = roomTypes;
    }

    public ArrayList<RateCode> getRateCodes() {
        return rateCodes;
    }

    public void setRateCodes(ArrayList<RateCode> rateCodes) {
        this.rateCodes = rateCodes;
    }

    public ArrayList<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(ArrayList<Permission> permissions) {
        this.permissions = permissions;
    }

    public ArrayList<DiscountsTransient> getDiscountsArrayList() {
        return discountsArrayList;
    }

    public void setDiscountsArrayList(ArrayList<DiscountsTransient> discountsArrayList) {
        this.discountsArrayList = discountsArrayList;
    }

    protected Property(Parcel in) {

        chargeType =in.createTypedArrayList(ChargeType.CREATOR);

        this.language = in.readString();
        this.timezoneOffset = in.readString();
        this.timezoneId = in.readString();
        this.businessDay = (BusinessDay)in.readParcelable(BusinessDay.class.getClassLoader());
        this.applicationConfiguration = (ApplicationConfiguration)in.readParcelable(ApplicationConfiguration.class.getClassLoader());
        this.rooms = in.createTypedArrayList(Room.CREATOR);
        this.roomTypes = in.createTypedArrayList(RoomType.CREATOR);
        this.rateCodes = in.createTypedArrayList(RateCode.CREATOR);
        this.permissions = in.createTypedArrayList(Permission.CREATOR);

    }

    public ArrayList<ChargeType> getChargeType() {
        return chargeType;
    }

    public void setChargeType(ArrayList<ChargeType> chargeType) {
        this.chargeType = chargeType;
    }

    public ArrayList<FolioTypeDB> getFolioTypeDBS() {
        return folioTypeDBS;
    }

    public void setFolioTypeDBS(ArrayList<FolioTypeDB> folioTypeDBS) {
        this.folioTypeDBS = folioTypeDBS;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip_code() {
        return zip_code;
    }

    public void setZip_code(String zip_code) {
        this.zip_code = zip_code;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(chargeType);
        parcel.writeString(this.language);
        parcel.writeString(this.timezoneOffset);
        parcel.writeString(this.timezoneId);
        parcel.writeParcelable(this.businessDay, i);
        parcel.writeParcelable(this.applicationConfiguration, i);
        parcel.writeTypedList(this.rooms);
        parcel.writeTypedList(this.roomTypes);
        parcel.writeTypedList(this.rateCodes);
        parcel.writeTypedList(this.permissions);

    }
}
