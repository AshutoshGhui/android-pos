package com.hotelkeyapp.android.pos.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hotelkeyapp.android.pos.model.PrinterProfile;
import com.hotelkeyapp.android.pos.model.Property;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by vrushalimankar on 22/05/18.
 */

public class AccountPreferences {

    private static final String KEY_BUSINESS_DAY_ID = "pref.businessDay.id";
    private static final String KEY_SHIFT_ID = "pref.shift.id";
    private static final String KEY_ORDER_BATCH_ID = "pref.order.batch.id";
    private static final String KEY_NOTIFICATIONS_LAST_SYNC = "pref.notifications.last.sync";
    private static final String KEY_NOTIFICATIONS = "pref.no.notifications";
    private static final String KEY_ORDERS_LAST_SYNC = "pref.orders.last.sync";
    private static final String KEY_RESERVED_TABLE_IDS = "pref.reserved.tables.keyset";
    private static final String KEY_PRINTER_PROFILES = "pref.printer.profiles";
    private static final String KEY_ADDRESS = "pref.property.address";

    public static String getBusinessDayID(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_BUSINESS_DAY_ID, "");
    }

    public static void saveBusinessDayID(Context context, String s) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(KEY_BUSINESS_DAY_ID, s).apply();
    }

    public static String getShiftID(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_SHIFT_ID, "");
    }

    public static void saveShiftID(Context context, String s) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(KEY_SHIFT_ID, s).apply();
    }

    public static String getOrderBatchId(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_ORDER_BATCH_ID, "");
    }

    public static void saveOrderBatchId(Context context, String s) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(KEY_ORDER_BATCH_ID, s).apply();
    }

    public static Long getNotificationsLastSync(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong(KEY_NOTIFICATIONS_LAST_SYNC, 0L);
    }

    public static void saveNotificationsLastSync(Context context, Long timestamp) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putLong(KEY_NOTIFICATIONS_LAST_SYNC, timestamp).apply();
    }

    public static int getNotifications(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(KEY_NOTIFICATIONS, 0);
    }

    public static void saveNotifications(Context context, int notifications) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(KEY_NOTIFICATIONS, notifications).apply();
    }

    public static Long getOrderLastSync(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong(KEY_ORDERS_LAST_SYNC, 0L);
    }

    public static void saveOrderLastSync(Context context, Long timestamp) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putLong(KEY_ORDERS_LAST_SYNC, timestamp).apply();
    }


    public static void saveReservedTableIds(Context context, String tableId, String tableStatus) {
        SharedPreferences pref = context.getSharedPreferences(KEY_RESERVED_TABLE_IDS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(tableId, tableStatus);
        editor.apply();
    }

    public static void removeTableId(Context context, String oldTableId) {
        SharedPreferences pref = context.getSharedPreferences(KEY_RESERVED_TABLE_IDS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        HashMap<String, String> map = (HashMap<String, String>) pref.getAll();
        if (map.containsKey(oldTableId)) {
            editor.remove(oldTableId);
        }
        editor.apply();
    }

    public static void clearReservationTableIds(Context context) {
        SharedPreferences pref = context.getSharedPreferences(KEY_RESERVED_TABLE_IDS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.apply();
    }

    public static String getTableStatusIfTableReserved(Context context, String key) {
        SharedPreferences pref = context.getSharedPreferences(KEY_RESERVED_TABLE_IDS, Context.MODE_PRIVATE);
        HashMap<String, String> map = (HashMap<String, String>) pref.getAll();
        if (map.containsKey(key)) {
            return map.get(key);
        } else {
            return "";
        }
    }

    public static void setPrinterProfiles(Context context, ArrayList<PrinterProfile> deviceInfoArrayList) {
        SharedPreferences.Editor defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context).edit();
        Gson gson = new Gson();
        String json = gson.toJson(deviceInfoArrayList);
        defaultSharedPreferences.putString(KEY_PRINTER_PROFILES, json);
        defaultSharedPreferences.apply();
    }

    public static ArrayList<PrinterProfile> getPrinterProfiles(Context context) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = sharedPrefs.getString(KEY_PRINTER_PROFILES, "");
        Type type = new TypeToken<ArrayList<PrinterProfile>>() {
        }.getType();
        ArrayList<PrinterProfile> arrayList = gson.fromJson(json, type);
        if (arrayList == null) {
            return new ArrayList<>();
        }
        return arrayList;
    }

    public static void deletePrinterProfile(Context context, String id) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = sharedPrefs.getString(KEY_PRINTER_PROFILES, "");
        Type type = new TypeToken<ArrayList<PrinterProfile>>() {
        }.getType();

        ArrayList<PrinterProfile> arrayList = gson.fromJson(json, type);
        if (arrayList != null && !arrayList.isEmpty()) {
            for (int i = 0; i < arrayList.size(); i++) {
                PrinterProfile printerProfile = arrayList.get(i);
                if (printerProfile.getId().equals(id)) {
                    arrayList.remove(i);
                    break;
                }
            }
        }
        String newProfiles = gson.toJson(arrayList);
        SharedPreferences.Editor defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context).edit();
        defaultSharedPreferences.putString(KEY_PRINTER_PROFILES, newProfiles);
        defaultSharedPreferences.apply();
    }

    public static void setPropertyAddress(Context context, Property property) {
        SharedPreferences.Editor defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context).edit();
        String address = property.getAddress() + "\n" + property.getCity() + ", " + property.getState() + " " + property.getZip_code();
        defaultSharedPreferences.putString(KEY_ADDRESS, address);
        defaultSharedPreferences.apply();
    }

    public static String getPropertyAddress(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_ADDRESS, null);
    }

    public static void clear(Context context) {
        SharedPreferences.Editor defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context).edit();
        defaultSharedPreferences.putString(KEY_BUSINESS_DAY_ID, null);
        defaultSharedPreferences.putString(KEY_SHIFT_ID, null);
        defaultSharedPreferences.putString(KEY_ORDER_BATCH_ID, null);
        defaultSharedPreferences.putLong(KEY_NOTIFICATIONS_LAST_SYNC, 0L);
        defaultSharedPreferences.putInt(KEY_NOTIFICATIONS, 0);
        defaultSharedPreferences.putLong(KEY_ORDERS_LAST_SYNC, 0L);
        defaultSharedPreferences.putString(KEY_ADDRESS,null);
        AccountPreferences.clearReservationTableIds(context);
        defaultSharedPreferences.apply();
    }
}
