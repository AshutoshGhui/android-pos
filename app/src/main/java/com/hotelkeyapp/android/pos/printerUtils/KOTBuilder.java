package com.hotelkeyapp.android.pos.printerUtils;

import android.util.Log;
import android.util.Pair;

import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.db.orm.OrderDB;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDB;
import com.hotelkeyapp.android.pos.db.orm.PropertyTableDB;
import com.hotelkeyapp.android.pos.utils.AccountPreferences;
import com.hotelkeyapp.android.pos.utils.FormatUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by vrushalimankar on 04/10/18.
 */

public class KOTBuilder {

    public static String TAG = KOTBuilder.class.getSimpleName();

    public String makeKOTRequest(OrderDB orderDB, PropertyTableDB propertyTableDB, ArrayList<String> newItemIds, boolean isNewOrder, boolean isItemDeleted) {

        ArrayList<OrderItemsDB> orderItemsDBArrayList = orderDB.getOrderItemsDBS();
        if (orderItemsDBArrayList == null || orderItemsDBArrayList.isEmpty()) {
            return null;
        }

        LinkedHashMap<String, ArrayList<OrderItemsDB>> itemsDBLinkedHashMap = new LinkedHashMap<>();

        for (OrderItemsDB orderItemsDB : orderItemsDBArrayList) {
            if (!isNewOrder && !newItemIds.contains(orderItemsDB.getId())) {
                continue;
            }

            ArrayList<OrderItemsDB> orderItemsDBS = itemsDBLinkedHashMap.get(orderItemsDB.getMenu_item_id());
            if (!itemsDBLinkedHashMap.containsKey(orderItemsDB.getMenu_item_id())) {
                orderItemsDBS = new ArrayList<>();
            }
            orderItemsDBS.add(orderItemsDB);
            itemsDBLinkedHashMap.put(orderItemsDB.getMenu_item_id(), orderItemsDBS);
        }

        String finalHtmlString = "";

        finalHtmlString = "<html>" +
                "<head>" +
                "<style>" +
                "th, td {" +
                "    text-align: left; font-size: 24px;" +
                "}" +
                "body {" +
                "        font-size: 24px; width:565px;" +
                "}" +
                "</style>" +
                "</head><body text = \"black\">";


        String[] address1 = AccountPreferences.getPropertyAddress(POSApplication.getInstance()).split("\n");

        finalHtmlString = finalHtmlString + (String.format("<div style=\"text-align: center; margin-top: 40px\"><b>%s</b></div>",
                PreferenceUtils.getCurrentProperty(POSApplication.getInstance()).getName()));

        finalHtmlString = finalHtmlString + (String.format("<div style=\"text-align: center;\">%s</div>",
                address1[0]));

        finalHtmlString = finalHtmlString + (String.format("<div style=\"text-align: center;\">%s</div>",
                address1[1]));

        String leftTableEntityStyle = " <td style=\"text-align: left;;\">%s</td>";
        String rightTableEntityStyle = "<td style=\"text-align: right;;\">%s</td> ";

        finalHtmlString = finalHtmlString + ("<table style=\"width:565px;margin-top: 50px\">" + "<col width=\"80%\">" + "<col width=\"20%\">" + "<tbody >");

        finalHtmlString = finalHtmlString + "<tr>" + String.format(leftTableEntityStyle, "Date:" + FormatUtils.formatBillDate());

        finalHtmlString = finalHtmlString + String.format(rightTableEntityStyle, "Covers:" + orderDB.getPersons()) + "</tr>";

        finalHtmlString = finalHtmlString + "<tr>" + String.format(leftTableEntityStyle, "Order:" + orderDB.getOrder_number() + "/" + orderDB.getSequence_number());

        finalHtmlString = finalHtmlString + String.format(rightTableEntityStyle, propertyTableDB.getName()) + "</tr>";

        finalHtmlString = finalHtmlString + "</tbody>" + "</table>";
        finalHtmlString = finalHtmlString + "<hr style=\"border-top: dashed 2px;\">";

        finalHtmlString = finalHtmlString + "<table style=\"width:565px;\">" + "<col width=\"80%\">" + "<col width=\"20%\">" + "<tbody>";

        finalHtmlString = finalHtmlString + ("<tr>") + (String.format(leftTableEntityStyle, "Description"));

        finalHtmlString = finalHtmlString + (String.format(rightTableEntityStyle, "Qty")) + "</tr>";

        finalHtmlString = finalHtmlString + ("</tbody>" + "</table>");
        finalHtmlString = finalHtmlString + ("<hr style=\"border-top: dashed 2px;\">");


        ArrayList<String> keyList = new ArrayList<>(itemsDBLinkedHashMap.keySet());

        for (int i = 0; i < itemsDBLinkedHashMap.size(); i++) {
            ArrayList<OrderItemsDB> orderItemsList = itemsDBLinkedHashMap.get(keyList.get(i));
            OrderItemsDB orderItem = orderItemsList.get(0);

            StringBuffer leftRowEntity = new StringBuffer();

            String name = orderItem.getDisplay_label();
            if (isItemDeleted) {
                name = name + " " + "(CANCELLED)";
            }

            leftRowEntity = leftRowEntity.append(name);

            String rightRowEntity = String.valueOf(orderItemsList.size());

            finalHtmlString = finalHtmlString + "<table style=\"width:565px;\">" +
                    "<col width=\"80%\">" +
                    "<col width=\"20%\">" +
                    "<tbody>";

            finalHtmlString = finalHtmlString + "<tr>" +
                    String.format(leftTableEntityStyle, leftRowEntity);

            finalHtmlString = finalHtmlString +
                    String.format(rightTableEntityStyle, rightRowEntity) +
                    "</tr>";

            String remarksEntity = "";

            if (orderItem.getRemarks() != null && !orderItem.getRemarks().isEmpty()) {
                remarksEntity = remarksEntity + ("<tr>");
                remarksEntity = remarksEntity + "<td style=\"text-align: left; padding-left: 14px;\">";
                remarksEntity = remarksEntity + ("<i>(");
                remarksEntity = remarksEntity + (orderItem.getRemarks());
                remarksEntity = remarksEntity + (")</i></td></tr>");
            }

            finalHtmlString = finalHtmlString + remarksEntity;

            finalHtmlString = finalHtmlString + "</tbody>" + "</table>";
        }

        finalHtmlString = finalHtmlString + "<hr style=\"margin-top: 50px; border-top: dashed 2px;\">";

        Log.d(TAG, finalHtmlString);

        return finalHtmlString + "</body><html>";
    }


    private Pair<String, Integer> getNewString(String mainString) {
        Integer height = 0;

        StringBuffer finalString = new StringBuffer();
        String[] stringArray = mainString.split("\\s+");
        String tmpString = "";

        for (String singleWord : stringArray) {

            if ((tmpString + singleWord + " ").length() > 41) {
                height = height + 1;
                finalString.append(tmpString + "\n");
                tmpString = singleWord + " ";

            } else {
                tmpString = tmpString + singleWord + " ";
            }
        }

        if (tmpString.length() > 0) {
            finalString.append(tmpString);
        }
        return new Pair<>(finalString.toString(), height);
    }
}
