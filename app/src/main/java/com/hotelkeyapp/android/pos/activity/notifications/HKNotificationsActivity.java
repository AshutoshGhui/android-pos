package com.hotelkeyapp.android.pos.activity.notifications;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.hotelkey.hkandroidlib.activity.common.AuthenticatedActivity;
import com.hotelkey.hkandroidlib.utils.DividerItemDecoration;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.adapter.NotificationsAdapter;
import com.hotelkeyapp.android.pos.controllers.NotificationsController;
import com.hotelkeyapp.android.pos.db.orm.DaoSession;
import com.hotelkeyapp.android.pos.db.orm.NotificationDB;
import com.hotelkeyapp.android.pos.db.orm.OrderDB;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDB;
import com.hotelkeyapp.android.pos.db.orm.PropertyTableDB;
import com.hotelkeyapp.android.pos.fragment.DashboardFragment;
import com.hotelkeyapp.android.pos.utils.AccountPreferences;
import com.hotelkeyapp.android.pos.utils.LocalizationConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.hotelkeyapp.android.pos.utils.Constants.ARG_MESSAGE;

public class HKNotificationsActivity extends AuthenticatedActivity implements NotificationsController.NotificationsCallbacks
        , SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView recyclerView;

    private NotificationsAdapter notificationsAdapter;

    private TextView txvNoNotification;

    private HashMap<String, OrderItemsDB> orderItemsDBHashMap;

    private HashMap<String, OrderDB> orderDBHashMap;

    private HashMap<String, PropertyTableDB> propertyTableDBHashMap;

    private List<NotificationDB> notificationsList;

    DaoSession daoSession;

    private SwipeRefreshLayout swipeRefreshLayout;

    public static Intent getLaunchIntent(Context context) {
        return new Intent(context, HKNotificationsActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hknotifications);

        Toolbar toolbar = findViewById(R.id.toolbar_top);
        TextView txvTitle = toolbar.findViewById(R.id.toolbar_title);
        txvTitle.setText(LocalizationConstants.getInstance().getKeyValue("lbl_notifications"));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        initViews();
        getNotifications();
        setUp();
    }

    private void initViews() {
        swipeRefreshLayout = findViewById(R.id.swipe_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        txvNoNotification = findViewById(R.id.txv_no_notifications);
        this.recyclerView = this.findViewById(R.id.rv_notifications);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, 1, com.hotelkey.hkandroidlib.R.drawable.divider);
        this.recyclerView.addItemDecoration(itemDecoration);
    }

    private void setUp() {
        daoSession = POSApplication.getInstance().getDaoSession();
        propertyTableDBHashMap = new HashMap<>();
        orderDBHashMap = new HashMap<>();
        orderItemsDBHashMap = new HashMap<>();
        notificationsList = new ArrayList<>();

        this.notificationsAdapter = new NotificationsAdapter(getApplicationContext(), notificationsList,
                orderItemsDBHashMap, orderDBHashMap, propertyTableDBHashMap);
        this.recyclerView.setAdapter(this.notificationsAdapter);

        prepareHashMaps();

        notificationsList = daoSession.getNotificationDBDao().loadAll();
        if (notificationsList != null && !notificationsList.isEmpty()) {
            for (NotificationDB notificationDB : notificationsList) {
                if (notificationDB.getOrder_id() != null) {
                    if (orderDBHashMap != null && orderDBHashMap.containsKey(notificationDB.getOrder_id())) {
                        notificationDB.setItem_recently_updated(false);
                        daoSession.update(notificationDB);

                    } else {
                        daoSession.delete(notificationDB);
                    }
                }
            }
        }
        notificationsList = daoSession.getNotificationDBDao().loadAll();
        if (notificationsList == null || notificationsList.isEmpty()) {
            notificationsList = new ArrayList<>();
            txvNoNotification.setVisibility(View.VISIBLE);
        } else {
            txvNoNotification.setVisibility(View.GONE);
        }
        this.notificationsAdapter.refresh(notificationsList, orderItemsDBHashMap, orderDBHashMap, propertyTableDBHashMap);

    }

    private void prepareHashMaps() {
        List<OrderItemsDB> orderItemsDBS = daoSession.getOrderItemsDBDao().loadAll();
        if (orderItemsDBS != null && !orderItemsDBS.isEmpty()) {
            for (OrderItemsDB orderItemsDB : orderItemsDBS) {
                orderItemsDBHashMap.put(orderItemsDB.getId(), orderItemsDB);
            }
        }

        List<OrderDB> orderDBS = daoSession.getOrderDBDao().loadAll();
        if (orderDBS != null && !orderDBS.isEmpty()) {
            for (OrderDB orderDB : orderDBS) {
                orderDBHashMap.put(orderDB.getId(), orderDB);
            }
        }

        List<PropertyTableDB> propertyTableDBS = daoSession.getPropertyTableDBDao().loadAll();
        if (propertyTableDBS != null && !propertyTableDBS.isEmpty()) {
            for (PropertyTableDB propertyTableDB : propertyTableDBS) {
                propertyTableDBHashMap.put(propertyTableDB.getId(), propertyTableDB);
            }
        }
    }

    private void getNotifications() {
        showLoadingDialog();
        NotificationsController notificationsController = new NotificationsController(this);
        notificationsController.downloadNotifications();
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 2000);
        NotificationsController notificationsController = new NotificationsController(this);
        notificationsController.downloadNotifications();
    }

    @Override
    public void onFailure(Pair<String, Boolean> message) {
        cancelLoadingDialog();
        if (message.second) {
            Intent intent = new Intent();
            intent.putExtra(ARG_MESSAGE, message.first);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            showMessageDialog(message.first, LocalizationConstants.getInstance().getKeyValue("btn_ok"), null);
        }
    }

    @Override
    public void onSuccess(List<NotificationDB> notifications) {
        prepareHashMaps();
        txvNoNotification.setVisibility(View.GONE);

        if (notificationsList != null && !notificationsList.isEmpty()) {
            if (!notifications.isEmpty()) {
                notificationsList.addAll(notifications);
                txvNoNotification.setVisibility(View.GONE);
            }
        } else {
            notificationsList = notifications;
            if (notificationsList == null || notificationsList.isEmpty()) {
                txvNoNotification.setVisibility(View.VISIBLE);
            }
        }

        this.notificationsAdapter.refresh(notificationsList, orderItemsDBHashMap, orderDBHashMap, propertyTableDBHashMap);

        AccountPreferences.saveNotifications(getApplicationContext(), 0);
        cancelLoadingDialog();

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(DashboardFragment.NotificationReceived.REFRESH_BADGE);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(broadcastIntent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(Activity.RESULT_OK);
        finish();
    }
}
