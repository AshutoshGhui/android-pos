package com.hotelkeyapp.android.pos.activity.printerSetup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.epson.epos2.discovery.DeviceInfo;
import com.hotelkey.hkandroidlib.common.BaseActivity;
import com.hotelkey.hkandroidlib.utils.DividerItemDecoration;
import com.hotelkey.hkandroidlib.utils.ItemClickSupport;
import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.adapter.NearbyDevicesAdapter;
import com.hotelkeyapp.android.pos.model.DeviceInfoTransient;
import com.hotelkeyapp.android.pos.model.PrinterProfile;
import com.hotelkeyapp.android.pos.utils.LocalizationConstants;
import com.hotelkeyapp.android.pos.utils.ProgressEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

/**
 * Created by vrushalimankar on 01/10/18.
 */

public class HKNearbyPrintersActivity extends BaseActivity implements View.OnClickListener, ItemClickSupport.OnItemClickListener {

    private HKNearbyPrintersActivity.DialogListenerCallbacks callbacks;

    private TextView txvTitle;

    private RecyclerView recyclerView;

    private NearbyDevicesAdapter nearbyDevicesAdapter;

    private PrinterProfile selectedDevice = null;

    public interface DialogListenerCallbacks {
        void selectedDevice(DeviceInfo deviceInfo);
    }

    public static Intent newInstance(Context context, ArrayList<PrinterProfile> deviceInfoArrayList) {
        Intent intent = new Intent(context, HKNearbyPrintersActivity.class);
        DeviceInfoTransient deviceInfoTransient = DeviceInfoTransient.getInstance();
        deviceInfoTransient.setDeviceInfoArrayList(deviceInfoArrayList);
        return intent;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_nearby_printers);

        Toolbar toolbar = findViewById(R.id.toolbar_top);
        txvTitle = toolbar.findViewById(R.id.toolbar_title);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        initViews();
    }


    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void progressStatusChanged(ProgressEvent event) {
        switch (event.getState()) {
            case ProgressEvent.PRINTER_DISCOVERED:
                if (event.getDeviceInfoArrayList() != null && !event.getDeviceInfoArrayList().isEmpty()) {
                    DeviceInfoTransient deviceInfoTransient = DeviceInfoTransient.getInstance();
                    deviceInfoTransient.setDeviceInfoArrayList(event.getDeviceInfoArrayList());
                    if (nearbyDevicesAdapter != null) {
                        nearbyDevicesAdapter.setItems(event.getDeviceInfoArrayList());
                        nearbyDevicesAdapter.notifyDataSetChanged();
                    }
                }
                break;

        }
    }


    private void initViews() {
        txvTitle.setText(LocalizationConstants.getInstance().getKeyValue("title_printers"));

        Button btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(this);
        btnSave.setText(LocalizationConstants.getInstance().getKeyValue("btn_save"));

        this.recyclerView = this.findViewById(R.id.ll_printers_list);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, 1, com.hotelkey.hkandroidlib.R.drawable.divider);
        this.recyclerView.addItemDecoration(itemDecoration);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(this);

        setUp();
    }

    private void setUp() {
        ArrayList<PrinterProfile> deviceInfoArrayList = DeviceInfoTransient.getInstance().getDeviceInfoArrayList();
        this.nearbyDevicesAdapter = new NearbyDevicesAdapter(deviceInfoArrayList);
        this.recyclerView.setAdapter(this.nearbyDevicesAdapter);

    }

    @Override
    public void onItemClicked(RecyclerView recyclerView, int i, View view) {
        nearbyDevicesAdapter.setSelectedPos(i);
        nearbyDevicesAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_save) {
            Integer selectedPos = nearbyDevicesAdapter.getSelectedPos();
            if (selectedPos != null) {
                selectedDevice = nearbyDevicesAdapter.getItems().get(selectedPos);
                DeviceInfoTransient.getInstance().setDeviceInfo(selectedDevice);
                setResult(RESULT_OK);
                finish();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}
