package com.hotelkeyapp.android.pos.db.orm;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END
/**
 * Entity mapped to table "DEPARTMENT_DB".
 */
public class DepartmentDB {

    private String id;
    private String name;
    private String type;
    private String property_id;
    private String folio_type_id;
    private Boolean active;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    public DepartmentDB() {
    }

    public DepartmentDB(String id, String name, String type, String property_id, String folio_type_id, Boolean active) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.property_id = property_id;
        this.folio_type_id = folio_type_id;
        this.active = active;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getProperty_id() {
        return property_id;
    }

    public void setProperty_id(String property_id) {
        this.property_id = property_id;
    }

    public String getFolio_type_id() {
        return folio_type_id;
    }

    public void setFolio_type_id(String folio_type_id) {
        this.folio_type_id = folio_type_id;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
