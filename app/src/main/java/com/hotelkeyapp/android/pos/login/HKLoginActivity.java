package com.hotelkeyapp.android.pos.login;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.hotelkey.hkandroidlib.activity.login.LoginActivity;
import com.hotelkey.hkandroidlib.models.VersionCheck;
import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.activity.common.HKHomeActivity;
import com.hotelkeyapp.android.pos.activity.common.HKSwitchPropertyActivity;
import com.hotelkeyapp.android.pos.fcm.RegisterFCMTask;

/**
 * Created by vrushalimankar on 09/05/18.
 */

public class HKLoginActivity extends com.hotelkey.hkandroidlib.activity.login.LoginActivity implements RegisterFCMTask.RegisterFCMTaskListener {

    private Dialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        checkPushNotification();
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    protected void checkPushNotification() {
        if (checkGooglePlayServices()) {
            String token = PreferenceUtils.getToken(this);
            if (token == null) {
                this.showLoadingDialog(this.getString(com.hotelkey.hkandroidlib.R.string.msg_register_push_notification));
                RegisterFCMTask task = new RegisterFCMTask(this, this);
                task.execute();
            } else {
                Log.d("Push", "Token:" + token);
            }
            PreferenceUtils.saveIsPlayStoreUpdated(this, true);
        } else {
            PreferenceUtils.saveIsPlayStoreUpdated(this, false);
        }
        supportInvalidateOptionsMenu();
    }

    @Override
    public void performSwitchProperty() {
        Intent intent = HKSwitchPropertyActivity.getLaunchIntent(this);
        startActivity(intent);
        finish();
    }

    @Override
    public void redirectToHomeScreen() {
        Intent intent = HKHomeActivity.getLaunchIntent(getApplicationContext());
        startActivity(intent);
        finish();
    }

    private void getData() {
        final VersionCheck versionCheck = PreferenceUtils.getUpgrade(POSApplication.getInstance());
        if (versionCheck != null) {
            if (versionCheck.getDownloadUrl() != null && !versionCheck.getDownloadUrl().isEmpty()) {
                System.out.println(LoginActivity.class.getSimpleName() + versionCheck.getDownloadUrl() + versionCheck.getVersionCode());

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String message = String.format(getString(R.string.msg_alert_upgrade_app), versionCheck.getVersionCode());

                        showNonCancelableMessageDialog(new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                PreferenceUtils.isUpgradeRequired(POSApplication.getInstance(), "", "");
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(versionCheck.getDownloadUrl()));
                                startActivity(intent);
                            }
                        }, message, getString(R.string.btn_ok));
                    }
                }, 500);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPushNotification();
        getData();
    }

    @Override
    public void onPushNotificationRegisterSuccessful() {
        this.hideLoading();
    }

    @Override
    public void onPushNotificationRegisterFailed() {
        this.hideLoading();
        this.showMessageDialog("Failed to register for PushNotification", getString(R.string.btn_ok_uppercase), null);
    }

    public boolean checkGooglePlayServices() {
        final int googlePlayServicesCheck = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(POSApplication.getApplication());
        switch (googlePlayServicesCheck) {
            case ConnectionResult.SUCCESS:
                return true;
            case ConnectionResult.SERVICE_DISABLED:
            case ConnectionResult.SERVICE_INVALID:
            case ConnectionResult.SERVICE_MISSING:
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:

                if (dialog == null) {
                    dialog = GooglePlayServicesUtil.getErrorDialog(googlePlayServicesCheck, this, 0);
                    dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            HKLoginActivity.this.finish();
                        }
                    });
                    dialog.show();
                }
        }
        return false;
    }
}
