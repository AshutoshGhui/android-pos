package com.hotelkeyapp.android.pos.activity.orderDetails;

import com.hotelkeyapp.android.pos.db.orm.OrderDB;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDB;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by vrushalimankar on 04/07/18.
 */

public class OrderHelper {

    private ArrayList<OrderItemsDB> orderItems;

    private LinkedHashMap<String, ArrayList<OrderItemsDB>> newOrderItems;

    private OrderDB orderDB;

    private static OrderHelper orderHelper;

    private OrderHelper() {
    }

    public static OrderHelper getInstance() {
        if (orderHelper == null) {
            orderHelper = new OrderHelper();
        }
        return orderHelper;
    }

    public ArrayList<OrderItemsDB> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(ArrayList<OrderItemsDB> orderItems) {
        this.orderItems = orderItems;
    }

    public OrderDB getOrderDB() {
        return orderDB;
    }

    public void setOrderDB(OrderDB orderDB) {
        this.orderDB = orderDB;
    }

    public LinkedHashMap<String, ArrayList<OrderItemsDB>> getNewOrderItems() {
        return newOrderItems;
    }

    public void setNewOrderItems(LinkedHashMap<String, ArrayList<OrderItemsDB>> newOrderItems) {
        this.newOrderItems = newOrderItems;
    }
}
