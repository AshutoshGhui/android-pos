package com.hotelkeyapp.android.pos.apiService;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.Nullable;
import android.util.Log;

import com.hotelkey.hkandroidlib.HKAndroidApplication;
import com.hotelkey.hkandroidlib.db.orm.PermissionDB;
import com.hotelkey.hkandroidlib.db.orm.PropertyDB;
import com.hotelkey.hkandroidlib.exceptions.AuthenticationFailedException;
import com.hotelkey.hkandroidlib.exceptions.ServerError;
import com.hotelkey.hkandroidlib.models.property.BusinessDay;
import com.hotelkey.hkandroidlib.models.property.Permission;
import com.hotelkey.hkandroidlib.service.ProgressEvent;
import com.hotelkey.hkandroidlib.service.SyncFailedException;
import com.hotelkey.hkandroidlib.utils.ExceptionHelper;
import com.hotelkey.hkandroidlib.utils.MessageHelper;
import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.db.orm.AdjustmentTaxDB;
import com.hotelkeyapp.android.pos.db.orm.AdjustmentTaxDBDao;
import com.hotelkeyapp.android.pos.db.orm.AdjustmentsDB;
import com.hotelkeyapp.android.pos.db.orm.AdjustmentsDBDao;
import com.hotelkeyapp.android.pos.db.orm.ChargeReqDB;
import com.hotelkeyapp.android.pos.db.orm.ChargeTypesDB;
import com.hotelkeyapp.android.pos.db.orm.DaoSession;
import com.hotelkeyapp.android.pos.db.orm.DepartmentDB;
import com.hotelkeyapp.android.pos.db.orm.DiscountPeriodsDB;
import com.hotelkeyapp.android.pos.db.orm.DiscountTypesDB;
import com.hotelkeyapp.android.pos.db.orm.FolioReqDB;
import com.hotelkeyapp.android.pos.db.orm.FolioTypeDB;
import com.hotelkeyapp.android.pos.db.orm.MenuGroupDB;
import com.hotelkeyapp.android.pos.db.orm.MenuItemDB;
import com.hotelkeyapp.android.pos.db.orm.MenuItemDBDao;
import com.hotelkeyapp.android.pos.db.orm.NotificationDB;
import com.hotelkeyapp.android.pos.db.orm.OrderDB;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDB;
import com.hotelkeyapp.android.pos.db.orm.OrderTypeDB;
import com.hotelkeyapp.android.pos.db.orm.PropertyTableDB;
import com.hotelkeyapp.android.pos.db.orm.TableGroupDB;
import com.hotelkeyapp.android.pos.db.orm.TaxReqDB;
import com.hotelkeyapp.android.pos.db.orm.TaxTypePeriodsDB;
import com.hotelkeyapp.android.pos.db.orm.TaxTypesDB;
import com.hotelkeyapp.android.pos.model.ChargeType;
import com.hotelkeyapp.android.pos.model.DiscountPeriods;
import com.hotelkeyapp.android.pos.model.DiscountsTransient;
import com.hotelkeyapp.android.pos.model.MenuGroup;
import com.hotelkeyapp.android.pos.model.MenuItems;
import com.hotelkeyapp.android.pos.model.OrderType;
import com.hotelkeyapp.android.pos.model.Property;
import com.hotelkeyapp.android.pos.model.Shift;
import com.hotelkeyapp.android.pos.model.TableGroup;
import com.hotelkeyapp.android.pos.model.TaxTypePeriod;
import com.hotelkeyapp.android.pos.model.TaxTypes;
import com.hotelkeyapp.android.pos.model.table.Table;
import com.hotelkeyapp.android.pos.network.POSNetworkService;
import com.hotelkeyapp.android.pos.utils.AccountPreferences;
import com.hotelkeyapp.android.pos.utils.Constants;
import com.hotelkeyapp.android.pos.utils.FormatUtils;
import com.hotelkeyapp.android.pos.utils.ServiceExecutionException;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

import de.greenrobot.dao.query.Query;
import de.greenrobot.dao.query.QueryBuilder;
import retrofit2.Call;
import retrofit2.Response;

import static com.hotelkeyapp.android.pos.utils.Constants.ORDER_STATUS_BILL_ISSUED;
import static com.hotelkeyapp.android.pos.utils.Constants.ORDER_STATUS_COMPLETED;
import static com.hotelkeyapp.android.pos.utils.Constants.ORDER_STATUS_OPEN;
import static com.hotelkeyapp.android.pos.utils.Constants.PERMISSION_INVENTORY_SYSTEM;
import static com.hotelkeyapp.android.pos.utils.Constants.UPDATE_ORDERS_TABLES;
import static com.hotelkeyapp.android.pos.utils.FormatUtils.SERVER_DATE_FORMAT;

/**
 * Created by vrushalimankar on 10/05/18.
 */

public class SyncService extends IntentService {
    private static final String TAG = "SyncService";

    public static final String CANCEL_ALL_REQUESTS = "CANCEL ALL REQUESTS";

    public SyncService() {
        super("Sync Service");
    }

    public SyncService(String name) {
        super(name);
    }

    public int retryCount = 0;

    public boolean stopServiceExecution;

    @Override
    public void onCreate() {
        super.onCreate();
        IntentFilter filter = new IntentFilter();
        filter.addAction(CANCEL_ALL_REQUESTS);
        registerReceiver(receiver, filter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null && action.equals("CANCEL ALL REQUESTS")) {
                stopServiceExecution = true;
            }
        }
    };

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        EventBus.getDefault().post(new ProgressEvent(ProgressEvent.IN_PROGRESS));

        try {
            POSApplication posApplication = POSApplication.getInstance();

            if (posApplication.getCurrentUser() == null || posApplication.getCurrentProperty() == null) {
                return;
            }
            PropertyDB currentProperty = posApplication.getCurrentProperty();

            String propertyId = currentProperty.getId();
            POSNetworkService networkService = posApplication.getNetworkService();
            DaoSession daoSession = posApplication.getDaoSession();
            com.hotelkey.hkandroidlib.db.orm.DaoSession masterDaoSession = posApplication.getMasterDaoSession();

            boolean syncOrdersTables = intent.getBooleanExtra(UPDATE_ORDERS_TABLES, false);

            boolean callPropertyApi = false;

            if (!syncOrdersTables) {

                if (PreferenceUtils.getPropertyCBD(posApplication) != null && !PreferenceUtils.getPropertyCBD(posApplication).isEmpty()) {
                    BusinessDay businessDay = getBusinessDayDetails(posApplication, propertyId);

                    if (businessDay != null && !businessDay.getCurrentBusinessDate().equals(PreferenceUtils.getCBD(posApplication))) {
                        PreferenceUtils.saveCBD(posApplication, businessDay.getCurrentBusinessDate().getTime());

                        AccountPreferences.saveOrderLastSync(posApplication, 0L);
                        AccountPreferences.clearReservationTableIds(posApplication);
                        daoSession.deleteAll(NotificationDB.class);
                        daoSession.deleteAll(OrderDB.class);
                        daoSession.deleteAll(OrderItemsDB.class);
                        daoSession.deleteAll(ChargeReqDB.class);
                        daoSession.deleteAll(TaxReqDB.class);
                        daoSession.deleteAll(FolioReqDB.class);
                        daoSession.deleteAll(AdjustmentsDB.class);
                        daoSession.deleteAll(AdjustmentTaxDB.class);

                    }
                    EventBus.getDefault().post(new com.hotelkeyapp.android.pos.utils.ProgressEvent(com.hotelkeyapp.android.pos.utils.ProgressEvent.CBD_FETCHED));

                } else {
                    callPropertyApi = true;
                }

                if (callPropertyApi || !PreferenceUtils.getAppReady(this)) {
                    Property property = getPropertyDetails(posApplication, propertyId, networkService);

                    AccountPreferences.saveOrderLastSync(posApplication, 0L);
                    masterDaoSession.deleteAll(PermissionDB.class);
                    daoSession.deleteAll(NotificationDB.class);
                    daoSession.deleteAll(OrderDB.class);
                    daoSession.deleteAll(OrderItemsDB.class);
                    daoSession.deleteAll(ChargeReqDB.class);
                    daoSession.deleteAll(TaxReqDB.class);
                    daoSession.deleteAll(FolioReqDB.class);
                    daoSession.deleteAll(AdjustmentsDB.class);
                    daoSession.deleteAll(AdjustmentTaxDB.class);

                    daoSession.deleteAll(DiscountTypesDB.class);
                    daoSession.deleteAll(DiscountPeriodsDB.class);
                    AccountPreferences.clearReservationTableIds(posApplication);

                    Date currentBusinessDate = property.getBusinessDay().getCurrentBusinessDate();
                    String currentBusinessDateId = property.getBusinessDay().getId();

                    AccountPreferences.saveBusinessDayID(posApplication, currentBusinessDateId);
                    AccountPreferences.setPropertyAddress(posApplication, property);
                    PreferenceUtils.savePropertyCBD(posApplication, FormatUtils.formatInventoryDate(currentBusinessDate));
                    PreferenceUtils.saveCBD(posApplication, currentBusinessDate.getTime());
                    PreferenceUtils.savePropLanguage(posApplication, property.getLanguage());

                    EventBus.getDefault().post(new com.hotelkeyapp.android.pos.utils.ProgressEvent(com.hotelkeyapp.android.pos.utils.ProgressEvent.CBD_FETCHED));

                    ArrayList<String> permissions = new ArrayList<>();
                    for (Permission permission : property.getPermissions()) {
                        String moduleCode = permission.getModule().getCode();
                        String objectCode = permission.getObject().getCode();

                        if (moduleCode == null || objectCode == null) {
                            continue;
                        }

                        if (Constants.PERMISSION_MODULES.contains(moduleCode)) {

                            if (Constants.PERMISSION_INVENTORY_SYSTEM.equals(objectCode)) {
                                if (permission.getAccessRight().equals(Constants.PERMISSION_READ)
                                        || permission.getAccessRight().equals(Constants.PERMISSION_READ_WRITE)) {
                                    permissions.add(objectCode);
                                }
                            }
                            if (Constants.PERMISSION_ORDER_MODULE.equals(objectCode)) {
                                if (!permission.getAccessRight().equals(Constants.PERMISSION_NONE)) {
                                    PermissionDB entity = permission.toDBModel();
                                    masterDaoSession.insert(entity);
                                }

                            } else if (permission.getAccessRight().equals(Constants.PERMISSION_READ_WRITE)) {
                                PermissionDB entity = permission.toDBModel();
                                masterDaoSession.insert(entity);
                            }
                        }
                    }

                    daoSession.deleteAll(ChargeTypesDB.class);
                    daoSession.deleteAll(TaxTypesDB.class);
                    daoSession.deleteAll(TaxTypePeriodsDB.class);

                    if (property.getChargeType() != null) {
                        ArrayList<ChargeType> chargeTypes = property.getChargeType();
                        Random random = new Random();
                        for (ChargeType aChargeType : chargeTypes) {

                            if (!aChargeType.isActive()) {
                                continue;
                            }
                            ChargeTypesDB chargeTypesDB = aChargeType.toChargeTypeDB();
                            daoSession.insert(chargeTypesDB);

                            if (aChargeType.getTaxTypes() != null) {
                                ArrayList<TaxTypes> taxTypes = aChargeType.getTaxTypes();
                                for (TaxTypes aTaxType : taxTypes) {
                                    if (!aTaxType.isActive()) {
                                        continue;
                                    }
                                    TaxTypesDB taxTypesDB = aTaxType.toTaxTypeDB();
                                    taxTypesDB.setChargeTypeId(aChargeType.getId());
                                    taxTypesDB.setId(random.nextLong());
                                    daoSession.insert(taxTypesDB);

                                    if (aTaxType.getTaxTypePeriod() != null) {
                                        ArrayList<TaxTypePeriod> taxTypePeriods = aTaxType.getTaxTypePeriod();
                                        for (TaxTypePeriod period : taxTypePeriods) {
                                            TaxTypePeriodsDB taxTypePeriodsDB = period.toTaxTypePeriodDB();
                                            taxTypePeriodsDB.setTaxTypeId(taxTypesDB.getId());
                                            daoSession.insert(taxTypePeriodsDB);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    ArrayList<DiscountsTransient> discountsTransients = property.getDiscountsArrayList();
                    for (DiscountsTransient discountsTransientDB : discountsTransients) {
                        if (!discountsTransientDB.getActive()) {
                            continue;
                        }
                        DiscountTypesDB discountDB = discountsTransientDB.toDiscountTypeDB();

                        boolean isActive = false;
                        ArrayList<DiscountPeriods> discountPeriodsArrayList = discountsTransientDB.getDiscountPeriodsArrayList();
                        for (DiscountPeriods discountPeriods : discountPeriodsArrayList) {
                            if (discountPeriods.getStartDate() != null) {

                                DateFormat format = new SimpleDateFormat(SERVER_DATE_FORMAT, Locale.ENGLISH);
                                Date startDate = format.parse(discountPeriods.getStartDate());
                                Date businessDate = PreferenceUtils.getCBD(HKAndroidApplication.getApplication());

                                if (discountPeriods.getEndDate() != null) {
                                    Date endDate = format.parse(discountPeriods.getEndDate());
                                    if ((businessDate.equals(startDate) || businessDate.after(startDate))
                                            && (businessDate.before(endDate) || businessDate.equals(endDate))) {
                                        DiscountPeriodsDB discountPeriodsDB = discountPeriods.toDiscountsPeriod();
                                        discountPeriodsDB.setDiscount_id(discountDB.getId());
                                        daoSession.insertOrReplace(discountPeriodsDB);
                                        isActive = true;
                                        break;
                                    }
                                } else if (businessDate.equals(startDate) || businessDate.after(startDate)) {
                                    DiscountPeriodsDB discountPeriodsDB = discountPeriods.toDiscountsPeriod();
                                    discountPeriodsDB.setDiscount_id(discountDB.getId());
                                    daoSession.insertOrReplace(discountPeriodsDB);
                                    isActive = true;
                                    break;

                                }
                            }
                        }

                        if (isActive) {
                            daoSession.insertOrReplace(discountDB);
                        }
                    }

                    daoSession.deleteAll(FolioTypeDB.class);
                    ArrayList<FolioTypeDB> folioTypeDBS = property.getFolioTypeDBS();
                    for (FolioTypeDB folioTypeDB : folioTypeDBS) {
                        daoSession.insertOrReplace(folioTypeDB);
                    }

                    ArrayList<OrderType> orderTypes = getOrderTypes(posApplication, propertyId, networkService);
                    if (orderTypes != null) {
                        daoSession.deleteAll(OrderTypeDB.class);

                        for (OrderType orderType : orderTypes) {
                            if (orderType.isActive()) {
                                OrderTypeDB orderTypeDB = orderType.toOrderTypeDB();
                                daoSession.insertOrReplace(orderTypeDB);
                            }
                        }
                    }

                    ArrayList<TableGroup> tableGroupArrayList = getTableGroups(posApplication, propertyId, networkService);
                    if (tableGroupArrayList != null) {
                        daoSession.deleteAll(TableGroupDB.class);

                        for (TableGroup tableGroup : tableGroupArrayList) {
                            if (tableGroup.isActive()) {
                                TableGroupDB tableGroupDB = tableGroup.toTableGroupDB();
                                daoSession.insertOrReplace(tableGroupDB);
                            }
                        }
                    }

                    ArrayList<MenuGroup> menuGroups = getMenuGroups(posApplication, propertyId, networkService);
                    if (menuGroups != null) {
                        daoSession.deleteAll(MenuGroupDB.class);
                        daoSession.deleteAll(MenuItemDB.class);

                        for (MenuGroup menuGroup : menuGroups) {
                            if (menuGroup.isActive() && menuGroup.isDisplay_on_order_taking()) {
                                MenuGroupDB menuGroupDB = menuGroup.toMenuGroupDB();
                                daoSession.insertOrReplace(menuGroupDB);

                                for (MenuItems menuItems : menuGroup.getMenuItems()) {
                                    if (menuItems.isActive()) {
                                        MenuItemDB menuItemDB = menuItems.toMenuItemDB();
                                        menuItemDB.setIsCustom(false);
                                        menuItemDB.setMenu_group_name(menuGroup.getName());
                                        menuItemDB.setMenuGroupID(menuGroup.getId());
                                        daoSession.insertOrReplace(menuItemDB);
                                    }
                                }
                            }
                        }
                    }

                    if (permissions.contains(PERMISSION_INVENTORY_SYSTEM)) {
                        ArrayList<DepartmentDB> departments = getDepartments(posApplication, propertyId, networkService);
                        if (departments != null) {
                            daoSession.deleteAll(DepartmentDB.class);

                            for (DepartmentDB departmentDB : departments) {
                                daoSession.insertOrReplace(departmentDB);
                            }
                        }
                    }
                }

                Shift shift = getShiftsAndOrderBatches(posApplication, propertyId, networkService);
                if (shift != null) {
                    getOrderBatches(posApplication, propertyId, networkService, shift.getId());
                }
            }

            ArrayList<Table> tableArrayList = getTables(posApplication, propertyId, networkService);

            ArrayList<OrderDB> orders = getOrders(posApplication, propertyId, networkService);

            if (stopServiceExecution) {
                throw new ServiceExecutionException();
            }

            if (tableArrayList != null && orders != null) {

                for (OrderDB order : orders) {
                    if (order.getTableId() == null || order.getTableId().isEmpty()) {
                        continue;
                    }

                    Date date = FormatUtils.toFormatedDate(order.getBusiness_date());
                    if (date == null || !date.equals(PreferenceUtils.getCBD(posApplication))) {
                        continue;
                    }

                    if (order.getStatus().equals(Constants.ORDER_STATUS_CANCELLED) || order.getStatus().equals(ORDER_STATUS_COMPLETED)) {
                        EventBus.getDefault().post(com.hotelkeyapp.android.pos.utils.ProgressEvent.orderStatus(order.getStatus(), order.getId()));
                        AccountPreferences.removeTableId(posApplication, order.getTableId());
                        daoSession.delete(order);
                        continue;
                    }


                    AccountPreferences.saveReservedTableIds(posApplication, order.getTableId(), order.getStatus());

                    order.setTableId(order.getTableId());
                    order.setServer_guest_name(order.getGuest_name());
                    order.setServer_persons(order.getPersons());
                    order.setServer_waiter_id(order.getWaiter_id());

                    if (order.getOrderItemsDBS() != null) {
                        ArrayList<OrderItemsDB> orderItems = order.getOrderItemsDBS();

                        for (OrderItemsDB orderItemsDB : orderItems) {
                            if (orderItemsDB.getChargeReqDB() != null) {
                                ChargeReqDB chargeReqDB = orderItemsDB.getChargeReqDB();
                                chargeReqDB.setFolio_id(orderItemsDB.getFolio_id());
                                orderItemsDB.setCharge_id(chargeReqDB.getId());
                                daoSession.insertOrReplace(chargeReqDB);

                                if (chargeReqDB.getLocalTaxes() != null && !chargeReqDB.getLocalTaxes().isEmpty()) {
                                    for (TaxReqDB taxReqDB : chargeReqDB.getLocalTaxes()) {
                                        taxReqDB.setChargeId(chargeReqDB.getId());
                                        daoSession.insertOrReplace(taxReqDB);
                                    }
                                }

                                Query<AdjustmentsDB> queryBuilder = daoSession.getAdjustmentsDBDao().queryBuilder()
                                        .where(AdjustmentsDBDao.Properties.Charge_id.eq(chargeReqDB.getId())).build();
                                if (queryBuilder != null && queryBuilder.list() != null && !queryBuilder.list().isEmpty()) {

                                    for (AdjustmentsDB adjustmentsDB : queryBuilder.list()) {
                                        Query<AdjustmentTaxDB> query = daoSession.getAdjustmentTaxDBDao().queryBuilder()
                                                .where(AdjustmentTaxDBDao.Properties.Adjustment_id.eq(adjustmentsDB.getId())).build();
                                        if (query != null && query.list() != null && !query.list().isEmpty()) {
                                            for (AdjustmentTaxDB adjustmentTaxDB : query.list()) {
                                                daoSession.delete(adjustmentTaxDB);
                                            }
                                        }
                                        daoSession.delete(adjustmentsDB);
                                    }
                                }

                                if (chargeReqDB.getAdjustments() != null && !chargeReqDB.getAdjustments().isEmpty()) {
                                    ArrayList<AdjustmentsDB> adjustments = chargeReqDB.getAdjustments();
                                    for (AdjustmentsDB adjustmentsDB : adjustments) {
                                        daoSession.insertOrReplace(adjustmentsDB);

                                        if (adjustmentsDB.getLocalTaxes() != null && !adjustmentsDB.getLocalTaxes().isEmpty()) {
                                            ArrayList<AdjustmentTaxDB> adjustmentTaxDBS = adjustmentsDB.getLocalTaxes();
                                            for (AdjustmentTaxDB adjustmentTaxDB : adjustmentTaxDBS) {
                                                if (adjustmentTaxDB != null && adjustmentsDB.getId() != null) {
                                                    adjustmentTaxDB.setAdjustment_id(adjustmentsDB.getId());
                                                    daoSession.insertOrReplace(adjustmentTaxDB);
                                                }
                                            }
                                        }
                                    }
                                }

                                if (orderItemsDB.getMenu_item_id() != null && orderItemsDB.getMenuItemDB() != null) {
                                    QueryBuilder<MenuItemDB> queryBuilder1 = daoSession.getMenuItemDBDao().queryBuilder()
                                            .where(MenuItemDBDao.Properties.Id.eq(orderItemsDB.getMenu_item_id()));
                                    if (queryBuilder1 == null || queryBuilder1.list() == null || queryBuilder1.list().isEmpty()) {
                                        MenuItemDB menuItemDB = orderItemsDB.getMenuItemDB();
                                        menuItemDB.setIsCustom(true);
                                        daoSession.insertOrReplace(menuItemDB);
                                    }
                                }
                            }

                            orderItemsDB.setFolio_id(orderItemsDB.getFolio_id());
                            orderItemsDB.setStatus(orderItemsDB.getStatus());
                            orderItemsDB.setOrderId(order.getId());
                            daoSession.insertOrReplace(orderItemsDB);
                        }

                        ArrayList<FolioReqDB> folioReqDBS = order.getFolioReqDBS();
                        for (FolioReqDB folioReqDB : folioReqDBS) {
                            folioReqDB.setOrderId(order.getId());
                            daoSession.insertOrReplace(folioReqDB);
                        }

                        order.setOrderItemsDBS(orderItems);
                        order.setId(order.getId());
                    }
                    daoSession.insertOrReplace(order);
                }

                daoSession.deleteAll(PropertyTableDB.class);

                for (Table table : tableArrayList) {
                    PropertyTableDB tableDB = table.toTableDB();
                    if (table.isActive()) {

                        if (!AccountPreferences.getTableStatusIfTableReserved(posApplication, table.getId()).isEmpty()) {
                            String tableStatus = AccountPreferences.getTableStatusIfTableReserved(posApplication, table.getId());
                            if (tableStatus.equals(ORDER_STATUS_OPEN)) {
                                tableDB.setTable_status(Constants.TABLE_STATUS_RESERVED);

                            } else if (tableStatus.equals(ORDER_STATUS_BILL_ISSUED)) {
                                tableDB.setTable_status(Constants.TABLE_STATUS_BILL_ISSUED);
                            }

                        } else if (table.isBlocked()) {
                            tableDB.setTable_status(Constants.TABLE_STATUS_BLOCKED);

                        } else if (table.isDirty()) {
                            tableDB.setTable_status(Constants.TABLE_STATUS_DIRTY);

                        } else {
                            tableDB.setTable_status(Constants.TABLE_STATUS_AVAILABLE);
                        }

                        daoSession.insertOrReplace(tableDB);
                    }
                }
                EventBus.getDefault().post(new com.hotelkeyapp.android.pos.utils.ProgressEvent(com.hotelkeyapp.android.pos.utils.ProgressEvent.ORDER_UPDATED));

            }

            stopServiceExecution = false;
            PreferenceUtils.saveAppReady(getApplication(), true);
            EventBus.getDefault().post(new ProgressEvent(ProgressEvent.IN_SUCCESS));

        } catch (ServiceExecutionException e) {
            stopServiceExecution = false;
            EventBus.getDefault().post(new ProgressEvent(ProgressEvent.IN_PROGRESS));

        } catch (SyncFailedException ex) {
            Log.e(TAG, ex.getMessage(), ex);
            EventBus.getDefault().post(ProgressEvent.error(ex.getMessage()));

        } catch (AuthenticationFailedException ex) {
            Log.i(TAG, "Received authentication Failed exception", ex);
            EventBus.getDefault().post(ProgressEvent.error(ex.getMessage()));

        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
            String message = MessageHelper.getMessageForError(POSApplication.getInstance(), e).first;
            EventBus.getDefault().post(ProgressEvent.error(message));

        }
    }

    private Shift getShiftsAndOrderBatches(POSApplication posApplication, String propertyId, POSNetworkService networkService) throws IOException, ServiceExecutionException {
        if (stopServiceExecution) {
            throw new ServiceExecutionException();
        }
        Call<Shift> shiftCall = networkService.getShifts(PreferenceUtils.getEntIdentifier(posApplication), propertyId);
        Response<Shift> shiftResponse = null;
        try {
            shiftResponse = shiftCall.execute();
            if (shiftResponse.isSuccessful() && shiftResponse.body() != null) {

                Shift shift = shiftResponse.body();
                if (shift.getStatus().equals("OPEN")) {
                    AccountPreferences.saveShiftID(posApplication, shiftResponse.body().getId());
                } else {
                    AccountPreferences.saveShiftID(posApplication, null);
                }
                retryCount = 0;

                Log.d(TAG, "Shift isSuccessful");
                return shiftResponse.body();

            } else if (shiftCall.isCanceled()) {
                Log.d(TAG, "Shift - isCanceled");

            } else {
                boolean retryReq = retryPolicy("Shift", shiftResponse);
                if (!retryReq) {
                    Log.d(TAG, "Shift - Server error " + retryCount);
                    getShiftsAndOrderBatches(posApplication, propertyId, networkService);
                }
                AccountPreferences.saveShiftID(posApplication, null);
            }

        } catch (IOException e) {
            boolean retryReq = retryWhenNoInternet();
            if (!retryReq) {
                Log.d(TAG, "Shift - Network error " + retryCount);
                getShiftsAndOrderBatches(posApplication, propertyId, networkService);

            } else {
                throw new IOException();
            }
        }

        return null;
    }

    private void getOrderBatches(POSApplication posApplication, String propertyId, POSNetworkService networkService, String id) throws IOException, ServiceExecutionException {
        if (stopServiceExecution) {
            throw new ServiceExecutionException();
        }
        Call<ArrayList<Shift>> orderBatches = networkService.getOrderBatches(PreferenceUtils.getEntIdentifier(posApplication), propertyId, id);
        Response<ArrayList<Shift>> response = null;

        try {
            response = orderBatches.execute();
            if (response.isSuccessful()) {

                if (response.body() != null && !response.body().isEmpty()) {
                    boolean isBatchOpen = false;

                    for (Shift orderBatch : response.body()) {
                        if (orderBatch.getShift_id().equals(AccountPreferences.getShiftID(posApplication))
                                && orderBatch.getStatus().equals("OPEN")) {
                            AccountPreferences.saveOrderBatchId(posApplication, orderBatch.getId());
                            isBatchOpen = true;
                        }
                    }

                    if (!isBatchOpen) {
                        AccountPreferences.saveOrderBatchId(posApplication, null);
                    }
                }
                retryCount = 0;
                Log.d(TAG, "Order Batch isSuccessful");

            } else if (orderBatches.isCanceled()) {
                Log.d(TAG, "Order Batch - isCanceled");

            } else {
                boolean retryReq = retryPolicy("Order Batch", response);
                if (!retryReq) {
                    Log.d(TAG, "Order Batch - Server Error " + retryCount);
                    getOrderBatches(posApplication, propertyId, networkService, id);
                }
            }

        } catch (IOException e) {
            boolean retryReq = retryWhenNoInternet();
            if (!retryReq) {
                Log.d(TAG, "Order Batch - Network Error " + retryCount);
                getOrderBatches(posApplication, propertyId, networkService, id);
            } else {
                throw new IOException();
            }
        }
    }

    private BusinessDay getBusinessDayDetails(POSApplication posApplication, String propertyId) throws AuthenticationFailedException, SyncFailedException, IOException, ServiceExecutionException {
        if (stopServiceExecution) {
            throw new ServiceExecutionException();
        }
        String region = PreferenceUtils.getPropertyRegion(posApplication);
        Call<BusinessDay> businessDayCall = posApplication.getNetworkServiceProvider().getCommonNetworkService(region).getCurrentBusinessDayDetails(PreferenceUtils.getEntIdentifier(posApplication), propertyId);
        Response<BusinessDay> businessDayResponse = null;

        try {
            businessDayResponse = businessDayCall.execute();

            if (businessDayResponse.isSuccessful()) {
                Date currentBusinessDate = businessDayResponse.body().getCurrentBusinessDate();
                String currentBusinessDateId = businessDayResponse.body().getId();

                AccountPreferences.saveBusinessDayID(posApplication, currentBusinessDateId);
                PreferenceUtils.savePropertyCBD(posApplication, FormatUtils.formatInventoryDate(currentBusinessDate));

                retryCount = 0;
                Log.d(TAG, "BusinessDay isSuccessful");
                return businessDayResponse.body();

            } else if (businessDayCall.isCanceled()) {
                Log.d(TAG, "BusinessDay - isCanceled");

            } else {
                ServerError exception = ExceptionHelper.parse(businessDayResponse);
                String message = MessageHelper.getMessageForError(posApplication, exception).first;

                boolean retryReq = retryPolicy("BusinessDay", businessDayResponse);
                if (!retryReq) {
                    Log.d(TAG, "BusinessDay - Server Error " + retryCount);
                    getBusinessDayDetails(posApplication, propertyId);

                } else {
                    throw new SyncFailedException(message);
                }
            }

        } catch (IOException e) {
            boolean retryReq = retryWhenNoInternet();
            if (!retryReq) {
                Log.d(TAG, "BusinessDay - Network Error " + retryCount);
                getBusinessDayDetails(posApplication, propertyId);
            } else {
                throw new IOException();
            }
        }

        return null;
    }

    private Property getPropertyDetails(POSApplication posApplication, String propertyId, POSNetworkService networkService) throws SyncFailedException, AuthenticationFailedException, IOException, ServiceExecutionException {
        if (stopServiceExecution) {
            throw new ServiceExecutionException();
        }
        Call<Property> propertyDetailsCall = networkService.getPropertyDetails(PreferenceUtils.getEntIdentifier(posApplication), propertyId);

        Response<Property> propertyDetailsResponse = null;
        try {
            propertyDetailsResponse = propertyDetailsCall.execute();
            if (propertyDetailsResponse.isSuccessful() && propertyDetailsResponse.body() != null) {
                retryCount = 0;
                Log.d(TAG, "Property isSuccessful");
                return propertyDetailsResponse.body();

            } else if (propertyDetailsCall.isCanceled()) {
                Log.d(TAG, "Property - isCanceled");

            } else {
                ServerError exception = ExceptionHelper.parse(propertyDetailsResponse);
                String message = MessageHelper.getMessageForError(posApplication, exception).first;

                boolean retryReq = retryPolicy("Property", propertyDetailsResponse);
                if (!retryReq) {
                    Log.d(TAG, "Property - Server Error " + retryCount);
                    getPropertyDetails(posApplication, propertyId, networkService);

                } else {
                    throw new SyncFailedException(message);
                }
            }

        } catch (IOException e) {
            boolean retryReq = retryWhenNoInternet();
            if (!retryReq) {
                Log.d(TAG, "Property - Network Error " + retryCount);
                getPropertyDetails(posApplication, propertyId, networkService);
            } else {
                throw new IOException();
            }
        }

        return null;
    }

    private ArrayList<OrderType> getOrderTypes(POSApplication posApplication, String propertyId, POSNetworkService networkService) throws SyncFailedException, AuthenticationFailedException, IOException, ServiceExecutionException {
        if (stopServiceExecution) {
            throw new ServiceExecutionException();
        }
        Call<ArrayList<OrderType>> orderTypes = networkService.getOrderTypes(PreferenceUtils.getEntIdentifier(posApplication), propertyId);

        Response<ArrayList<OrderType>> response = null;
        try {
            response = orderTypes.execute();
            if (response.isSuccessful()) {
                retryCount = 0;
                Log.d(TAG, "OrderType isSuccessful");
                return response.body();

            } else if (orderTypes.isCanceled()) {
                Log.d(TAG, "OrderType - isCanceled");

            } else {
                ServerError exception = ExceptionHelper.parse(response);
                String message = MessageHelper.getMessageForError(posApplication, exception).first;

                boolean retryReq = retryPolicy("OrderType", response);
                if (!retryReq) {
                    Log.d(TAG, "OrderType - Server Error " + retryCount);
                    getOrderTypes(posApplication, propertyId, networkService);

                } else {
                    throw new SyncFailedException(message);
                }
            }

        } catch (IOException e) {
            boolean retryReq = retryWhenNoInternet();
            if (!retryReq) {
                Log.d(TAG, "OrderType - Network Error " + retryCount);
                getOrderTypes(posApplication, propertyId, networkService);

            } else {
                throw new IOException();
            }
        }

        return null;
    }

    private ArrayList<TableGroup> getTableGroups(POSApplication posApplication, String propertyId, POSNetworkService networkService) throws AuthenticationFailedException, SyncFailedException, IOException, ServiceExecutionException {
        if (stopServiceExecution) {
            throw new ServiceExecutionException();
        }
        Call<ArrayList<TableGroup>> tableGroups = networkService.getTableGroups(PreferenceUtils.getEntIdentifier(posApplication), propertyId);

        Response<ArrayList<TableGroup>> propertyDetailsResponse = null;
        try {
            propertyDetailsResponse = tableGroups.execute();
            if (propertyDetailsResponse.isSuccessful()) {
                retryCount = 0;
                Log.d(TAG, "TableGroup isSuccessful");
                return propertyDetailsResponse.body();

            } else if (tableGroups.isCanceled()) {
                Log.d(TAG, "TableGroup - isCanceled");

            } else {
                ServerError exception = ExceptionHelper.parse(propertyDetailsResponse);
                String message = MessageHelper.getMessageForError(posApplication, exception).first;

                boolean retryReq = retryPolicy("TableGroup", propertyDetailsResponse);
                if (!retryReq) {
                    Log.d(TAG, "TableGroup - Server Error " + retryCount);
                    getTableGroups(posApplication, propertyId, networkService);

                } else {
                    throw new SyncFailedException(message);
                }
            }
        } catch (IOException e) {
            boolean retryReq = retryWhenNoInternet();
            if (!retryReq) {
                Log.d(TAG, "TableGroup - Network Error " + retryCount);
                getTableGroups(posApplication, propertyId, networkService);
            } else {
                throw new IOException();
            }
        }

        return null;
    }

    private ArrayList<MenuGroup> getMenuGroups(POSApplication posApplication, String propertyId, POSNetworkService networkService) throws AuthenticationFailedException, SyncFailedException, IOException, ServiceExecutionException {
        if (stopServiceExecution) {
            throw new ServiceExecutionException();
        }
        Call<ArrayList<MenuGroup>> menuGroups = networkService.getMenus(PreferenceUtils.getEntIdentifier(posApplication), propertyId);

        Response<ArrayList<MenuGroup>> listResponse = null;
        try {
            listResponse = menuGroups.execute();
            if (listResponse.isSuccessful()) {
                retryCount = 0;
                Log.d(TAG, "MenuGroup - isSuccessful");
                return listResponse.body();

            } else if (menuGroups.isCanceled()) {
                Log.d(TAG, "MenuGroup - isCanceled");

            } else {
                ServerError exception = ExceptionHelper.parse(listResponse);
                String message = MessageHelper.getMessageForError(posApplication, exception).first;

                boolean retryReq = retryPolicy("MenuGroup", listResponse);
                if (!retryReq) {
                    Log.d(TAG, "MenuGroup - Server Error " + retryCount);
                    getMenuGroups(posApplication, propertyId, networkService);

                } else {
                    throw new SyncFailedException(message);
                }
            }
        } catch (IOException e) {
            boolean retryReq = retryWhenNoInternet();
            if (!retryReq) {
                Log.d(TAG, "MenuGroup - Network Error " + retryCount);
                getMenuGroups(posApplication, propertyId, networkService);
            } else {
                throw new IOException();
            }
        }


        return null;
    }

    private ArrayList<DepartmentDB> getDepartments(POSApplication posApplication, String propertyId, POSNetworkService networkService) throws IOException, ServiceExecutionException {
        if (stopServiceExecution) {
            throw new ServiceExecutionException();
        }
        Call<ArrayList<DepartmentDB>> departments = networkService.getDepartments(PreferenceUtils.getEntIdentifier(posApplication), propertyId);
        Response<ArrayList<DepartmentDB>> listResponse = null;
        try {
            listResponse = departments.execute();
            if (listResponse.isSuccessful()) {
                retryCount = 0;
                Log.d(TAG, "DepartmentDB - isSuccessful");
                return listResponse.body();

            } else if (departments.isCanceled()) {
                Log.d(TAG, "DepartmentDB - isCanceled");

            } else {
                boolean retryReq = retryPolicy("DepartmentDB", listResponse);
                if (!retryReq) {
                    Log.d(TAG, "DepartmentDB - Server Error " + retryCount);
                    getDepartments(posApplication, propertyId, networkService);
                }
            }
        } catch (IOException e) {
            boolean retryReq = retryWhenNoInternet();
            if (!retryReq) {
                Log.d(TAG, "DepartmentDB - Network Error " + retryCount);
                getDepartments(posApplication, propertyId, networkService);
            } else {
                throw new IOException();
            }
        }

        return null;
    }

    private ArrayList<Table> getTables(POSApplication posApplication, String propertyId, POSNetworkService networkService) throws SyncFailedException, AuthenticationFailedException, IOException, ServiceExecutionException {
        if (stopServiceExecution) {
            throw new ServiceExecutionException();
        }
        Call<ArrayList<Table>> tablesCall = networkService.getTables(PreferenceUtils.getEntIdentifier(posApplication), propertyId);

        Response<ArrayList<Table>> propertyDetailsResponse = null;
        try {
            propertyDetailsResponse = tablesCall.execute();
            if (propertyDetailsResponse.isSuccessful()) {
                retryCount = 0;
                Log.d(TAG, "Table - isSuccessful");
                return propertyDetailsResponse.body();

            } else if (tablesCall.isCanceled()) {
                Log.d(TAG, "Table - isCanceled");

            } else {
                ServerError exception = ExceptionHelper.parse(propertyDetailsResponse);
                String message = MessageHelper.getMessageForError(posApplication, exception).first;

                boolean retryReq = retryPolicy("Table", propertyDetailsResponse);
                if (!retryReq) {
                    Log.d(TAG, "Table - Server Error " + retryCount);
                    getTables(posApplication, propertyId, networkService);

                } else {
                    throw new SyncFailedException(message);
                }
            }
        } catch (IOException e) {
            boolean retryReq = retryWhenNoInternet();
            if (!retryReq) {
                Log.d(TAG, "Table - Network Error " + retryCount);
                getTables(posApplication, propertyId, networkService);
            } else {
                throw new IOException();
            }
        }

        return null;
    }

    private ArrayList<OrderDB> getOrders(POSApplication posApplication, String propertyId, POSNetworkService networkService) throws SyncFailedException, AuthenticationFailedException, IOException, ServiceExecutionException {
        if (stopServiceExecution) {
            throw new ServiceExecutionException();
        }
        Long lastSync = AccountPreferences.getOrderLastSync(posApplication);
        if (lastSync == 0) {
            lastSync = null;
        }

        Call<ArrayList<OrderDB>> propertyDetailsCall = networkService.getOrders(PreferenceUtils.getEntIdentifier(posApplication), propertyId, lastSync);

        Response<ArrayList<OrderDB>> propertyDetailsResponse = null;
        try {
            propertyDetailsResponse = propertyDetailsCall.execute();
            if (propertyDetailsResponse.isSuccessful()) {

                retryCount = 0;
                Log.d(TAG, "Order - isSuccessful " + lastSync);

                Calendar calendar = Calendar.getInstance();
                TimeZone tz = TimeZone.getTimeZone(PreferenceUtils.getTimeZoneOffset(POSApplication.getInstance()));
                calendar.setTimeZone(tz);
                calendar.add(Calendar.SECOND, -60);
                Long currentLastSync = calendar.getTimeInMillis();

                AccountPreferences.saveOrderLastSync(posApplication, currentLastSync);
                return propertyDetailsResponse.body();

            } else if (propertyDetailsCall.isCanceled()) {
                Log.d(TAG, "Order - isCanceled");

            } else {
                ServerError exception = ExceptionHelper.parse(propertyDetailsResponse);
                String message = MessageHelper.getMessageForError(posApplication, exception).first;

                boolean retryReq = retryPolicy("Order", propertyDetailsResponse);
                if (!retryReq) {
                    Log.d(TAG, "Order - Server Error " + retryCount);
                    getOrders(posApplication, propertyId, networkService);

                } else {
                    throw new SyncFailedException(message);
                }
            }

        } catch (IOException e) {
            boolean retryReq = retryWhenNoInternet();
            if (!retryReq) {
                Log.d(TAG, "Order - Network Error " + retryCount);
                getOrders(posApplication, propertyId, networkService);
            } else {
                throw new IOException();
            }
        }


        return null;
    }

    private boolean retryPolicy(String apiName, Response<?> response) {
        int httpStatusCode = response.code();
        if (httpStatusCode == 401
                || httpStatusCode == 403
                || httpStatusCode == 424
                || httpStatusCode == 423
                || httpStatusCode == 412
                || httpStatusCode == 400
                || httpStatusCode == 404) {

            Log.d(TAG, apiName + " " + response.code());
            retryCount = 0;
            return true;

        } else if (retryCount >= 4) {
            retryCount = 0;
            return true;

        } else {
            threadSleepMode();
            retryCount++;
            return false;
        }
    }

    private boolean retryWhenNoInternet() {
        if (retryCount >= 4) {
            retryCount = 0;
            return true;

        } else {
            threadSleepMode();
            retryCount++;
            return false;
        }
    }

    private void threadSleepMode() {
        try {
            Thread.sleep(5000);
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage(), ex);
        }
    }

}

