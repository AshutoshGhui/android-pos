package com.hotelkeyapp.android.pos.db.orm;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
// KEEP INCLUDES END

/**
 * Entity mapped to table "MENU_ITEM_DB".
 */
public class MenuItemDB implements Parcelable, Comparable<MenuItemDB> {

    private String id;
    private String name;
    private String code;
    private Boolean active;
    private String description;
    private String image;
    private Double price;
    private String charge_type_id;
    private String menu_group_name;
    private String product_id;
    private Boolean isCustom;
    private String inventory_system_id;

    @SerializedName("menu_id")
    private String menuGroupID;

    // KEEP FIELDS - put your custom fields here
    private transient int quantity = 0;
    // KEEP FIELDS END

    public MenuItemDB() {
    }

    public MenuItemDB(String id) {
        this.id = id;
    }

    public MenuItemDB(String id, String name, String code, Boolean active, String description, String image, Double price, String charge_type_id, String menu_group_name, String product_id, Boolean isCustom, String inventory_system_id, String menuGroupID) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.active = active;
        this.description = description;
        this.image = image;
        this.price = price;
        this.charge_type_id = charge_type_id;
        this.menu_group_name = menu_group_name;
        this.product_id = product_id;
        this.isCustom = isCustom;
        this.inventory_system_id = inventory_system_id;
        this.menuGroupID = menuGroupID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getCharge_type_id() {
        return charge_type_id;
    }

    public void setCharge_type_id(String charge_type_id) {
        this.charge_type_id = charge_type_id;
    }

    public String getMenu_group_name() {
        return menu_group_name;
    }

    public void setMenu_group_name(String menu_group_name) {
        this.menu_group_name = menu_group_name;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public Boolean getIsCustom() {
        return isCustom;
    }

    public void setIsCustom(Boolean isCustom) {
        this.isCustom = isCustom;
    }

    public String getInventory_system_id() {
        return inventory_system_id;
    }

    public void setInventory_system_id(String inventory_system_id) {
        this.inventory_system_id = inventory_system_id;
    }

    public String getMenuGroupID() {
        return menuGroupID;
    }

    public void setMenuGroupID(String menuGroupID) {
        this.menuGroupID = menuGroupID;
    }

    // KEEP METHODS - put your custom methods here

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public int compareTo(@NonNull MenuItemDB menuItemDB) {
        return name.compareToIgnoreCase(menuItemDB.getName());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(code);
        parcel.writeByte((byte) (active == null ? 0 : active ? 1 : 2));
        parcel.writeString(description);
        parcel.writeString(image);
        if (price == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeDouble(price);
        }
        parcel.writeString(charge_type_id);
        parcel.writeString(menu_group_name);
        parcel.writeString(product_id);
        parcel.writeByte((byte) (isCustom == null ? 0 : isCustom ? 1 : 2));
        parcel.writeString(menuGroupID);
    }

    protected MenuItemDB(Parcel in) {
        id = in.readString();
        name = in.readString();
        code = in.readString();
        byte tmpActive = in.readByte();
        active = tmpActive == 0 ? null : tmpActive == 1;
        description = in.readString();
        image = in.readString();
        if (in.readByte() == 0) {
            price = null;
        } else {
            price = in.readDouble();
        }
        charge_type_id = in.readString();
        menu_group_name = in.readString();
        product_id = in.readString();
        byte tmpIsCustom = in.readByte();
        isCustom = tmpIsCustom == 0 ? null : tmpIsCustom == 1;
        menuGroupID = in.readString();
    }

    public static final Creator<MenuItemDB> CREATOR = new Creator<MenuItemDB>() {
        @Override
        public MenuItemDB createFromParcel(Parcel in) {
            return new MenuItemDB(in);
        }

        @Override
        public MenuItemDB[] newArray(int size) {
            return new MenuItemDB[size];
        }
    };
    // KEEP METHODS END

}
