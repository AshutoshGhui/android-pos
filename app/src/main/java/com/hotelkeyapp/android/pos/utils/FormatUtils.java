package com.hotelkeyapp.android.pos.utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.util.TypedValue;

import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkeyapp.android.pos.BuildConfig;
import com.hotelkeyapp.android.pos.POSApplication;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by vrushalimankar on 10/05/18.
 */

public class FormatUtils {

    private static final String DISPLAY_DATE_FORMAT = "MMM dd, yyyy";
    private static final String INPUT_DATE_FORMAT = "yyyy-MM-dd";
    public static final String SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private static final String CREATED_AT_FORMAT = "hh:mm a";

    public static String[] selectUserRegion() {
        if (BuildConfig.LOCALISATION && PreferenceUtils.getCurrentUser(POSApplication.getInstance()) != null) {
            String langCountry = PreferenceUtils.getCurrentUser(POSApplication.getInstance()).getLanguageCode();
            if (langCountry != null && !langCountry.isEmpty()) {
                String[] parts = langCountry.split("_");
                String lang = parts[0];
                String region = parts[1];
                return new String[]{lang, region};

            }
            return new String[]{"en", "US"};
        } else {
            return new String[]{"en", "US"};
        }
    }

    public static String formatInventoryDate(Date date) {
        String[] locale = selectUserRegion();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DISPLAY_DATE_FORMAT, new Locale(locale[0], locale[1]));
        return dateFormat.format(date.getTime());
    }

    public static String parseDate(Date dateString) throws ParseException {
        String[] locale = selectUserRegion();
        SimpleDateFormat dateFormat = new SimpleDateFormat(INPUT_DATE_FORMAT, new Locale(locale[0], locale[1]));
        return dateFormat.format(dateString);
    }

    public static Date toFormatedDate(String date) {
        SimpleDateFormat curFormatter = new SimpleDateFormat(SERVER_DATE_FORMAT, Locale.US);
        Date dateObj = null;
        try {
            dateObj = curFormatter.parse(date);
        } catch (ParseException e) {
            Log.e(FormatUtils.class.getSimpleName(), e.getMessage(), e);
        }
        return dateObj;
    }

    public static String formatBillDate() {
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        TimeZone timeZone = TimeZone.getTimeZone(PreferenceUtils.getTimeZoneOffset(POSApplication.getInstance()));
        simpleDateFormat1.setTimeZone(timeZone);
        return simpleDateFormat1.format(System.currentTimeMillis());
    }

    public static String formatRequestDate(Date date) {
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00.000Z", Locale.US);
        TimeZone tz = TimeZone.getTimeZone("GMT");
        simpleDateFormat1.setTimeZone(tz);
        return simpleDateFormat1.format(date.getTime());
    }

    public static String formatDateWithDeviceTimezone() {
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(SERVER_DATE_FORMAT, Locale.US);
        TimeZone tz = TimeZone.getTimeZone("GMT");
        simpleDateFormat1.setTimeZone(tz);
        return simpleDateFormat1.format(System.currentTimeMillis());
    }

    public static String formatElapsedTime(String stringDate) {
        TimeZone timeZone = TimeZone.getTimeZone(PreferenceUtils.getTimeZoneOffset(POSApplication.getInstance()));

        DateFormat parseFormat = new SimpleDateFormat(SERVER_DATE_FORMAT);
        Date dt = null;
        try {
            dt = parseFormat.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat printFormat = new SimpleDateFormat(CREATED_AT_FORMAT);
        printFormat.setTimeZone(timeZone);
        return printFormat.format(dt);
    }

    public static String convertTimeStamp(Double deviceCreatedAt) {
        long timeStamp = new BigDecimal(deviceCreatedAt).longValue() * 1000L;

        TimeZone tz = TimeZone.getTimeZone("GMT");
        Calendar c = Calendar.getInstance(tz);
        long currentTime = c.getTimeInMillis();

        long elapsedTime = currentTime - timeStamp;

        String[] locale = selectUserRegion();
        Date date = new Date(elapsedTime);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("HH:mm", new Locale(locale[0], locale[1]));
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return sdf.format(date);
    }

    private static String[] selectPropertyRegion() {
        if (BuildConfig.LOCALISATION) {
            String langCountry = PreferenceUtils.getPropLanguage(POSApplication.getInstance());
            if (langCountry != null && !langCountry.isEmpty()) {
                String[] parts = langCountry.split("_");
                String lang = parts[0];
                String region = parts[1];
                return new String[]{lang, region, langCountry};

            }
            return new String[]{"en", "US", "en_US"};
        } else {
            return new String[]{"en", "US", "en_US"};
        }
    }

    public static String getCurrencySymbol() {
        String[] propLang = selectPropertyRegion();
        Currency currency = Currency.getInstance(new Locale(propLang[0], propLang[1]));
        return currency.getSymbol();
    }

    public static String getCurrencyCode() {
        String[] propLang = selectPropertyRegion();
        Currency currency = Currency.getInstance(new Locale(propLang[0], propLang[1]));
        return currency.getCurrencyCode();
    }

    public static int convertDpToPixels(float dp, Context context) {
        Resources resources = context.getResources();
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                resources.getDisplayMetrics()
        );
    }
}
