package com.hotelkeyapp.android.pos.fcm;

import android.util.Log;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.hotelkey.hkandroidlib.HKAndroidApplication;
import com.hotelkey.hkandroidlib.db.orm.PropertyDB;
import com.hotelkey.hkandroidlib.models.SwitchPropertyReq;
import com.hotelkey.hkandroidlib.models.User;
import com.hotelkey.hkandroidlib.network.CommonNetworkService;
import com.hotelkey.hkandroidlib.utils.Constants;
import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkey.hkandroidlib.utils.RetrofitCallback;
import com.hotelkey.hkandroidlib.utils.StringUtils;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        if (refreshedToken == null || StringUtils.isEmpty(refreshedToken)) {
            return;
        }

        Log.e("Token", refreshedToken);
        PreferenceUtils.saveToken(getApplicationContext(), refreshedToken);

        HKAndroidApplication application = (HKAndroidApplication) getApplicationContext();

        PropertyDB currentMiniProperty = application.getCurrentProperty();

        User currentUser = application.getCurrentUser();

        if (currentMiniProperty == null || currentUser == null) {
            return;
        }

        CommonNetworkService commonNetworkService = application.getNetworkServiceProvider().getCommonNetworkService(Constants.TYPE_ENTERPRISE);

        SwitchPropertyReq switchPropertyReq = new SwitchPropertyReq();
        switchPropertyReq.setFromPropertyId(currentMiniProperty.getId());
        switchPropertyReq.setNewPropertyId(currentMiniProperty.getId());

        Call<ResponseBody> responseBodyCall = commonNetworkService.switchProperty(PreferenceUtils.getEntIdentifier(getApplication()), switchPropertyReq);

        responseBodyCall.enqueue(switchPropertyCallbacks);

    }

    private RetrofitCallback<ResponseBody> switchPropertyCallbacks = new RetrofitCallback<ResponseBody>() {

        @Override
        public void onResponse(ResponseBody body) {

            try {
                Toast.makeText(getApplicationContext(), "Device setting updated", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onPasswordChanged(ResponseBody errorBody) throws IOException {
        }

        @Override
        public void validate2StepsAuth(ResponseBody responseBody, boolean b) throws IOException {
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {
            try {
                Toast.makeText(getApplicationContext(), "Failed to update device settings", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
}
