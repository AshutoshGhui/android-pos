package com.hotelkeyapp.android.pos.activity.common;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.login.HKLoginActivity;

public class HKLauncherActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hklauncher);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                postTimerAction();
            }
        }, 1000);
    }

    private void postTimerAction() {
        Intent mainIntent = new Intent(this, HKLoginActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        this.startActivity(mainIntent);
        this.finish();
    }
}
