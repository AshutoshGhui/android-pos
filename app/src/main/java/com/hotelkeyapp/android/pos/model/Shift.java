package com.hotelkeyapp.android.pos.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vrushalimankar on 22/05/18.
 */

public class Shift {

    @SerializedName("id")
    private String id;

    @SerializedName("shift_id")
    private String shift_id;

    @SerializedName("status")
    private String status;

    @SerializedName("user_id")
    private String user_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShift_id() {
        return shift_id;
    }

    public void setShift_id(String shift_id) {
        this.shift_id = shift_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
