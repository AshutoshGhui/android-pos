package com.hotelkeyapp.android.pos.controllers;

import android.util.Pair;

import com.hotelkey.hkandroidlib.common.BaseController;
import com.hotelkey.hkandroidlib.exceptions.ServerError;
import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.db.orm.DaoSession;
import com.hotelkeyapp.android.pos.db.orm.WaiterDB;
import com.hotelkeyapp.android.pos.model.Waiter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vrushalimankar on 14/05/18.
 */

public class WaitersController extends BaseController {

    public interface WaitersControllerData {
        void onFailure(Pair<String, Boolean> message);

        void onSuccess(List<Waiter> waiters);
    }

    private WaitersControllerData waitersControllerData;

    public WaitersController(WaitersControllerData waitersControllerData) {
        this.waitersControllerData = waitersControllerData;
    }

    public void downloadWaitersList(String propertyId) {

        final POSApplication posApplication = POSApplication.getInstance();

        String userID = PreferenceUtils.getCurrentUser(posApplication).getId();
        Call<ArrayList<Waiter>> property = POSApplication.getInstance().getNetworkService()
                .getWaiterList(PreferenceUtils.getEntIdentifier(posApplication), propertyId, userID, "WAITER");

        property.enqueue(new Callback<ArrayList<Waiter>>() {
            @Override
            public void onResponse(Call<ArrayList<Waiter>> call, Response<ArrayList<Waiter>> response) {
                if (response.isSuccessful()) {
                    DaoSession daoSession = posApplication.getDaoSession();
                    daoSession.deleteAll(WaiterDB.class);

                    for (Waiter waiter : response.body()) {
                        if (waiter.isActive()) {
                            WaiterDB waiterDB = waiter.toWaiterDB();
                            daoSession.insertOrReplace(waiterDB);
                        }
                    }
                    waitersControllerData.onSuccess(response.body());
                } else {
                    onFailure(call, new ServerError(response.code(), response.errorBody().toString()));
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Waiter>> call, Throwable t) {
                waitersControllerData.onFailure(getErrorMessage(t));
            }
        });

    }

}
