package com.hotelkeyapp.android.pos.activity.orderDetails;

import android.os.AsyncTask;

import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.db.orm.AdjustmentTaxDB;
import com.hotelkeyapp.android.pos.db.orm.AdjustmentTaxDBDao;
import com.hotelkeyapp.android.pos.db.orm.AdjustmentsDB;
import com.hotelkeyapp.android.pos.db.orm.AdjustmentsDBDao;
import com.hotelkeyapp.android.pos.db.orm.ChargeReqDB;
import com.hotelkeyapp.android.pos.db.orm.ChargeReqDBDao;
import com.hotelkeyapp.android.pos.db.orm.DaoSession;
import com.hotelkeyapp.android.pos.db.orm.FolioReqDB;
import com.hotelkeyapp.android.pos.db.orm.FolioReqDBDao;
import com.hotelkeyapp.android.pos.db.orm.OrderDB;
import com.hotelkeyapp.android.pos.db.orm.OrderDBDao;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDB;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDBDao;
import com.hotelkeyapp.android.pos.db.orm.PropertyTableDB;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.dao.query.Query;
import de.greenrobot.dao.query.QueryBuilder;

import static com.hotelkeyapp.android.pos.utils.Constants.ORDER_STATUS_BILL_ISSUED;
import static com.hotelkeyapp.android.pos.utils.Constants.ORDER_STATUS_OPEN;

/**
 * Created by vrushalimankar on 04/07/18.
 */

public class OrderAsyncTask extends AsyncTask<PropertyTableDB, Void, OrderDB> {

    public interface OrderCallback {
        void orderCallback(OrderDB orderDB, boolean updateOrder);

        void orderModified(OrderDB orderDB);
    }

    private boolean isOrderModified = false;

    private OrderCallback orderCallback;

    OrderAsyncTask(OrderCallback orderCallback, boolean isOrderModified) {
        this.orderCallback = orderCallback;
        this.isOrderModified = isOrderModified;
    }

    private boolean updateOrder = false;

    @Override
    protected OrderDB doInBackground(PropertyTableDB... propertyTableDBS) {

        OrderDB orderDB = null;
        PropertyTableDB propertyTableDB = propertyTableDBS[0];

        DaoSession daoSession = POSApplication.getInstance().getDaoSession();
        if (propertyTableDB != null && propertyTableDB.getId() != null) {
            QueryBuilder<OrderDB> builder = daoSession.getOrderDBDao().queryBuilder();
            builder.where(OrderDBDao.Properties.TableId.eq(propertyTableDB.getId()));

            if (builder.list() != null && !builder.list().isEmpty()) {
                List<OrderDB> orderDBS = builder.list();
                for (OrderDB orderDB1 : orderDBS) {
                    if (orderDB1.getStatus().equals(ORDER_STATUS_OPEN)
                            || orderDB1.getStatus().equals(ORDER_STATUS_BILL_ISSUED)) {
                        orderDB = orderDB1;
                        break;
                    }
                }
            }

            if (orderDB == null) {
                return null;
            }

            OrderItemsDBDao itemsDBDao = daoSession.getOrderItemsDBDao();
            Query<OrderItemsDB> query = itemsDBDao.queryBuilder()
                    .where(OrderItemsDBDao.Properties.OrderId.eq(orderDB.getId())).build();

            ArrayList<OrderItemsDB> orderItemsDBList = null;
            if (query != null && query.list() != null && !query.list().isEmpty()) {

                itemsDBDao.detach(query.list().get(0));
                query.setParameter(0, orderDB.getId());
                orderItemsDBList = (ArrayList<OrderItemsDB>) query.list();
            }

            if (orderItemsDBList == null) {
                orderDB.setOrderItemsDBS(new ArrayList<OrderItemsDB>());
            } else {
                orderDB.setOrderItemsDBS(orderItemsDBList);
            }

            if (orderDB.getOrderItemsDBS() == null || orderDB.getOrderItemsDBS().isEmpty()) {
                return orderDB;
            }

            for (OrderItemsDB orderItemsDB : orderDB.getOrderItemsDBS()) {
                ChargeReqDBDao chargeReqDBDao = daoSession.getChargeReqDBDao();
                Query<ChargeReqDB> chargeReqDBQuery = chargeReqDBDao.queryBuilder()
                        .where(ChargeReqDBDao.Properties.Id.eq(orderItemsDB.getCharge_id())).build();

                if (chargeReqDBQuery != null && chargeReqDBQuery.list() != null && !chargeReqDBQuery.list().isEmpty()) {
                    chargeReqDBDao.detach(chargeReqDBQuery.list().get(0));
                    chargeReqDBQuery.setParameter(0, orderItemsDB.getCharge_id());

                    ChargeReqDB chargeReqDB = chargeReqDBQuery.list().get(0);
                    chargeReqDB.setFolio_id(orderItemsDB.getFolio_id());
                    orderItemsDB.setChargeReqDB(chargeReqDB);

//                            Query<TaxReqDB> taxReqDBQuery = daoSession.getTaxReqDBDao().queryBuilder()
//                                    .where(TaxReqDBDao.Properties.ChargeId.eq(orderItemsDB.getCharge_id())).build();
//                            if (taxReqDBQuery != null && taxReqDBQuery.list() != null && !taxReqDBQuery.list().isEmpty()) {
                    chargeReqDB.setLocalTaxes(new ArrayList<>(chargeReqDB.getTaxes()));
//                            }

                    AdjustmentsDBDao adjustmentsDBDao = daoSession.getAdjustmentsDBDao();
                    Query<AdjustmentsDB> adjustmentsDBQuery = adjustmentsDBDao.queryBuilder()
                            .where(AdjustmentsDBDao.Properties.Charge_id.eq(chargeReqDB.getId())).build();
                    if (adjustmentsDBQuery != null  && adjustmentsDBQuery.list() != null && !adjustmentsDBQuery.list().isEmpty()) {

                        adjustmentsDBDao.detach(adjustmentsDBQuery.list().get(0));
                        adjustmentsDBQuery.setParameter(0, chargeReqDB.getId());

                        List<AdjustmentsDB> adjustmentsDBS = adjustmentsDBQuery.list();

                        for (AdjustmentsDB adjustmentsDB : adjustmentsDBS) {
                            Query<AdjustmentTaxDB> taxDBQuery = daoSession.getAdjustmentTaxDBDao().queryBuilder()
                                    .where(AdjustmentTaxDBDao.Properties.Adjustment_id.eq(adjustmentsDB.getId())).build();

                            if (taxDBQuery != null && taxDBQuery.list() != null && !taxDBQuery.list().isEmpty()) {
                                adjustmentsDB.setLocalTaxes(new ArrayList<>(taxDBQuery.list()));
                            }
                        }

                        chargeReqDB.setAdjustments(new ArrayList<>(adjustmentsDBS));
                    }
                }
            }
//                }

            Query<FolioReqDB> qb = daoSession.getFolioReqDBDao().queryBuilder()
                    .where(FolioReqDBDao.Properties.OrderId.eq(orderDB.getId())).build();
            if (qb != null && qb.list() != null && !qb.list().isEmpty()) {
                orderDB.setFolioReqDBS(new ArrayList<>(qb.list()));
            }
            updateOrder = true;

        }

        return orderDB;
    }

    @Override
    protected void onPostExecute(OrderDB orderDB) {
        super.onPostExecute(orderDB);
        if (isOrderModified) {
            orderCallback.orderModified(orderDB);
        } else {
            orderCallback.orderCallback(orderDB, updateOrder);
        }
    }
}
