package com.hotelkeyapp.android.pos.utils;

import java.util.HashSet;

/**
 * Created by vrushalimankar on 10/05/18.
 */

public class Constants {

    public static final int TYPE_ITEM = 1;
    public static final int TYPE_SECTION = 0;
    public static final int TYPE_FOOTER = 2;

    public static final String PERMISSION_NONE = "NONE";
    public static final String PERMISSION_READ_WRITE = "READ_WRITE";
    public static final String PERMISSION_READ = "READ";

    public static final String PERMISSION_MODULE_POS = "POS";
    public static final String PERMISSION_MODULE_INVENTORY_SYSTEM = "PROPERTY_INVENTORY_SYSTEMS";

    public static final String PERMISSION_DELETE_ORDER = "SPL_POS_CANCEL_ORDER";
    public static final String PERMISSION_APPLY_DISCOUNT = "SPL_POS_APPLY_DISCOUNT";
    public static final String PERMISSION_CUSTOM_ORDER = "SPL_POS_ALLOW_CUSTOM_ORDER";
    public static final String PERMISSION_INVENTORY_SYSTEM = "INVENTORY_SYSTEM";
    public static final String PERMISSION_ORDER_MODULE = "SPL_POS_ORDER_MODULE";


    public static final HashSet<String> PERMISSION_MODULES = new HashSet<>();
    public static final String UPDATED_ORDER_ITEMS = "UPDATED ORDER ITEM WITH DISCOUNTS";

    static {
        PERMISSION_MODULES.add(PERMISSION_MODULE_POS);
        PERMISSION_MODULES.add(PERMISSION_MODULE_INVENTORY_SYSTEM);
    }

    //ORDER STATUS
    public static final String ORDER_STATUS_OPEN = "OPEN";
    public static final String ORDER_STATUS_BILL_ISSUED = "BILL_ISSUED";
    public static final String ORDER_STATUS_COMPLETED = "COMPLETED";
    public static final String ORDER_STATUS_CANCELLED = "CANCELLED";

    //TABLE STATUS
    public static final String TABLE_STATUS_DIRTY = "DIRTY";
    public static final String TABLE_STATUS_BLOCKED = "BLOCKED";
    public static final String TABLE_STATUS_RESERVED = "RESERVED";
    public static final String TABLE_STATUS_AVAILABLE = "AVAILABLE";
    public static final String TABLE_STATUS_BILL_ISSUED = "BILL_ISSUED";

    public static final String UPDATE_ORDERS_TABLES = "Request sync for updating orders and tables";
    public static final String ARG_CUSTOM_ORDER_ITEM = "CUSTOM ORDER ITEM";
    public static final String ARG_UPDATED_ORDER_ITEM = "UPDATED ORDER ITEM";
    public static final String ARG_ORDER_ITEMS = "NEW ORDER ITEMS";
    public static final String ARG_ORDER = "ORDER";
    public static final String ARG_WAITER_NAME = "WAITER NAME";
    public static final String ARG_MESSAGE = "ERROR MESSAGE";


    //CHARGE STATUS
    public static final String CHARGE_STATUS_PENDING = "PENDING";
    public static final String CHARGE_STATUS_CONFIRMED = "CONFIRMED";
    public static final String CHARGE_STATUS_CANCELLED = "CANCELLED";
    public static final String CHARGE_STATUS_USER_CANCELLED = "USER_CANCELLED";


    //PUSH EVENTS
    public static final String EVENT_NIGHT_AUDIT_COMPLETED = "NIGHT_AUDIT_COMPLETED";
    public static final String EVENT_ORDER_ITEM_MINOR_UPDATE = "ORDER_ITEM_MINOR_UPDATE";
    public static final String EVENT_ORDER_UPDATED = "ORDER_UPDATED";
    public static final String EVENT_SHIFT_UPDATED = "SHIFT_UPDATED";
    public static final String EVENT_TABLE_UPDATED = "TABLE_UPDATED";
}
