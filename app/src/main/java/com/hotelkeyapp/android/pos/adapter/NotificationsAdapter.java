package com.hotelkeyapp.android.pos.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.db.orm.NotificationDB;
import com.hotelkeyapp.android.pos.db.orm.OrderDB;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDB;
import com.hotelkeyapp.android.pos.db.orm.PropertyTableDB;
import com.hotelkeyapp.android.pos.utils.FormatUtils;
import com.hotelkeyapp.android.pos.utils.LocalizationConstants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * Created by vrushalimankar on 23/05/18.
 */

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ItemViewHolder> {

    private static String TODO = "TODO";
    private static String IN_PROGRESS = "IN_PROGRESS";
    private static String DONE = "DONE";
    private static String COMPLETED = "COMPLETED";
    private static String OPEN = "OPEN";
    private static String CANCELLED = "CANCELLED";
    private static String DISPATCHED = "DISPATCHED";
    private static String IN_DELIVERY = "IN_DELIVERY";

    private List<NotificationDB> mItems;

    private HashMap<String, OrderItemsDB> orderItemsDBHashMap;

    private HashMap<String, OrderDB> orderDBHashMap;

    private HashMap<String, PropertyTableDB> propertyTableDBHashMap;

    private Context mContext;

    public NotificationsAdapter(Context context,
                                List<NotificationDB> notifications,
                                HashMap<String, OrderItemsDB> orderItemsDBHashMap,
                                HashMap<String, OrderDB> orderDBHashMap,
                                HashMap<String, PropertyTableDB> propertyTableDBHashMap) {

        this.mContext = context;
        this.mItems = notifications;
        this.orderItemsDBHashMap = orderItemsDBHashMap;
        this.orderDBHashMap = orderDBHashMap;
        this.propertyTableDBHashMap = propertyTableDBHashMap;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_notification_row, viewGroup, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        if (mItems != null && position < mItems.size()) {
            NotificationDB notificationDB = mItems.get(position);

            String itemStatus = notificationDB.getStatus();
            String sequenceNo = "", displayLabel = "", tableName = "";

            if (notificationDB.getOrder_id() != null) {
                if (orderDBHashMap != null && orderDBHashMap.containsKey(notificationDB.getOrder_id())) {
                    sequenceNo = String.valueOf(orderDBHashMap.get(notificationDB.getOrder_id()).getSequence_number());
                }
            }

            SpannableString displaySpannable = new SpannableString(displayLabel);
            if (notificationDB.getOrder_item_id() != null) {
                if (orderItemsDBHashMap != null && orderItemsDBHashMap.containsKey(notificationDB.getOrder_item_id())) {
                    displayLabel = orderItemsDBHashMap.get(notificationDB.getOrder_item_id()).getDisplay_label();
                    int color = ContextCompat.getColor(mContext, R.color.colorBlackVariant);
                    displaySpannable = new SpannableString(displayLabel);
                    final ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(color);
                    if (!displayLabel.isEmpty()) {
                        displaySpannable.setSpan(foregroundColorSpan, 0, displayLabel.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                    }
                }
            }

            SpannableString spannable = new SpannableString(tableName);
            if (notificationDB.getTable_id() != null) {
                if (propertyTableDBHashMap != null && propertyTableDBHashMap.containsKey(notificationDB.getTable_id())) {
                    tableName = propertyTableDBHashMap.get(notificationDB.getTable_id()).getName();

                    spannable = new SpannableString(tableName);
                    int color = ContextCompat.getColor(mContext, R.color.colorPrimary);

                    final ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(color);
                    final StyleSpan styleSpan = new StyleSpan(Typeface.ITALIC);

                    spannable.setSpan(foregroundColorSpan, 0, tableName.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                    spannable.setSpan(styleSpan, 0, tableName.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                }
            }

            holder.txvSeqNo.setText(sequenceNo);
            boolean isItemUpdated = notificationDB.getItem_recently_updated();
            if (isItemUpdated) {
                holder.llSeqNo.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_circular));
            } else {
                holder.llSeqNo.setBackground(ContextCompat.getDrawable(mContext, R.drawable.bg_circular_color_primary));
            }

            SpannableStringBuilder notificationTitle = new SpannableStringBuilder();

            String backText = String.format(" is %s ", showStatus(itemStatus));

            notificationTitle.append(displaySpannable)
                    .append(" for ")
                    .append(spannable)
                    .append(backText);

            holder.txvItemName.setText(String.format("%s", displayLabel));
            holder.txvTableName.setText(tableName);
            holder.txvItemStatus.setText(String.format("%s", showStatus(itemStatus)));
            holder.txvTimeElapsed.setText(FormatUtils.formatElapsedTime(notificationDB.getCreated_at()));
        }
    }

    @Override
    public int getItemCount() {
        if (mItems != null) {
            return mItems.size();
        }
        return 0;
    }

    public void refresh(List<NotificationDB> notificationDBS, HashMap<String, OrderItemsDB> orderItemsDBHashMap,
                        HashMap<String, OrderDB> orderDBHashMap, HashMap<String, PropertyTableDB> propertyTableDBHashMap) {

        this.mItems = notificationDBS;
        this.orderItemsDBHashMap = orderItemsDBHashMap;
        this.orderDBHashMap = orderDBHashMap;
        this.propertyTableDBHashMap = propertyTableDBHashMap;

        if (mItems != null && !mItems.isEmpty()) {
            Collections.sort(mItems, new Comparator<NotificationDB>() {
                @Override
                public int compare(NotificationDB o1, NotificationDB o2) {
                    try {
                        return new SimpleDateFormat(FormatUtils.SERVER_DATE_FORMAT).parse(o2.getCreated_at())
                                .compareTo(new SimpleDateFormat(FormatUtils.SERVER_DATE_FORMAT).parse(o1.getCreated_at()));

                    } catch (ParseException e) {
                        e.printStackTrace();
                        return 0;
                    }
                }
            });
            notifyDataSetChanged();
        }
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView txvSeqNo, txvItemName, txvTimeElapsed, txvTableName, txvItemStatus;
        LinearLayout llSeqNo;

        ItemViewHolder(View view) {
            super(view);
            txvSeqNo = view.findViewById(R.id.txv_sequence_no);
            txvItemName = view.findViewById(R.id.txv_item_name);
            txvTableName = view.findViewById(R.id.txv_table_name);
            txvItemStatus = view.findViewById(R.id.txv_item_status);
            txvTimeElapsed = view.findViewById(R.id.txv_elapsed_time);
            llSeqNo = view.findViewById(R.id.ll_sequence_no);
        }
    }


    private String showStatus(String status) {
        String order_status = "";
        if (Objects.equals(status, DONE)) {
            order_status = LocalizationConstants.getInstance().getKeyValue("btn_done_uppercase");
        } else if (Objects.equals(status, TODO)) {
            order_status = LocalizationConstants.getInstance().getKeyValue("lbl_order_stat_todo_uppercase");
        } else if (Objects.equals(status, IN_PROGRESS)) {
            order_status = LocalizationConstants.getInstance().getKeyValue("lbl_order_stat_in_progress");
        } else if (Objects.equals(status, OPEN)) {
            order_status = LocalizationConstants.getInstance().getKeyValue("lbl_order_stat_open");
        } else if (Objects.equals(status, IN_DELIVERY)) {
            order_status = LocalizationConstants.getInstance().getKeyValue("lbl_order_stat_in_delivery");
        } else if (Objects.equals(status, COMPLETED)) {
            order_status = LocalizationConstants.getInstance().getKeyValue("lbl_order_stat_completed");
        } else if (Objects.equals(status, CANCELLED)) {
            order_status = LocalizationConstants.getInstance().getKeyValue("lbl_order_stat_cancelled");
        } else if (Objects.equals(status, DISPATCHED)) {
            order_status = LocalizationConstants.getInstance().getKeyValue("lbl_order_stat_dispatched");
        }
        return order_status;

    }
}
