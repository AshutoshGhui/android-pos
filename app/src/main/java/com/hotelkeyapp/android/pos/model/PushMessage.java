package com.hotelkeyapp.android.pos.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PushMessage implements Parcelable {
    @SerializedName("property_id")
    private String propertyId;

    @SerializedName("event")
    private String event;

    @SerializedName("sender_token_id")
    private String senderTokenId;

    @SerializedName("hk_message_id")
    private String messageId;

    @SerializedName("message_to_alert")
    private String alertMessage;

    @SerializedName("object_id")
    private ArrayList<String> object_id;

    @SerializedName("message_base64_content")
    private String messageBase64Content;

    protected PushMessage(Parcel in) {
        propertyId = in.readString();
        event = in.readString();
        senderTokenId = in.readString();
        messageId = in.readString();
        alertMessage = in.readString();
        object_id = in.createStringArrayList();
        messageBase64Content = in.readString();
    }

    public static final Creator<PushMessage> CREATOR = new Creator<PushMessage>() {
        @Override
        public PushMessage createFromParcel(Parcel in) {
            return new PushMessage(in);
        }

        @Override
        public PushMessage[] newArray(int size) {
            return new PushMessage[size];
        }
    };

    public String getPropertyId() {
        return propertyId;
    }

    public String getEvent() {
        return event;
    }

    public String getSenderTokenId() {
        return senderTokenId;
    }

    public String getMessageId() {
        return messageId;
    }

    public ArrayList<String> getObject_id() {
        return object_id;
    }

    public String getAlertMessage() {
        return alertMessage;
    }

    public void setAlertMessage(String alertMessage) {
        this.alertMessage = alertMessage;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public void setSenderTokenId(String senderTokenId) {
        this.senderTokenId = senderTokenId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public void setObject_id(ArrayList<String> object_id) {
        this.object_id = object_id;
    }

    public String getMessageBase64Content() {
        return messageBase64Content;
    }

    public void setMessageBase64Content(String messageBase64Content) {
        this.messageBase64Content = messageBase64Content;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(propertyId);
        dest.writeString(event);
        dest.writeString(senderTokenId);
        dest.writeString(messageId);
        dest.writeString(alertMessage);
        dest.writeStringList(object_id);
        dest.writeString(messageBase64Content);
    }
}
