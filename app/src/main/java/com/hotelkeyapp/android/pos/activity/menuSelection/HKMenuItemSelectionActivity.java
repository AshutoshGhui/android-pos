package com.hotelkeyapp.android.pos.activity.menuSelection;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.hotelkey.hkandroidlib.activity.common.AuthenticatedActivity;
import com.hotelkey.hkandroidlib.utils.ItemClickSupport;
import com.hotelkey.hkandroidlib.utils.LocalizationStringHelper;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.activity.orderDetails.HKCustomOrderCreationActivity;
import com.hotelkeyapp.android.pos.activity.orderDetails.OrderHelper;
import com.hotelkeyapp.android.pos.adapter.MenuItemSectionAdapter;
import com.hotelkeyapp.android.pos.db.orm.DaoSession;
import com.hotelkeyapp.android.pos.db.orm.MenuItemDB;
import com.hotelkeyapp.android.pos.db.orm.MenuItemDBDao;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDB;
import com.hotelkeyapp.android.pos.fragment.CreateOrderDialogFragment;
import com.hotelkeyapp.android.pos.utils.Constants;
import com.hotelkeyapp.android.pos.utils.DiscountsHelper;
import com.hotelkeyapp.android.pos.utils.LocalizationConstants;
import com.hotelkeyapp.android.pos.utils.OrderRequestCreationHelper;
import com.hotelkeyapp.android.pos.utils.PermissionHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import de.greenrobot.dao.query.Query;

import static com.hotelkeyapp.android.pos.utils.Constants.ARG_CUSTOM_ORDER_ITEM;

public class HKMenuItemSelectionActivity extends AuthenticatedActivity implements ItemClickSupport.OnItemClickListener,
        View.OnClickListener, SearchView.OnQueryTextListener, ItemClickSupport.OnItemLongClickListener,
        CreateOrderDialogFragment.DialogListenerCallbacks, DiscountsHelper.DiscountController {

    private static final int RQ_CUSTOM_ORDER = 5006;

    private static final String ARG_TABLE_ID = "TABLE ID";
    private static final String ARG_MENU_QUANTITY = "MENU QUANTITY";
    private static final String ARG_NEW_ITEMS_COUNT = "NEW ITEMS COUNT";

    private RecyclerView recyclerView;

    private FloatingActionButton fabCustomMenu;

    private SearchView searchView;

    private MenuItemSectionAdapter menuItemSectionAdapter;

    private List<MenuItemDB> menuItemsList;

    private ArrayList<OrderItemsDB> finalOrderItemsList;

    private TextView txvNoOfOrdersPlaced;

    private LinearLayout llEmptyCart;

    private int newItemAdded = 0;

    private LinkedHashMap<String, ArrayList<OrderItemsDB>> orderItemsMap;

    private HashMap<String, Integer> menuQuantityMap;

    public static Intent getLaunchIntent(Context context, HashMap<String, Integer> menuQuantityMap, String tableId, int newItemsCount) {
        Intent intent = new Intent(context, HKMenuItemSelectionActivity.class);
        intent.putExtra(ARG_TABLE_ID, tableId);
        intent.putExtra(ARG_MENU_QUANTITY, menuQuantityMap);
        intent.putExtra(ARG_NEW_ITEMS_COUNT, newItemsCount);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hkmenu_item_selection);

        Toolbar toolbar = findViewById(R.id.toolbar_top);
        ImageView imgBackArrow = toolbar.findViewById(R.id.img_back_arrow);
        imgBackArrow.setOnClickListener(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);

        initViews();
        setUp();
        setupSearchView();
    }

    private void initViews() {
        finalOrderItemsList = new ArrayList<>();
        recyclerView = findViewById(R.id.rv_menu_selection);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ItemClickSupport.addTo(this.recyclerView).setOnItemClickListener(this);
        ItemClickSupport.addTo(recyclerView).setOnItemLongClickListener(this);

        llEmptyCart = findViewById(R.id.ll_empty_cart);
        llEmptyCart.setVisibility(View.VISIBLE);

        fabCustomMenu = findViewById(R.id.fab_custom_menu);
        fabCustomMenu.setOnClickListener(this);

        HashMap<String, String> permissions = PermissionHelper.getUserPermission();
        if (permissions.containsKey(Constants.PERMISSION_CUSTOM_ORDER)) {
            fabCustomMenu.setVisibility(View.VISIBLE);
        } else {
            fabCustomMenu.setVisibility(View.GONE);
        }

        searchView = findViewById(R.id.simpleSearchView);

        orderItemsMap = OrderHelper.getInstance().getNewOrderItems();
        if (orderItemsMap == null) {
            orderItemsMap = new LinkedHashMap<>();
        }
        newItemAdded = getIntent().getIntExtra(ARG_NEW_ITEMS_COUNT, 0);
        OrderHelper.getInstance().setNewOrderItems(new LinkedHashMap<String, ArrayList<OrderItemsDB>>());

        menuQuantityMap = (HashMap<String, Integer>) getIntent().getSerializableExtra(ARG_MENU_QUANTITY);
        if (menuQuantityMap == null) {
            menuQuantityMap = new HashMap<>();
        }
    }

    private void setUp() {
        DaoSession daoSession = POSApplication.getInstance().getDaoSession();
        Query<MenuItemDB> queryBuilder = daoSession.getMenuItemDBDao().queryBuilder()
                .where(MenuItemDBDao.Properties.Active.eq("true")).build();

        if (queryBuilder != null && queryBuilder.list() != null && !queryBuilder.list().isEmpty()) {
            menuItemsList = queryBuilder.list();
            Collections.sort(menuItemsList);

            try {
                Collections.sort(menuItemsList, new Comparator<MenuItemDB>() {
                    @Override
                    public int compare(MenuItemDB menuItemDB, MenuItemDB menuItemDB1) {
                        String group1 = menuItemDB.getMenu_group_name();
                        String group2 = menuItemDB1.getMenu_group_name();

                        if ((group1 == null || group1.isEmpty())) {
                            if (group1 != null && !group1.isEmpty()) {
                                return -1;
                            } else {
                                return 0;
                            }
                        }
                        if (group2 == null || group2.isEmpty()) {
                            return 1;
                        }
                        return group1.compareToIgnoreCase(group2);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        menuItemSectionAdapter = new MenuItemSectionAdapter(getApplicationContext(), new ArrayList<MenuItemDB>());
        recyclerView.setAdapter(menuItemSectionAdapter);
        menuItemSectionAdapter.setOrderItemsMap(orderItemsMap);
        recyclerView.addOnScrollListener(mListener);
    }

    private RecyclerView.OnScrollListener mListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            try {
                InputMethodManager im = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                if (im != null) {
                    im.hideSoftInputFromWindow(recyclerView.getWindowToken(), 0);
                }
                searchView.clearFocus();
                recyclerView.requestFocus();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void setupSearchView() {
        if (searchView != null) {
            SearchManager searchManager = (SearchManager) this.getSystemService(Context.SEARCH_SERVICE);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(this.getComponentName()));
            searchView.setIconifiedByDefault(false);
            searchView.setQueryHint(LocalizationStringHelper.getInstance().fetchLocalizedKeyString("btn_search"));
            searchView.setOnQueryTextListener(this);
            searchView.setFocusable(true);
            searchView.requestFocus();

            try {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                TextView searchText = findViewById(android.support.v7.appcompat.R.id.search_src_text);
                Typeface typeface = ResourcesCompat.getFont(getApplicationContext(), R.font.montserrat);
                searchText.setTypeface(typeface);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item_cart, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        final MenuItem alertMenuItem = menu.findItem(R.id.action_cart);
        RelativeLayout rootView = (RelativeLayout) alertMenuItem.getActionView();
        RelativeLayout rlRedBadge = rootView.findViewById(R.id.view_alert_red_circle);
        txvNoOfOrdersPlaced = rootView.findViewById(R.id.view_alert_count_textview);

        if (newItemAdded != 0) {
            rlRedBadge.setVisibility(View.VISIBLE);
            YoYo.with(Techniques.Pulse)
                    .duration(200)
                    .repeat(0)
                    .playOn(rootView);
            txvNoOfOrdersPlaced.setText(String.valueOf(newItemAdded));

        } else {
            rlRedBadge.setVisibility(View.GONE);
        }

        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOptionsItemSelected(alertMenuItem);
            }
        });
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onItemClicked(RecyclerView recyclerView, int i, View view) {
        List<MenuItemDB> menuItemDBList = menuItemSectionAdapter.getItems();

        if (menuItemDBList != null && menuItemDBList.size() > i && i >= 0) {

            if (menuItemDBList.get(i) != null) {
                MenuItemDB menuItemDB = menuItemDBList.get(i);
                Integer quantity = menuQuantityMap.get(menuItemDB.getId());

                if (quantity == null || quantity < 99) {
                    YoYo.with(Techniques.Pulse)
                            .duration(200)
                            .repeat(0)
                            .playOn(view);
                    txvNoOfOrdersPlaced.setText(String.valueOf(newItemAdded));
                    newItemAdded = newItemAdded + 1;

                    ArrayList<OrderItemsDB> orderItemsDBArrayList = OrderRequestCreationHelper.createRequest(menuItemDB, 1, "");

                    DiscountsHelper discountsHelper = new DiscountsHelper(this);
                    orderItemsDBArrayList = discountsHelper.applyDiscounts(orderItemsDBArrayList, OrderHelper.getInstance().getOrderItems(), menuItemDB.getId());

                    if (orderItemsDBArrayList == null || orderItemsDBArrayList.isEmpty()) {
                        return;
                    }
                    updateOrder(orderItemsDBArrayList);
                    createOrderItemMap(menuItemDB.getId(), orderItemsDBArrayList);

                    menuItemSectionAdapter.setOrderItemsMap(orderItemsMap);
                    menuItemSectionAdapter.notifyItemChanged(i);

                } else {
                    if (mToast != null) {
                        if (mToast.getView().isShown()) {
                            mToast.cancel();
                        }
                    } else {
                        mToast = Toast.makeText(this, LocalizationConstants.getInstance().getKeyValue("toast_max_limit_reached"), Toast.LENGTH_SHORT);
                    }
                    mToast.show();
                }
            }
        }
    }

    private Toast mToast;

    private void createOrderItemMap(String menuId, ArrayList<OrderItemsDB> orderItemsDBArrayList) {
        if (orderItemsDBArrayList != null && !orderItemsDBArrayList.isEmpty()) {
            if (orderItemsMap == null) {
                orderItemsMap = new LinkedHashMap<>();
            }

            if (!orderItemsMap.containsKey(menuId)) {
                orderItemsMap.put(menuId, new ArrayList<OrderItemsDB>());
            }
            orderItemsMap.get(menuId).addAll(orderItemsDBArrayList);

            if (menuQuantityMap == null) {
                menuQuantityMap = new HashMap<>();
            }

            if (menuQuantityMap.containsKey(menuId)) {
                Integer quantity = menuQuantityMap.get(menuId);
                quantity = quantity + orderItemsDBArrayList.size();
                menuQuantityMap.put(menuId, quantity);
            } else {
                menuQuantityMap.put(menuId, orderItemsDBArrayList.size());
            }
        }
    }

    @Override
    public boolean onItemLongClicked(RecyclerView recyclerView, int i, View view) {
        List<MenuItemDB> menuItemDBS = menuItemSectionAdapter.getItems();
        if (menuItemDBS != null && menuItemDBS.size() > i && i >= 0) {

            MenuItemDB menuItemDBList = menuItemSectionAdapter.getItems().get(i);
            ArrayList<OrderItemsDB> orderItemsDBS;
            if (orderItemsMap.containsKey(menuItemDBList.getId())) {
                orderItemsDBS = orderItemsMap.get(menuItemDBList.getId());
            } else {
                orderItemsDBS = new ArrayList<>();
            }

            Integer quantity = menuQuantityMap.get(menuItemDBList.getId());
            if (quantity == null) {
                quantity = 0;
            }

            CreateOrderDialogFragment createOrderDialogFragment = CreateOrderDialogFragment.newInstance(orderItemsDBS, i, menuItemDBList.getId(), quantity);
            createOrderDialogFragment.show(getSupportFragmentManager(), "OrderUpdate");
            return true;
        }
        return false;
    }

    @Override
    public void increaseItemQuantity(ArrayList<OrderItemsDB> orderItemsRequests, int updateAtListIndex, String menuId) {
        DiscountsHelper discountsHelper = new DiscountsHelper(this);
        orderItemsRequests = discountsHelper.applyDiscounts(orderItemsRequests, OrderHelper.getInstance().getOrderItems(), menuId);
        if (orderItemsRequests == null || orderItemsRequests.isEmpty()) {
            return;
        }

        createOrderItemMap(orderItemsRequests.get(0).getMenu_item_id(), orderItemsRequests);
        newItemAdded = newItemAdded + orderItemsRequests.size();
        txvNoOfOrdersPlaced.setText(String.valueOf(newItemAdded));
        menuItemSectionAdapter.setOrderItemsMap(orderItemsMap);
        menuItemSectionAdapter.notifyItemChanged(updateAtListIndex);
        updateOrder(orderItemsRequests);
    }

    @Override
    public void reduceItemQuantity(int newOrderItems, String menuId, int updateAtListIndex) {
        if (orderItemsMap != null && !orderItemsMap.isEmpty() && orderItemsMap.containsKey(menuId)) {
            ArrayList<OrderItemsDB> orderItemsDBS = orderItemsMap.get(menuId);
            newItemAdded = newItemAdded - newOrderItems;
            txvNoOfOrdersPlaced.setText(String.valueOf(newItemAdded));

            int newOrderQuantity = newOrderItems;
            if (orderItemsDBS != null && !orderItemsDBS.isEmpty()) {
                for (int i = 0; i < orderItemsDBS.size(); i++) {
                    OrderItemsDB orderItemsDB = orderItemsDBS.get(i);
                    if (newOrderQuantity != 0) {
                        orderItemsDBS.remove(orderItemsDB);
                        i--;
                        newOrderQuantity--;
                    }
                }

                if (finalOrderItemsList != null && !finalOrderItemsList.isEmpty()) {
                    for (int i = 0; i < finalOrderItemsList.size(); i++) {
                        if (newOrderItems != 0) {
                            OrderItemsDB aOrderItemsDB = finalOrderItemsList.get(i);
                            if (aOrderItemsDB.getMenu_item_id().equals(menuId)) {
                                finalOrderItemsList.remove(aOrderItemsDB);
                                newOrderItems--;
                                int quantity = menuQuantityMap.get(menuId);
                                menuQuantityMap.put(menuId, quantity - 1);
                                i--;
                            }
                        }
                    }
                }

                menuItemSectionAdapter.setOrderItemsMap(orderItemsMap);
                menuItemSectionAdapter.notifyItemChanged(updateAtListIndex);
                invalidateOptionsMenu();
            }
        }
    }

    @Override
    public void updateRemarks(String menuId, int updateAtListIndex, String remarks) {
    }

    @Override
    public void onClick(View view) {
        if (view == fabCustomMenu) {
            String tableId = getIntent().getStringExtra(ARG_TABLE_ID);
            Intent intent = HKCustomOrderCreationActivity.getLaunchIntent(getApplicationContext(), tableId);
            startActivityForResult(intent, RQ_CUSTOM_ORDER);

        } else if (view.getId() == R.id.img_back_arrow) {
            onBackPressed();
        }
    }

    @Override
    public void removeCustomDiscount() {
        showMessageDialog(LocalizationConstants.getInstance().getKeyValue("msg_remove_custom_discount"),
                LocalizationConstants.getInstance().getKeyValue("btn_ok"), null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == RQ_CUSTOM_ORDER && resultCode == Activity.RESULT_OK) {
                OrderItemsDB orderItemsRequest = data.getParcelableExtra(ARG_CUSTOM_ORDER_ITEM);
                newItemAdded = newItemAdded + 1;
                Toast.makeText(this, LocalizationConstants.getInstance().getKeyValue("toast_item_added"), Toast.LENGTH_SHORT).show();

                ArrayList<OrderItemsDB> customOrder = new ArrayList<>();
                customOrder.add(orderItemsRequest);

                updateOrder(customOrder);
            }
        }
    }

    private void updateOrder(ArrayList<OrderItemsDB> orderItemsRequest) {

        if (finalOrderItemsList == null) {
            finalOrderItemsList = new ArrayList<>();
        }

        boolean containsMenu = false;
        OrderItemsDB newMenuItem = orderItemsRequest.get(0);
        for (int i = 0; i < finalOrderItemsList.size(); i++) {
            OrderItemsDB orderItemsDB = finalOrderItemsList.get(i);
            if (orderItemsDB.getMenu_item_id().equals(newMenuItem.getMenu_item_id())) {
                finalOrderItemsList.addAll(i, orderItemsRequest);
                containsMenu = true;
                break;
            }
        }

        if (!containsMenu) {
            finalOrderItemsList.addAll(orderItemsRequest);
        }
        invalidateOptionsMenu();
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText.isEmpty()) {
            menuItemSectionAdapter.setItems(new ArrayList<MenuItemDB>(), orderItemsMap);
            llEmptyCart.setVisibility(View.VISIBLE);

        } else {
            if (menuItemsList != null && !menuItemsList.isEmpty()) {
                llEmptyCart.setVisibility(View.GONE);
                menuItemSectionAdapter.setItems(menuItemsList, orderItemsMap);
                menuItemSectionAdapter.getFilter().filter(newText);
            }
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_cart) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (finalOrderItemsList != null) {
            OrderHelper orderHelper = OrderHelper.getInstance();
            orderHelper.setOrderItems(finalOrderItemsList);
            setResult(RESULT_OK);
        }
        finish();
        super.onBackPressed();
    }


}
