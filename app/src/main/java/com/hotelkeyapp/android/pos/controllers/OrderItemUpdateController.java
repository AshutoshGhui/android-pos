package com.hotelkeyapp.android.pos.controllers;

import android.util.Pair;

import com.hotelkey.hkandroidlib.common.BaseController;
import com.hotelkey.hkandroidlib.exceptions.ServerError;
import com.hotelkey.hkandroidlib.utils.MessageHelper;
import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.model.OrderItemUpdate;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vrushalimankar on 11/08/18.
 */

public class OrderItemUpdateController extends BaseController {

    public interface OrderUpdateCallbacks {
        void orderItemCancelled(ArrayList<String> orderItemCancelled);

        void onFailure(Pair<String, Boolean> errorMessage);
    }

    private OrderItemUpdateController.OrderUpdateCallbacks orderUpdateCallbacks;

    private ArrayList<String> orderItemCancelled;

    public OrderItemUpdateController(OrderItemUpdateController.OrderUpdateCallbacks orderUpdateCallbacks) {
        this.orderUpdateCallbacks = orderUpdateCallbacks;
    }

    public void updateOrderItems(OrderItemUpdate orderItemUpdate, String orderID) {
        this.orderItemCancelled = orderItemUpdate.getOrder_item_ids();
        POSApplication posApplication = POSApplication.getInstance();
        String propertyId = PreferenceUtils.getCurrentProperty(posApplication).getId();

        Call<ResponseBody> property = POSApplication.getInstance().getNetworkService()
                .updateOrderItem(PreferenceUtils.getEntIdentifier(posApplication), propertyId, orderID, orderItemUpdate);

        property.enqueue(callbacks);
    }

    private Callback<ResponseBody> callbacks = new Callback<ResponseBody>() {
        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            if (response.isSuccessful()) {
                orderUpdateCallbacks.orderItemCancelled(orderItemCancelled);

            } else if (response.code() == 400) {
                try {
                    JSONObject jsonObject = new JSONObject(response.errorBody().string());
                    orderUpdateCallbacks.onFailure(new Pair<>(jsonObject.getString("message"), false));

                } catch (JSONException e) {
                    e.printStackTrace();
                    orderUpdateCallbacks.onFailure(new Pair<>("Bad Request", false));

                } catch (IOException e) {
                    orderUpdateCallbacks.onFailure(MessageHelper.getMessageForError(POSApplication.getInstance(), e));
                }

            } else {
                onFailure(call, new ServerError(response.code(), response.errorBody().toString()));
            }
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {
            orderUpdateCallbacks.onFailure(getErrorMessage(t));
        }
    };

}
