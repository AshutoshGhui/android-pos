package com.hotelkeyapp.android.pos.model.order;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.hotelkeyapp.android.pos.model.Tax;

import java.util.ArrayList;

/**
 * Created by vrushalimankar on 10/05/18.
 */

public class Charge implements Parcelable {

    @SerializedName("id")
    private String id;

    @SerializedName("amount")
    private double amount;

    @SerializedName("quantity")
    private int quantity;

    @SerializedName("business_day_id")
    private String business_day_id;

    @SerializedName("adults")
    private int adults;

    @SerializedName("user_id")
    private String user_id;

    @SerializedName("date")
    private String date;

    @SerializedName("product_id")
    private String product_id;

    @SerializedName("charge_type_id")
    private String charge_type_id;

    @SerializedName("taxes")
    private ArrayList<Tax> tax;

    @SerializedName("shift_id")
    private String shift_id;

    @SerializedName("status")
    private String status;

    @SerializedName("original_amount")
    private double original_amount = 0;

    @SerializedName("children")
    private int children = 0;

    @SerializedName("posted_date")
    private String posted_date;

    @SerializedName("posted_business_day_id")
    private String posted_business_day_id;

    @SerializedName("adjustments")
    private ArrayList adjustments =new ArrayList();

    public Charge() {
    }

    protected Charge(Parcel in) {
        id = in.readString();
        amount = in.readDouble();
        quantity = in.readInt();
        business_day_id = in.readString();
        adults = in.readInt();
        user_id = in.readString();
        date = in.readString();
        product_id = in.readString();
        charge_type_id = in.readString();
        tax = in.createTypedArrayList(Tax.CREATOR);
        shift_id = in.readString();
        status = in.readString();
        original_amount = in.readDouble();
        children =in.readInt();
        posted_date =in.readString();
        posted_business_day_id =in.readString();
    }

    public static final Creator<Charge> CREATOR = new Creator<Charge>() {
        @Override
        public Charge createFromParcel(Parcel in) {
            return new Charge(in);
        }

        @Override
        public Charge[] newArray(int size) {
            return new Charge[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getBusiness_day_id() {
        return business_day_id;
    }

    public void setBusiness_day_id(String business_day_id) {
        this.business_day_id = business_day_id;
    }

    public int getAdults() {
        return adults;
    }

    public void setAdults(int adults) {
        this.adults = adults;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getCharge_type_id() {
        return charge_type_id;
    }

    public void setCharge_type_id(String charge_type_id) {
        this.charge_type_id = charge_type_id;
    }

    public ArrayList<Tax> getTax() {
        return tax;
    }

    public void setTax(ArrayList<Tax> tax) {
        this.tax = tax;
    }

    public String getShift_id() {
        return shift_id;
    }

    public void setShift_id(String shift_id) {
        this.shift_id = shift_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getOriginal_amount() {
        return original_amount;
    }

    public void setOriginal_amount(double original_amount) {
        this.original_amount = original_amount;
    }

    public int getChildren() {
        return children;
    }

    public void setChildren(int children) {
        this.children = children;
    }

    public String getPosted_date() {
        return posted_date;
    }

    public void setPosted_date(String posted_date) {
        this.posted_date = posted_date;
    }

    public String getPosted_business_day_id() {
        return posted_business_day_id;
    }

    public void setPosted_business_day_id(String posted_business_day_id) {
        this.posted_business_day_id = posted_business_day_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeDouble(amount);
        parcel.writeInt(quantity);
        parcel.writeString(business_day_id);
        parcel.writeInt(adults);
        parcel.writeString(user_id);
        parcel.writeString(date);
        parcel.writeString(product_id);
        parcel.writeString(charge_type_id);
        parcel.writeTypedList(tax);
        parcel.writeString(shift_id);
        parcel.writeString(status);
        parcel.writeDouble(original_amount);
        parcel.writeInt(children);
        parcel.writeString(posted_date);
        parcel.writeString(posted_business_day_id);
    }
}
