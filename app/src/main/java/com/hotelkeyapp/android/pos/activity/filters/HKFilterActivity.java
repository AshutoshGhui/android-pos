package com.hotelkeyapp.android.pos.activity.filters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hotelkey.hkandroidlib.activity.common.AuthenticatedActivity;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.db.orm.DaoSession;
import com.hotelkeyapp.android.pos.db.orm.TableGroupDB;
import com.hotelkeyapp.android.pos.db.orm.TableGroupDBDao;
import com.hotelkeyapp.android.pos.utils.LocalizationConstants;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.dao.query.QueryBuilder;

public class HKFilterActivity extends AuthenticatedActivity implements OnClickListener {

    private LinearLayout llZones, llStatus;

    private ArrayList<String> selectedTableGroupList, selectedStatusList;

    private TextView txvSelectZone;

    private String selectAllTableGroups, selectAllStatus;

    public static Intent getLaunchIntent(Context context, ArrayList<String> selectedTableGroupList, ArrayList<String> selectedStatusList) {
        Intent intent = new Intent(context, HKFilterActivity.class);
        intent.putStringArrayListExtra("ARG_SELECTED_ZONES", selectedTableGroupList);
        intent.putStringArrayListExtra("ARG_SELECTED_STATUS", selectedStatusList);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hkfilter);

        Toolbar toolbar = findViewById(R.id.toolbar_top);
        TextView txvTitle = toolbar.findViewById(R.id.toolbar_title);
        txvTitle.setText(LocalizationConstants.getInstance().getKeyValue("btn_filter"));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        initViews();
    }

    private void initViews() {
        selectedStatusList = getIntent().getStringArrayListExtra("ARG_SELECTED_STATUS");
        selectedTableGroupList = getIntent().getStringArrayListExtra("ARG_SELECTED_ZONES");
        selectAllTableGroups = LocalizationConstants.getInstance().getKeyValue("btn_all");
        selectAllStatus = LocalizationConstants.getInstance().getKeyValue("btn_all");

        TextView txvSelectStatus = findViewById(R.id.txv_select_status);
        txvSelectStatus.setText(LocalizationConstants.getInstance().getKeyValue("lbl_select_status"));
        llStatus = findViewById(R.id.ll_status);

        ArrayList<String> statusList = new ArrayList<>();
        statusList.add(selectAllStatus);
        statusList.add(LocalizationConstants.getInstance().getKeyValue("lbl_dirty"));
        statusList.add(LocalizationConstants.getInstance().getKeyValue("lbl_blocked"));
        statusList.add(LocalizationConstants.getInstance().getKeyValue("lbl_available"));
        statusList.add(LocalizationConstants.getInstance().getKeyValue("lbl_reserved"));
        statusList.add(LocalizationConstants.getInstance().getKeyValue("lbl_order_stat_bill_issued"));

        populateStatusList(statusList);

        txvSelectZone = findViewById(R.id.txv_select_zone);
        txvSelectZone.setText(LocalizationConstants.getInstance().getKeyValue("lbl_plhldr_choose_zone"));
        llZones = findViewById(R.id.ll_zones);

        DaoSession daoSession = POSApplication.getInstance().getDaoSession();
        QueryBuilder<TableGroupDB> tableGroupQB = daoSession.getTableGroupDBDao().queryBuilder()
                .where(TableGroupDBDao.Properties.Active.eq("true")).orderAsc(TableGroupDBDao.Properties.Name);
        List<TableGroupDB> tableGroupDBList = tableGroupQB.list();

        if (tableGroupDBList != null && !tableGroupDBList.isEmpty()) {
            TableGroupDB tableGroupDB = new TableGroupDB();
            tableGroupDB.setId(selectAllTableGroups);
            tableGroupDB.setName(selectAllTableGroups);
            tableGroupDBList.add(0, tableGroupDB);
        }

        populateTableGroups(tableGroupDBList);

        Button btnDone = findViewById(R.id.btn_done);
        btnDone.setText(LocalizationConstants.getInstance().getKeyValue("btn_done"));
        btnDone.setOnClickListener(this);
    }

    private void populateStatusList(final ArrayList<String> statusList) {
        llStatus.removeAllViews();

        for (int i = 0; i < statusList.size(); i++) {

            View convertView = getLayoutInflater().inflate(R.layout.layout_checkbox, null);
            TextView label = convertView.findViewById(R.id.txv_title);
            label.setText(statusList.get(i));
            if (i == 0) {
                label.setTypeface(Typeface.DEFAULT, Typeface.ITALIC);
            }

            final CheckBox checkBox = convertView.findViewById(R.id.checkbox);

            checkBox.setChecked(selectedStatusList.contains(statusList.get(i)));
            checkBox.setTag(statusList.get(i));

            convertView.findViewById(R.id.parent_layout).setTag(statusList.get(i));

            convertView.findViewById(R.id.parent_layout).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    checkBox.setChecked(!checkBox.isChecked());
                }
            });

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                    String selected = compoundButton.getTag().toString();
                    if (selected.equalsIgnoreCase(selectAllStatus)) {
                        if (checked) {
                            selectedStatusList.clear();
                            ArrayList<String> tableIds = new ArrayList<>();
                            tableIds.addAll(statusList);

                            selectedStatusList.addAll(tableIds);
                            populateStatusList(statusList);

                        } else {
                            selectedStatusList.clear();
                            populateStatusList(statusList);
                        }
                    }

                    if (!checked && selectedStatusList.contains(selected)) {
                        selectedStatusList.remove(selected);

                        if (selectedStatusList.contains(selectAllStatus)) {
                            selectedStatusList.remove(selectAllStatus);
                            populateStatusList(statusList);
                        }

                    } else if (checked && !selectedStatusList.contains(selected)) {
                        selectedStatusList.add(selected);

                        if (selectedStatusList.size() + 1 == statusList.size()) {
                            selectedStatusList.add(0, selectAllStatus);
                            populateStatusList(statusList);
                        }
                    }

                }
            });
            llStatus.addView(convertView);
        }
    }


    private void populateTableGroups(final List<TableGroupDB> tableGroupDBList) {
        llZones.removeAllViews();

        if (tableGroupDBList != null && !tableGroupDBList.isEmpty()) {

            txvSelectZone.setVisibility(View.VISIBLE);
            llZones.setVisibility(View.VISIBLE);

            for (int i = 0; i < tableGroupDBList.size(); i++) {

                View convertView = getLayoutInflater().inflate(R.layout.layout_checkbox, null);
                TextView label = convertView.findViewById(R.id.txv_title);
                label.setText(tableGroupDBList.get(i).getName());
                if (i == 0) {
                    label.setTypeface(Typeface.DEFAULT, Typeface.ITALIC);
                }

                final CheckBox checkBox = convertView.findViewById(R.id.checkbox);

                checkBox.setChecked(selectedTableGroupList.contains(tableGroupDBList.get(i).getId()));
                checkBox.setTag(tableGroupDBList.get(i).getId());

                convertView.findViewById(R.id.parent_layout).setTag(tableGroupDBList.get(i));

                convertView.findViewById(R.id.parent_layout).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        checkBox.setChecked(!checkBox.isChecked());
                    }
                });

                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                        String selected = compoundButton.getTag().toString();
                        if (selected.equalsIgnoreCase(selectAllTableGroups)) {
                            if (checked) {
                                selectedTableGroupList.clear();
                                ArrayList<String> tableIds = new ArrayList<>();
                                for (TableGroupDB tableGroupDB : tableGroupDBList) {
                                    tableIds.add(tableGroupDB.getId());
                                }
                                selectedTableGroupList.addAll(tableIds);
                                populateTableGroups(tableGroupDBList);

                            } else {
                                selectedTableGroupList.clear();
                                populateTableGroups(tableGroupDBList);
                            }
                        }

                        if (!checked && selectedTableGroupList.contains(selected)) {
                            selectedTableGroupList.remove(selected);

                            if (selectedTableGroupList.contains(selectAllTableGroups)) {
                                selectedTableGroupList.remove(selectAllTableGroups);
                                populateTableGroups(tableGroupDBList);
                            }

                        } else if (checked && !selectedTableGroupList.contains(selected)) {
                            selectedTableGroupList.add(selected);

                            if (selectedTableGroupList.size() + 1 == tableGroupDBList.size()) {
                                selectedTableGroupList.add(0, selectAllTableGroups);
                                populateTableGroups(tableGroupDBList);
                            }
                        }

                    }
                });
                llZones.addView(convertView);
            }
        } else {
            txvSelectZone.setVisibility(View.GONE);
            llZones.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_done) {
            Intent intent = new Intent();
            intent.putStringArrayListExtra("ARG_SELECTED_STATUS", selectedStatusList);
            intent.putStringArrayListExtra("ARG_SELECTED_ZONES", selectedTableGroupList);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (menu!=null){
            MenuItem menuItem =menu.findItem(R.id.btn_done);
            menuItem.setTitle(LocalizationConstants.getInstance().getKeyValue("btn_clear_uppercase"));
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.btn_done){
            Intent intent = new Intent();
            selectedStatusList.clear();
            selectedTableGroupList.clear();

            intent.putStringArrayListExtra("ARG_SELECTED_STATUS", selectedStatusList);
            intent.putStringArrayListExtra("ARG_SELECTED_ZONES", selectedTableGroupList);
            setResult(RESULT_OK, intent);
            finish();
        }else {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
