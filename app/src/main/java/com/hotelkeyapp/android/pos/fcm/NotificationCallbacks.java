package com.hotelkeyapp.android.pos.fcm;

public interface NotificationCallbacks {
    void pushNotificationReceived(String requestPayload);
}
