package com.hotelkeyapp.android.pos.activity.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Toast;

import com.hotelkey.hkandroidlib.HKAndroidApplication;
import com.hotelkey.hkandroidlib.activity.common.AuthenticatedActivity;
import com.hotelkey.hkandroidlib.service.ProgressEvent;
import com.hotelkey.hkandroidlib.service.RefreshEvent;
import com.hotelkey.hkandroidlib.utils.LocalizationStringHelper;
import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.apiService.SyncService;
import com.hotelkeyapp.android.pos.fragment.DashboardFragment;
import com.hotelkeyapp.android.pos.fragment.PrinterProfilesFragment;
import com.hotelkeyapp.android.pos.utils.AccountPreferences;
import com.hotelkeyapp.android.pos.utils.FormatUtils;
import com.hotelkeyapp.android.pos.utils.LocalizationConstants;
import com.hotelkeyapp.android.pos.utils.RefreshService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Date;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

import static com.hotelkeyapp.android.pos.utils.Constants.UPDATE_ORDERS_TABLES;

/**
 * Created by vrushalimankar on 09/05/18.
 */

public class HKHomeActivity extends HKNavigationDrawerActivity implements RefreshService {

    public static final String LOGOUT_SCHEMA_UPDATE = "LOGOUT ON SCHEMA UPDATE";

    private LocalizationConstants localizationConstants;

    private DashboardFragment dashboardFragment;

    private PrinterProfilesFragment printerSetupFragment;

    private Snackbar snackbar;

    private MaterialProgressBar progressBar;

    public static Intent getLaunchIntent(Context context) {
        Intent intent = new Intent(context, HKHomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    private View.OnClickListener retryActionCallback = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            requestHardRefresh();
            snackbar.dismiss();
        }
    };

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent(AuthenticatedActivity.LOGOUT_EVENT));
            POSApplication.getInstance().logout();
            finish();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter(LOGOUT_SCHEMA_UPDATE));

        init();
        checkForDBSchemaUpdate();
        requestHardRefresh();
        checkAppStatus();
        setTitles();
    }

    private void checkForDBSchemaUpdate() {
        POSApplication.getInstance().getDaoSession();
    }


    private void init() {
        progressBar = findViewById(R.id.progress_bar);
        localizationConstants = LocalizationConstants.getInstance();
        dashboardFragment = DashboardFragment.newInstance();
        printerSetupFragment = PrinterProfilesFragment.newInstance();
        loadContent(dashboardFragment);
    }

    private void requestHardRefresh() {
        if (PreferenceUtils.getCurrentProperty(this) != null) {
            Intent msgIntent = new Intent(this, SyncService.class);
            msgIntent.putExtra(UPDATE_ORDERS_TABLES, false);
            startService(msgIntent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean appReady = PreferenceUtils.getAppReady(this);
        if (appReady) {
            EventBus.getDefault().post(new RefreshEvent());
        }
    }

    private void requestSoftRefresh() {
        if (PreferenceUtils.getCurrentProperty(this) != null) {
            Intent msgIntent = new Intent(this, SyncService.class);
            msgIntent.putExtra(UPDATE_ORDERS_TABLES, true);
            startService(msgIntent);
        }
    }

    private void checkAppStatus() {
        boolean appReady = PreferenceUtils.getAppReady(this);
        if (!appReady) {
            this.showLoadingDialog(LocalizationStringHelper.getInstance().fetchLocalizedKeyString("ldr_login_progress_title"));
        }
    }

    @Override
    protected void onPropertySwitched() {
        super.onPropertySwitched();
        init();
        this.showLoadingDialog(LocalizationStringHelper.getInstance().fetchLocalizedKeyString("ldr_login_progress_title"));
        requestHardRefresh();
    }

    public void setTitles() {
        try {
            HKAndroidApplication application = HKAndroidApplication.getApplication();
            getSupportActionBar().setTitle(PreferenceUtils.getCurrentProperty(application).getName());
            Date CBD = PreferenceUtils.getCBD(this);
            getSupportActionBar().setSubtitle(FormatUtils.formatInventoryDate(CBD));

        } catch (Exception e) {
            e.printStackTrace();
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent(AuthenticatedActivity.LOGOUT_EVENT));
            POSApplication.getInstance().logout();
            finish();
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void progressStatusChanged(ProgressEvent event) {
        String message = "";

        switch (event.getState()) {
            case com.hotelkeyapp.android.pos.utils.ProgressEvent.CBD_FETCHED:
                setTitles();
                message = "CBD_FETCHED";
                break;

            case ProgressEvent.IN_SUCCESS:
                progressBar.setVisibility(View.GONE);
                EventBus.getDefault().post(new RefreshEvent());
                cancelLoadingDialog();
                if (AccountPreferences.getPropertyAddress(getApplicationContext()) == null) {
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent(AuthenticatedActivity.LOGOUT_EVENT));
                    POSApplication.getInstance().logout();
                    finish();
                }

                message = "IN_SUCCESS";
                break;

            case ProgressEvent.IN_PROGRESS:
                progressBar.setVisibility(View.VISIBLE);
                message = "IN_PROGRESS";
                break;

            case ProgressEvent.IN_ERROR:
                progressBar.setVisibility(View.GONE);
                boolean fatalError = checkOfFatalError(event.getMessage());
                if (!fatalError) {
                    showSnackBar(localizationConstants.getKeyValue("msg_failed_to_sync_data"));
                }
                message = "IN_ERROR";
                break;
        }

        Log.d("HOME_ACTIVITY", message);
    }

    private boolean checkOfFatalError(String message) {
        boolean appReady = PreferenceUtils.getAppReady(this);
        cancelLoadingDialog();

        if (message.equals(LocalizationStringHelper.getInstance().fetchLocalizedKeyString("msg_session_expired"))
                || message.equals(LocalizationStringHelper.getInstance().fetchLocalizedKeyString("msg_no_permissions"))
                || message.equals(LocalizationStringHelper.getInstance().fetchLocalizedKeyString("msg_user_locked")) ||
                message.equals(LocalizationStringHelper.getInstance().fetchLocalizedKeyString("msg_user_inactive"))) {

            showNonCancelableMessageDialog(new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    logout();
                }
            }, message, localizationConstants.getKeyValue("btn_logout"));
            return true;

        } else if (!appReady) {

            if (PreferenceUtils.getUpgrade(HKAndroidApplication.getApplication().getApplicationContext()) == null) {
                String okBtn = LocalizationStringHelper.getInstance().fetchLocalizedKeyString("btn_ok");
                showMessageDialog(message, okBtn, null);
                return true;
            } else {

                final Uri url = Uri.parse(PreferenceUtils.getUpgrade(getApplicationContext()).getDownloadUrl());
                if (URLUtil.isValidUrl(url.toString()) && Patterns.WEB_URL.matcher(url.toString()).matches()) {
                    logout();

                } else {
                    Toast.makeText(HKHomeActivity.this, "Invalid url", Toast.LENGTH_SHORT).show();
                    logout();
                }
                return true;
            }
        }
        return false;
    }


    public void showSnackBar(String message) {
        if (snackbar != null) {
            snackbar.dismiss();
        }
        snackbar = Snackbar
                .make(contentLayout, message, Snackbar.LENGTH_LONG)
                .setDuration(Snackbar.LENGTH_LONG);
        snackbar.setAction(localizationConstants.getKeyValue("btn_retry_uppercase"), retryActionCallback);
        snackbar.show();
    }


    @Override
    protected boolean handleNavigation(int id) {
        if (id == R.id.nav_dashboard) {
            loadContent(dashboardFragment);
            getNavigationView().setCheckedItem(R.id.nav_dashboard);
            getNavigationView().getMenu().findItem(R.id.nav_printer_setup).setChecked(false);

        } else if (id == R.id.nav_printer_setup) {
            loadContent(printerSetupFragment);
            getNavigationView().setCheckedItem(R.id.nav_printer_setup);
            getNavigationView().getMenu().findItem(R.id.nav_dashboard).setChecked(false);
        }
        return super.handleNavigation(id);
    }

    private void loadContent(Fragment fragment) {
        try {
            getSupportFragmentManager().beginTransaction().replace(R.id.nav_content, fragment).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void refresh() {
        requestSoftRefresh();
    }

    @Override
    public void handleApiFailure(String message) {
        checkOfFatalError(message);
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, localizationConstants.getKeyValue("msg_please_click_back_again"), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
