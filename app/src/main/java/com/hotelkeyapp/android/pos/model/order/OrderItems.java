package com.hotelkeyapp.android.pos.model.order;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by vrushalimankar on 10/05/18.
 */

public class OrderItems implements Serializable{

    @SerializedName("order_id")
    private String  order_id;

    @SerializedName("charge_id")
    private String charge_id;

    @SerializedName("display_label")
    private String display_label;

    @SerializedName("charge")
    private Charge charge;

    @SerializedName("menu_item_id")
    private String menu_item_id;

    @SerializedName("menu_item")
    private MeneItem menu_item;

    public String getCharge_id() {
        return charge_id;
    }

    public void setCharge_id(String charge_id) {
        this.charge_id = charge_id;
    }

    public String getDisplay_label() {
        return display_label;
    }

    public void setDisplay_label(String display_label) {
        this.display_label = display_label;
    }

    public Charge getCharge() {
        return charge;
    }

    public void setCharge(Charge charge) {
        this.charge = charge;
    }

    public String getMenu_item_id() {
        return menu_item_id;
    }

    public void setMenu_item_id(String menu_item_id) {
        this.menu_item_id = menu_item_id;
    }

    public MeneItem getMenu_item() {
        return menu_item;
    }

    public void setMenu_item(MeneItem menu_item) {
        this.menu_item = menu_item;
    }

    public String getOrder_id() {

        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }
}
