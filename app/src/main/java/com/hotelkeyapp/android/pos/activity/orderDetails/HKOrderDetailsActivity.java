package com.hotelkeyapp.android.pos.activity.orderDetails;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hotelkey.hkandroidlib.activity.common.AuthenticatedActivity;
import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.activity.discounts.HKDiscountsActivity;
import com.hotelkeyapp.android.pos.activity.menuSelection.HKMenuItemSelectionActivity;
import com.hotelkeyapp.android.pos.activity.orderDetails.OrderAsyncTask.OrderCallback;
import com.hotelkeyapp.android.pos.controllers.OrderCreationController;
import com.hotelkeyapp.android.pos.controllers.OrderCreationController.OrderCreationCallbacks;
import com.hotelkeyapp.android.pos.controllers.OrderItemUpdateController;
import com.hotelkeyapp.android.pos.controllers.OrderItemUpdateController.OrderUpdateCallbacks;
import com.hotelkeyapp.android.pos.controllers.WaitersController;
import com.hotelkeyapp.android.pos.db.orm.AdjustmentsDB;
import com.hotelkeyapp.android.pos.db.orm.ChargeReqDB;
import com.hotelkeyapp.android.pos.db.orm.DaoSession;
import com.hotelkeyapp.android.pos.db.orm.OrderDB;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDB;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDBDao;
import com.hotelkeyapp.android.pos.db.orm.PropertyTableDB;
import com.hotelkeyapp.android.pos.db.orm.PropertyTableDBDao;
import com.hotelkeyapp.android.pos.db.orm.WaiterDB;
import com.hotelkeyapp.android.pos.db.orm.WaiterDBDao;
import com.hotelkeyapp.android.pos.fragment.CreateOrderDialogFragment;
import com.hotelkeyapp.android.pos.fragment.CreateOrderDialogFragment.DialogListenerCallbacks;
import com.hotelkeyapp.android.pos.fragment.OrderPlacedAlertFragment;
import com.hotelkeyapp.android.pos.model.OrderItemUpdate;
import com.hotelkeyapp.android.pos.model.PrinterProfile;
import com.hotelkeyapp.android.pos.model.Waiter;
import com.hotelkeyapp.android.pos.model.order.AuditLogs;
import com.hotelkeyapp.android.pos.printerUtils.KOTBuilder;
import com.hotelkeyapp.android.pos.printerUtils.PrinterSetupAsyncTask;
import com.hotelkeyapp.android.pos.utils.AccountPreferences;
import com.hotelkeyapp.android.pos.utils.Constants;
import com.hotelkeyapp.android.pos.utils.CustomBottomSheet;
import com.hotelkeyapp.android.pos.utils.DiscountsHelper;
import com.hotelkeyapp.android.pos.utils.DiscountsHelper.DiscountController;
import com.hotelkeyapp.android.pos.utils.FormatUtils;
import com.hotelkeyapp.android.pos.utils.LocalizationConstants;
import com.hotelkeyapp.android.pos.utils.PermissionHelper;
import com.hotelkeyapp.android.pos.utils.ProgressEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

import de.greenrobot.dao.DaoException;
import de.greenrobot.dao.query.Query;
import de.greenrobot.dao.query.QueryBuilder;

import static com.hotelkeyapp.android.pos.activity.discounts.HKDiscountsActivity.ARG_AUDIT_LOGS;
import static com.hotelkeyapp.android.pos.utils.Constants.ARG_MESSAGE;
import static com.hotelkeyapp.android.pos.utils.Constants.CHARGE_STATUS_CANCELLED;
import static com.hotelkeyapp.android.pos.utils.Constants.ORDER_STATUS_BILL_ISSUED;
import static com.hotelkeyapp.android.pos.utils.Constants.ORDER_STATUS_CANCELLED;
import static com.hotelkeyapp.android.pos.utils.Constants.ORDER_STATUS_COMPLETED;
import static com.hotelkeyapp.android.pos.utils.Constants.PERMISSION_APPLY_DISCOUNT;
import static com.hotelkeyapp.android.pos.utils.Constants.PERMISSION_DELETE_ORDER;
import static com.hotelkeyapp.android.pos.utils.Constants.PERMISSION_ORDER_MODULE;
import static com.hotelkeyapp.android.pos.utils.Constants.PERMISSION_READ_WRITE;

public class HKOrderDetailsActivity extends AuthenticatedActivity implements
        View.OnClickListener, OrderCreationCallbacks, OrderPlacedAlertFragment.OrderPlacedAlertCallbacks,
        DialogListenerCallbacks, OrderCallback, WaitersController.WaitersControllerData, OrderUpdateCallbacks, DiscountController {

    private static final String ARG_TABLE_ID = "Order id";

    private static final int RQ_ADD_MENU_ITEMS = 456;
    private static final int RQ_GUEST_DETAILS = 457;
    private static final int RQ_APPLY_PROMO = 458;

    private Button btnCreate, btnPlaceOrder;

    private PropertyTableDB propertyTableDB;

    private LinearLayout layoutEmptyCartItems, layoutExistingOrder, layoutCartItems;

    private TextView txvTitle, txvGuestName, txvHeadCount, txvWaiterName, txvOrderNo, txvCurrentOrder;

    private OrderDB orderDB;

    private View view;

    private ImageView imgEditUserDetails;

    ArrayList<String> existingOrderItemIds = new ArrayList<>();
    ArrayList<String> newOrderItemIds = new ArrayList<>();

    private boolean isActive;

    private HashMap<String, String> permissions;

    public static Intent getLaunchIntent(Context context, String tableId) {
        Intent intent = new Intent(context, HKOrderDetailsActivity.class);
        intent.putExtra(ARG_TABLE_ID, tableId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hkorder_details);
        EventBus.getDefault().register(this);

        Toolbar toolbar = findViewById(R.id.toolbar_top);
        txvTitle = toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        HashMap<String, WaiterDB> waiterDBList = getWaitersList();
        if (waiterDBList == null || waiterDBList.isEmpty()) {
            makeWaitersReq();
        }

        permissions = PermissionHelper.getUserPermission();
        initViews();
    }


    private HashMap<String, WaiterDB> getWaitersList() {
        HashMap<String, WaiterDB> waiterDBList = new HashMap<>();

        DaoSession daoSession = POSApplication.getInstance().getDaoSession();
        QueryBuilder<WaiterDB> waiterDBS = daoSession.getWaiterDBDao().queryBuilder().where(WaiterDBDao.Properties.Active.eq("true"));
        if (waiterDBS != null && waiterDBS.list() != null) {
            for (WaiterDB waiterDB : waiterDBS.list()) {
                waiterDBList.put(waiterDB.getId(), waiterDB);
            }
        }
        return waiterDBList;
    }

    private void makeWaitersReq() {
        WaitersController waitersController = new WaitersController(this);
        waitersController.downloadWaitersList(PreferenceUtils.getCurrentProperty(getApplicationContext()).getId());
    }

    private void initViews() {
        DaoSession daoSession = POSApplication.getInstance().getDaoSession();

        String tableId = getIntent().getStringExtra(ARG_TABLE_ID);
        if (tableId != null) {
            QueryBuilder<PropertyTableDB> queryBuilder = daoSession.getPropertyTableDBDao().queryBuilder().where(PropertyTableDBDao.Properties.Id.eq(tableId));
            if (queryBuilder != null) {

                try {
                    propertyTableDB = queryBuilder.unique();
                    if (propertyTableDB != null && propertyTableDB.getNumber() != null) {
                        String tableNo = String.format(LocalizationConstants.getInstance().getKeyValue("lbl_table_no_with_value"), propertyTableDB.getNumber());
                        txvTitle.setText(tableNo);
                    }
                } catch (DaoException e) {
                    e.printStackTrace();
                }
            }
        }

        if (propertyTableDB == null) {
            finish();
            return;
        }

        txvGuestName = findViewById(R.id.txv_guest_name);
        txvGuestName.setHint(LocalizationConstants.getInstance().getKeyValue("lbl_customer"));
        txvHeadCount = findViewById(R.id.txv_head_count);
        txvHeadCount.setText(String.valueOf(1));
        txvWaiterName = findViewById(R.id.txv_waiter);
        txvWaiterName.setHint(LocalizationConstants.getInstance().getKeyValue("lbl_no_waiter_assigned"));
        TextView txvListEmpty = findViewById(R.id.txv_list_empty);
        txvListEmpty.setText(LocalizationConstants.getInstance().getKeyValue("lbl_list_empty"));

        layoutEmptyCartItems = findViewById(R.id.ll_empty_cart);
        layoutCartItems = findViewById(R.id.ll_order_items);
        layoutExistingOrder = findViewById(R.id.ll_existing_order_section);
        txvOrderNo = findViewById(R.id.txv_order_no);
        txvCurrentOrder = findViewById(R.id.txv_current_order);
        view = findViewById(R.id.view);

        btnCreate = findViewById(R.id.btn_add_items);
        btnCreate.setOnClickListener(this);
        btnCreate.setText(LocalizationConstants.getInstance().getKeyValue("lbl_add_items"));

        btnPlaceOrder = findViewById(R.id.btn_place_order);
        btnPlaceOrder.setOnClickListener(this);
        btnPlaceOrder.setText(LocalizationConstants.getInstance().getKeyValue("lbl_place_order"));

        imgEditUserDetails = findViewById(R.id.img_edit_user_details);

        if (permissions.containsKey(PERMISSION_ORDER_MODULE) && permissions.get(PERMISSION_ORDER_MODULE).equals(Constants.PERMISSION_READ_WRITE)) {
            imgEditUserDetails.setVisibility(View.VISIBLE);
            imgEditUserDetails.setOnClickListener(this);
            findViewById(R.id.ll_guest_details).setOnClickListener(this);
        } else {
            imgEditUserDetails.setVisibility(View.GONE);
            findViewById(R.id.ll_guest_details).setOnClickListener(null);
        }

        showLoadingDialog(LocalizationConstants.getInstance().getKeyValue("ldr_loading"));
        OrderAsyncTask orderAsyncTask = new OrderAsyncTask(this, false);
        orderAsyncTask.execute(propertyTableDB);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void progressStatusChanged(ProgressEvent event) {
        switch (event.getState()) {
            case ProgressEvent.ORDER_UPDATED:
                if (propertyTableDB != null) {
                    OrderAsyncTask orderAsyncTask = new OrderAsyncTask(this, true);
                    orderAsyncTask.execute(propertyTableDB);
                }
                break;

            case ProgressEvent.ORDER_CANCELLED_OR_COMPLETED:
                if (orderDB != null && event.getOrderId() != null && event.getStatus() != null) {
                    if (orderDB.getId().equals(event.getOrderId())) {

                        if (event.getStatus().equals(ORDER_STATUS_COMPLETED)) {
                            Toast.makeText(this, LocalizationConstants.getInstance().getKeyValue("toast_order_completed"), Toast.LENGTH_SHORT).show();
                        } else if (event.getStatus().equals(ORDER_STATUS_CANCELLED)) {
                            Toast.makeText(this, LocalizationConstants.getInstance().getKeyValue("toast_order_cancelled"), Toast.LENGTH_SHORT).show();
                        }

                        setResult(RESULT_OK);
                        finish();
                    }
                }
                break;
        }
    }

    @Override
    public void orderCallback(OrderDB orderDB, boolean updateOrder) {
        this.orderDB = orderDB;
        if (updateOrder) {
            btnPlaceOrder.setText(LocalizationConstants.getInstance().getKeyValue("lbl_update_order"));
        }
        setUp();
        cancelLoadingDialog();
    }

    boolean isCurrentTableOrderModified = false;

    @Override
    public void orderModified(OrderDB modifiedOrderDB) {
        if (modifiedOrderDB == null || modifiedOrderDB.getOrderItemsDBS() == null) {
            return;

        } else if (modifiedOrderDB.getStatus().equals(ORDER_STATUS_BILL_ISSUED)) {
            orderDB = modifiedOrderDB;
            setUp();
            return;

        } else if (!orderDB.getId().equals(modifiedOrderDB.getId())) {
            orderDB.newOrder(modifiedOrderDB);
        }

        if (orderDB.getGuest_name().equals(orderDB.getServer_guest_name())) {
            orderDB.setGuest_name(modifiedOrderDB.getGuest_name());
        }
        if (orderDB.getPersons().equals(orderDB.getServer_persons())) {
            orderDB.setPersons(modifiedOrderDB.getPersons());
        }
        if (orderDB.getWaiter_id() == null || orderDB.getWaiter_id().equals(orderDB.getServer_waiter_id())) {
            orderDB.setWaiter_id(modifiedOrderDB.getWaiter_id());
        }

        if (modifiedOrderDB.getOrderItemsDBS() != null) {
            ArrayList<OrderItemsDB> modifiedOrderItems = modifiedOrderDB.getOrderItemsDBS();

            for (OrderItemsDB aOrderItemsDB : modifiedOrderItems) {

                //Handle New items added
                if (!existingOrderItemIds.contains(aOrderItemsDB.getId())) {
                    existingOrderItemIds.add(aOrderItemsDB.getId());

                    ArrayList<OrderItemsDB> itemsDBS = orderDB.getOrderItemsDBS();
                    if (itemsDBS == null) {
                        itemsDBS = new ArrayList<>();
                    }
                    itemsDBS.add(itemsDBS.size(), aOrderItemsDB);
                    orderDB.setOrderItemsDBS(itemsDBS);

                    isCurrentTableOrderModified = true;

                    //Handle existing orderItem changes
                } else {
                    ArrayList<OrderItemsDB> orderItemsDBS = orderDB.getOrderItemsDBS();
                    for (int i = 0; i < orderItemsDBS.size(); i++) {
                        OrderItemsDB existingItem = orderItemsDBS.get(i);

                        if (existingItem.getId().equals(aOrderItemsDB.getId())) {
                            orderItemsDBS.set(i, aOrderItemsDB);
                            isCurrentTableOrderModified = true;
                            break;
                        }
                    }
                }
            }
        }
        if (isCurrentTableOrderModified) {
            setUp();
        }
    }

    private void setUp() {
        if (!isActive) {
            return;
        }
        txvCurrentOrder.setText(LocalizationConstants.getInstance().getKeyValue("lbl_existing_order"));

        DaoSession daoSession = POSApplication.getInstance().getDaoSession();

        LinkedHashMap<String, ArrayList<OrderItemsDB>> orderItemsRequests = new LinkedHashMap<>();
        if (orderDB != null) {
            txvGuestName.setText(orderDB.getGuest_name());
            txvHeadCount.setText(String.valueOf(orderDB.getPersons()));

            if (orderDB.getSequence_number() != null) {
                String orderNo = String.format(LocalizationConstants.getInstance().getKeyValue("lbl_order_no_with_value"), orderDB.getSequence_number());
                txvOrderNo.setText(orderNo);
                txvOrderNo.setVisibility(View.VISIBLE);
            }

            if (orderDB.getWaiter_id() != null && !orderDB.getWaiter_id().isEmpty()) {
                String waiterName = orderDB.getWaiterFullName(orderDB.getWaiter_id());
                txvWaiterName.setText(waiterName);
            }

            if (orderDB.getOrderItemsDBS() != null) {
                existingOrderItemIds.clear();
                newOrderItemIds.clear();

                Query<OrderItemsDB> builder = daoSession.getOrderItemsDBDao().queryBuilder()
                        .where(OrderItemsDBDao.Properties.OrderId.eq(orderDB.getId())).build();

                if (builder != null && builder.list() != null && !builder.list().isEmpty()) {
                    for (OrderItemsDB orderItemsDB : builder.list()) {
                        existingOrderItemIds.add(orderItemsDB.getId());
                    }
                }

                LinkedHashMap<String, ArrayList<OrderItemsDB>> oldItemsMap = new LinkedHashMap<>();
                LinkedHashMap<String, ArrayList<OrderItemsDB>> newItemsMap = new LinkedHashMap<>();

                for (OrderItemsDB orderItemsDB : orderDB.getOrderItemsDBS()) {
                    if (existingOrderItemIds.contains(orderItemsDB.getId())) {
                        String chargeStatus = "";
                        if (orderItemsDB.getChargeReqDB() != null) {
                            ChargeReqDB chargeReqDB = orderItemsDB.getChargeReqDB();
                            chargeStatus = chargeReqDB.getStatus();
                        }
                        if (orderItemsDB.getStatus() != null
                                && !orderItemsDB.getStatus().equals(ORDER_STATUS_CANCELLED)
                                && !chargeStatus.equals(CHARGE_STATUS_CANCELLED)) {

                            String menuId = orderItemsDB.getMenu_item_id() + "_" + "OLD";

                            if (oldItemsMap.containsKey(menuId)) {
                                ArrayList<OrderItemsDB> orderItems = oldItemsMap.get(menuId);
                                orderItems.add(orderItemsDB);
                                oldItemsMap.put(menuId, orderItems);

                            } else {
                                ArrayList<OrderItemsDB> orderItems = new ArrayList<>();
                                orderItems.add(orderItemsDB);
                                oldItemsMap.put(menuId, orderItems);
                            }
                        }
                    } else {
                        String menuId = orderItemsDB.getMenu_item_id() + "_" + "NEW";
                        newOrderItemIds.add(orderItemsDB.getId());
                        if (newItemsMap.containsKey(menuId)) {
                            ArrayList<OrderItemsDB> orderItems = newItemsMap.get(menuId);
                            orderItems.add(orderItemsDB);
                            newItemsMap.put(menuId, orderItems);

                        } else {
                            ArrayList<OrderItemsDB> orderItems = new ArrayList<>();
                            orderItems.add(orderItemsDB);
                            newItemsMap.put(menuId, orderItems);
                        }
                    }
                }

                btnPlaceOrder.setVisibility(View.VISIBLE);

                if (oldItemsMap.isEmpty()) {
                    txvCurrentOrder.setText(LocalizationConstants.getInstance().getKeyValue("lbl_current_order"));
                    orderItemsRequests.putAll(newItemsMap);
                    if (newItemsMap.isEmpty()) {
                        txvCurrentOrder.setText(LocalizationConstants.getInstance().getKeyValue("lbl_existing_order"));
                    }

                } else {
                    if (!newItemsMap.isEmpty()) {
                        txvCurrentOrder.setText(LocalizationConstants.getInstance().getKeyValue("lbl_current_order"));
                        orderItemsRequests.putAll(newItemsMap);
                        orderItemsRequests.put("EXISTING ORDER", new ArrayList<OrderItemsDB>());
                    } else {
                        txvCurrentOrder.setText(LocalizationConstants.getInstance().getKeyValue("lbl_existing_order"));
                        btnPlaceOrder.setVisibility(View.GONE);
                    }
                    orderItemsRequests.putAll(oldItemsMap);
                }
                btnCreate.setText(LocalizationConstants.getInstance().getKeyValue("lbl_add_items"));

            } else {
                orderItemsRequests = new LinkedHashMap<>();
                btnPlaceOrder.setVisibility(View.GONE);
            }

        } else {
            orderDB = new OrderDB();
            orderDB.setId(UUID.randomUUID().toString());
            orderDB.setGuest_name(txvGuestName.getText().toString());
            orderDB.setPersons(Integer.valueOf(txvHeadCount.getText().toString()));
            orderDB.setStatus("OPEN");
            if (PreferenceUtils.getCurrentUser(getApplicationContext()) != null) {
                orderDB.setUser_id(PreferenceUtils.getCurrentUser(POSApplication.getInstance()).getId());
            }
            if (propertyTableDB != null) {
                orderDB.setTableId(propertyTableDB.getId());
            }
            orderItemsRequests = new LinkedHashMap<>();
            btnPlaceOrder.setVisibility(View.GONE);
            txvOrderNo.setVisibility(View.GONE);

            imgEditUserDetails.performClick();
        }

        createOrderItemLayout(orderItemsRequests);
        handleViewVisibility(orderItemsRequests);
    }

    public void createOrderItemLayout(final LinkedHashMap<String, ArrayList<OrderItemsDB>> orderMenuIdMap) {
        layoutCartItems.removeAllViews();

        ArrayList<String> menuItemIds = new ArrayList<>();
        if (!orderMenuIdMap.isEmpty()) {
            menuItemIds = new ArrayList<>(orderMenuIdMap.keySet());
        }

        for (int i = 0; i < menuItemIds.size(); i++) {

            final String menuId = menuItemIds.get(i);
            View convertView;
            if (menuId.equals("EXISTING ORDER")) {
                convertView = getLayoutInflater().inflate(R.layout.layout_new_order_section_header, null);
                TextView txvHeaderName = convertView.findViewById(R.id.txv_current_order);
                LinearLayout layout = convertView.findViewById(R.id.ll_header);
                layout.setEnabled(false);
                txvHeaderName.setEnabled(false);
                layout.setClickable(false);
                txvHeaderName.setClickable(false);
                txvHeaderName.setText(LocalizationConstants.getInstance().getKeyValue("lbl_existing_order"));

                layoutCartItems.addView(convertView);

            } else {
                convertView = getLayoutInflater().inflate(R.layout.layout_placed_order, null);
                TextView txvItemName = convertView.findViewById(R.id.txv_item_name);
                TextView txvItemQuantity = convertView.findViewById(R.id.txv_quantity);
                TextView txvRemarks = convertView.findViewById(R.id.txv_remarks);
                TextView txvDiscountPercent = convertView.findViewById(R.id.txv_discount);
                TextView txvOldPrice = convertView.findViewById(R.id.txv_old_price);
                TextView txvNewPrice = convertView.findViewById(R.id.txv_new_price);
                LinearLayout viewForeground = convertView.findViewById(R.id.view_foreground);
                LinearLayout rlRemarks = convertView.findViewById(R.id.rl_remarks);
                LinearLayout llDiscount = convertView.findViewById(R.id.ll_discount);

                if (!orderMenuIdMap.isEmpty() && orderMenuIdMap.containsKey(menuId)) {

                    final ArrayList<OrderItemsDB> orderItemsDBArrayList = orderMenuIdMap.get(menuId);

                    if (orderItemsDBArrayList != null && !orderItemsDBArrayList.isEmpty()) {

                        double charge = 0.0, adjustedCharge = 0.0;
                        String remarks = "";
                        Set<String> appliedDiscountsLbl = new HashSet<String>();

                        boolean isDiscountApplied = false;

                        for (OrderItemsDB item : orderItemsDBArrayList) {
                            ChargeReqDB chargeReqDB = item.getChargeReqDB();
                            if (chargeReqDB != null && !chargeReqDB.getStatus().equals(CHARGE_STATUS_CANCELLED)) {
                                charge = charge + item.getChargeReqDB().getAmount();
                                if (item.getChargeReqDB().getAdjustments() != null && !item.getChargeReqDB().getAdjustments().isEmpty()) {
                                    ArrayList<AdjustmentsDB> adjustmentsDBS = item.getChargeReqDB().getAdjustments();
                                    for (AdjustmentsDB adjustmentsDB : adjustmentsDBS) {
                                        if (!adjustmentsDB.getStatus().equals(CHARGE_STATUS_CANCELLED)) {
                                            adjustedCharge = adjustedCharge + adjustmentsDB.getAmount();
                                            appliedDiscountsLbl.add(adjustmentsDB.getName());
                                            isDiscountApplied = true;
                                        }
                                    }
                                }
                            }
                            if (item.getRemarks() != null && !item.getRemarks().isEmpty()) {
                                remarks = item.getRemarks();
                            }
                        }

                        String currency = FormatUtils.getCurrencySymbol();
                        String newCharge = new DecimalFormat("0.00").format(charge);
                        txvNewPrice.setText(String.valueOf(currency + " " + newCharge));

                        if (isDiscountApplied) {
                            String newPrice = new DecimalFormat("0.00").format(charge + adjustedCharge);
                            txvNewPrice.setText(String.format("%s %s", currency, newPrice));
                            txvOldPrice.setPaintFlags(txvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            txvOldPrice.setVisibility(View.VISIBLE);
                            txvOldPrice.setText(String.valueOf(currency + " " + newCharge));

                            String newDiscounts = appliedDiscountsLbl.toString();
                            if (newDiscounts.contains("[")) {
                                newDiscounts = newDiscounts.replace("[", "(");
                            }
                            if (newDiscounts.contains("]")) {
                                newDiscounts = newDiscounts.replace("]", ")");
                            }

                            txvDiscountPercent.setText(newDiscounts);
                            llDiscount.setVisibility(View.VISIBLE);

                        } else {
                            llDiscount.setVisibility(View.GONE);
                        }

                        final OrderItemsDB item = orderItemsDBArrayList.get(0);
                        txvItemName.setText(item.getDisplay_label());

                        if (!remarks.isEmpty()) {
                            txvRemarks.setText(remarks);
                            rlRemarks.setVisibility(View.VISIBLE);
                            txvRemarks.setTypeface(txvRemarks.getTypeface());
                        } else {
                            txvRemarks.setText(LocalizationConstants.getInstance().getKeyValue("lbl_no_remarks"));
                            rlRemarks.setVisibility(View.VISIBLE);
                            txvRemarks.setTypeface(txvRemarks.getTypeface(), Typeface.ITALIC);
                        }

                        txvItemQuantity.setText(String.format(Locale.US, "X %d", orderItemsDBArrayList.size()));

                        final int finalI = i;

                        if (orderDB != null
                                && !orderDB.getStatus().equals(Constants.TABLE_STATUS_BILL_ISSUED)
                                && permissions.containsKey(PERMISSION_ORDER_MODULE)
                                && permissions.get(PERMISSION_ORDER_MODULE).equals(PERMISSION_READ_WRITE)) {

                            viewForeground.setEnabled(true);
                            viewForeground.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    onItemClicked(orderItemsDBArrayList, finalI);
                                }
                            });

                            viewForeground.setOnLongClickListener(new View.OnLongClickListener() {
                                @Override
                                public boolean onLongClick(View view) {
                                    //Need New order items while discount is applied on old items with similar menu id
                                    ArrayList<OrderItemsDB> newOrderItems = null;
                                    if (menuId.contains("OLD")) {
                                        String newMenuId = menuId.replace("OLD", "NEW");
                                        newOrderItems = orderMenuIdMap.get(newMenuId);
                                    }
                                    showBottomSheet(item.getDisplay_label(), orderItemsDBArrayList, newOrderItems);
                                    return true;
                                }
                            });

                        } else {
                            viewForeground.setOnClickListener(null);
                            viewForeground.setOnLongClickListener(null);
                            viewForeground.setEnabled(false);
                        }

                        layoutCartItems.addView(convertView);
                    }
                }
            }
        }
    }

    public void onItemClicked(ArrayList<OrderItemsDB> orderItemsDBS, int i) {

        boolean allowEdit = false;
        if (!newOrderItemIds.isEmpty()) {
            for (OrderItemsDB orderItemsDB : orderItemsDBS) {
                if (newOrderItemIds.contains(orderItemsDB.getId()) && orderItemsDB.getMenuItemDB() == null) {
                    allowEdit = true;
                    break;
                }
            }

        } else {
            allowEdit = false;
        }
        if (allowEdit) {
            String menuId = orderItemsDBS.get(0).getMenu_item_id();
            int existingOrderItemQuant = 0;
            for (OrderItemsDB orderItemsDB : orderDB.getOrderItemsDBS()) {
                if (orderItemsDB.getStatus().equals(Constants.ORDER_STATUS_CANCELLED)) {
                    continue;
                }
                if (orderItemsDB.getMenu_item_id().contains(menuId)) {
                    existingOrderItemQuant++;
                }
            }

            CreateOrderDialogFragment createOrderDialogFragment = CreateOrderDialogFragment.newInstance(orderItemsDBS, i, menuId, existingOrderItemQuant);
            createOrderDialogFragment.show(getSupportFragmentManager(), "OrderUpdate");
        }
    }

    @Override
    public void onClick(View view) {
        if (view == btnCreate && btnCreate.getText().equals(LocalizationConstants.getInstance().getKeyValue("lbl_add_items"))) {
            if (orderDB != null) {

                HashMap<String, Integer> menuQuantityMap = new HashMap<>();
                LinkedHashMap<String, ArrayList<OrderItemsDB>> orderItems = new LinkedHashMap<>();
                ArrayList<OrderItemsDB> oldOrderItems = new ArrayList<>();
                int newItemsCount = 0;

                if (orderDB.getOrderItemsDBS() != null && !orderDB.getOrderItemsDBS().isEmpty()) {
                    for (OrderItemsDB orderItemsDB : orderDB.getOrderItemsDBS()) {
                        if (!orderItemsDB.getStatus().equals(ORDER_STATUS_CANCELLED)) {

                            if (newOrderItemIds.contains(orderItemsDB.getId())) {
                                newItemsCount++;

                                if (!orderItems.containsKey(orderItemsDB.getMenu_item_id())) {
                                    orderItems.put(orderItemsDB.getMenu_item_id(), new ArrayList<OrderItemsDB>());
                                }
                                ArrayList<OrderItemsDB> newList = orderItems.get(orderItemsDB.getMenu_item_id());
                                newList.add(orderItemsDB);
                            }

                            if (menuQuantityMap.containsKey(orderItemsDB.getMenu_item_id())) {
                                Integer quantity = menuQuantityMap.get(orderItemsDB.getMenu_item_id());
                                quantity = quantity + 1;
                                menuQuantityMap.put(orderItemsDB.getMenu_item_id(), quantity);

                            } else {
                                menuQuantityMap.put(orderItemsDB.getMenu_item_id(), 1);
                            }

                            oldOrderItems.add(orderItemsDB);
                        }
                    }
                }

                OrderHelper orderHelper = OrderHelper.getInstance();
                orderHelper.setNewOrderItems(orderItems);
                orderHelper.setOrderItems(oldOrderItems);

                Intent intent = HKMenuItemSelectionActivity.getLaunchIntent(getApplicationContext(), menuQuantityMap, orderDB.getTableId(), newItemsCount);
                startActivityForResult(intent, RQ_ADD_MENU_ITEMS);

            }

        } else if (view == btnPlaceOrder) {
            makeApiRequest(true, newOrderItemIds);

        } else if (view.getId() == R.id.ll_guest_details || view == imgEditUserDetails) {
            if (propertyTableDB != null && orderDB != null) {
                Intent intent = HKGuestDetailsActivity.getLaunchIntent(getApplicationContext(), orderDB);
                startActivityForResult(intent, RQ_GUEST_DETAILS);
            }
        }
    }

    @Override
    public void onFailure(Pair<String, Boolean> message) {
        cancelLoadingDialog();
        if (message.second) {
            Intent intent = new Intent();
            intent.putExtra(ARG_MESSAGE, message.first);
            setResult(RESULT_OK, intent);
            finish();
        } else if (!message.first.isEmpty()) {
            showMessageDialog(message.first, LocalizationConstants.getInstance().getKeyValue("btn_ok"), null);
        }
    }

    @Override
    public void onSuccess(List<Waiter> waiters) {
        if (waiters == null || waiters.isEmpty()) {
            return;
        }

        if (orderDB != null && orderDB.getWaiter_id() != null && !orderDB.getWaiter_id().isEmpty()) {
            for (Waiter waiter : waiters) {
                if (orderDB.getWaiter_id().equals(waiter.getId())) {
                    String waiterName = waiter.getFirst_name() + " " + waiter.getLast_name();
                    txvWaiterName.setText(waiterName);
                    break;
                }
            }
        }
    }

    @Override
    public void onSuccess(boolean isNewOrder, OrderDB orderDB, ArrayList<String> newOrderItemIds) {
        boolean isItemDeleted = false;
        try {
            if (mIsExistingItemDeleted) {
                isItemDeleted = true;
                mIsExistingItemDeleted = false;
                Toast.makeText(this, LocalizationConstants.getInstance().getKeyValue("toast_item_deleted"), Toast.LENGTH_SHORT).show();

            } else if (isNewOrder) {
                OrderPlacedAlertFragment alertFragment = new OrderPlacedAlertFragment();
                alertFragment.show(getSupportFragmentManager(), "ORDER_CREATED");

            } else {
                Toast.makeText(this, LocalizationConstants.getInstance().getKeyValue("toast_order_updated"), Toast.LENGTH_SHORT).show();
            }

            if (isItemDeleted || isNewOrder || !newOrderItemIds.isEmpty()) {
                makeKOTRequest(orderDB, isNewOrder, isItemDeleted, newOrderItemIds);
            }

            cancelLoadingDialog();
            setUp();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void makeKOTRequest(OrderDB orderDB, boolean isNewOrder, boolean isItemDeleted, ArrayList<String> newOrderItemIds) {
        if (this.orderDB != null && propertyTableDB != null && newOrderItemIds != null) {
            KOTBuilder billPrinter = new KOTBuilder();
            String bill = billPrinter.makeKOTRequest(orderDB, propertyTableDB, newOrderItemIds, isNewOrder, isItemDeleted);

            if (bill != null && !bill.isEmpty()) {
                ArrayList<PrinterProfile> deviceInfoArrayList = AccountPreferences.getPrinterProfiles(getApplicationContext());

                boolean isActive = false;
                for (PrinterProfile profile : deviceInfoArrayList) {
                    if (profile.isActive()) {
                        isActive = true;
                        break;
                    }
                }

                if (isActive) {
                    PrinterSetupAsyncTask printerSetup = new PrinterSetupAsyncTask(getApplicationContext(), bill);
                    printerSetup.execute();
                }
            }
        }
    }

    private void makeApiRequest(boolean showLoader, ArrayList<String> addLogsForIds) {
        if (orderDB != null && orderDB.getOrderItemsDBS() != null) {
            if (showLoader) {
                showLoadingDialog(LocalizationConstants.getInstance().getKeyValue("ldr_loading"));
            }
            OrderCreationController orderCreationController = new OrderCreationController(this, addLogsForIds);
            orderCreationController.execute(orderDB);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        isActive = true;
        if (data != null) {
            if (requestCode == RQ_GUEST_DETAILS && resultCode == RESULT_OK) {

                String message = data.getStringExtra(ARG_MESSAGE);
                if (message != null && !message.isEmpty()) {
                    Intent intent = new Intent();
                    intent.putExtra(ARG_MESSAGE, message);
                    setResult(RESULT_OK, intent);
                    finish();

                } else {
                    orderDB = OrderHelper.getInstance().getOrderDB();
                    OrderHelper.getInstance().setOrderDB(null);
                    if (orderDB != null) {
                        txvGuestName.setText(orderDB.getGuest_name());
                        txvHeadCount.setText(String.valueOf(orderDB.getPersons()));

                        String waiterName = data.getStringExtra(Constants.ARG_WAITER_NAME);
                        txvWaiterName.setText(waiterName);
                    }
                }

            } else if (requestCode == RQ_APPLY_PROMO && resultCode == RESULT_OK) {
                ArrayList<OrderItemsDB> newOrderItems = OrderHelper.getInstance().getOrderItems();
                ArrayList<AuditLogs> newAuditLogs = data.getParcelableArrayListExtra(ARG_AUDIT_LOGS);

                if (orderDB != null && orderDB.getOrderItemsDBS() != null) {
                    if (newOrderItems != null && !newOrderItems.isEmpty()) {

                        HashMap<String, OrderItemsDB> map = new HashMap<>();
                        for (OrderItemsDB orderItemsDB : newOrderItems) {
                            map.put(orderItemsDB.getId(), orderItemsDB);
                        }

                        ArrayList<OrderItemsDB> orderItems = orderDB.getOrderItemsDBS();
                        for (int i = 0; i < orderItems.size(); i++) {
                            OrderItemsDB orderItemsDB = orderItems.get(i);
                            if (map.containsKey(orderItemsDB.getId())) {
                                orderItemsDB = map.get(orderItemsDB.getId());
                                orderItems.set(i, orderItemsDB);
                            }
                        }
                    }
                    ArrayList<AuditLogs> auditLogs = orderDB.getAuditLogs();
                    if (auditLogs == null) {
                        auditLogs = new ArrayList<>();
                    }
                    auditLogs.addAll(newAuditLogs);
                    orderDB.setAuditLogs(auditLogs);
                }
                setUp();
                OrderHelper orderHelper = OrderHelper.getInstance();
                orderHelper.setOrderItems(new ArrayList<OrderItemsDB>());
                btnPlaceOrder.setVisibility(View.VISIBLE);
            }

        } else if (requestCode == RQ_ADD_MENU_ITEMS && resultCode == RESULT_OK) {
            ArrayList<OrderItemsDB> newOrderItems = OrderHelper.getInstance().getOrderItems();

            if (newOrderItems != null && !newOrderItems.isEmpty() && orderDB != null) {
                if (orderDB.getOrderItemsDBS() == null) {
                    orderDB.setOrderItemsDBS(new ArrayList<OrderItemsDB>());
                }

                ArrayList<OrderItemsDB> oldList = orderDB.getOrderItemsDBS();

                if (!oldList.isEmpty()) {
                    oldList.addAll(newOrderItems);
                    LinkedHashMap<String, ArrayList<OrderItemsDB>> orderItems = new LinkedHashMap<>();
                    for (OrderItemsDB aOrderItems : oldList) {
                        for (OrderItemsDB newItem : newOrderItems) {
                            if (!existingOrderItemIds.contains(aOrderItems.getId()) && aOrderItems.getMenu_item_id().equals(newItem.getMenu_item_id())) {
                                aOrderItems.setRemarks(newItem.getRemarks());
                                break;
                            }
                        }
                        if (!orderItems.containsKey(aOrderItems.getMenu_item_id())) {
                            orderItems.put(aOrderItems.getMenu_item_id(), new ArrayList<OrderItemsDB>());
                        }
                        orderItems.get(aOrderItems.getMenu_item_id()).add(aOrderItems);
                    }

                    if (!orderItems.isEmpty()) {
                        orderDB.getOrderItemsDBS().clear();
                        for (ArrayList<OrderItemsDB> orderItemsDBS : orderItems.values()) {
                            ArrayList<OrderItemsDB> itemsDBS = orderDB.getOrderItemsDBS();
                            itemsDBS.addAll(orderItemsDBS);
                        }
                    }
                } else {
                    oldList.addAll(newOrderItems);
                }
                setUp();
                OrderHelper orderHelper = OrderHelper.getInstance();
                orderHelper.setOrderItems(new ArrayList<OrderItemsDB>());
            }
        }

        if (isCurrentTableOrderModified) {
            setUp();
            isCurrentTableOrderModified = false;
        }
    }


    @Override
    public void increaseItemQuantity(ArrayList<OrderItemsDB> orderItemsRequest, int updateAtListIndex, String menuId) {
        if (orderDB != null && orderDB.getOrderItemsDBS() != null) {
            ArrayList<OrderItemsDB> orderItems = orderDB.getOrderItemsDBS();

            for (int i = 0; i < orderItems.size(); i++) {
                OrderItemsDB orderItemsDB = orderItems.get(i);

                if (orderItemsDB.getMenu_item_id().equals(orderItemsRequest.get(0).getMenu_item_id())) {
                    DiscountsHelper discountsHelper = new DiscountsHelper(this);
                    orderItemsRequest = discountsHelper.applyDiscounts(orderItemsRequest, orderItems, menuId);

                    if (orderItemsRequest == null || orderItemsRequest.isEmpty()) {
                        return;
                    }

                    orderItems.addAll(i, orderItemsRequest);
                    break;
                }
            }

            orderDB.setOrderItemsDBS(orderItems);
            setUp();
        }
    }

    @Override
    public void reduceItemQuantity(int newOrderItems, String menuId, int updateAtListIndex) {
        if (orderDB != null && orderDB.getOrderItemsDBS() != null) {
            ArrayList<OrderItemsDB> orderItemsDBS = orderDB.getOrderItemsDBS();

            for (int i = 0; i < orderItemsDBS.size(); i++) {
                OrderItemsDB orderItemsDB = orderItemsDBS.get(i);
                if (orderItemsDB.getMenu_item_id().equals(menuId) && !existingOrderItemIds.contains(orderItemsDB.getId())) {
                    if (newOrderItems != 0) {
                        orderItemsDBS.remove(orderItemsDB);
                        i--;
                        newOrderItems--;

                    } else {
                        break;
                    }
                }
            }
            setUp();
        }
    }

    @Override
    public void updateRemarks(String menuId, int updateAtListIndex, String remarks) {
        setUp();
    }

    private void showBottomSheet(String menuDisplayLbl, final ArrayList<OrderItemsDB> finalOrderItems, final ArrayList<OrderItemsDB> newOrderItems) {

        String[] titles = new String[0];

        boolean isNewOrder = false;
        for (OrderItemsDB orderItems : finalOrderItems) {
            if (newOrderItemIds.contains(orderItems.getId())) {
                isNewOrder = true;
                break;
            }
        }

        boolean allowOrderItemCancellation, allowApplyingDiscount;
        allowOrderItemCancellation = isNewOrder || permissions.containsKey(PERMISSION_DELETE_ORDER);
        allowApplyingDiscount = !isNewOrder && permissions.containsKey(PERMISSION_APPLY_DISCOUNT);

        if (allowOrderItemCancellation && allowApplyingDiscount) {
            titles = new String[]{LocalizationConstants.getInstance().getKeyValue("btn_delete")
                    , LocalizationConstants.getInstance().getKeyValue("btn_apply_promo")};

        } else if (allowOrderItemCancellation) {
            titles = new String[]{LocalizationConstants.getInstance().getKeyValue("btn_delete")};

        } else if (allowApplyingDiscount) {
            titles = new String[]{LocalizationConstants.getInstance().getKeyValue("btn_apply_promo")};
        }

        if (titles.length != 0) {
            final String message = String.format(LocalizationConstants.getInstance().getKeyValue("msg_delete_item_with_value"), menuDisplayLbl);

            final String[] finalTitles = titles;
            CustomBottomSheet.createBuilder(this, getSupportFragmentManager())
                    .setCancelButtonTitle(LocalizationConstants.getInstance().getKeyValue("btn_cancel"))
                    .setOtherButtonTitles(titles)
                    .setCancelableOnTouchOutside(true)
                    .setListener(new CustomBottomSheet.CustomBottomSheetListener() {
                        @Override
                        public void onDismiss(CustomBottomSheet actionSheet, boolean isCancel) {
                            actionSheet.dismiss();
                        }

                        @Override
                        public void onOtherButtonClick(CustomBottomSheet actionSheet, int index) {
                            if (finalTitles[index].equals(LocalizationConstants.getInstance().getKeyValue("btn_delete"))) {
                                String yesLbl = LocalizationConstants.getInstance().getKeyValue("btn_yes");
                                String noLbl = LocalizationConstants.getInstance().getKeyValue("btn_no");

                                showYesNoDialog(message, yesLbl, noLbl, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        if (i == Dialog.BUTTON_POSITIVE) {
                                            boolean isExistingItemDeleted = deleteItems(finalOrderItems);
                                            if (isExistingItemDeleted) {
                                                makeOrderItemCancellationReq(finalOrderItems);
                                            } else {
                                                setUp();
                                                Toast.makeText(getApplicationContext(), LocalizationConstants.getInstance().getKeyValue("toast_item_deleted"), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                        dialogInterface.dismiss();
                                    }
                                });
                            } else {
                                if (newOrderItems != null && !newOrderItems.isEmpty()) {
                                    finalOrderItems.addAll(newOrderItems);
                                }
                                Intent myIntent = HKDiscountsActivity.getLaunchIntent(getApplicationContext(), finalOrderItems);
                                startActivityForResult(myIntent, RQ_APPLY_PROMO);
                            }
                        }
                    }).show();

        }
    }

    private void makeOrderItemCancellationReq(ArrayList<OrderItemsDB> finalOrderItems) {
        if (orderDB != null && finalOrderItems != null) {
            showLoadingDialog(LocalizationConstants.getInstance().getKeyValue("ldr_loading"));

            OrderItemUpdate orderItemUpdate = new OrderItemUpdate();
            orderItemUpdate.setStatus(ORDER_STATUS_CANCELLED);
            orderItemUpdate.setTable_id(orderDB.getTableId());
            ArrayList<String> orderItemIds = new ArrayList<>();
            for (OrderItemsDB orderItemsDB : finalOrderItems) {
                orderItemIds.add(orderItemsDB.getId());
            }
            orderItemUpdate.setOrder_item_ids(orderItemIds);

            OrderItemUpdateController orderItemUpdateController = new OrderItemUpdateController(this);
            orderItemUpdateController.updateOrderItems(orderItemUpdate, orderDB.getId());
        }
    }

    @Override
    public void orderItemCancelled(ArrayList<String> orderItemCancelled) {
        makeApiRequest(false, orderItemCancelled);
    }

    boolean mIsExistingItemDeleted = false;

    private boolean deleteItems(ArrayList<OrderItemsDB> finalOrderItems) {
        boolean isExistingItemDeleted = false;
        for (OrderItemsDB orderItem : finalOrderItems) {
            if (existingOrderItemIds.contains(orderItem.getId())) {
                orderItem.setStatus(CHARGE_STATUS_CANCELLED);
                orderItem.setCancelled_user_id(PreferenceUtils.getCurrentUser(getApplicationContext()).getId());
                ChargeReqDB chargeReqDB = orderItem.getChargeReqDB();
                chargeReqDB.setStatus(CHARGE_STATUS_CANCELLED);
                orderItem.setChargeReqDB(chargeReqDB);
                isExistingItemDeleted = true;
                mIsExistingItemDeleted = true;

            } else if (newOrderItemIds.contains(orderItem.getId())) {
                ArrayList<OrderItemsDB> orderItemsDBS = orderDB.getOrderItemsDBS();
                for (int i = 0; i < orderItemsDBS.size(); i++) {
                    if (orderItemsDBS.get(i).getId().equals(orderItem.getId())) {
                        orderItemsDBS.remove(i);
                        i--;
                    }
                }
            }
        }
        return isExistingItemDeleted;
    }

    private void handleViewVisibility(LinkedHashMap<String, ArrayList<OrderItemsDB>> orderItemsRequests) {
        if (!orderItemsRequests.isEmpty()) {
            layoutEmptyCartItems.setVisibility(View.GONE);
            layoutCartItems.setVisibility(View.VISIBLE);
            layoutExistingOrder.setVisibility(View.VISIBLE);
            view.setVisibility(View.VISIBLE);
        } else {
            layoutEmptyCartItems.setVisibility(View.VISIBLE);
            layoutCartItems.setVisibility(View.GONE);
            layoutExistingOrder.setVisibility(View.GONE);
            view.setVisibility(View.GONE);
        }

        if (mIsExistingItemDeleted) {
            btnPlaceOrder.setVisibility(View.VISIBLE);

        } else if (newOrderItemIds.isEmpty()) {
            btnPlaceOrder.setVisibility(View.GONE);
        } else {
            btnPlaceOrder.setVisibility(View.VISIBLE);
        }

        boolean hasOrderModificationPerm = permissions.containsKey(PERMISSION_ORDER_MODULE) && permissions.get(PERMISSION_ORDER_MODULE).equals(PERMISSION_READ_WRITE);

        if (orderDB != null && (orderDB.getStatus().equals(ORDER_STATUS_BILL_ISSUED) || !hasOrderModificationPerm)) {
            findViewById(R.id.ll_buttons).setVisibility(View.GONE);
            btnCreate.setVisibility(View.GONE);
            btnPlaceOrder.setVisibility(View.GONE);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (btnPlaceOrder.getVisibility() == View.VISIBLE) {
            String message = LocalizationConstants.getInstance().getKeyValue("message_save_changes");
            String yesLbl = LocalizationConstants.getInstance().getKeyValue("btn_yes");
            String noLbl = LocalizationConstants.getInstance().getKeyValue("btn_no");

            showYesNoDialog(message, yesLbl, noLbl, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (i == Dialog.BUTTON_POSITIVE) {
                        dialogInterface.cancel();
                        makeApiRequest(true, newOrderItemIds);
                    } else {
                        orderDB = null;
                        dialogInterface.cancel();
                        setResult(RESULT_OK);
                        finish();
                    }
                }
            });
        } else {
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void okBtnClicked() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void removeCustomDiscount() {
        showMessageDialog(LocalizationConstants.getInstance().getKeyValue("msg_remove_custom_discount")
                , LocalizationConstants.getInstance().getKeyValue("btn_ok"), null);
    }
}
