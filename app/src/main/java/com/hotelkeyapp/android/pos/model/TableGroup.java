package com.hotelkeyapp.android.pos.model;

import com.google.gson.annotations.SerializedName;
import com.hotelkeyapp.android.pos.db.orm.TableGroupDB;

/**
 * Created by vrushalimankar on 11/05/18.
 */

public class TableGroup {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("code")
    private String code;

    @SerializedName("active")
    private boolean active;

    @SerializedName("property_id")
    private String property_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getProperty_id() {
        return property_id;
    }

    public void setProperty_id(String property_id) {
        this.property_id = property_id;
    }

    public TableGroupDB toTableGroupDB() {
        TableGroupDB tableDB = new TableGroupDB();
        tableDB.setId(id);
        tableDB.setName(name);
        tableDB.setCode(code);
        tableDB.setProperty_id(property_id);
        tableDB.setActive(active);
        return tableDB;
    }


}
