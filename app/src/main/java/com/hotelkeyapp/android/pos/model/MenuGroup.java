package com.hotelkeyapp.android.pos.model;

import com.google.gson.annotations.SerializedName;
import com.hotelkeyapp.android.pos.db.orm.MenuGroupDB;

import java.util.ArrayList;

/**
 * Created by vrushalimankar on 10/05/18.
 */

public class MenuGroup {

    @SerializedName("id")
    private String  id;

    @SerializedName("property_id")
    private String property_id;

    @SerializedName("name")
    private String  name;

    @SerializedName("code")
    private String  code;

    @SerializedName("created_user_id")
    private String created_user_id;

    @SerializedName("active")
    private boolean active;

    @SerializedName("menu_items")
    private ArrayList<MenuItems> menuItems;

    @SerializedName("display_on_order_taking")
    private boolean display_on_order_taking;

    public MenuGroupDB toMenuGroupDB(){
        MenuGroupDB menuGroupDB =new MenuGroupDB();
        menuGroupDB.setId(id);
        menuGroupDB.setProperty_id(property_id);
        menuGroupDB.setName(name);
        menuGroupDB.setCode(code);
        menuGroupDB.setActive(active);
        return  menuGroupDB;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProperty_id() {
        return property_id;
    }

    public void setProperty_id(String property_id) {
        this.property_id = property_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCreated_user_id() {
        return created_user_id;
    }

    public void setCreated_user_id(String created_user_id) {
        this.created_user_id = created_user_id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public ArrayList<MenuItems> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(ArrayList<MenuItems> menuItems) {
        this.menuItems = menuItems;
    }

    public boolean isDisplay_on_order_taking() {
        return display_on_order_taking;
    }

    public void setDisplay_on_order_taking(boolean display_on_order_taking) {
        this.display_on_order_taking = display_on_order_taking;
    }


}
