package com.hotelkeyapp.android.pos.model;

import com.google.gson.annotations.SerializedName;
import com.hotelkeyapp.android.pos.db.orm.OrderTypeDB;

import java.util.Arrays;

/**
 * Created by vrushalimankar on 10/05/18.
 */

public class OrderType {
    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("code")
    private String code;

    @SerializedName("property_id")
    private String property_id;

    @SerializedName("active")
    private boolean active;

    @SerializedName("collect_tip")
    private boolean collect_tip;

    @SerializedName("default_type")
    private boolean default_type;

    @SerializedName("require_card_as_tab")
    private boolean require_card_as_tab;

    @SerializedName("prepaid")
    private boolean prepaid;

    @SerializedName("item_statuses")
    private String[] item_statuses;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getProperty_id() {
        return property_id;
    }

    public void setProperty_id(String property_id) {
        this.property_id = property_id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isCollect_tip() {
        return collect_tip;
    }

    public void setCollect_tip(boolean collect_tip) {
        this.collect_tip = collect_tip;
    }

    public boolean isDefault_type() {
        return default_type;
    }

    public void setDefault_type(boolean default_type) {
        this.default_type = default_type;
    }

    public boolean isRequire_card_as_tab() {
        return require_card_as_tab;
    }

    public void setRequire_card_as_tab(boolean require_card_as_tab) {
        this.require_card_as_tab = require_card_as_tab;
    }

    public boolean isPrepaid() {
        return prepaid;
    }

    public void setPrepaid(boolean prepaid) {
        this.prepaid = prepaid;
    }

    public String[] getItem_statuses() {
        return item_statuses;
    }

    public void setItem_statuses(String[] item_statuses) {
        this.item_statuses = item_statuses;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public OrderTypeDB toOrderTypeDB(){
        OrderTypeDB orderItems =new OrderTypeDB();
        orderItems.setId(id);
        orderItems.setName(name);
        orderItems.setCode(code);
        orderItems.setActive(isActive());
        orderItems.setCollect_tip(collect_tip);
        orderItems.setDefault_type(default_type);
        orderItems.setPrepaid(prepaid);
        orderItems.setProperty_id(property_id);
        orderItems.setItem_statuses(Arrays.toString(item_statuses));
        return orderItems;
    }
}
