package com.hotelkeyapp.android.pos.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.R;
import com.hotelkeyapp.android.pos.activity.orderDetails.OrderHelper;
import com.hotelkeyapp.android.pos.db.orm.DaoSession;
import com.hotelkeyapp.android.pos.db.orm.MenuItemDB;
import com.hotelkeyapp.android.pos.db.orm.MenuItemDBDao;
import com.hotelkeyapp.android.pos.db.orm.OrderItemsDB;
import com.hotelkeyapp.android.pos.utils.LocalizationConstants;
import com.hotelkeyapp.android.pos.utils.OrderRequestCreationHelper;

import java.util.ArrayList;

import de.greenrobot.dao.query.QueryBuilder;


public class CreateOrderDialogFragment extends android.support.v4.app.DialogFragment {

    private static final String ARG_NEW_ORDER_ITEMS = "Existing order items";
    private static final String ARG_UPDATE_AT_INDEX = "Update At List Index";
    private static final String ARG_MENU_ID = "Menu Id";
    private static final String ARG_MENU_ITEM_QUANTITY = "Menu Quantity";

    private int existingQuantity = 1, quantity = 1, updateAtListIndex;

    DialogListenerCallbacks callbacks;

    private Context mContext;

    ArrayList<OrderItemsDB> orderItemsRequests;

    ArrayList<OrderItemsDB> oldOrderItems;

    private MenuItemDB menuItemDB;

    private String mRemarks = "", menuId = "";

    private Button btnUpdate, btnCancel;

    private TextView txvItemName;

    private EditText edtQuantityValue, edtRemarks;

    private ImageButton decQuantity, incQuantity;

    private int maxQuantity;

    public interface DialogListenerCallbacks {
        void increaseItemQuantity(ArrayList<OrderItemsDB> orderItemsRequests, int updateAtListIndex, String menuId);

        void reduceItemQuantity(int newOrderItems, String menuId, int updateAtListIndex);

        void updateRemarks(String menuId, int updateAtListIndex, String remarks);

    }

    public CreateOrderDialogFragment() {
        super();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    public static CreateOrderDialogFragment newInstance(ArrayList<OrderItemsDB> orderItemsDBS, int updateAtListIndex, String menuId, Integer existingItemQuantity) {
        CreateOrderDialogFragment createOrderDialogFragment = new CreateOrderDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(ARG_NEW_ORDER_ITEMS, orderItemsDBS);
        bundle.putInt(ARG_UPDATE_AT_INDEX, updateAtListIndex);
        bundle.putString(ARG_MENU_ID, menuId);
        bundle.putInt(ARG_MENU_ITEM_QUANTITY, existingItemQuantity);
        createOrderDialogFragment.setArguments(bundle);
        return createOrderDialogFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            orderItemsRequests = getArguments().getParcelableArrayList(ARG_NEW_ORDER_ITEMS);
            updateAtListIndex = getArguments().getInt(ARG_UPDATE_AT_INDEX);
            menuId = getArguments().getString(ARG_MENU_ID);
            existingQuantity = getArguments().getInt(ARG_MENU_ITEM_QUANTITY, 1);
        }
        oldOrderItems = OrderHelper.getInstance().getOrderItems();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.layout_create_order, container);

        callbacks = (DialogListenerCallbacks) getTargetFragment();

        if (callbacks == null) {
            callbacks = (DialogListenerCallbacks) getActivity();
        }

        initViews(view);
        int oldOrders = existingQuantity - orderItemsRequests.size();

        maxQuantity = oldOrders;
        if (oldOrders < 99) {
            maxQuantity = 99 - oldOrders;
        }

        if (orderItemsRequests.size() == 0) {
            edtQuantityValue.setText("1");
        } else {
            edtQuantityValue.setText(String.valueOf(orderItemsRequests.size()));
        }

        quantity = Integer.parseInt(edtQuantityValue.getText().toString());

        if (orderItemsRequests.size() <= 1) {
            decQuantity.setBackground(ContextCompat.getDrawable(mContext, R.drawable.button_disable));
        }

        if (existingQuantity >= 99) {
            incQuantity.setBackground(ContextCompat.getDrawable(mContext, R.drawable.button_disable));
        }

        for (OrderItemsDB orderItemsDB : orderItemsRequests) {
            if (orderItemsDB.getRemarks() != null && !orderItemsDB.getRemarks().isEmpty()) {
                mRemarks = orderItemsDB.getRemarks();
                edtRemarks.setText(mRemarks);
                edtRemarks.setSelection(mRemarks.length());
                break;
            }
        }

        DaoSession daoSession = POSApplication.getInstance().getDaoSession();
        final QueryBuilder<MenuItemDB> qb = daoSession.getMenuItemDBDao().queryBuilder();
        qb.where(MenuItemDBDao.Properties.Id.eq(menuId));

        if (qb.list() != null && !qb.list().isEmpty()) {
            menuItemDB = qb.list().get(0);
            if (menuItemDB != null && menuItemDB.getName() != null) {
                txvItemName.setText(menuItemDB.getName());
            }
        }

        incQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    quantity = edtQuantityValue.getText().toString().isEmpty() ? 0 : Integer.parseInt(edtQuantityValue.getText().toString());
                } catch (Exception e) {
                    quantity = 0;
                    e.printStackTrace();
                }

                if (quantity < maxQuantity) {
                    quantity++;
                    edtQuantityValue.setCursorVisible(false);
                    edtQuantityValue.setText(String.valueOf(quantity));
                    edtQuantityValue.setSelection(edtQuantityValue.getText().length());

                    YoYo.with(Techniques.SlideInUp)
                            .duration(250)
                            .repeat(0)
                            .playOn(edtQuantityValue);
                }
            }
        });

        decQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    quantity = edtQuantityValue.getText().toString().isEmpty() ? 0 : Integer.parseInt(edtQuantityValue.getText().toString());
                } catch (Exception e) {
                    quantity = 0;
                    e.printStackTrace();
                }

                if (quantity > 1) {
                    quantity--;
                    edtQuantityValue.setCursorVisible(false);
                    edtQuantityValue.setText(String.valueOf(quantity));
                    edtQuantityValue.setSelection(edtQuantityValue.getText().length());

                    YoYo.with(Techniques.SlideInDown)
                            .duration(250)
                            .repeat(0)
                            .playOn(edtQuantityValue);

                }
            }
        });

        edtQuantityValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    quantity = edtQuantityValue.getText().toString().isEmpty() ? 0 : Integer.parseInt(edtQuantityValue.getText().toString());
                } catch (Exception e) {
                    quantity = 0;
                    e.printStackTrace();
                }


                if (quantity >= maxQuantity) {
                    incQuantity.setBackground(ContextCompat.getDrawable(mContext, R.drawable.button_disable));
                } else {
                    incQuantity.setBackground(ContextCompat.getDrawable(mContext, R.drawable.button_enable));
                }

                if (quantity < 2) {
                    decQuantity.setBackground(ContextCompat.getDrawable(mContext, R.drawable.button_disable));
                } else {
                    decQuantity.setBackground(ContextCompat.getDrawable(mContext, R.drawable.button_enable));
                }

                edtQuantityValue.setCursorVisible(true);
            }
        });

        btnUpdate.setText(LocalizationConstants.getInstance().getKeyValue("btn_update"));
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (quantity == 0) {
                    Toast.makeText(mContext, LocalizationConstants.getInstance().getKeyValue("toast_increase_quantity"), Toast.LENGTH_SHORT).show();
                    return;
                } else if (quantity > maxQuantity) {
                    Toast.makeText(mContext, LocalizationConstants.getInstance().getKeyValue("toast_decrease_quantity"), Toast.LENGTH_SHORT).show();
                    return;
                }

                String remarks = edtRemarks.getText().toString();
                if (!remarks.equals(mRemarks)) {
                    for (OrderItemsDB orderItemsDB : orderItemsRequests) {
                        orderItemsDB.setRemarks(remarks);
                    }
                }

                int newQuantity1;
                if (orderItemsRequests.size() < quantity) {
                    newQuantity1 = quantity - orderItemsRequests.size();
                    ArrayList<OrderItemsDB> orderItemsDBArrayList = OrderRequestCreationHelper.createRequest(menuItemDB, newQuantity1, edtRemarks.getText().toString());
                    callbacks.increaseItemQuantity(orderItemsDBArrayList, updateAtListIndex, menuItemDB.getId());

                } else if (orderItemsRequests.size() > quantity) {
                    newQuantity1 = orderItemsRequests.size() - quantity;
                    callbacks.reduceItemQuantity(newQuantity1, menuId, updateAtListIndex);

                } else if (orderItemsRequests.size() == quantity) {
                    callbacks.updateRemarks(menuId, updateAtListIndex, remarks);
                }
                dismiss();
            }
        });

        btnCancel.setText(LocalizationConstants.getInstance().getKeyValue("btn_cancel"));
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        try {
            getDialog().getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    private void initViews(View view) {
        txvItemName = view.findViewById(R.id.edt_name);

        TextView txvQuantity = view.findViewById(R.id.txv_quantity);
        txvQuantity.setText(LocalizationConstants.getInstance().getKeyValue("lbl_quantity"));

        edtRemarks = view.findViewById(R.id.edt_remarks);
        edtRemarks.requestFocus();
        edtRemarks.setHint(LocalizationConstants.getInstance().getKeyValue("lbl_remarks"));

        btnUpdate = view.findViewById(R.id.btn_add);
        btnCancel = view.findViewById(R.id.btn_cancel);

        edtQuantityValue = view.findViewById(R.id.txv_quantity_value);

        incQuantity = view.findViewById(R.id.btn_plus);
        decQuantity = view.findViewById(R.id.btn_minus);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }
}

