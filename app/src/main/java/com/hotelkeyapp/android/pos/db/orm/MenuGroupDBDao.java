package com.hotelkeyapp.android.pos.db.orm;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import com.hotelkeyapp.android.pos.db.orm.MenuGroupDB;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "MENU_GROUP_DB".
*/
public class MenuGroupDBDao extends AbstractDao<MenuGroupDB, String> {

    public static final String TABLENAME = "MENU_GROUP_DB";

    /**
     * Properties of entity MenuGroupDB.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, String.class, "id", true, "ID");
        public final static Property Name = new Property(1, String.class, "name", false, "NAME");
        public final static Property Code = new Property(2, String.class, "code", false, "CODE");
        public final static Property Property_id = new Property(3, String.class, "property_id", false, "PROPERTY_ID");
        public final static Property Active = new Property(4, Boolean.class, "active", false, "ACTIVE");
        public final static Property Description = new Property(5, String.class, "description", false, "DESCRIPTION");
    };

    private DaoSession daoSession;


    public MenuGroupDBDao(DaoConfig config) {
        super(config);
    }
    
    public MenuGroupDBDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"MENU_GROUP_DB\" (" + //
                "\"ID\" TEXT PRIMARY KEY NOT NULL ," + // 0: id
                "\"NAME\" TEXT," + // 1: name
                "\"CODE\" TEXT," + // 2: code
                "\"PROPERTY_ID\" TEXT," + // 3: property_id
                "\"ACTIVE\" INTEGER," + // 4: active
                "\"DESCRIPTION\" TEXT);"); // 5: description
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"MENU_GROUP_DB\"";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, MenuGroupDB entity) {
        stmt.clearBindings();
 
        String id = entity.getId();
        if (id != null) {
            stmt.bindString(1, id);
        }
 
        String name = entity.getName();
        if (name != null) {
            stmt.bindString(2, name);
        }
 
        String code = entity.getCode();
        if (code != null) {
            stmt.bindString(3, code);
        }
 
        String property_id = entity.getProperty_id();
        if (property_id != null) {
            stmt.bindString(4, property_id);
        }
 
        Boolean active = entity.getActive();
        if (active != null) {
            stmt.bindLong(5, active ? 1L: 0L);
        }
 
        String description = entity.getDescription();
        if (description != null) {
            stmt.bindString(6, description);
        }
    }

    @Override
    protected void attachEntity(MenuGroupDB entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    /** @inheritdoc */
    @Override
    public String readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public MenuGroupDB readEntity(Cursor cursor, int offset) {
        MenuGroupDB entity = new MenuGroupDB( //
            cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // name
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // code
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // property_id
            cursor.isNull(offset + 4) ? null : cursor.getShort(offset + 4) != 0, // active
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5) // description
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, MenuGroupDB entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0));
        entity.setName(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setCode(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setProperty_id(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setActive(cursor.isNull(offset + 4) ? null : cursor.getShort(offset + 4) != 0);
        entity.setDescription(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
     }
    
    /** @inheritdoc */
    @Override
    protected String updateKeyAfterInsert(MenuGroupDB entity, long rowId) {
        return entity.getId();
    }
    
    /** @inheritdoc */
    @Override
    public String getKey(MenuGroupDB entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
