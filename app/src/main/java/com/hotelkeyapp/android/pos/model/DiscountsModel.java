package com.hotelkeyapp.android.pos.model;

import com.hotelkey.hkandroidlib.utils.PreferenceUtils;
import com.hotelkeyapp.android.pos.POSApplication;
import com.hotelkeyapp.android.pos.db.orm.AdjustmentsDB;
import com.hotelkeyapp.android.pos.utils.AccountPreferences;

import java.util.UUID;

import static com.hotelkeyapp.android.pos.utils.Constants.CHARGE_STATUS_CONFIRMED;

/**
 * Created by vrushalimankar on 28/06/18.
 */

public class DiscountsModel {

    private String id;

    private String name;

    private String code;

    private String adjustmentCodeId;

    private Boolean active;

    private double amount;

    private boolean isCustom;

    private boolean isPercentage;

    private boolean isConfirmAdjustment = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAdjustmentCodeId() {
        return adjustmentCodeId;
    }

    public void setAdjustmentCodeId(String adjustmentCodeId) {
        this.adjustmentCodeId = adjustmentCodeId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public boolean isPercentage() {
        return isPercentage;
    }

    public void setPercentage(boolean percentage) {
        isPercentage = percentage;
    }

    public boolean isCustom() {
        return isCustom;
    }

    public void setCustom(boolean custom) {
        isCustom = custom;
    }

    public boolean isConfirmAdjustment() {
        return isConfirmAdjustment;
    }

    public void setConfirmAdjustment(boolean confirmAdjustment) {
        this.isConfirmAdjustment = confirmAdjustment;
    }

    public AdjustmentsDB toAdjustments(double newCharge, String chargeTypeId, String chargeId){
        AdjustmentsDB adjustmentsDB = new AdjustmentsDB();
        adjustmentsDB.setId(UUID.randomUUID().toString());
        adjustmentsDB.setAmount(-newCharge);
        adjustmentsDB.setCharge_id(chargeId);
        adjustmentsDB.setCharge_type_id(chargeTypeId);
        adjustmentsDB.setAdjustment_code_id(adjustmentCodeId);
        adjustmentsDB.setBusiness_day_id(AccountPreferences.getBusinessDayID(POSApplication.getInstance()));
        adjustmentsDB.setPosted_business_day_id(AccountPreferences.getBusinessDayID(POSApplication.getInstance()));
        adjustmentsDB.setDiscount_id(this.id);
        adjustmentsDB.setDate(PreferenceUtils.getCBD(POSApplication.getInstance()));
        adjustmentsDB.setName(name);
        adjustmentsDB.setDiscountCode(code);
        adjustmentsDB.setPosted_date(PreferenceUtils.getCBD(POSApplication.getInstance()));
        adjustmentsDB.setQuantity(1);
        adjustmentsDB.setShift_id(AccountPreferences.getShiftID(POSApplication.getInstance()));
        adjustmentsDB.setStatus(CHARGE_STATUS_CONFIRMED);

        return adjustmentsDB;
    }
}
