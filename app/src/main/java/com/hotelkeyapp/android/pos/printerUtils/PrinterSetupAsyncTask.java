package com.hotelkeyapp.android.pos.printerUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.epson.epos2.Epos2Exception;
import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;
import com.hotelkeyapp.android.pos.model.PrinterProfile;
import com.hotelkeyapp.android.pos.utils.AccountPreferences;
import com.izettle.html2bitmap.Html2Bitmap;
import com.izettle.html2bitmap.content.WebViewContent;

import java.util.ArrayList;
import java.util.HashMap;

import io.fabric.sdk.android.services.concurrency.AsyncTask;

/**
 * Created by vrushalimankar on 01/10/18.
 */

public class PrinterSetupAsyncTask extends AsyncTask<Boolean, Printer, Bitmap> implements ReceiveListener {

    private static final String TAG = PrinterSetupAsyncTask.class.toString();

    private Context mContext;

    private String preview;

    public PrinterSetupAsyncTask(Context mContext, String preview) {
        this.mContext = mContext;
        this.preview = preview;
    }

    @Override
    protected Bitmap doInBackground(Boolean... voids) {
        return new Html2Bitmap.Builder().setBitmapWidth(1600).setContext(mContext).setContent(WebViewContent.html(preview)).build().getBitmap();
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        if (bitmap != null) {
            makePrint(bitmap);
        }
    }

    private void makePrint(Bitmap bitmap) {
        ArrayList<PrinterProfile> deviceInfoArrayList = AccountPreferences.getPrinterProfiles(mContext);
        HashMap<String, PrinterProfile> printerProfileMap = new HashMap<>();

        for (PrinterProfile printerProfile1 : deviceInfoArrayList) {
            printerProfileMap.put(printerProfile1.getIpAddress(), printerProfile1);
        }

        for (String id : printerProfileMap.keySet()) {
            PrinterProfile printerProfile = printerProfileMap.get(id);

            Printer printer = initializeObject();
            if (printer == null) {
                continue;
            }

            if (!createReceiptData(printer, printerProfile.getNumberOfCopies(), bitmap)) {
                finalizeObject(printer);
                continue;
            }

            if (!printData(printer,printerProfile)) {
                finalizeObject(printer);
            }
        }
    }

    private Printer initializeObject() {
        try {
            Printer printer = new Printer(Printer.TM_T82, Printer.MODEL_ANK, mContext);
            printer.setReceiveEventListener(this);
            return printer;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean createReceiptData(Printer printer, int numberOfCopies, Bitmap bitmap) {
        if (printer == null) {
            return false;
        }
        try {
            printer.addTextAlign(Printer.ALIGN_CENTER);

            for (int i = 0; i < numberOfCopies; i++) {

                printer.addImage(bitmap, 0, 0,
                        bitmap.getWidth(),
                        bitmap.getHeight(),
                        Printer.COLOR_1,
                        Printer.MODE_MONO,
                        Printer.HALFTONE_DITHER,
                        0.4,
                        Printer.COMPRESS_AUTO);

                printer.addFeedLine(1);
                printer.addCut(Printer.CUT_FEED);
            }

        } catch (Exception e) {
            e.printStackTrace();
            finalizeObject(printer);
            return false;
        }

        return true;
    }

    private boolean printData(Printer printer ,PrinterProfile activeDeviceProfile) {
        if (printer == null) {
            return false;
        }

        if (!connectPrinter(printer,activeDeviceProfile)) {
            return false;
        }

        PrinterStatusInfo status = printer.getStatus();

        if (!isPrintable(status)) {
            try {
                printer.disconnect();
                Log.d(TAG, "disconnect done");

            } catch (Exception ex) {
                Log.d(TAG, "disconnect failed");
                // Do nothing
            }
            return false;
        }

        Log.d(TAG, "Printer is ready");


        try {
            printer.sendData(Printer.PARAM_DEFAULT);
            Log.d(TAG, "Printer send data");

        } catch (Exception e) {
            e.printStackTrace();
            try {
                printer.disconnect();
                Log.d(TAG, "disconnect done");
            } catch (Exception ex) {
                Log.d(TAG, "disconnect failed");
                // Do nothing
            }
            return false;
        }

        return true;
    }

    private boolean isPrintable(PrinterStatusInfo status) {
        if (status == null) {
            return false;
        }

        if (status.getConnection() == Printer.FALSE) {
            return false;

        } else if (status.getOnline() == Printer.FALSE) {
            return false;
        } else {
        }
        return true;
    }

    private boolean connectPrinter(Printer printer, PrinterProfile activeDeviceProfile) {
        boolean isBeginTransaction = false;

        if (printer == null) {
            return false;
        }

        try {
            String target = String.format("TCP:%s", activeDeviceProfile.getIpAddress());
            Log.d(TAG, target);
            printer.connect(target, Printer.PARAM_DEFAULT);
            Log.d(TAG, "Connect done");

        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Connect failed");
            return false;
        }

        try {
            printer.beginTransaction();
            isBeginTransaction = true;
            Log.d(TAG, "beginTransaction done");

        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "beginTransaction failed");
        }

        if (!isBeginTransaction) {
            try {
                printer.disconnect();
                Log.d(TAG, "disconnect done");
            } catch (Epos2Exception e) {
                Log.d(TAG, "disconnect failed");
                return false;
            }
        }

        return true;
    }

    @Override
    public void onPtrReceive(final Printer printer, int i, final PrinterStatusInfo printerStatusInfo, String printJobId) {
        new Thread(new Runnable() {
            @Override
            public synchronized void run() {
                disconnectPrinter(printer);
            }
        }).start();
    }


    private void disconnectPrinter(Printer printer) {
        if (printer == null) {
            return;
        }
        try {
            printer.endTransaction();
            printer.disconnect();
            Log.d(TAG, "disconnect done");

        } catch (final Exception e) {
            Log.d(TAG, "disconnect failed");
            e.printStackTrace();
        }
        finalizeObject(printer);
    }

    private void finalizeObject(Printer printer) {
        if (printer == null) {
            return;
        }
        printer.clearCommandBuffer();
        printer.setReceiveEventListener(null);
        printer = null;
        Log.d(TAG, "printer finalised");
    }
}
