package com.hotelkeyapp.android.pos.printerUtils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.epson.epos2.Epos2Exception;
import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;
import com.hotelkeyapp.android.pos.utils.LocalizationConstants;

/**
 * Created by vrushalimankar on 06/10/18.
 */

public class TestConnectionAsyncTask extends AsyncTask<Void, Printer, String> implements ReceiveListener {

    private final String hostIpAddress;

    private String message;

    private Context mContext;

    private Printer mPrinter;

    private TestConnectionCallbacks callbacks;

    public interface TestConnectionCallbacks {
        void connectionCallbacks(String message);
    }

    public TestConnectionAsyncTask(Context mContext, String hostIpAddress, TestConnectionCallbacks testConnectionCallbacks) {
        this.mContext = mContext;
        this.hostIpAddress = hostIpAddress;
        this.callbacks = testConnectionCallbacks;
    }

    @Override
    protected String doInBackground(Void... voids) {
        return initializeObject();
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        callbacks.connectionCallbacks(result);
    }

    private String initializeObject() {
        try {
            mPrinter = PrinterConfigModel.getInstance().getPrinter();
            if (mPrinter == null) {
                mPrinter = new Printer(Printer.TM_T82, Printer.MODEL_ANK, mContext);
                PrinterConfigModel.getInstance().setPrinter(mPrinter);
            }
            mPrinter.setReceiveEventListener(this);

            boolean isPrinterConnected = connectPrinter();
            if (isPrinterConnected) {
                PrinterStatusInfo status = mPrinter.getStatus();

                if (!isPrintable(status)) {
                    mPrinter.disconnect();
                    message = LocalizationConstants.getInstance().getKeyValue("msg_unable_to_connect_to_printer");

                }
            } else {
                throw new Exception("Printer not connected");
            }

        } catch (Exception e) {
            if (e instanceof Epos2Exception) {
                String message = getEposExceptionText(((Epos2Exception) e).getErrorStatus());
                System.out.println("Epos2Exception : " + message);
            }
            message = LocalizationConstants.getInstance().getKeyValue("msg_unable_to_connect_to_printer");
            e.printStackTrace();
        }
        disconnectPrinter();
        return message;
    }

    private boolean isPrintable(PrinterStatusInfo status) {
        if (status == null) {
            return false;
        }
        if (status.getConnection() == Printer.FALSE) {
            return false;
        } else if (status.getOnline() == Printer.FALSE) {
            return false;
        }
        return true;
    }

    private boolean connectPrinter() {
        boolean isBeginTransaction = false;

        try {
            String target = String.format("TCP:%s", this.hostIpAddress);
            Log.d(KOTBuilder.TAG, target);
            mPrinter.connect(target, Printer.PARAM_DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        try {
            mPrinter.beginTransaction();
            isBeginTransaction = true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!isBeginTransaction) {
            try {
                mPrinter.disconnect();
            } catch (Epos2Exception e) {
                return false;
            }
        }

        return true;
    }

    @Override
    public void onPtrReceive(Printer printer, int code, PrinterStatusInfo printerStatusInfo, String s) {
        new Thread(new Runnable() {
            @Override
            public synchronized void run() {
                disconnectPrinter();
            }
        }).start();

    }

    private void disconnectPrinter() {
        if (mPrinter == null) {
            return;
        }
        try {
            mPrinter.endTransaction();
            mPrinter.disconnect();
        } catch (final Exception e) {
            e.printStackTrace();
        }
        finalizeObject(mPrinter);
    }

    private void finalizeObject(Printer printer) {
        mPrinter = printer;
        if (mPrinter == null) {
            return;
        }
        mPrinter.clearCommandBuffer();
        mPrinter.setReceiveEventListener(null);
        mPrinter = null;
    }

    private static String getEposExceptionText(int state) {
        String return_text = "";
        switch (state) {
            case Epos2Exception.ERR_PARAM:
                return_text = "ERR_PARAM";
                break;
            case Epos2Exception.ERR_CONNECT:
                return_text = "ERR_CONNECT";
                break;
            case Epos2Exception.ERR_TIMEOUT:
                return_text = "ERR_TIMEOUT";
                break;
            case Epos2Exception.ERR_MEMORY:
                return_text = "ERR_MEMORY";
                break;
            case Epos2Exception.ERR_ILLEGAL:
                return_text = "ERR_ILLEGAL";
                break;
            case Epos2Exception.ERR_PROCESSING:
                return_text = "ERR_PROCESSING";
                break;
            case Epos2Exception.ERR_NOT_FOUND:
                return_text = "ERR_NOT_FOUND";
                break;
            case Epos2Exception.ERR_IN_USE:
                return_text = "ERR_IN_USE";
                break;
            case Epos2Exception.ERR_TYPE_INVALID:
                return_text = "ERR_TYPE_INVALID";
                break;
            case Epos2Exception.ERR_DISCONNECT:
                return_text = "ERR_DISCONNECT";
                break;
            case Epos2Exception.ERR_ALREADY_OPENED:
                return_text = "ERR_ALREADY_OPENED";
                break;
            case Epos2Exception.ERR_ALREADY_USED:
                return_text = "ERR_ALREADY_USED";
                break;
            case Epos2Exception.ERR_BOX_COUNT_OVER:
                return_text = "ERR_BOX_COUNT_OVER";
                break;
            case Epos2Exception.ERR_BOX_CLIENT_OVER:
                return_text = "ERR_BOX_CLIENT_OVER";
                break;
            case Epos2Exception.ERR_UNSUPPORTED:
                return_text = "ERR_UNSUPPORTED";
                break;
            case Epos2Exception.ERR_FAILURE:
                return_text = "ERR_FAILURE";
                break;
            default:
                return_text = String.format("%d", state);
                break;
        }
        return return_text;
    }

}
