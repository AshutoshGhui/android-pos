package com.hotelkeyapp.android.pos.model.order;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by vrushalimankar on 19/08/18.
 */

public class AuditLogs implements Parcelable {

    @SerializedName("action")
    private String action;

    @SerializedName("business_date")
    private Date business_date;

    @SerializedName("previous_value")
    private String previous_value;

    @SerializedName("current_value")
    private String current_value;

    @SerializedName("remark")
    private String remark;

    @SerializedName("date")
    private String date;

    @SerializedName("user")
    private String user;

    @SerializedName("item_name")
    private String item_name;

    @SerializedName("id")
    private String id;

    protected AuditLogs(Parcel in) {
        action = in.readString();
        business_date = new Date(in.readLong());
        previous_value = in.readString();
        current_value = in.readString();
        remark = in.readString();
        date = in.readString();
        user = in.readString();
        item_name = in.readString();
        id = in.readString();
    }

    public AuditLogs() {
    }

    public static final Creator<AuditLogs> CREATOR = new Creator<AuditLogs>() {
        @Override
        public AuditLogs createFromParcel(Parcel in) {
            return new AuditLogs(in);
        }

        @Override
        public AuditLogs[] newArray(int size) {
            return new AuditLogs[size];
        }
    };

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Date getBusiness_date() {
        return business_date;
    }

    public void setBusiness_date(Date business_date) {
        this.business_date = business_date;
    }

    public String getPrevious_value() {
        return previous_value;
    }

    public void setPrevious_value(String previous_value) {
        this.previous_value = previous_value;
    }

    public String getCurrent_value() {
        return current_value;
    }

    public void setCurrent_value(String current_value) {
        this.current_value = current_value;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(action);
        parcel.writeLong(business_date.getTime());
        parcel.writeString(previous_value);
        parcel.writeString(current_value);
        parcel.writeString(remark);
        parcel.writeString(date);
        parcel.writeString(user);
        parcel.writeString(item_name);
        parcel.writeString(id);
    }
}
