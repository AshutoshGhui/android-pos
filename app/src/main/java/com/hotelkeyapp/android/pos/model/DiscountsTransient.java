package com.hotelkeyapp.android.pos.model;

import com.google.gson.annotations.SerializedName;
import com.hotelkey.hkandroidlib.db.orm.DiscountDB;
import com.hotelkeyapp.android.pos.db.orm.DiscountTypesDB;
import com.hotelkeyapp.android.pos.db.orm.OrderTypeDB;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ashutoshghui on 01/06/18.
 */

public class DiscountsTransient {

    @SerializedName("name")
    private  String name;
    @SerializedName("code")
    private String code;
    @SerializedName("adjustment_code_id")
    private  String adjustmentCodeId;
    @SerializedName("active")
    private Boolean isActive;
    @SerializedName("id")
    private String id;
    @SerializedName("discount_periods")
    private ArrayList<DiscountPeriods> discountPeriodsArrayList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAdjustmentCodeId() {
        return adjustmentCodeId;
    }

    public void setAdjustmentCodeId(String adjustmentCodeId) {
        this.adjustmentCodeId = adjustmentCodeId;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<DiscountPeriods> getDiscountPeriodsArrayList() {
        return discountPeriodsArrayList;
    }

    public void setDiscountPeriodsArrayList(ArrayList<DiscountPeriods> discountPeriodsArrayList) {
        this.discountPeriodsArrayList = discountPeriodsArrayList;
    }


    public DiscountTypesDB toDiscountTypeDB(){
        DiscountTypesDB discountDB =new DiscountTypesDB();
        discountDB.setActive(this.getActive());
        discountDB.setName(this.getName());
        discountDB.setAdjustmentCodeId(this.getAdjustmentCodeId());
        discountDB.setCode(this.getCode());
        discountDB.setId(this.getId());
        return discountDB;
    }
}
