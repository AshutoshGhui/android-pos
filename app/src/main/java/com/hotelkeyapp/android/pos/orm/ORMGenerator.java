package com.hotelkeyapp.android.pos.orm;


import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;
import de.greenrobot.daogenerator.ToMany;

public class ORMGenerator {

    private static final String PROJECT_DIR = System.getProperty("user.dir");

    private static final String SCHEMA = "com.hotelkeyapp.android.pos.db.orm";

    public static void main(String[] args) {

        Schema schema = new Schema(30, SCHEMA);
        schema.enableKeepSectionsByDefault();

        addTableGroups(schema);
        addTables(schema);
        addWaiters(schema);
        addChargeTypes(schema);
        addOrderTypes(schema);
        addNotifications(schema);
        addFolioTypes(schema);
        addDepartments(schema);
        addFolios(schema);
        addDiscountsChargeDB(schema);
        addAdjustments(schema);

        try {
            new DaoGenerator().generateAll(schema, PROJECT_DIR + "/app/src/main/java");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addFolios(Schema schema) {
        Entity folioReqDB = schema.addEntity("FolioReqDB");
        folioReqDB.addStringProperty("id");
        folioReqDB.addStringProperty("name");
        folioReqDB.addStringProperty("code");
        folioReqDB.addDoubleProperty("net_payments");
        folioReqDB.addDoubleProperty("total_taxes");
        folioReqDB.addDoubleProperty("total_charges");
        folioReqDB.addStringProperty("folio_type_id");
        folioReqDB.addDoubleProperty("device_created_at");
        folioReqDB.addStringProperty("orderId");
    }

    private static void addDepartments(Schema schema) {
        Entity departmentDB = schema.addEntity("DepartmentDB");
        departmentDB.addStringProperty("id");
        departmentDB.addStringProperty("name");
        departmentDB.addStringProperty("type");
        departmentDB.addStringProperty("property_id");
        departmentDB.addStringProperty("folio_type_id");
        departmentDB.addBooleanProperty("active");
    }

    private static void addFolioTypes(Schema schema) {
        Entity folioTypeDB = schema.addEntity("FolioTypeDB");
        folioTypeDB.addStringProperty("id");
        folioTypeDB.addStringProperty("name");
        folioTypeDB.addStringProperty("code");
        folioTypeDB.addStringProperty("property_id");
        folioTypeDB.addBooleanProperty("hide_rates");
    }

    private static void addNotifications(Schema schema) {
        Entity notificationDB = schema.addEntity("NotificationDB");
        notificationDB.addStringProperty("id").primaryKey();
        notificationDB.addStringProperty("order_id");
        notificationDB.addStringProperty("order_item_id");
        notificationDB.addStringProperty("user_id");
        notificationDB.addStringProperty("status");
        notificationDB.addStringProperty("table_id");
        notificationDB.addStringProperty("created_at");
        notificationDB.addStringProperty("updated_at");
        notificationDB.addBooleanProperty("item_recently_updated");
    }

    private static void addWaiters(Schema schema) {
        Entity waiterDB = schema.addEntity("WaiterDB");

        waiterDB.addStringProperty("id");
        waiterDB.addStringProperty("first_name");
        waiterDB.addStringProperty("last_name");
        waiterDB.addStringProperty("username");
        waiterDB.addBooleanProperty("active");
        waiterDB.addStringProperty("language_code");
        waiterDB.addIntProperty("counter");
    }

    private static void addOrderTypes(Schema schema) {
        Entity orderTypeDB = schema.addEntity("OrderTypeDB");

        orderTypeDB.addStringProperty("id");
        orderTypeDB.addStringProperty("name");
        orderTypeDB.addStringProperty("code");
        orderTypeDB.addStringProperty("property_id");
        orderTypeDB.addBooleanProperty("active");
        orderTypeDB.addBooleanProperty("default_type");
        orderTypeDB.addBooleanProperty("prepaid");
        orderTypeDB.addBooleanProperty("collect_tip");
        orderTypeDB.addStringProperty("item_statuses");
    }

    private static void addTableGroups(Schema schema) {
        Entity tableGroupDB = schema.addEntity("TableGroupDB");

        tableGroupDB.addStringProperty("id");
        tableGroupDB.addStringProperty("name");
        tableGroupDB.addStringProperty("code");
        tableGroupDB.addStringProperty("property_id");
        tableGroupDB.addBooleanProperty("active");
    }

    private static void addTables(final Schema schema) {
        addTableDetails(schema);
    }

    private static void addTableDetails(Schema schema) {
        Entity propertyTableDB = schema.addEntity("PropertyTableDB");

        Property.PropertyBuilder propertyTableMasterId = propertyTableDB.addStringProperty("id").primaryKey();
        propertyTableDB.addStringProperty("name");
        propertyTableDB.addStringProperty("code");
        propertyTableDB.addStringProperty("number");
        propertyTableDB.addStringProperty("property_id");
        propertyTableDB.addIntProperty("headcount");
        propertyTableDB.addBooleanProperty("active");
        propertyTableDB.addBooleanProperty("blocked");
        propertyTableDB.addBooleanProperty("dirty");
        propertyTableDB.addStringProperty("table_group_id");
        propertyTableDB.addStringProperty("table_status");

        Entity orderDB = schema.addEntity("OrderDB");

        Property.PropertyBuilder orderMasterId = orderDB.addStringProperty("id").primaryKey();
        orderDB.addStringProperty("guest_name");
        orderDB.addStringProperty("status");
        orderDB.addIntProperty("persons");
        orderDB.addStringProperty("server_guest_name");
        orderDB.addIntProperty("server_persons");
        orderDB.addStringProperty("server_waiter_id");
        orderDB.addStringProperty("user_id");
        orderDB.addDoubleProperty("device_created_at");
        orderDB.addStringProperty("business_date");
        orderDB.addStringProperty("order_type_id");
        orderDB.addStringProperty("order_batch_id");
        orderDB.addDoubleProperty("total_charges");
        orderDB.addDoubleProperty("total_taxes");
        orderDB.addDoubleProperty("net_payments");
        orderDB.addStringProperty("waiter_id");
        orderDB.addLongProperty("modifiedTimestamp");
        orderDB.addStringProperty("property_id");
        orderDB.addStringProperty("order_number");
        orderDB.addStringProperty("display_label");
        orderDB.addIntProperty("sequence_number");

        Property.PropertyBuilder tableId = orderDB.addStringProperty("tableId");
        ToMany orderMap = propertyTableDB.addToMany(propertyTableMasterId.getProperty(), orderDB, tableId.getProperty());
        orderMap.setName("orders");

        Entity orderItemRequest = schema.addEntity("OrderItemsDB");
        Property.PropertyBuilder orderItemMasterId = orderItemRequest.addStringProperty("id").primaryKey();
        orderItemRequest.addStringProperty("status");
        orderItemRequest.addStringProperty("display_label");
        orderItemRequest.addStringProperty("menu_item_id");
        orderItemRequest.addStringProperty("remarks");
        orderItemRequest.addStringProperty("user_id");
        orderItemRequest.addStringProperty("charge_id");
        orderItemRequest.addIntProperty("quantity");
        orderItemRequest.addStringProperty("cancelled_user_id");
        orderItemRequest.addStringProperty("folio_id");

        Property.PropertyBuilder orderId = orderItemRequest.addStringProperty("orderId");
        ToMany orderItemsMap = orderDB.addToMany(orderMasterId.getProperty(), orderItemRequest, orderId.getProperty());
        orderItemsMap.setName("order_items");

        Entity chargeReq = schema.addEntity("ChargeReqDB");
        Property.PropertyBuilder chargeId = chargeReq.addStringProperty("id").primaryKey();
        chargeReq.addDoubleProperty("amount");
        chargeReq.addIntProperty("quantity");
        chargeReq.addStringProperty("business_day_id");
        chargeReq.addIntProperty("adults");
        chargeReq.addStringProperty("user_id");
        chargeReq.addStringProperty("date");
        chargeReq.addStringProperty("product_id");
        chargeReq.addStringProperty("charge_type_id");
        chargeReq.addStringProperty("shift_id");
        chargeReq.addStringProperty("status");
        chargeReq.addDoubleProperty("original_amount");
        chargeReq.addIntProperty("children");
        chargeReq.addStringProperty("posted_date");
        chargeReq.addStringProperty("posted_business_day_id");
        chargeReq.addStringProperty("folio_id");
        chargeReq.addStringProperty("custom_field_10");

        Property.PropertyBuilder orderItemId = chargeReq.addStringProperty("orderItemId");
        ToMany chargeMap =  orderItemRequest.addToMany(orderItemMasterId.getProperty(),chargeReq, orderItemId.getProperty());
        chargeMap.setName("charges");

        Entity taxReq = schema.addEntity("TaxReqDB");
        taxReq.addStringProperty("id").primaryKey();
        taxReq.addDoubleProperty("amount");
        taxReq.addStringProperty("name");
        taxReq.addStringProperty("tax_type_id");
        taxReq.addStringProperty("date");

        Property.PropertyBuilder chargeId1 = taxReq.addStringProperty("chargeId");
        ToMany chargeTaxMap = chargeReq.addToMany(chargeId.getProperty(), taxReq, chargeId1.getProperty());
        chargeTaxMap.setName("taxes");

        Entity menu = schema.addEntity("MenuGroupDB");
        Property.PropertyBuilder propertyMenuGroupMasterId = menu.addStringProperty("id").primaryKey();
        menu.addStringProperty("name");
        menu.addStringProperty("code");
        menu.addStringProperty("property_id");
        menu.addBooleanProperty("active");
        menu.addStringProperty("description");

        Entity menuItem = schema.addEntity("MenuItemDB");
        Property.PropertyBuilder menuItemId = menuItem.addStringProperty("id").primaryKey();
        menuItem.addStringProperty("name");
        menuItem.addStringProperty("code");
        menuItem.addBooleanProperty("active");
        menuItem.addStringProperty("description");
        menuItem.addStringProperty("image");
        menuItem.addDoubleProperty("price");
        menuItem.addStringProperty("charge_type_id");
        menuItem.addStringProperty("menu_group_name");
        menuItem.addStringProperty("product_id");
        menuItem.addBooleanProperty("isCustom");
        menuItem.addStringProperty("inventory_system_id");

        Property.PropertyBuilder menuGroupID = menuItem.addStringProperty("menuGroupID");
        ToMany menuItemMap = menu.addToMany(propertyMenuGroupMasterId.getProperty(), menuItem, menuGroupID.getProperty());
        menuItemMap.setName("menuItems");
    }

    private static void addDiscountsChargeDB(Schema schema) {
        Entity discountsTypeEntity = schema.addEntity("DiscountTypesDB");
        Property.PropertyBuilder discountTypeMasterId = discountsTypeEntity.addStringProperty("id");
        discountsTypeEntity.addStringProperty("name");
        discountsTypeEntity.addStringProperty("code");
        discountsTypeEntity.addStringProperty("adjustmentCodeId");
        discountsTypeEntity.addBooleanProperty("active");


        Entity discountPeriodsEntity = schema.addEntity("DiscountPeriodsDB");
        Property.PropertyBuilder discountPeriodId = discountPeriodsEntity.addStringProperty("id");
        discountPeriodsEntity.addDoubleProperty("percentage");
        discountPeriodsEntity.addDoubleProperty("flatAmount");
        discountPeriodsEntity.addStringProperty("start_date");
        discountPeriodsEntity.addStringProperty("end_date");

        Property.PropertyBuilder discountId = discountPeriodsEntity.addStringProperty("discount_id");

        ToMany toMany = discountsTypeEntity.addToMany(discountTypeMasterId.getProperty(), discountPeriodsEntity, discountId.getProperty());
        toMany.setName("discountPeriods");
    }

    private static void addAdjustments(Schema schema) {
        Entity adjustmentsEntity = schema.addEntity("AdjustmentsDB");
        Property.PropertyBuilder adjustmentMasterId = adjustmentsEntity.addStringProperty("id").primaryKey();
        adjustmentsEntity.addIntProperty("quantity");
        adjustmentsEntity.addStringProperty("business_day_id");
        adjustmentsEntity.addDoubleProperty("amount");
        adjustmentsEntity.addStringProperty("discount_id");
        adjustmentsEntity.addStringProperty("posted_business_day_id");
        adjustmentsEntity.addStringProperty("shift_id");
        adjustmentsEntity.addDateProperty("date");
        adjustmentsEntity.addStringProperty("charge_id");
        adjustmentsEntity.addStringProperty("charge_type_id");
        adjustmentsEntity.addDateProperty("posted_date");
        adjustmentsEntity.addStringProperty("status");
        adjustmentsEntity.addStringProperty("name");
        adjustmentsEntity.addDateProperty("deleted_at");
        adjustmentsEntity.addStringProperty("adjustment_code_id");

        Entity adjustmentTaxEntity = schema.addEntity("AdjustmentTaxDB");
        adjustmentTaxEntity.addStringProperty("id").primaryKey();
        adjustmentTaxEntity.addDoubleProperty("amount");
        adjustmentTaxEntity.addStringProperty("name");
        adjustmentTaxEntity.addStringProperty("tax_type_id");
        adjustmentTaxEntity.addDateProperty("date");

        Property.PropertyBuilder adjustmentId = adjustmentTaxEntity.addStringProperty("adjustment_id");

        ToMany toMany = adjustmentsEntity.addToMany(adjustmentMasterId.getProperty(), adjustmentTaxEntity, adjustmentId.getProperty());
        toMany.setName("adjustmentTaxes");
    }

    private static void addChargeTypes(Schema schema) {
        Entity chargeTypesEntity = schema.addEntity("ChargeTypesDB");

        Property.PropertyBuilder chargeTypeMasterId = chargeTypesEntity.addStringProperty("id");
        chargeTypesEntity.addStringProperty("name");
        chargeTypesEntity.addStringProperty("code");
        chargeTypesEntity.addBooleanProperty("is_active");
        chargeTypesEntity.addStringProperty("custom_field_10_label");

        //Tax types
        Entity taxTypesEntity = schema.addEntity("TaxTypesDB");
        taxTypesEntity.addIdProperty();
        taxTypesEntity.addStringProperty("taxTypeId");
        taxTypesEntity.addStringProperty("name");
        taxTypesEntity.addStringProperty("code");
        taxTypesEntity.addBooleanProperty("is_active");
        taxTypesEntity.addBooleanProperty("floating");
        taxTypesEntity.addStringProperty("parent_tax_type_id");
        taxTypesEntity.addStringProperty("property_id");

        Property.PropertyBuilder chargeTypeId = taxTypesEntity.addStringProperty("chargeTypeId");

        ToMany filtersMap = chargeTypesEntity.addToMany(chargeTypeMasterId.getProperty(), taxTypesEntity, chargeTypeId.getProperty());
        filtersMap.setName("taxTypes");

        //Tax type Periods
        Entity taxTypePeriodsEntity = schema.addEntity("TaxTypePeriodsDB");
        taxTypePeriodsEntity.addStringProperty("id");
        taxTypePeriodsEntity.addDoubleProperty("percentage");
        taxTypePeriodsEntity.addDoubleProperty("flat_amount");
        taxTypePeriodsEntity.addStringProperty("start_date");
        taxTypePeriodsEntity.addStringProperty("end_date");

        Property taxTypeIdProperty = taxTypePeriodsEntity.addLongProperty("taxTypeId").getProperty();

        ToMany filterValueRelation = taxTypesEntity.addToMany(taxTypePeriodsEntity, taxTypeIdProperty);
        filterValueRelation.setName("taxTypePeriods");
    }


}
