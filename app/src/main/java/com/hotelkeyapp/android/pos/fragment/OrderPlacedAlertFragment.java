package com.hotelkeyapp.android.pos.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.hotelkeyapp.android.pos.R;

/**
 * Created by vrushalimankar on 18/05/18.
 */

public class OrderPlacedAlertFragment extends DialogFragment {

    public interface OrderPlacedAlertCallbacks {
        void okBtnClicked();
    }

    private OrderPlacedAlertCallbacks orderPlacedAlertCallbacks;

    public OrderPlacedAlertFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_order_placed, container);

        orderPlacedAlertCallbacks = (OrderPlacedAlertCallbacks) getTargetFragment();

        if (orderPlacedAlertCallbacks == null) {
            orderPlacedAlertCallbacks = (OrderPlacedAlertCallbacks) getActivity();
        }

        Button btnOk = view.findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                orderPlacedAlertCallbacks.okBtnClicked();
            }
        });

        return view;
    }
}
